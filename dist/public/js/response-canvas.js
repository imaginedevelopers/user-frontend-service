/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/public/js/response-canvas/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/axios/index.js":
/*!*************************************!*\
  !*** ./node_modules/axios/index.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! ./lib/axios */ "./node_modules/axios/lib/axios.js");

/***/ }),

/***/ "./node_modules/axios/lib/adapters/xhr.js":
/*!************************************************!*\
  !*** ./node_modules/axios/lib/adapters/xhr.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");

var settle = __webpack_require__(/*! ./../core/settle */ "./node_modules/axios/lib/core/settle.js");

var buildURL = __webpack_require__(/*! ./../helpers/buildURL */ "./node_modules/axios/lib/helpers/buildURL.js");

var parseHeaders = __webpack_require__(/*! ./../helpers/parseHeaders */ "./node_modules/axios/lib/helpers/parseHeaders.js");

var isURLSameOrigin = __webpack_require__(/*! ./../helpers/isURLSameOrigin */ "./node_modules/axios/lib/helpers/isURLSameOrigin.js");

var createError = __webpack_require__(/*! ../core/createError */ "./node_modules/axios/lib/core/createError.js");

module.exports = function xhrAdapter(config) {
  return new Promise(function dispatchXhrRequest(resolve, reject) {
    var requestData = config.data;
    var requestHeaders = config.headers;

    if (utils.isFormData(requestData)) {
      delete requestHeaders['Content-Type']; // Let the browser set it
    }

    var request = new XMLHttpRequest(); // HTTP basic authentication

    if (config.auth) {
      var username = config.auth.username || '';
      var password = config.auth.password || '';
      requestHeaders.Authorization = 'Basic ' + btoa(username + ':' + password);
    }

    request.open(config.method.toUpperCase(), buildURL(config.url, config.params, config.paramsSerializer), true); // Set the request timeout in MS

    request.timeout = config.timeout; // Listen for ready state

    request.onreadystatechange = function handleLoad() {
      if (!request || request.readyState !== 4) {
        return;
      } // The request errored out and we didn't get a response, this will be
      // handled by onerror instead
      // With one exception: request that using file: protocol, most browsers
      // will return status as 0 even though it's a successful request


      if (request.status === 0 && !(request.responseURL && request.responseURL.indexOf('file:') === 0)) {
        return;
      } // Prepare the response


      var responseHeaders = 'getAllResponseHeaders' in request ? parseHeaders(request.getAllResponseHeaders()) : null;
      var responseData = !config.responseType || config.responseType === 'text' ? request.responseText : request.response;
      var response = {
        data: responseData,
        status: request.status,
        statusText: request.statusText,
        headers: responseHeaders,
        config: config,
        request: request
      };
      settle(resolve, reject, response); // Clean up request

      request = null;
    }; // Handle browser request cancellation (as opposed to a manual cancellation)


    request.onabort = function handleAbort() {
      if (!request) {
        return;
      }

      reject(createError('Request aborted', config, 'ECONNABORTED', request)); // Clean up request

      request = null;
    }; // Handle low level network errors


    request.onerror = function handleError() {
      // Real errors are hidden from us by the browser
      // onerror should only fire if it's a network error
      reject(createError('Network Error', config, null, request)); // Clean up request

      request = null;
    }; // Handle timeout


    request.ontimeout = function handleTimeout() {
      reject(createError('timeout of ' + config.timeout + 'ms exceeded', config, 'ECONNABORTED', request)); // Clean up request

      request = null;
    }; // Add xsrf header
    // This is only done if running in a standard browser environment.
    // Specifically not if we're in a web worker, or react-native.


    if (utils.isStandardBrowserEnv()) {
      var cookies = __webpack_require__(/*! ./../helpers/cookies */ "./node_modules/axios/lib/helpers/cookies.js"); // Add xsrf header


      var xsrfValue = (config.withCredentials || isURLSameOrigin(config.url)) && config.xsrfCookieName ? cookies.read(config.xsrfCookieName) : undefined;

      if (xsrfValue) {
        requestHeaders[config.xsrfHeaderName] = xsrfValue;
      }
    } // Add headers to the request


    if ('setRequestHeader' in request) {
      utils.forEach(requestHeaders, function setRequestHeader(val, key) {
        if (typeof requestData === 'undefined' && key.toLowerCase() === 'content-type') {
          // Remove Content-Type if data is undefined
          delete requestHeaders[key];
        } else {
          // Otherwise add header to the request
          request.setRequestHeader(key, val);
        }
      });
    } // Add withCredentials to request if needed


    if (config.withCredentials) {
      request.withCredentials = true;
    } // Add responseType to request if needed


    if (config.responseType) {
      try {
        request.responseType = config.responseType;
      } catch (e) {
        // Expected DOMException thrown by browsers not compatible XMLHttpRequest Level 2.
        // But, this can be suppressed for 'json' type as it can be parsed by default 'transformResponse' function.
        if (config.responseType !== 'json') {
          throw e;
        }
      }
    } // Handle progress if needed


    if (typeof config.onDownloadProgress === 'function') {
      request.addEventListener('progress', config.onDownloadProgress);
    } // Not all browsers support upload events


    if (typeof config.onUploadProgress === 'function' && request.upload) {
      request.upload.addEventListener('progress', config.onUploadProgress);
    }

    if (config.cancelToken) {
      // Handle cancellation
      config.cancelToken.promise.then(function onCanceled(cancel) {
        if (!request) {
          return;
        }

        request.abort();
        reject(cancel); // Clean up request

        request = null;
      });
    }

    if (requestData === undefined) {
      requestData = null;
    } // Send the request


    request.send(requestData);
  });
};

/***/ }),

/***/ "./node_modules/axios/lib/axios.js":
/*!*****************************************!*\
  !*** ./node_modules/axios/lib/axios.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./utils */ "./node_modules/axios/lib/utils.js");

var bind = __webpack_require__(/*! ./helpers/bind */ "./node_modules/axios/lib/helpers/bind.js");

var Axios = __webpack_require__(/*! ./core/Axios */ "./node_modules/axios/lib/core/Axios.js");

var mergeConfig = __webpack_require__(/*! ./core/mergeConfig */ "./node_modules/axios/lib/core/mergeConfig.js");

var defaults = __webpack_require__(/*! ./defaults */ "./node_modules/axios/lib/defaults.js");
/**
 * Create an instance of Axios
 *
 * @param {Object} defaultConfig The default config for the instance
 * @return {Axios} A new instance of Axios
 */


function createInstance(defaultConfig) {
  var context = new Axios(defaultConfig);
  var instance = bind(Axios.prototype.request, context); // Copy axios.prototype to instance

  utils.extend(instance, Axios.prototype, context); // Copy context to instance

  utils.extend(instance, context);
  return instance;
} // Create the default instance to be exported


var axios = createInstance(defaults); // Expose Axios class to allow class inheritance

axios.Axios = Axios; // Factory for creating new instances

axios.create = function create(instanceConfig) {
  return createInstance(mergeConfig(axios.defaults, instanceConfig));
}; // Expose Cancel & CancelToken


axios.Cancel = __webpack_require__(/*! ./cancel/Cancel */ "./node_modules/axios/lib/cancel/Cancel.js");
axios.CancelToken = __webpack_require__(/*! ./cancel/CancelToken */ "./node_modules/axios/lib/cancel/CancelToken.js");
axios.isCancel = __webpack_require__(/*! ./cancel/isCancel */ "./node_modules/axios/lib/cancel/isCancel.js"); // Expose all/spread

axios.all = function all(promises) {
  return Promise.all(promises);
};

axios.spread = __webpack_require__(/*! ./helpers/spread */ "./node_modules/axios/lib/helpers/spread.js");
module.exports = axios; // Allow use of default import syntax in TypeScript

module.exports.default = axios;

/***/ }),

/***/ "./node_modules/axios/lib/cancel/Cancel.js":
/*!*************************************************!*\
  !*** ./node_modules/axios/lib/cancel/Cancel.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/**
 * A `Cancel` is an object that is thrown when an operation is canceled.
 *
 * @class
 * @param {string=} message The message.
 */

function Cancel(message) {
  this.message = message;
}

Cancel.prototype.toString = function toString() {
  return 'Cancel' + (this.message ? ': ' + this.message : '');
};

Cancel.prototype.__CANCEL__ = true;
module.exports = Cancel;

/***/ }),

/***/ "./node_modules/axios/lib/cancel/CancelToken.js":
/*!******************************************************!*\
  !*** ./node_modules/axios/lib/cancel/CancelToken.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var Cancel = __webpack_require__(/*! ./Cancel */ "./node_modules/axios/lib/cancel/Cancel.js");
/**
 * A `CancelToken` is an object that can be used to request cancellation of an operation.
 *
 * @class
 * @param {Function} executor The executor function.
 */


function CancelToken(executor) {
  if (typeof executor !== 'function') {
    throw new TypeError('executor must be a function.');
  }

  var resolvePromise;
  this.promise = new Promise(function promiseExecutor(resolve) {
    resolvePromise = resolve;
  });
  var token = this;
  executor(function cancel(message) {
    if (token.reason) {
      // Cancellation has already been requested
      return;
    }

    token.reason = new Cancel(message);
    resolvePromise(token.reason);
  });
}
/**
 * Throws a `Cancel` if cancellation has been requested.
 */


CancelToken.prototype.throwIfRequested = function throwIfRequested() {
  if (this.reason) {
    throw this.reason;
  }
};
/**
 * Returns an object that contains a new `CancelToken` and a function that, when called,
 * cancels the `CancelToken`.
 */


CancelToken.source = function source() {
  var cancel;
  var token = new CancelToken(function executor(c) {
    cancel = c;
  });
  return {
    token: token,
    cancel: cancel
  };
};

module.exports = CancelToken;

/***/ }),

/***/ "./node_modules/axios/lib/cancel/isCancel.js":
/*!***************************************************!*\
  !*** ./node_modules/axios/lib/cancel/isCancel.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function isCancel(value) {
  return !!(value && value.__CANCEL__);
};

/***/ }),

/***/ "./node_modules/axios/lib/core/Axios.js":
/*!**********************************************!*\
  !*** ./node_modules/axios/lib/core/Axios.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");

var buildURL = __webpack_require__(/*! ../helpers/buildURL */ "./node_modules/axios/lib/helpers/buildURL.js");

var InterceptorManager = __webpack_require__(/*! ./InterceptorManager */ "./node_modules/axios/lib/core/InterceptorManager.js");

var dispatchRequest = __webpack_require__(/*! ./dispatchRequest */ "./node_modules/axios/lib/core/dispatchRequest.js");

var mergeConfig = __webpack_require__(/*! ./mergeConfig */ "./node_modules/axios/lib/core/mergeConfig.js");
/**
 * Create a new instance of Axios
 *
 * @param {Object} instanceConfig The default config for the instance
 */


function Axios(instanceConfig) {
  this.defaults = instanceConfig;
  this.interceptors = {
    request: new InterceptorManager(),
    response: new InterceptorManager()
  };
}
/**
 * Dispatch a request
 *
 * @param {Object} config The config specific for this request (merged with this.defaults)
 */


Axios.prototype.request = function request(config) {
  /*eslint no-param-reassign:0*/
  // Allow for axios('example/url'[, config]) a la fetch API
  if (typeof config === 'string') {
    config = arguments[1] || {};
    config.url = arguments[0];
  } else {
    config = config || {};
  }

  config = mergeConfig(this.defaults, config);
  config.method = config.method ? config.method.toLowerCase() : 'get'; // Hook up interceptors middleware

  var chain = [dispatchRequest, undefined];
  var promise = Promise.resolve(config);
  this.interceptors.request.forEach(function unshiftRequestInterceptors(interceptor) {
    chain.unshift(interceptor.fulfilled, interceptor.rejected);
  });
  this.interceptors.response.forEach(function pushResponseInterceptors(interceptor) {
    chain.push(interceptor.fulfilled, interceptor.rejected);
  });

  while (chain.length) {
    promise = promise.then(chain.shift(), chain.shift());
  }

  return promise;
};

Axios.prototype.getUri = function getUri(config) {
  config = mergeConfig(this.defaults, config);
  return buildURL(config.url, config.params, config.paramsSerializer).replace(/^\?/, '');
}; // Provide aliases for supported request methods


utils.forEach(['delete', 'get', 'head', 'options'], function forEachMethodNoData(method) {
  /*eslint func-names:0*/
  Axios.prototype[method] = function (url, config) {
    return this.request(utils.merge(config || {}, {
      method: method,
      url: url
    }));
  };
});
utils.forEach(['post', 'put', 'patch'], function forEachMethodWithData(method) {
  /*eslint func-names:0*/
  Axios.prototype[method] = function (url, data, config) {
    return this.request(utils.merge(config || {}, {
      method: method,
      url: url,
      data: data
    }));
  };
});
module.exports = Axios;

/***/ }),

/***/ "./node_modules/axios/lib/core/InterceptorManager.js":
/*!***********************************************************!*\
  !*** ./node_modules/axios/lib/core/InterceptorManager.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");

function InterceptorManager() {
  this.handlers = [];
}
/**
 * Add a new interceptor to the stack
 *
 * @param {Function} fulfilled The function to handle `then` for a `Promise`
 * @param {Function} rejected The function to handle `reject` for a `Promise`
 *
 * @return {Number} An ID used to remove interceptor later
 */


InterceptorManager.prototype.use = function use(fulfilled, rejected) {
  this.handlers.push({
    fulfilled: fulfilled,
    rejected: rejected
  });
  return this.handlers.length - 1;
};
/**
 * Remove an interceptor from the stack
 *
 * @param {Number} id The ID that was returned by `use`
 */


InterceptorManager.prototype.eject = function eject(id) {
  if (this.handlers[id]) {
    this.handlers[id] = null;
  }
};
/**
 * Iterate over all the registered interceptors
 *
 * This method is particularly useful for skipping over any
 * interceptors that may have become `null` calling `eject`.
 *
 * @param {Function} fn The function to call for each interceptor
 */


InterceptorManager.prototype.forEach = function forEach(fn) {
  utils.forEach(this.handlers, function forEachHandler(h) {
    if (h !== null) {
      fn(h);
    }
  });
};

module.exports = InterceptorManager;

/***/ }),

/***/ "./node_modules/axios/lib/core/createError.js":
/*!****************************************************!*\
  !*** ./node_modules/axios/lib/core/createError.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var enhanceError = __webpack_require__(/*! ./enhanceError */ "./node_modules/axios/lib/core/enhanceError.js");
/**
 * Create an Error with the specified message, config, error code, request and response.
 *
 * @param {string} message The error message.
 * @param {Object} config The config.
 * @param {string} [code] The error code (for example, 'ECONNABORTED').
 * @param {Object} [request] The request.
 * @param {Object} [response] The response.
 * @returns {Error} The created error.
 */


module.exports = function createError(message, config, code, request, response) {
  var error = new Error(message);
  return enhanceError(error, config, code, request, response);
};

/***/ }),

/***/ "./node_modules/axios/lib/core/dispatchRequest.js":
/*!********************************************************!*\
  !*** ./node_modules/axios/lib/core/dispatchRequest.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");

var transformData = __webpack_require__(/*! ./transformData */ "./node_modules/axios/lib/core/transformData.js");

var isCancel = __webpack_require__(/*! ../cancel/isCancel */ "./node_modules/axios/lib/cancel/isCancel.js");

var defaults = __webpack_require__(/*! ../defaults */ "./node_modules/axios/lib/defaults.js");

var isAbsoluteURL = __webpack_require__(/*! ./../helpers/isAbsoluteURL */ "./node_modules/axios/lib/helpers/isAbsoluteURL.js");

var combineURLs = __webpack_require__(/*! ./../helpers/combineURLs */ "./node_modules/axios/lib/helpers/combineURLs.js");
/**
 * Throws a `Cancel` if cancellation has been requested.
 */


function throwIfCancellationRequested(config) {
  if (config.cancelToken) {
    config.cancelToken.throwIfRequested();
  }
}
/**
 * Dispatch a request to the server using the configured adapter.
 *
 * @param {object} config The config that is to be used for the request
 * @returns {Promise} The Promise to be fulfilled
 */


module.exports = function dispatchRequest(config) {
  throwIfCancellationRequested(config); // Support baseURL config

  if (config.baseURL && !isAbsoluteURL(config.url)) {
    config.url = combineURLs(config.baseURL, config.url);
  } // Ensure headers exist


  config.headers = config.headers || {}; // Transform request data

  config.data = transformData(config.data, config.headers, config.transformRequest); // Flatten headers

  config.headers = utils.merge(config.headers.common || {}, config.headers[config.method] || {}, config.headers || {});
  utils.forEach(['delete', 'get', 'head', 'post', 'put', 'patch', 'common'], function cleanHeaderConfig(method) {
    delete config.headers[method];
  });
  var adapter = config.adapter || defaults.adapter;
  return adapter(config).then(function onAdapterResolution(response) {
    throwIfCancellationRequested(config); // Transform response data

    response.data = transformData(response.data, response.headers, config.transformResponse);
    return response;
  }, function onAdapterRejection(reason) {
    if (!isCancel(reason)) {
      throwIfCancellationRequested(config); // Transform response data

      if (reason && reason.response) {
        reason.response.data = transformData(reason.response.data, reason.response.headers, config.transformResponse);
      }
    }

    return Promise.reject(reason);
  });
};

/***/ }),

/***/ "./node_modules/axios/lib/core/enhanceError.js":
/*!*****************************************************!*\
  !*** ./node_modules/axios/lib/core/enhanceError.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/**
 * Update an Error with the specified config, error code, and response.
 *
 * @param {Error} error The error to update.
 * @param {Object} config The config.
 * @param {string} [code] The error code (for example, 'ECONNABORTED').
 * @param {Object} [request] The request.
 * @param {Object} [response] The response.
 * @returns {Error} The error.
 */

module.exports = function enhanceError(error, config, code, request, response) {
  error.config = config;

  if (code) {
    error.code = code;
  }

  error.request = request;
  error.response = response;
  error.isAxiosError = true;

  error.toJSON = function () {
    return {
      // Standard
      message: this.message,
      name: this.name,
      // Microsoft
      description: this.description,
      number: this.number,
      // Mozilla
      fileName: this.fileName,
      lineNumber: this.lineNumber,
      columnNumber: this.columnNumber,
      stack: this.stack,
      // Axios
      config: this.config,
      code: this.code
    };
  };

  return error;
};

/***/ }),

/***/ "./node_modules/axios/lib/core/mergeConfig.js":
/*!****************************************************!*\
  !*** ./node_modules/axios/lib/core/mergeConfig.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ../utils */ "./node_modules/axios/lib/utils.js");
/**
 * Config-specific merge-function which creates a new config-object
 * by merging two configuration objects together.
 *
 * @param {Object} config1
 * @param {Object} config2
 * @returns {Object} New object resulting from merging config2 to config1
 */


module.exports = function mergeConfig(config1, config2) {
  // eslint-disable-next-line no-param-reassign
  config2 = config2 || {};
  var config = {};
  utils.forEach(['url', 'method', 'params', 'data'], function valueFromConfig2(prop) {
    if (typeof config2[prop] !== 'undefined') {
      config[prop] = config2[prop];
    }
  });
  utils.forEach(['headers', 'auth', 'proxy'], function mergeDeepProperties(prop) {
    if (utils.isObject(config2[prop])) {
      config[prop] = utils.deepMerge(config1[prop], config2[prop]);
    } else if (typeof config2[prop] !== 'undefined') {
      config[prop] = config2[prop];
    } else if (utils.isObject(config1[prop])) {
      config[prop] = utils.deepMerge(config1[prop]);
    } else if (typeof config1[prop] !== 'undefined') {
      config[prop] = config1[prop];
    }
  });
  utils.forEach(['baseURL', 'transformRequest', 'transformResponse', 'paramsSerializer', 'timeout', 'withCredentials', 'adapter', 'responseType', 'xsrfCookieName', 'xsrfHeaderName', 'onUploadProgress', 'onDownloadProgress', 'maxContentLength', 'validateStatus', 'maxRedirects', 'httpAgent', 'httpsAgent', 'cancelToken', 'socketPath'], function defaultToConfig2(prop) {
    if (typeof config2[prop] !== 'undefined') {
      config[prop] = config2[prop];
    } else if (typeof config1[prop] !== 'undefined') {
      config[prop] = config1[prop];
    }
  });
  return config;
};

/***/ }),

/***/ "./node_modules/axios/lib/core/settle.js":
/*!***********************************************!*\
  !*** ./node_modules/axios/lib/core/settle.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var createError = __webpack_require__(/*! ./createError */ "./node_modules/axios/lib/core/createError.js");
/**
 * Resolve or reject a Promise based on response status.
 *
 * @param {Function} resolve A function that resolves the promise.
 * @param {Function} reject A function that rejects the promise.
 * @param {object} response The response.
 */


module.exports = function settle(resolve, reject, response) {
  var validateStatus = response.config.validateStatus;

  if (!validateStatus || validateStatus(response.status)) {
    resolve(response);
  } else {
    reject(createError('Request failed with status code ' + response.status, response.config, null, response.request, response));
  }
};

/***/ }),

/***/ "./node_modules/axios/lib/core/transformData.js":
/*!******************************************************!*\
  !*** ./node_modules/axios/lib/core/transformData.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");
/**
 * Transform the data for a request or a response
 *
 * @param {Object|String} data The data to be transformed
 * @param {Array} headers The headers for the request or response
 * @param {Array|Function} fns A single function or Array of functions
 * @returns {*} The resulting transformed data
 */


module.exports = function transformData(data, headers, fns) {
  /*eslint no-param-reassign:0*/
  utils.forEach(fns, function transform(fn) {
    data = fn(data, headers);
  });
  return data;
};

/***/ }),

/***/ "./node_modules/axios/lib/defaults.js":
/*!********************************************!*\
  !*** ./node_modules/axios/lib/defaults.js ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(process) {

var utils = __webpack_require__(/*! ./utils */ "./node_modules/axios/lib/utils.js");

var normalizeHeaderName = __webpack_require__(/*! ./helpers/normalizeHeaderName */ "./node_modules/axios/lib/helpers/normalizeHeaderName.js");

var DEFAULT_CONTENT_TYPE = {
  'Content-Type': 'application/x-www-form-urlencoded'
};

function setContentTypeIfUnset(headers, value) {
  if (!utils.isUndefined(headers) && utils.isUndefined(headers['Content-Type'])) {
    headers['Content-Type'] = value;
  }
}

function getDefaultAdapter() {
  var adapter; // Only Node.JS has a process variable that is of [[Class]] process

  if (typeof process !== 'undefined' && Object.prototype.toString.call(process) === '[object process]') {
    // For node use HTTP adapter
    adapter = __webpack_require__(/*! ./adapters/http */ "./node_modules/axios/lib/adapters/xhr.js");
  } else if (typeof XMLHttpRequest !== 'undefined') {
    // For browsers use XHR adapter
    adapter = __webpack_require__(/*! ./adapters/xhr */ "./node_modules/axios/lib/adapters/xhr.js");
  }

  return adapter;
}

var defaults = {
  adapter: getDefaultAdapter(),
  transformRequest: [function transformRequest(data, headers) {
    normalizeHeaderName(headers, 'Accept');
    normalizeHeaderName(headers, 'Content-Type');

    if (utils.isFormData(data) || utils.isArrayBuffer(data) || utils.isBuffer(data) || utils.isStream(data) || utils.isFile(data) || utils.isBlob(data)) {
      return data;
    }

    if (utils.isArrayBufferView(data)) {
      return data.buffer;
    }

    if (utils.isURLSearchParams(data)) {
      setContentTypeIfUnset(headers, 'application/x-www-form-urlencoded;charset=utf-8');
      return data.toString();
    }

    if (utils.isObject(data)) {
      setContentTypeIfUnset(headers, 'application/json;charset=utf-8');
      return JSON.stringify(data);
    }

    return data;
  }],
  transformResponse: [function transformResponse(data) {
    /*eslint no-param-reassign:0*/
    if (typeof data === 'string') {
      try {
        data = JSON.parse(data);
      } catch (e) {
        /* Ignore */
      }
    }

    return data;
  }],

  /**
   * A timeout in milliseconds to abort a request. If set to 0 (default) a
   * timeout is not created.
   */
  timeout: 0,
  xsrfCookieName: 'XSRF-TOKEN',
  xsrfHeaderName: 'X-XSRF-TOKEN',
  maxContentLength: -1,
  validateStatus: function validateStatus(status) {
    return status >= 200 && status < 300;
  }
};
defaults.headers = {
  common: {
    'Accept': 'application/json, text/plain, */*'
  }
};
utils.forEach(['delete', 'get', 'head'], function forEachMethodNoData(method) {
  defaults.headers[method] = {};
});
utils.forEach(['post', 'put', 'patch'], function forEachMethodWithData(method) {
  defaults.headers[method] = utils.merge(DEFAULT_CONTENT_TYPE);
});
module.exports = defaults;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./node_modules/axios/lib/helpers/bind.js":
/*!************************************************!*\
  !*** ./node_modules/axios/lib/helpers/bind.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function bind(fn, thisArg) {
  return function wrap() {
    var args = new Array(arguments.length);

    for (var i = 0; i < args.length; i++) {
      args[i] = arguments[i];
    }

    return fn.apply(thisArg, args);
  };
};

/***/ }),

/***/ "./node_modules/axios/lib/helpers/buildURL.js":
/*!****************************************************!*\
  !*** ./node_modules/axios/lib/helpers/buildURL.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");

function encode(val) {
  return encodeURIComponent(val).replace(/%40/gi, '@').replace(/%3A/gi, ':').replace(/%24/g, '$').replace(/%2C/gi, ',').replace(/%20/g, '+').replace(/%5B/gi, '[').replace(/%5D/gi, ']');
}
/**
 * Build a URL by appending params to the end
 *
 * @param {string} url The base of the url (e.g., http://www.google.com)
 * @param {object} [params] The params to be appended
 * @returns {string} The formatted url
 */


module.exports = function buildURL(url, params, paramsSerializer) {
  /*eslint no-param-reassign:0*/
  if (!params) {
    return url;
  }

  var serializedParams;

  if (paramsSerializer) {
    serializedParams = paramsSerializer(params);
  } else if (utils.isURLSearchParams(params)) {
    serializedParams = params.toString();
  } else {
    var parts = [];
    utils.forEach(params, function serialize(val, key) {
      if (val === null || typeof val === 'undefined') {
        return;
      }

      if (utils.isArray(val)) {
        key = key + '[]';
      } else {
        val = [val];
      }

      utils.forEach(val, function parseValue(v) {
        if (utils.isDate(v)) {
          v = v.toISOString();
        } else if (utils.isObject(v)) {
          v = JSON.stringify(v);
        }

        parts.push(encode(key) + '=' + encode(v));
      });
    });
    serializedParams = parts.join('&');
  }

  if (serializedParams) {
    var hashmarkIndex = url.indexOf('#');

    if (hashmarkIndex !== -1) {
      url = url.slice(0, hashmarkIndex);
    }

    url += (url.indexOf('?') === -1 ? '?' : '&') + serializedParams;
  }

  return url;
};

/***/ }),

/***/ "./node_modules/axios/lib/helpers/combineURLs.js":
/*!*******************************************************!*\
  !*** ./node_modules/axios/lib/helpers/combineURLs.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/**
 * Creates a new URL by combining the specified URLs
 *
 * @param {string} baseURL The base URL
 * @param {string} relativeURL The relative URL
 * @returns {string} The combined URL
 */

module.exports = function combineURLs(baseURL, relativeURL) {
  return relativeURL ? baseURL.replace(/\/+$/, '') + '/' + relativeURL.replace(/^\/+/, '') : baseURL;
};

/***/ }),

/***/ "./node_modules/axios/lib/helpers/cookies.js":
/*!***************************************************!*\
  !*** ./node_modules/axios/lib/helpers/cookies.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");

module.exports = utils.isStandardBrowserEnv() ? // Standard browser envs support document.cookie
function standardBrowserEnv() {
  return {
    write: function write(name, value, expires, path, domain, secure) {
      var cookie = [];
      cookie.push(name + '=' + encodeURIComponent(value));

      if (utils.isNumber(expires)) {
        cookie.push('expires=' + new Date(expires).toGMTString());
      }

      if (utils.isString(path)) {
        cookie.push('path=' + path);
      }

      if (utils.isString(domain)) {
        cookie.push('domain=' + domain);
      }

      if (secure === true) {
        cookie.push('secure');
      }

      document.cookie = cookie.join('; ');
    },
    read: function read(name) {
      var match = document.cookie.match(new RegExp('(^|;\\s*)(' + name + ')=([^;]*)'));
      return match ? decodeURIComponent(match[3]) : null;
    },
    remove: function remove(name) {
      this.write(name, '', Date.now() - 86400000);
    }
  };
}() : // Non standard browser env (web workers, react-native) lack needed support.
function nonStandardBrowserEnv() {
  return {
    write: function write() {},
    read: function read() {
      return null;
    },
    remove: function remove() {}
  };
}();

/***/ }),

/***/ "./node_modules/axios/lib/helpers/isAbsoluteURL.js":
/*!*********************************************************!*\
  !*** ./node_modules/axios/lib/helpers/isAbsoluteURL.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/**
 * Determines whether the specified URL is absolute
 *
 * @param {string} url The URL to test
 * @returns {boolean} True if the specified URL is absolute, otherwise false
 */

module.exports = function isAbsoluteURL(url) {
  // A URL is considered absolute if it begins with "<scheme>://" or "//" (protocol-relative URL).
  // RFC 3986 defines scheme name as a sequence of characters beginning with a letter and followed
  // by any combination of letters, digits, plus, period, or hyphen.
  return /^([a-z][a-z\d\+\-\.]*:)?\/\//i.test(url);
};

/***/ }),

/***/ "./node_modules/axios/lib/helpers/isURLSameOrigin.js":
/*!***********************************************************!*\
  !*** ./node_modules/axios/lib/helpers/isURLSameOrigin.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");

module.exports = utils.isStandardBrowserEnv() ? // Standard browser envs have full support of the APIs needed to test
// whether the request URL is of the same origin as current location.
function standardBrowserEnv() {
  var msie = /(msie|trident)/i.test(navigator.userAgent);
  var urlParsingNode = document.createElement('a');
  var originURL;
  /**
  * Parse a URL to discover it's components
  *
  * @param {String} url The URL to be parsed
  * @returns {Object}
  */

  function resolveURL(url) {
    var href = url;

    if (msie) {
      // IE needs attribute set twice to normalize properties
      urlParsingNode.setAttribute('href', href);
      href = urlParsingNode.href;
    }

    urlParsingNode.setAttribute('href', href); // urlParsingNode provides the UrlUtils interface - http://url.spec.whatwg.org/#urlutils

    return {
      href: urlParsingNode.href,
      protocol: urlParsingNode.protocol ? urlParsingNode.protocol.replace(/:$/, '') : '',
      host: urlParsingNode.host,
      search: urlParsingNode.search ? urlParsingNode.search.replace(/^\?/, '') : '',
      hash: urlParsingNode.hash ? urlParsingNode.hash.replace(/^#/, '') : '',
      hostname: urlParsingNode.hostname,
      port: urlParsingNode.port,
      pathname: urlParsingNode.pathname.charAt(0) === '/' ? urlParsingNode.pathname : '/' + urlParsingNode.pathname
    };
  }

  originURL = resolveURL(window.location.href);
  /**
  * Determine if a URL shares the same origin as the current location
  *
  * @param {String} requestURL The URL to test
  * @returns {boolean} True if URL shares the same origin, otherwise false
  */

  return function isURLSameOrigin(requestURL) {
    var parsed = utils.isString(requestURL) ? resolveURL(requestURL) : requestURL;
    return parsed.protocol === originURL.protocol && parsed.host === originURL.host;
  };
}() : // Non standard browser envs (web workers, react-native) lack needed support.
function nonStandardBrowserEnv() {
  return function isURLSameOrigin() {
    return true;
  };
}();

/***/ }),

/***/ "./node_modules/axios/lib/helpers/normalizeHeaderName.js":
/*!***************************************************************!*\
  !*** ./node_modules/axios/lib/helpers/normalizeHeaderName.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ../utils */ "./node_modules/axios/lib/utils.js");

module.exports = function normalizeHeaderName(headers, normalizedName) {
  utils.forEach(headers, function processHeader(value, name) {
    if (name !== normalizedName && name.toUpperCase() === normalizedName.toUpperCase()) {
      headers[normalizedName] = value;
      delete headers[name];
    }
  });
};

/***/ }),

/***/ "./node_modules/axios/lib/helpers/parseHeaders.js":
/*!********************************************************!*\
  !*** ./node_modules/axios/lib/helpers/parseHeaders.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js"); // Headers whose duplicates are ignored by node
// c.f. https://nodejs.org/api/http.html#http_message_headers


var ignoreDuplicateOf = ['age', 'authorization', 'content-length', 'content-type', 'etag', 'expires', 'from', 'host', 'if-modified-since', 'if-unmodified-since', 'last-modified', 'location', 'max-forwards', 'proxy-authorization', 'referer', 'retry-after', 'user-agent'];
/**
 * Parse headers into an object
 *
 * ```
 * Date: Wed, 27 Aug 2014 08:58:49 GMT
 * Content-Type: application/json
 * Connection: keep-alive
 * Transfer-Encoding: chunked
 * ```
 *
 * @param {String} headers Headers needing to be parsed
 * @returns {Object} Headers parsed into an object
 */

module.exports = function parseHeaders(headers) {
  var parsed = {};
  var key;
  var val;
  var i;

  if (!headers) {
    return parsed;
  }

  utils.forEach(headers.split('\n'), function parser(line) {
    i = line.indexOf(':');
    key = utils.trim(line.substr(0, i)).toLowerCase();
    val = utils.trim(line.substr(i + 1));

    if (key) {
      if (parsed[key] && ignoreDuplicateOf.indexOf(key) >= 0) {
        return;
      }

      if (key === 'set-cookie') {
        parsed[key] = (parsed[key] ? parsed[key] : []).concat([val]);
      } else {
        parsed[key] = parsed[key] ? parsed[key] + ', ' + val : val;
      }
    }
  });
  return parsed;
};

/***/ }),

/***/ "./node_modules/axios/lib/helpers/spread.js":
/*!**************************************************!*\
  !*** ./node_modules/axios/lib/helpers/spread.js ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/**
 * Syntactic sugar for invoking a function and expanding an array for arguments.
 *
 * Common use case would be to use `Function.prototype.apply`.
 *
 *  ```js
 *  function f(x, y, z) {}
 *  var args = [1, 2, 3];
 *  f.apply(null, args);
 *  ```
 *
 * With `spread` this example can be re-written.
 *
 *  ```js
 *  spread(function(x, y, z) {})([1, 2, 3]);
 *  ```
 *
 * @param {Function} callback
 * @returns {Function}
 */

module.exports = function spread(callback) {
  return function wrap(arr) {
    return callback.apply(null, arr);
  };
};

/***/ }),

/***/ "./node_modules/axios/lib/utils.js":
/*!*****************************************!*\
  !*** ./node_modules/axios/lib/utils.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var bind = __webpack_require__(/*! ./helpers/bind */ "./node_modules/axios/lib/helpers/bind.js");

var isBuffer = __webpack_require__(/*! is-buffer */ "./node_modules/axios/node_modules/is-buffer/index.js");
/*global toString:true*/
// utils is a library of generic helper functions non-specific to axios


var toString = Object.prototype.toString;
/**
 * Determine if a value is an Array
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an Array, otherwise false
 */

function isArray(val) {
  return toString.call(val) === '[object Array]';
}
/**
 * Determine if a value is an ArrayBuffer
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an ArrayBuffer, otherwise false
 */


function isArrayBuffer(val) {
  return toString.call(val) === '[object ArrayBuffer]';
}
/**
 * Determine if a value is a FormData
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an FormData, otherwise false
 */


function isFormData(val) {
  return typeof FormData !== 'undefined' && val instanceof FormData;
}
/**
 * Determine if a value is a view on an ArrayBuffer
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a view on an ArrayBuffer, otherwise false
 */


function isArrayBufferView(val) {
  var result;

  if (typeof ArrayBuffer !== 'undefined' && ArrayBuffer.isView) {
    result = ArrayBuffer.isView(val);
  } else {
    result = val && val.buffer && val.buffer instanceof ArrayBuffer;
  }

  return result;
}
/**
 * Determine if a value is a String
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a String, otherwise false
 */


function isString(val) {
  return typeof val === 'string';
}
/**
 * Determine if a value is a Number
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Number, otherwise false
 */


function isNumber(val) {
  return typeof val === 'number';
}
/**
 * Determine if a value is undefined
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if the value is undefined, otherwise false
 */


function isUndefined(val) {
  return typeof val === 'undefined';
}
/**
 * Determine if a value is an Object
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an Object, otherwise false
 */


function isObject(val) {
  return val !== null && typeof val === 'object';
}
/**
 * Determine if a value is a Date
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Date, otherwise false
 */


function isDate(val) {
  return toString.call(val) === '[object Date]';
}
/**
 * Determine if a value is a File
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a File, otherwise false
 */


function isFile(val) {
  return toString.call(val) === '[object File]';
}
/**
 * Determine if a value is a Blob
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Blob, otherwise false
 */


function isBlob(val) {
  return toString.call(val) === '[object Blob]';
}
/**
 * Determine if a value is a Function
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Function, otherwise false
 */


function isFunction(val) {
  return toString.call(val) === '[object Function]';
}
/**
 * Determine if a value is a Stream
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Stream, otherwise false
 */


function isStream(val) {
  return isObject(val) && isFunction(val.pipe);
}
/**
 * Determine if a value is a URLSearchParams object
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a URLSearchParams object, otherwise false
 */


function isURLSearchParams(val) {
  return typeof URLSearchParams !== 'undefined' && val instanceof URLSearchParams;
}
/**
 * Trim excess whitespace off the beginning and end of a string
 *
 * @param {String} str The String to trim
 * @returns {String} The String freed of excess whitespace
 */


function trim(str) {
  return str.replace(/^\s*/, '').replace(/\s*$/, '');
}
/**
 * Determine if we're running in a standard browser environment
 *
 * This allows axios to run in a web worker, and react-native.
 * Both environments support XMLHttpRequest, but not fully standard globals.
 *
 * web workers:
 *  typeof window -> undefined
 *  typeof document -> undefined
 *
 * react-native:
 *  navigator.product -> 'ReactNative'
 * nativescript
 *  navigator.product -> 'NativeScript' or 'NS'
 */


function isStandardBrowserEnv() {
  if (typeof navigator !== 'undefined' && (navigator.product === 'ReactNative' || navigator.product === 'NativeScript' || navigator.product === 'NS')) {
    return false;
  }

  return typeof window !== 'undefined' && typeof document !== 'undefined';
}
/**
 * Iterate over an Array or an Object invoking a function for each item.
 *
 * If `obj` is an Array callback will be called passing
 * the value, index, and complete array for each item.
 *
 * If 'obj' is an Object callback will be called passing
 * the value, key, and complete object for each property.
 *
 * @param {Object|Array} obj The object to iterate
 * @param {Function} fn The callback to invoke for each item
 */


function forEach(obj, fn) {
  // Don't bother if no value provided
  if (obj === null || typeof obj === 'undefined') {
    return;
  } // Force an array if not already something iterable


  if (typeof obj !== 'object') {
    /*eslint no-param-reassign:0*/
    obj = [obj];
  }

  if (isArray(obj)) {
    // Iterate over array values
    for (var i = 0, l = obj.length; i < l; i++) {
      fn.call(null, obj[i], i, obj);
    }
  } else {
    // Iterate over object keys
    for (var key in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, key)) {
        fn.call(null, obj[key], key, obj);
      }
    }
  }
}
/**
 * Accepts varargs expecting each argument to be an object, then
 * immutably merges the properties of each object and returns result.
 *
 * When multiple objects contain the same key the later object in
 * the arguments list will take precedence.
 *
 * Example:
 *
 * ```js
 * var result = merge({foo: 123}, {foo: 456});
 * console.log(result.foo); // outputs 456
 * ```
 *
 * @param {Object} obj1 Object to merge
 * @returns {Object} Result of all merge properties
 */


function merge()
/* obj1, obj2, obj3, ... */
{
  var result = {};

  function assignValue(val, key) {
    if (typeof result[key] === 'object' && typeof val === 'object') {
      result[key] = merge(result[key], val);
    } else {
      result[key] = val;
    }
  }

  for (var i = 0, l = arguments.length; i < l; i++) {
    forEach(arguments[i], assignValue);
  }

  return result;
}
/**
 * Function equal to merge with the difference being that no reference
 * to original objects is kept.
 *
 * @see merge
 * @param {Object} obj1 Object to merge
 * @returns {Object} Result of all merge properties
 */


function deepMerge()
/* obj1, obj2, obj3, ... */
{
  var result = {};

  function assignValue(val, key) {
    if (typeof result[key] === 'object' && typeof val === 'object') {
      result[key] = deepMerge(result[key], val);
    } else if (typeof val === 'object') {
      result[key] = deepMerge({}, val);
    } else {
      result[key] = val;
    }
  }

  for (var i = 0, l = arguments.length; i < l; i++) {
    forEach(arguments[i], assignValue);
  }

  return result;
}
/**
 * Extends object a by mutably adding to it the properties of object b.
 *
 * @param {Object} a The object to be extended
 * @param {Object} b The object to copy properties from
 * @param {Object} thisArg The object to bind function to
 * @return {Object} The resulting value of object a
 */


function extend(a, b, thisArg) {
  forEach(b, function assignValue(val, key) {
    if (thisArg && typeof val === 'function') {
      a[key] = bind(val, thisArg);
    } else {
      a[key] = val;
    }
  });
  return a;
}

module.exports = {
  isArray: isArray,
  isArrayBuffer: isArrayBuffer,
  isBuffer: isBuffer,
  isFormData: isFormData,
  isArrayBufferView: isArrayBufferView,
  isString: isString,
  isNumber: isNumber,
  isObject: isObject,
  isUndefined: isUndefined,
  isDate: isDate,
  isFile: isFile,
  isBlob: isBlob,
  isFunction: isFunction,
  isStream: isStream,
  isURLSearchParams: isURLSearchParams,
  isStandardBrowserEnv: isStandardBrowserEnv,
  forEach: forEach,
  merge: merge,
  deepMerge: deepMerge,
  extend: extend,
  trim: trim
};

/***/ }),

/***/ "./node_modules/axios/node_modules/is-buffer/index.js":
/*!************************************************************!*\
  !*** ./node_modules/axios/node_modules/is-buffer/index.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/*!
 * Determine if an object is a Buffer
 *
 * @author   Feross Aboukhadijeh <https://feross.org>
 * @license  MIT
 */
module.exports = function isBuffer(obj) {
  return obj != null && obj.constructor != null && typeof obj.constructor.isBuffer === 'function' && obj.constructor.isBuffer(obj);
};

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars.runtime.js":
/*!****************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars.runtime.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true; // istanbul ignore next

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    'default': obj
  };
} // istanbul ignore next


function _interopRequireWildcard(obj) {
  if (obj && obj.__esModule) {
    return obj;
  } else {
    var newObj = {};

    if (obj != null) {
      for (var key in obj) {
        if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key];
      }
    }

    newObj['default'] = obj;
    return newObj;
  }
}

var _handlebarsBase = __webpack_require__(/*! ./handlebars/base */ "./node_modules/handlebars/dist/cjs/handlebars/base.js");

var base = _interopRequireWildcard(_handlebarsBase); // Each of these augment the Handlebars object. No need to setup here.
// (This is done to easily share code between commonjs and browse envs)


var _handlebarsSafeString = __webpack_require__(/*! ./handlebars/safe-string */ "./node_modules/handlebars/dist/cjs/handlebars/safe-string.js");

var _handlebarsSafeString2 = _interopRequireDefault(_handlebarsSafeString);

var _handlebarsException = __webpack_require__(/*! ./handlebars/exception */ "./node_modules/handlebars/dist/cjs/handlebars/exception.js");

var _handlebarsException2 = _interopRequireDefault(_handlebarsException);

var _handlebarsUtils = __webpack_require__(/*! ./handlebars/utils */ "./node_modules/handlebars/dist/cjs/handlebars/utils.js");

var Utils = _interopRequireWildcard(_handlebarsUtils);

var _handlebarsRuntime = __webpack_require__(/*! ./handlebars/runtime */ "./node_modules/handlebars/dist/cjs/handlebars/runtime.js");

var runtime = _interopRequireWildcard(_handlebarsRuntime);

var _handlebarsNoConflict = __webpack_require__(/*! ./handlebars/no-conflict */ "./node_modules/handlebars/dist/cjs/handlebars/no-conflict.js");

var _handlebarsNoConflict2 = _interopRequireDefault(_handlebarsNoConflict); // For compatibility and usage outside of module systems, make the Handlebars object a namespace


function create() {
  var hb = new base.HandlebarsEnvironment();
  Utils.extend(hb, base);
  hb.SafeString = _handlebarsSafeString2['default'];
  hb.Exception = _handlebarsException2['default'];
  hb.Utils = Utils;
  hb.escapeExpression = Utils.escapeExpression;
  hb.VM = runtime;

  hb.template = function (spec) {
    return runtime.template(spec, hb);
  };

  return hb;
}

var inst = create();
inst.create = create;

_handlebarsNoConflict2['default'](inst);

inst['default'] = inst;
exports['default'] = inst;
module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/base.js":
/*!*************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/base.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.HandlebarsEnvironment = HandlebarsEnvironment; // istanbul ignore next

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    'default': obj
  };
}

var _utils = __webpack_require__(/*! ./utils */ "./node_modules/handlebars/dist/cjs/handlebars/utils.js");

var _exception = __webpack_require__(/*! ./exception */ "./node_modules/handlebars/dist/cjs/handlebars/exception.js");

var _exception2 = _interopRequireDefault(_exception);

var _helpers = __webpack_require__(/*! ./helpers */ "./node_modules/handlebars/dist/cjs/handlebars/helpers.js");

var _decorators = __webpack_require__(/*! ./decorators */ "./node_modules/handlebars/dist/cjs/handlebars/decorators.js");

var _logger = __webpack_require__(/*! ./logger */ "./node_modules/handlebars/dist/cjs/handlebars/logger.js");

var _logger2 = _interopRequireDefault(_logger);

var VERSION = '4.1.2';
exports.VERSION = VERSION;
var COMPILER_REVISION = 7;
exports.COMPILER_REVISION = COMPILER_REVISION;
var REVISION_CHANGES = {
  1: '<= 1.0.rc.2',
  // 1.0.rc.2 is actually rev2 but doesn't report it
  2: '== 1.0.0-rc.3',
  3: '== 1.0.0-rc.4',
  4: '== 1.x.x',
  5: '== 2.0.0-alpha.x',
  6: '>= 2.0.0-beta.1',
  7: '>= 4.0.0'
};
exports.REVISION_CHANGES = REVISION_CHANGES;
var objectType = '[object Object]';

function HandlebarsEnvironment(helpers, partials, decorators) {
  this.helpers = helpers || {};
  this.partials = partials || {};
  this.decorators = decorators || {};

  _helpers.registerDefaultHelpers(this);

  _decorators.registerDefaultDecorators(this);
}

HandlebarsEnvironment.prototype = {
  constructor: HandlebarsEnvironment,
  logger: _logger2['default'],
  log: _logger2['default'].log,
  registerHelper: function registerHelper(name, fn) {
    if (_utils.toString.call(name) === objectType) {
      if (fn) {
        throw new _exception2['default']('Arg not supported with multiple helpers');
      }

      _utils.extend(this.helpers, name);
    } else {
      this.helpers[name] = fn;
    }
  },
  unregisterHelper: function unregisterHelper(name) {
    delete this.helpers[name];
  },
  registerPartial: function registerPartial(name, partial) {
    if (_utils.toString.call(name) === objectType) {
      _utils.extend(this.partials, name);
    } else {
      if (typeof partial === 'undefined') {
        throw new _exception2['default']('Attempting to register a partial called "' + name + '" as undefined');
      }

      this.partials[name] = partial;
    }
  },
  unregisterPartial: function unregisterPartial(name) {
    delete this.partials[name];
  },
  registerDecorator: function registerDecorator(name, fn) {
    if (_utils.toString.call(name) === objectType) {
      if (fn) {
        throw new _exception2['default']('Arg not supported with multiple decorators');
      }

      _utils.extend(this.decorators, name);
    } else {
      this.decorators[name] = fn;
    }
  },
  unregisterDecorator: function unregisterDecorator(name) {
    delete this.decorators[name];
  }
};
var log = _logger2['default'].log;
exports.log = log;
exports.createFrame = _utils.createFrame;
exports.logger = _logger2['default'];

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/decorators.js":
/*!*******************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/decorators.js ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.registerDefaultDecorators = registerDefaultDecorators; // istanbul ignore next

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    'default': obj
  };
}

var _decoratorsInline = __webpack_require__(/*! ./decorators/inline */ "./node_modules/handlebars/dist/cjs/handlebars/decorators/inline.js");

var _decoratorsInline2 = _interopRequireDefault(_decoratorsInline);

function registerDefaultDecorators(instance) {
  _decoratorsInline2['default'](instance);
}

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/decorators/inline.js":
/*!**************************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/decorators/inline.js ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _utils = __webpack_require__(/*! ../utils */ "./node_modules/handlebars/dist/cjs/handlebars/utils.js");

exports['default'] = function (instance) {
  instance.registerDecorator('inline', function (fn, props, container, options) {
    var ret = fn;

    if (!props.partials) {
      props.partials = {};

      ret = function (context, options) {
        // Create a new partials stack frame prior to exec.
        var original = container.partials;
        container.partials = _utils.extend({}, original, props.partials);
        var ret = fn(context, options);
        container.partials = original;
        return ret;
      };
    }

    props.partials[options.args[0]] = options.fn;
    return ret;
  });
};

module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/exception.js":
/*!******************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/exception.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
var errorProps = ['description', 'fileName', 'lineNumber', 'message', 'name', 'number', 'stack'];

function Exception(message, node) {
  var loc = node && node.loc,
      line = undefined,
      column = undefined;

  if (loc) {
    line = loc.start.line;
    column = loc.start.column;
    message += ' - ' + line + ':' + column;
  }

  var tmp = Error.prototype.constructor.call(this, message); // Unfortunately errors are not enumerable in Chrome (at least), so `for prop in tmp` doesn't work.

  for (var idx = 0; idx < errorProps.length; idx++) {
    this[errorProps[idx]] = tmp[errorProps[idx]];
  }
  /* istanbul ignore else */


  if (Error.captureStackTrace) {
    Error.captureStackTrace(this, Exception);
  }

  try {
    if (loc) {
      this.lineNumber = line; // Work around issue under safari where we can't directly set the column value

      /* istanbul ignore next */

      if (Object.defineProperty) {
        Object.defineProperty(this, 'column', {
          value: column,
          enumerable: true
        });
      } else {
        this.column = column;
      }
    }
  } catch (nop) {
    /* Ignore if the browser is very particular */
  }
}

Exception.prototype = new Error();
exports['default'] = Exception;
module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/helpers.js":
/*!****************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/helpers.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.registerDefaultHelpers = registerDefaultHelpers; // istanbul ignore next

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    'default': obj
  };
}

var _helpersBlockHelperMissing = __webpack_require__(/*! ./helpers/block-helper-missing */ "./node_modules/handlebars/dist/cjs/handlebars/helpers/block-helper-missing.js");

var _helpersBlockHelperMissing2 = _interopRequireDefault(_helpersBlockHelperMissing);

var _helpersEach = __webpack_require__(/*! ./helpers/each */ "./node_modules/handlebars/dist/cjs/handlebars/helpers/each.js");

var _helpersEach2 = _interopRequireDefault(_helpersEach);

var _helpersHelperMissing = __webpack_require__(/*! ./helpers/helper-missing */ "./node_modules/handlebars/dist/cjs/handlebars/helpers/helper-missing.js");

var _helpersHelperMissing2 = _interopRequireDefault(_helpersHelperMissing);

var _helpersIf = __webpack_require__(/*! ./helpers/if */ "./node_modules/handlebars/dist/cjs/handlebars/helpers/if.js");

var _helpersIf2 = _interopRequireDefault(_helpersIf);

var _helpersLog = __webpack_require__(/*! ./helpers/log */ "./node_modules/handlebars/dist/cjs/handlebars/helpers/log.js");

var _helpersLog2 = _interopRequireDefault(_helpersLog);

var _helpersLookup = __webpack_require__(/*! ./helpers/lookup */ "./node_modules/handlebars/dist/cjs/handlebars/helpers/lookup.js");

var _helpersLookup2 = _interopRequireDefault(_helpersLookup);

var _helpersWith = __webpack_require__(/*! ./helpers/with */ "./node_modules/handlebars/dist/cjs/handlebars/helpers/with.js");

var _helpersWith2 = _interopRequireDefault(_helpersWith);

function registerDefaultHelpers(instance) {
  _helpersBlockHelperMissing2['default'](instance);

  _helpersEach2['default'](instance);

  _helpersHelperMissing2['default'](instance);

  _helpersIf2['default'](instance);

  _helpersLog2['default'](instance);

  _helpersLookup2['default'](instance);

  _helpersWith2['default'](instance);
}

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/helpers/block-helper-missing.js":
/*!*************************************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/helpers/block-helper-missing.js ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _utils = __webpack_require__(/*! ../utils */ "./node_modules/handlebars/dist/cjs/handlebars/utils.js");

exports['default'] = function (instance) {
  instance.registerHelper('blockHelperMissing', function (context, options) {
    var inverse = options.inverse,
        fn = options.fn;

    if (context === true) {
      return fn(this);
    } else if (context === false || context == null) {
      return inverse(this);
    } else if (_utils.isArray(context)) {
      if (context.length > 0) {
        if (options.ids) {
          options.ids = [options.name];
        }

        return instance.helpers.each(context, options);
      } else {
        return inverse(this);
      }
    } else {
      if (options.data && options.ids) {
        var data = _utils.createFrame(options.data);

        data.contextPath = _utils.appendContextPath(options.data.contextPath, options.name);
        options = {
          data: data
        };
      }

      return fn(context, options);
    }
  });
};

module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/helpers/each.js":
/*!*********************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/helpers/each.js ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true; // istanbul ignore next

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    'default': obj
  };
}

var _utils = __webpack_require__(/*! ../utils */ "./node_modules/handlebars/dist/cjs/handlebars/utils.js");

var _exception = __webpack_require__(/*! ../exception */ "./node_modules/handlebars/dist/cjs/handlebars/exception.js");

var _exception2 = _interopRequireDefault(_exception);

exports['default'] = function (instance) {
  instance.registerHelper('each', function (context, options) {
    if (!options) {
      throw new _exception2['default']('Must pass iterator to #each');
    }

    var fn = options.fn,
        inverse = options.inverse,
        i = 0,
        ret = '',
        data = undefined,
        contextPath = undefined;

    if (options.data && options.ids) {
      contextPath = _utils.appendContextPath(options.data.contextPath, options.ids[0]) + '.';
    }

    if (_utils.isFunction(context)) {
      context = context.call(this);
    }

    if (options.data) {
      data = _utils.createFrame(options.data);
    }

    function execIteration(field, index, last) {
      if (data) {
        data.key = field;
        data.index = index;
        data.first = index === 0;
        data.last = !!last;

        if (contextPath) {
          data.contextPath = contextPath + field;
        }
      }

      ret = ret + fn(context[field], {
        data: data,
        blockParams: _utils.blockParams([context[field], field], [contextPath + field, null])
      });
    }

    if (context && typeof context === 'object') {
      if (_utils.isArray(context)) {
        for (var j = context.length; i < j; i++) {
          if (i in context) {
            execIteration(i, i, i === context.length - 1);
          }
        }
      } else {
        var priorKey = undefined;

        for (var key in context) {
          if (context.hasOwnProperty(key)) {
            // We're running the iterations one step out of sync so we can detect
            // the last iteration without have to scan the object twice and create
            // an itermediate keys array.
            if (priorKey !== undefined) {
              execIteration(priorKey, i - 1);
            }

            priorKey = key;
            i++;
          }
        }

        if (priorKey !== undefined) {
          execIteration(priorKey, i - 1, true);
        }
      }
    }

    if (i === 0) {
      ret = inverse(this);
    }

    return ret;
  });
};

module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/helpers/helper-missing.js":
/*!*******************************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/helpers/helper-missing.js ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true; // istanbul ignore next

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    'default': obj
  };
}

var _exception = __webpack_require__(/*! ../exception */ "./node_modules/handlebars/dist/cjs/handlebars/exception.js");

var _exception2 = _interopRequireDefault(_exception);

exports['default'] = function (instance) {
  instance.registerHelper('helperMissing', function ()
  /* [args, ]options */
  {
    if (arguments.length === 1) {
      // A missing field in a {{foo}} construct.
      return undefined;
    } else {
      // Someone is actually trying to call something, blow up.
      throw new _exception2['default']('Missing helper: "' + arguments[arguments.length - 1].name + '"');
    }
  });
};

module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/helpers/if.js":
/*!*******************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/helpers/if.js ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _utils = __webpack_require__(/*! ../utils */ "./node_modules/handlebars/dist/cjs/handlebars/utils.js");

exports['default'] = function (instance) {
  instance.registerHelper('if', function (conditional, options) {
    if (_utils.isFunction(conditional)) {
      conditional = conditional.call(this);
    } // Default behavior is to render the positive path if the value is truthy and not empty.
    // The `includeZero` option may be set to treat the condtional as purely not empty based on the
    // behavior of isEmpty. Effectively this determines if 0 is handled by the positive path or negative.


    if (!options.hash.includeZero && !conditional || _utils.isEmpty(conditional)) {
      return options.inverse(this);
    } else {
      return options.fn(this);
    }
  });
  instance.registerHelper('unless', function (conditional, options) {
    return instance.helpers['if'].call(this, conditional, {
      fn: options.inverse,
      inverse: options.fn,
      hash: options.hash
    });
  });
};

module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/helpers/log.js":
/*!********************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/helpers/log.js ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

exports['default'] = function (instance) {
  instance.registerHelper('log', function ()
  /* message, options */
  {
    var args = [undefined],
        options = arguments[arguments.length - 1];

    for (var i = 0; i < arguments.length - 1; i++) {
      args.push(arguments[i]);
    }

    var level = 1;

    if (options.hash.level != null) {
      level = options.hash.level;
    } else if (options.data && options.data.level != null) {
      level = options.data.level;
    }

    args[0] = level;
    instance.log.apply(instance, args);
  });
};

module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/helpers/lookup.js":
/*!***********************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/helpers/lookup.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

exports['default'] = function (instance) {
  instance.registerHelper('lookup', function (obj, field) {
    if (!obj) {
      return obj;
    }

    if (field === 'constructor' && !obj.propertyIsEnumerable(field)) {
      return undefined;
    }

    return obj[field];
  });
};

module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/helpers/with.js":
/*!*********************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/helpers/with.js ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _utils = __webpack_require__(/*! ../utils */ "./node_modules/handlebars/dist/cjs/handlebars/utils.js");

exports['default'] = function (instance) {
  instance.registerHelper('with', function (context, options) {
    if (_utils.isFunction(context)) {
      context = context.call(this);
    }

    var fn = options.fn;

    if (!_utils.isEmpty(context)) {
      var data = options.data;

      if (options.data && options.ids) {
        data = _utils.createFrame(options.data);
        data.contextPath = _utils.appendContextPath(options.data.contextPath, options.ids[0]);
      }

      return fn(context, {
        data: data,
        blockParams: _utils.blockParams([context], [data && data.contextPath])
      });
    } else {
      return options.inverse(this);
    }
  });
};

module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/logger.js":
/*!***************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/logger.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _utils = __webpack_require__(/*! ./utils */ "./node_modules/handlebars/dist/cjs/handlebars/utils.js");

var logger = {
  methodMap: ['debug', 'info', 'warn', 'error'],
  level: 'info',
  // Maps a given level value to the `methodMap` indexes above.
  lookupLevel: function lookupLevel(level) {
    if (typeof level === 'string') {
      var levelMap = _utils.indexOf(logger.methodMap, level.toLowerCase());

      if (levelMap >= 0) {
        level = levelMap;
      } else {
        level = parseInt(level, 10);
      }
    }

    return level;
  },
  // Can be overridden in the host environment
  log: function log(level) {
    level = logger.lookupLevel(level);

    if (typeof console !== 'undefined' && logger.lookupLevel(logger.level) <= level) {
      var method = logger.methodMap[level];

      if (!console[method]) {
        // eslint-disable-line no-console
        method = 'log';
      }

      for (var _len = arguments.length, message = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        message[_key - 1] = arguments[_key];
      }

      console[method].apply(console, message); // eslint-disable-line no-console
    }
  }
};
exports['default'] = logger;
module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/no-conflict.js":
/*!********************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/no-conflict.js ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {/* global window */


exports.__esModule = true;

exports['default'] = function (Handlebars) {
  /* istanbul ignore next */
  var root = typeof global !== 'undefined' ? global : window,
      $Handlebars = root.Handlebars;
  /* istanbul ignore next */

  Handlebars.noConflict = function () {
    if (root.Handlebars === Handlebars) {
      root.Handlebars = $Handlebars;
    }

    return Handlebars;
  };
};

module.exports = exports['default'];
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/runtime.js":
/*!****************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/runtime.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.checkRevision = checkRevision;
exports.template = template;
exports.wrapProgram = wrapProgram;
exports.resolvePartial = resolvePartial;
exports.invokePartial = invokePartial;
exports.noop = noop; // istanbul ignore next

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    'default': obj
  };
} // istanbul ignore next


function _interopRequireWildcard(obj) {
  if (obj && obj.__esModule) {
    return obj;
  } else {
    var newObj = {};

    if (obj != null) {
      for (var key in obj) {
        if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key];
      }
    }

    newObj['default'] = obj;
    return newObj;
  }
}

var _utils = __webpack_require__(/*! ./utils */ "./node_modules/handlebars/dist/cjs/handlebars/utils.js");

var Utils = _interopRequireWildcard(_utils);

var _exception = __webpack_require__(/*! ./exception */ "./node_modules/handlebars/dist/cjs/handlebars/exception.js");

var _exception2 = _interopRequireDefault(_exception);

var _base = __webpack_require__(/*! ./base */ "./node_modules/handlebars/dist/cjs/handlebars/base.js");

function checkRevision(compilerInfo) {
  var compilerRevision = compilerInfo && compilerInfo[0] || 1,
      currentRevision = _base.COMPILER_REVISION;

  if (compilerRevision !== currentRevision) {
    if (compilerRevision < currentRevision) {
      var runtimeVersions = _base.REVISION_CHANGES[currentRevision],
          compilerVersions = _base.REVISION_CHANGES[compilerRevision];
      throw new _exception2['default']('Template was precompiled with an older version of Handlebars than the current runtime. ' + 'Please update your precompiler to a newer version (' + runtimeVersions + ') or downgrade your runtime to an older version (' + compilerVersions + ').');
    } else {
      // Use the embedded version info since the runtime doesn't know about this revision yet
      throw new _exception2['default']('Template was precompiled with a newer version of Handlebars than the current runtime. ' + 'Please update your runtime to a newer version (' + compilerInfo[1] + ').');
    }
  }
}

function template(templateSpec, env) {
  /* istanbul ignore next */
  if (!env) {
    throw new _exception2['default']('No environment passed to template');
  }

  if (!templateSpec || !templateSpec.main) {
    throw new _exception2['default']('Unknown template object: ' + typeof templateSpec);
  }

  templateSpec.main.decorator = templateSpec.main_d; // Note: Using env.VM references rather than local var references throughout this section to allow
  // for external users to override these as psuedo-supported APIs.

  env.VM.checkRevision(templateSpec.compiler);

  function invokePartialWrapper(partial, context, options) {
    if (options.hash) {
      context = Utils.extend({}, context, options.hash);

      if (options.ids) {
        options.ids[0] = true;
      }
    }

    partial = env.VM.resolvePartial.call(this, partial, context, options);
    var result = env.VM.invokePartial.call(this, partial, context, options);

    if (result == null && env.compile) {
      options.partials[options.name] = env.compile(partial, templateSpec.compilerOptions, env);
      result = options.partials[options.name](context, options);
    }

    if (result != null) {
      if (options.indent) {
        var lines = result.split('\n');

        for (var i = 0, l = lines.length; i < l; i++) {
          if (!lines[i] && i + 1 === l) {
            break;
          }

          lines[i] = options.indent + lines[i];
        }

        result = lines.join('\n');
      }

      return result;
    } else {
      throw new _exception2['default']('The partial ' + options.name + ' could not be compiled when running in runtime-only mode');
    }
  } // Just add water


  var container = {
    strict: function strict(obj, name) {
      if (!(name in obj)) {
        throw new _exception2['default']('"' + name + '" not defined in ' + obj);
      }

      return obj[name];
    },
    lookup: function lookup(depths, name) {
      var len = depths.length;

      for (var i = 0; i < len; i++) {
        if (depths[i] && depths[i][name] != null) {
          return depths[i][name];
        }
      }
    },
    lambda: function lambda(current, context) {
      return typeof current === 'function' ? current.call(context) : current;
    },
    escapeExpression: Utils.escapeExpression,
    invokePartial: invokePartialWrapper,
    fn: function fn(i) {
      var ret = templateSpec[i];
      ret.decorator = templateSpec[i + '_d'];
      return ret;
    },
    programs: [],
    program: function program(i, data, declaredBlockParams, blockParams, depths) {
      var programWrapper = this.programs[i],
          fn = this.fn(i);

      if (data || depths || blockParams || declaredBlockParams) {
        programWrapper = wrapProgram(this, i, fn, data, declaredBlockParams, blockParams, depths);
      } else if (!programWrapper) {
        programWrapper = this.programs[i] = wrapProgram(this, i, fn);
      }

      return programWrapper;
    },
    data: function data(value, depth) {
      while (value && depth--) {
        value = value._parent;
      }

      return value;
    },
    merge: function merge(param, common) {
      var obj = param || common;

      if (param && common && param !== common) {
        obj = Utils.extend({}, common, param);
      }

      return obj;
    },
    // An empty object to use as replacement for null-contexts
    nullContext: Object.seal({}),
    noop: env.VM.noop,
    compilerInfo: templateSpec.compiler
  };

  function ret(context) {
    var options = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];
    var data = options.data;

    ret._setup(options);

    if (!options.partial && templateSpec.useData) {
      data = initData(context, data);
    }

    var depths = undefined,
        blockParams = templateSpec.useBlockParams ? [] : undefined;

    if (templateSpec.useDepths) {
      if (options.depths) {
        depths = context != options.depths[0] ? [context].concat(options.depths) : options.depths;
      } else {
        depths = [context];
      }
    }

    function main(context
    /*, options*/
    ) {
      return '' + templateSpec.main(container, context, container.helpers, container.partials, data, blockParams, depths);
    }

    main = executeDecorators(templateSpec.main, main, container, options.depths || [], data, blockParams);
    return main(context, options);
  }

  ret.isTop = true;

  ret._setup = function (options) {
    if (!options.partial) {
      container.helpers = container.merge(options.helpers, env.helpers);

      if (templateSpec.usePartial) {
        container.partials = container.merge(options.partials, env.partials);
      }

      if (templateSpec.usePartial || templateSpec.useDecorators) {
        container.decorators = container.merge(options.decorators, env.decorators);
      }
    } else {
      container.helpers = options.helpers;
      container.partials = options.partials;
      container.decorators = options.decorators;
    }
  };

  ret._child = function (i, data, blockParams, depths) {
    if (templateSpec.useBlockParams && !blockParams) {
      throw new _exception2['default']('must pass block params');
    }

    if (templateSpec.useDepths && !depths) {
      throw new _exception2['default']('must pass parent depths');
    }

    return wrapProgram(container, i, templateSpec[i], data, 0, blockParams, depths);
  };

  return ret;
}

function wrapProgram(container, i, fn, data, declaredBlockParams, blockParams, depths) {
  function prog(context) {
    var options = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];
    var currentDepths = depths;

    if (depths && context != depths[0] && !(context === container.nullContext && depths[0] === null)) {
      currentDepths = [context].concat(depths);
    }

    return fn(container, context, container.helpers, container.partials, options.data || data, blockParams && [options.blockParams].concat(blockParams), currentDepths);
  }

  prog = executeDecorators(fn, prog, container, depths, data, blockParams);
  prog.program = i;
  prog.depth = depths ? depths.length : 0;
  prog.blockParams = declaredBlockParams || 0;
  return prog;
}

function resolvePartial(partial, context, options) {
  if (!partial) {
    if (options.name === '@partial-block') {
      partial = options.data['partial-block'];
    } else {
      partial = options.partials[options.name];
    }
  } else if (!partial.call && !options.name) {
    // This is a dynamic partial that returned a string
    options.name = partial;
    partial = options.partials[partial];
  }

  return partial;
}

function invokePartial(partial, context, options) {
  // Use the current closure context to save the partial-block if this partial
  var currentPartialBlock = options.data && options.data['partial-block'];
  options.partial = true;

  if (options.ids) {
    options.data.contextPath = options.ids[0] || options.data.contextPath;
  }

  var partialBlock = undefined;

  if (options.fn && options.fn !== noop) {
    (function () {
      options.data = _base.createFrame(options.data); // Wrapper function to get access to currentPartialBlock from the closure

      var fn = options.fn;

      partialBlock = options.data['partial-block'] = function partialBlockWrapper(context) {
        var options = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1]; // Restore the partial-block from the closure for the execution of the block
        // i.e. the part inside the block of the partial call.

        options.data = _base.createFrame(options.data);
        options.data['partial-block'] = currentPartialBlock;
        return fn(context, options);
      };

      if (fn.partials) {
        options.partials = Utils.extend({}, options.partials, fn.partials);
      }
    })();
  }

  if (partial === undefined && partialBlock) {
    partial = partialBlock;
  }

  if (partial === undefined) {
    throw new _exception2['default']('The partial ' + options.name + ' could not be found');
  } else if (partial instanceof Function) {
    return partial(context, options);
  }
}

function noop() {
  return '';
}

function initData(context, data) {
  if (!data || !('root' in data)) {
    data = data ? _base.createFrame(data) : {};
    data.root = context;
  }

  return data;
}

function executeDecorators(fn, prog, container, depths, data, blockParams) {
  if (fn.decorator) {
    var props = {};
    prog = fn.decorator(prog, props, container, depths && depths[0], data, blockParams, depths);
    Utils.extend(prog, props);
  }

  return prog;
}

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/safe-string.js":
/*!********************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/safe-string.js ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Build out our basic SafeString type


exports.__esModule = true;

function SafeString(string) {
  this.string = string;
}

SafeString.prototype.toString = SafeString.prototype.toHTML = function () {
  return '' + this.string;
};

exports['default'] = SafeString;
module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/utils.js":
/*!**************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/utils.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.extend = extend;
exports.indexOf = indexOf;
exports.escapeExpression = escapeExpression;
exports.isEmpty = isEmpty;
exports.createFrame = createFrame;
exports.blockParams = blockParams;
exports.appendContextPath = appendContextPath;
var escape = {
  '&': '&amp;',
  '<': '&lt;',
  '>': '&gt;',
  '"': '&quot;',
  "'": '&#x27;',
  '`': '&#x60;',
  '=': '&#x3D;'
};
var badChars = /[&<>"'`=]/g,
    possible = /[&<>"'`=]/;

function escapeChar(chr) {
  return escape[chr];
}

function extend(obj
/* , ...source */
) {
  for (var i = 1; i < arguments.length; i++) {
    for (var key in arguments[i]) {
      if (Object.prototype.hasOwnProperty.call(arguments[i], key)) {
        obj[key] = arguments[i][key];
      }
    }
  }

  return obj;
}

var toString = Object.prototype.toString;
exports.toString = toString; // Sourced from lodash
// https://github.com/bestiejs/lodash/blob/master/LICENSE.txt

/* eslint-disable func-style */

var isFunction = function isFunction(value) {
  return typeof value === 'function';
}; // fallback for older versions of Chrome and Safari

/* istanbul ignore next */


if (isFunction(/x/)) {
  exports.isFunction = isFunction = function (value) {
    return typeof value === 'function' && toString.call(value) === '[object Function]';
  };
}

exports.isFunction = isFunction;
/* eslint-enable func-style */

/* istanbul ignore next */

var isArray = Array.isArray || function (value) {
  return value && typeof value === 'object' ? toString.call(value) === '[object Array]' : false;
};

exports.isArray = isArray; // Older IE versions do not directly support indexOf so we must implement our own, sadly.

function indexOf(array, value) {
  for (var i = 0, len = array.length; i < len; i++) {
    if (array[i] === value) {
      return i;
    }
  }

  return -1;
}

function escapeExpression(string) {
  if (typeof string !== 'string') {
    // don't escape SafeStrings, since they're already safe
    if (string && string.toHTML) {
      return string.toHTML();
    } else if (string == null) {
      return '';
    } else if (!string) {
      return string + '';
    } // Force a string conversion as this will be done by the append regardless and
    // the regex test will do this transparently behind the scenes, causing issues if
    // an object's to string has escaped characters in it.


    string = '' + string;
  }

  if (!possible.test(string)) {
    return string;
  }

  return string.replace(badChars, escapeChar);
}

function isEmpty(value) {
  if (!value && value !== 0) {
    return true;
  } else if (isArray(value) && value.length === 0) {
    return true;
  } else {
    return false;
  }
}

function createFrame(object) {
  var frame = extend({}, object);
  frame._parent = object;
  return frame;
}

function blockParams(params, ids) {
  params.path = ids;
  return params;
}

function appendContextPath(contextPath, id) {
  return (contextPath ? contextPath + '.' : '') + id;
}

/***/ }),

/***/ "./node_modules/handlebars/runtime.js":
/*!********************************************!*\
  !*** ./node_modules/handlebars/runtime.js ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Create a simple path alias to allow browserify to resolve
// the runtime on a supported path.
module.exports = __webpack_require__(/*! ./dist/cjs/handlebars.runtime */ "./node_modules/handlebars/dist/cjs/handlebars.runtime.js")['default'];

/***/ }),

/***/ "./node_modules/process/browser.js":
/*!*****************************************!*\
  !*** ./node_modules/process/browser.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// shim for using process in browser
var process = module.exports = {}; // cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
  throw new Error('setTimeout has not been defined');
}

function defaultClearTimeout() {
  throw new Error('clearTimeout has not been defined');
}

(function () {
  try {
    if (typeof setTimeout === 'function') {
      cachedSetTimeout = setTimeout;
    } else {
      cachedSetTimeout = defaultSetTimout;
    }
  } catch (e) {
    cachedSetTimeout = defaultSetTimout;
  }

  try {
    if (typeof clearTimeout === 'function') {
      cachedClearTimeout = clearTimeout;
    } else {
      cachedClearTimeout = defaultClearTimeout;
    }
  } catch (e) {
    cachedClearTimeout = defaultClearTimeout;
  }
})();

function runTimeout(fun) {
  if (cachedSetTimeout === setTimeout) {
    //normal enviroments in sane situations
    return setTimeout(fun, 0);
  } // if setTimeout wasn't available but was latter defined


  if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
    cachedSetTimeout = setTimeout;
    return setTimeout(fun, 0);
  }

  try {
    // when when somebody has screwed with setTimeout but no I.E. maddness
    return cachedSetTimeout(fun, 0);
  } catch (e) {
    try {
      // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
      return cachedSetTimeout.call(null, fun, 0);
    } catch (e) {
      // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
      return cachedSetTimeout.call(this, fun, 0);
    }
  }
}

function runClearTimeout(marker) {
  if (cachedClearTimeout === clearTimeout) {
    //normal enviroments in sane situations
    return clearTimeout(marker);
  } // if clearTimeout wasn't available but was latter defined


  if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
    cachedClearTimeout = clearTimeout;
    return clearTimeout(marker);
  }

  try {
    // when when somebody has screwed with setTimeout but no I.E. maddness
    return cachedClearTimeout(marker);
  } catch (e) {
    try {
      // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
      return cachedClearTimeout.call(null, marker);
    } catch (e) {
      // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
      // Some versions of I.E. have different rules for clearTimeout vs setTimeout
      return cachedClearTimeout.call(this, marker);
    }
  }
}

var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
  if (!draining || !currentQueue) {
    return;
  }

  draining = false;

  if (currentQueue.length) {
    queue = currentQueue.concat(queue);
  } else {
    queueIndex = -1;
  }

  if (queue.length) {
    drainQueue();
  }
}

function drainQueue() {
  if (draining) {
    return;
  }

  var timeout = runTimeout(cleanUpNextTick);
  draining = true;
  var len = queue.length;

  while (len) {
    currentQueue = queue;
    queue = [];

    while (++queueIndex < len) {
      if (currentQueue) {
        currentQueue[queueIndex].run();
      }
    }

    queueIndex = -1;
    len = queue.length;
  }

  currentQueue = null;
  draining = false;
  runClearTimeout(timeout);
}

process.nextTick = function (fun) {
  var args = new Array(arguments.length - 1);

  if (arguments.length > 1) {
    for (var i = 1; i < arguments.length; i++) {
      args[i - 1] = arguments[i];
    }
  }

  queue.push(new Item(fun, args));

  if (queue.length === 1 && !draining) {
    runTimeout(drainQueue);
  }
}; // v8 likes predictible objects


function Item(fun, array) {
  this.fun = fun;
  this.array = array;
}

Item.prototype.run = function () {
  this.fun.apply(null, this.array);
};

process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues

process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;
process.prependListener = noop;
process.prependOnceListener = noop;

process.listeners = function (name) {
  return [];
};

process.binding = function (name) {
  throw new Error('process.binding is not supported');
};

process.cwd = function () {
  return '/';
};

process.chdir = function (dir) {
  throw new Error('process.chdir is not supported');
};

process.umask = function () {
  return 0;
};

/***/ }),

/***/ "./node_modules/text-mask-addons/dist/textMaskAddons.js":
/*!**************************************************************!*\
  !*** ./node_modules/text-mask-addons/dist/textMaskAddons.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

!function (e, t) {
   true ? module.exports = t() : undefined;
}(this, function () {
  return function (e) {
    function t(r) {
      if (n[r]) return n[r].exports;
      var i = n[r] = {
        exports: {},
        id: r,
        loaded: !1
      };
      return e[r].call(i.exports, i, i.exports, t), i.loaded = !0, i.exports;
    }

    var n = {};
    return t.m = e, t.c = n, t.p = "", t(0);
  }([function (e, t, n) {
    "use strict";

    function r(e) {
      return e && e.__esModule ? e : {
        default: e
      };
    }

    Object.defineProperty(t, "__esModule", {
      value: !0
    });
    var i = n(1);
    Object.defineProperty(t, "createAutoCorrectedDatePipe", {
      enumerable: !0,
      get: function () {
        return r(i).default;
      }
    });
    var o = n(2);
    Object.defineProperty(t, "createNumberMask", {
      enumerable: !0,
      get: function () {
        return r(o).default;
      }
    });
    var u = n(3);
    Object.defineProperty(t, "emailMask", {
      enumerable: !0,
      get: function () {
        return r(u).default;
      }
    });
  }, function (e, t) {
    "use strict";

    function n() {
      var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "mm dd yyyy",
          t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {},
          n = t.minYear,
          o = void 0 === n ? 1 : n,
          u = t.maxYear,
          a = void 0 === u ? 9999 : u,
          c = e.split(/[^dmyHMS]+/).sort(function (e, t) {
        return i.indexOf(e) - i.indexOf(t);
      });
      return function (t) {
        var n = [],
            i = {
          dd: 31,
          mm: 12,
          yy: 99,
          yyyy: a,
          HH: 23,
          MM: 59,
          SS: 59
        },
            u = {
          dd: 1,
          mm: 1,
          yy: 0,
          yyyy: o,
          HH: 0,
          MM: 0,
          SS: 0
        },
            l = t.split("");
        c.forEach(function (t) {
          var r = e.indexOf(t),
              o = parseInt(i[t].toString().substr(0, 1), 10);
          parseInt(l[r], 10) > o && (l[r + 1] = l[r], l[r] = 0, n.push(r));
        });
        var s = 0,
            d = c.some(function (n) {
          var c = e.indexOf(n),
              l = n.length,
              d = t.substr(c, l).replace(/\D/g, ""),
              f = parseInt(d, 10);
          "mm" === n && (s = f || 0);
          var p = "dd" === n ? r[s] : i[n];

          if ("yyyy" === n && (1 !== o || 9999 !== a)) {
            var v = parseInt(i[n].toString().substring(0, d.length), 10),
                y = parseInt(u[n].toString().substring(0, d.length), 10);
            return f < y || f > v;
          }

          return f > p || d.length === l && f < u[n];
        });
        return !d && {
          value: l.join(""),
          indexesOfPipedChars: n
        };
      };
    }

    Object.defineProperty(t, "__esModule", {
      value: !0
    }), t.default = n;
    var r = [31, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
        i = ["yyyy", "yy", "mm", "dd", "HH", "MM", "SS"];
  }, function (e, t) {
    "use strict";

    function n() {
      function e() {
        var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : a,
            t = e.length;
        if (e === a || e[0] === g[0] && 1 === t) return g.split(a).concat([v]).concat(h.split(a));
        if (e === P && _) return g.split(a).concat(["0", P, v]).concat(h.split(a));
        var n = e[0] === s && D;
        n && (e = e.toString().substr(1));
        var u = e.lastIndexOf(P),
            c = u !== -1,
            l = void 0,
            m = void 0,
            b = void 0;

        if (e.slice($ * -1) === h && (e = e.slice(0, $ * -1)), c && (_ || I) ? (l = e.slice(e.slice(0, N) === g ? N : 0, u), m = e.slice(u + 1, t), m = r(m.replace(f, a))) : l = e.slice(0, N) === g ? e.slice(N) : e, L && ("undefined" == typeof L ? "undefined" : o(L)) === p) {
          var O = "." === S ? "[.]" : "" + S,
              M = (l.match(new RegExp(O, "g")) || []).length;
          l = l.slice(0, L + M * V);
        }

        return l = l.replace(f, a), R || (l = l.replace(/^0+(0$|[^0])/, "$1")), l = x ? i(l, S) : l, b = r(l), (c && _ || I === !0) && (e[u - 1] !== P && b.push(y), b.push(P, y), m && (("undefined" == typeof C ? "undefined" : o(C)) === p && (m = m.slice(0, C)), b = b.concat(m)), I === !0 && e[u - 1] === P && b.push(v)), N > 0 && (b = g.split(a).concat(b)), n && (b.length === N && b.push(v), b = [d].concat(b)), h.length > 0 && (b = b.concat(h.split(a))), b;
      }

      var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
          n = t.prefix,
          g = void 0 === n ? u : n,
          m = t.suffix,
          h = void 0 === m ? a : m,
          b = t.includeThousandsSeparator,
          x = void 0 === b || b,
          O = t.thousandsSeparatorSymbol,
          S = void 0 === O ? c : O,
          M = t.allowDecimal,
          _ = void 0 !== M && M,
          j = t.decimalSymbol,
          P = void 0 === j ? l : j,
          w = t.decimalLimit,
          C = void 0 === w ? 2 : w,
          H = t.requireDecimal,
          I = void 0 !== H && H,
          k = t.allowNegative,
          D = void 0 !== k && k,
          E = t.allowLeadingZeroes,
          R = void 0 !== E && E,
          A = t.integerLimit,
          L = void 0 === A ? null : A,
          N = g && g.length || 0,
          $ = h && h.length || 0,
          V = S && S.length || 0;

      return e.instanceOf = "createNumberMask", e;
    }

    function r(e) {
      return e.split(a).map(function (e) {
        return v.test(e) ? v : e;
      });
    }

    function i(e, t) {
      return e.replace(/\B(?=(\d{3})+(?!\d))/g, t);
    }

    Object.defineProperty(t, "__esModule", {
      value: !0
    });
    var o = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
      return typeof e;
    } : function (e) {
      return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e;
    };
    t.default = n;
    var u = "$",
        a = "",
        c = ",",
        l = ".",
        s = "-",
        d = /-/,
        f = /\D+/g,
        p = "number",
        v = /\d/,
        y = "[]";
  }, function (e, t, n) {
    "use strict";

    function r(e) {
      return e && e.__esModule ? e : {
        default: e
      };
    }

    function i(e, t) {
      e = e.replace(O, v);
      var n = t.placeholderChar,
          r = t.currentCaretPosition,
          i = e.indexOf(y),
          s = e.lastIndexOf(p),
          d = s < i ? -1 : s,
          f = o(e, i + 1, y),
          g = o(e, d - 1, p),
          m = u(e, i, n),
          h = a(e, i, d, n),
          b = c(e, d, n, r);
      m = l(m), h = l(h), b = l(b, !0);
      var x = m.concat(f).concat(h).concat(g).concat(b);
      return x;
    }

    function o(e, t, n) {
      var r = [];
      return e[t] === n ? r.push(n) : r.push(g, n), r.push(g), r;
    }

    function u(e, t) {
      return t === -1 ? e : e.slice(0, t);
    }

    function a(e, t, n, r) {
      var i = v;
      return t !== -1 && (i = n === -1 ? e.slice(t + 1, e.length) : e.slice(t + 1, n)), i = i.replace(new RegExp("[\\s" + r + "]", h), v), i === y ? f : i.length < 1 ? m : i[i.length - 1] === p ? i.slice(0, i.length - 1) : i;
    }

    function c(e, t, n, r) {
      var i = v;
      return t !== -1 && (i = e.slice(t + 1, e.length)), i = i.replace(new RegExp("[\\s" + n + ".]", h), v), 0 === i.length ? e[t - 1] === p && r !== e.length ? f : v : i;
    }

    function l(e, t) {
      return e.split(v).map(function (e) {
        return e === m ? e : t ? x : b;
      });
    }

    Object.defineProperty(t, "__esModule", {
      value: !0
    });
    var s = n(4),
        d = r(s),
        f = "*",
        p = ".",
        v = "",
        y = "@",
        g = "[]",
        m = " ",
        h = "g",
        b = /[^\s]/,
        x = /[^.\s]/,
        O = /\s/g;
    t.default = {
      mask: i,
      pipe: d.default
    };
  }, function (e, t) {
    "use strict";

    function n(e, t) {
      var n = t.currentCaretPosition,
          o = t.rawValue,
          f = t.previousConformedValue,
          p = t.placeholderChar,
          v = e;
      v = r(v);
      var y = v.indexOf(a),
          g = null === o.match(new RegExp("[^@\\s." + p + "]"));
      if (g) return u;
      if (v.indexOf(l) !== -1 || y !== -1 && n !== y + 1 || o.indexOf(i) === -1 && f !== u && o.indexOf(c) !== -1) return !1;
      var m = v.indexOf(i),
          h = v.slice(m + 1, v.length);
      return (h.match(d) || s).length > 1 && v.substr(-1) === c && n !== o.length && (v = v.slice(0, v.length - 1)), v;
    }

    function r(e) {
      var t = 0;
      return e.replace(o, function () {
        return t++, 1 === t ? i : u;
      });
    }

    Object.defineProperty(t, "__esModule", {
      value: !0
    }), t.default = n;
    var i = "@",
        o = /@/g,
        u = "",
        a = "@.",
        c = ".",
        l = "..",
        s = [],
        d = /\./g;
  }]);
});

/***/ }),

/***/ "./node_modules/vanilla-text-mask/dist/vanillaTextMask.js":
/*!****************************************************************!*\
  !*** ./node_modules/vanilla-text-mask/dist/vanillaTextMask.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

!function (e, r) {
   true ? module.exports = r() : undefined;
}(this, function () {
  return function (e) {
    function r(n) {
      if (t[n]) return t[n].exports;
      var o = t[n] = {
        exports: {},
        id: n,
        loaded: !1
      };
      return e[n].call(o.exports, o, o.exports, r), o.loaded = !0, o.exports;
    }

    var t = {};
    return r.m = e, r.c = t, r.p = "", r(0);
  }([function (e, r, t) {
    "use strict";

    function n(e) {
      return e && e.__esModule ? e : {
        default: e
      };
    }

    function o(e) {
      var r = e.inputElement,
          t = (0, u.default)(e),
          n = function (e) {
        var r = e.target.value;
        return t.update(r);
      };

      return r.addEventListener("input", n), t.update(r.value), {
        textMaskInputElement: t,
        destroy: function () {
          r.removeEventListener("input", n);
        }
      };
    }

    Object.defineProperty(r, "__esModule", {
      value: !0
    }), r.conformToMask = void 0, r.maskInput = o;
    var i = t(2);
    Object.defineProperty(r, "conformToMask", {
      enumerable: !0,
      get: function () {
        return n(i).default;
      }
    });
    var a = t(5),
        u = n(a);
    r.default = o;
  }, function (e, r) {
    "use strict";

    Object.defineProperty(r, "__esModule", {
      value: !0
    }), r.placeholderChar = "_", r.strFunction = "function";
  }, function (e, r, t) {
    "use strict";

    function n() {
      var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : l,
          r = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : u,
          t = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : {};

      if (!(0, i.isArray)(r)) {
        if (("undefined" == typeof r ? "undefined" : o(r)) !== a.strFunction) throw new Error("Text-mask:conformToMask; The mask property must be an array.");
        r = r(e, t), r = (0, i.processCaretTraps)(r).maskWithoutCaretTraps;
      }

      var n = t.guide,
          s = void 0 === n || n,
          f = t.previousConformedValue,
          d = void 0 === f ? l : f,
          c = t.placeholderChar,
          p = void 0 === c ? a.placeholderChar : c,
          v = t.placeholder,
          h = void 0 === v ? (0, i.convertMaskToPlaceholder)(r, p) : v,
          m = t.currentCaretPosition,
          y = t.keepCharPositions,
          g = s === !1 && void 0 !== d,
          b = e.length,
          C = d.length,
          k = h.length,
          x = r.length,
          P = b - C,
          T = P > 0,
          O = m + (T ? -P : 0),
          M = O + Math.abs(P);

      if (y === !0 && !T) {
        for (var w = l, S = O; S < M; S++) h[S] === p && (w += p);

        e = e.slice(0, O) + w + e.slice(O, b);
      }

      for (var _ = e.split(l).map(function (e, r) {
        return {
          char: e,
          isNew: r >= O && r < M
        };
      }), j = b - 1; j >= 0; j--) {
        var V = _[j].char;

        if (V !== p) {
          var A = j >= O && C === x;
          V === h[A ? j - P : j] && _.splice(j, 1);
        }
      }

      var E = l,
          N = !1;

      e: for (var F = 0; F < k; F++) {
        var I = h[F];

        if (I === p) {
          if (_.length > 0) for (; _.length > 0;) {
            var L = _.shift(),
                R = L.char,
                J = L.isNew;

            if (R === p && g !== !0) {
              E += p;
              continue e;
            }

            if (r[F].test(R)) {
              if (y === !0 && J !== !1 && d !== l && s !== !1 && T) {
                for (var W = _.length, q = null, z = 0; z < W; z++) {
                  var B = _[z];
                  if (B.char !== p && B.isNew === !1) break;

                  if (B.char === p) {
                    q = z;
                    break;
                  }
                }

                null !== q ? (E += R, _.splice(q, 1)) : F--;
              } else E += R;

              continue e;
            }

            N = !0;
          }
          g === !1 && (E += h.substr(F, k));
          break;
        }

        E += I;
      }

      if (g && T === !1) {
        for (var D = null, G = 0; G < E.length; G++) h[G] === p && (D = G);

        E = null !== D ? E.substr(0, D + 1) : l;
      }

      return {
        conformedValue: E,
        meta: {
          someCharsRejected: N
        }
      };
    }

    Object.defineProperty(r, "__esModule", {
      value: !0
    });
    var o = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
      return typeof e;
    } : function (e) {
      return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e;
    };
    r.default = n;
    var i = t(3),
        a = t(1),
        u = [],
        l = "";
  }, function (e, r, t) {
    "use strict";

    function n() {
      var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : s,
          r = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : l.placeholderChar;
      if (!o(e)) throw new Error("Text-mask:convertMaskToPlaceholder; The mask property must be an array.");
      if (e.indexOf(r) !== -1) throw new Error("Placeholder character must not be used as part of the mask. Please specify a character that is not present in your mask as your placeholder character.\n\n" + ("The placeholder character that was received is: " + JSON.stringify(r) + "\n\n") + ("The mask that was received is: " + JSON.stringify(e)));
      return e.map(function (e) {
        return e instanceof RegExp ? r : e;
      }).join("");
    }

    function o(e) {
      return Array.isArray && Array.isArray(e) || e instanceof Array;
    }

    function i(e) {
      return "string" == typeof e || e instanceof String;
    }

    function a(e) {
      return "number" == typeof e && void 0 === e.length && !isNaN(e);
    }

    function u(e) {
      for (var r = [], t = void 0; t = e.indexOf(f), t !== -1;) r.push(t), e.splice(t, 1);

      return {
        maskWithoutCaretTraps: e,
        indexes: r
      };
    }

    Object.defineProperty(r, "__esModule", {
      value: !0
    }), r.convertMaskToPlaceholder = n, r.isArray = o, r.isString = i, r.isNumber = a, r.processCaretTraps = u;
    var l = t(1),
        s = [],
        f = "[]";
  }, function (e, r) {
    "use strict";

    function t(e) {
      var r = e.previousConformedValue,
          t = void 0 === r ? o : r,
          i = e.previousPlaceholder,
          a = void 0 === i ? o : i,
          u = e.currentCaretPosition,
          l = void 0 === u ? 0 : u,
          s = e.conformedValue,
          f = e.rawValue,
          d = e.placeholderChar,
          c = e.placeholder,
          p = e.indexesOfPipedChars,
          v = void 0 === p ? n : p,
          h = e.caretTrapIndexes,
          m = void 0 === h ? n : h;
      if (0 === l || !f.length) return 0;
      var y = f.length,
          g = t.length,
          b = c.length,
          C = s.length,
          k = y - g,
          x = k > 0,
          P = 0 === g,
          T = k > 1 && !x && !P;
      if (T) return l;
      var O = x && (t === s || s === c),
          M = 0,
          w = void 0,
          S = void 0;
      if (O) M = l - k;else {
        var _ = s.toLowerCase(),
            j = f.toLowerCase(),
            V = j.substr(0, l).split(o),
            A = V.filter(function (e) {
          return _.indexOf(e) !== -1;
        });

        S = A[A.length - 1];
        var E = a.substr(0, A.length).split(o).filter(function (e) {
          return e !== d;
        }).length,
            N = c.substr(0, A.length).split(o).filter(function (e) {
          return e !== d;
        }).length,
            F = N !== E,
            I = void 0 !== a[A.length - 1] && void 0 !== c[A.length - 2] && a[A.length - 1] !== d && a[A.length - 1] !== c[A.length - 1] && a[A.length - 1] === c[A.length - 2];
        !x && (F || I) && E > 0 && c.indexOf(S) > -1 && void 0 !== f[l] && (w = !0, S = f[l]);

        for (var L = v.map(function (e) {
          return _[e];
        }), R = L.filter(function (e) {
          return e === S;
        }).length, J = A.filter(function (e) {
          return e === S;
        }).length, W = c.substr(0, c.indexOf(d)).split(o).filter(function (e, r) {
          return e === S && f[r] !== e;
        }).length, q = W + J + R + (w ? 1 : 0), z = 0, B = 0; B < C; B++) {
          var D = _[B];
          if (M = B + 1, D === S && z++, z >= q) break;
        }
      }

      if (x) {
        for (var G = M, H = M; H <= b; H++) if (c[H] === d && (G = H), c[H] === d || m.indexOf(H) !== -1 || H === b) return G;
      } else if (w) {
        for (var K = M - 1; K >= 0; K--) if (s[K] === S || m.indexOf(K) !== -1 || 0 === K) return K;
      } else for (var Q = M; Q >= 0; Q--) if (c[Q - 1] === d || m.indexOf(Q) !== -1 || 0 === Q) return Q;
    }

    Object.defineProperty(r, "__esModule", {
      value: !0
    }), r.default = t;
    var n = [],
        o = "";
  }, function (e, r, t) {
    "use strict";

    function n(e) {
      return e && e.__esModule ? e : {
        default: e
      };
    }

    function o(e) {
      var r = {
        previousConformedValue: void 0,
        previousPlaceholder: void 0
      };
      return {
        state: r,
        update: function (t) {
          var n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : e,
              o = n.inputElement,
              s = n.mask,
              d = n.guide,
              m = n.pipe,
              g = n.placeholderChar,
              b = void 0 === g ? v.placeholderChar : g,
              C = n.keepCharPositions,
              k = void 0 !== C && C,
              x = n.showMask,
              P = void 0 !== x && x;

          if ("undefined" == typeof t && (t = o.value), t !== r.previousConformedValue) {
            ("undefined" == typeof s ? "undefined" : l(s)) === y && void 0 !== s.pipe && void 0 !== s.mask && (m = s.pipe, s = s.mask);
            var T = void 0,
                O = void 0;

            if (s instanceof Array && (T = (0, p.convertMaskToPlaceholder)(s, b)), s !== !1) {
              var M = a(t),
                  w = o.selectionEnd,
                  S = r.previousConformedValue,
                  _ = r.previousPlaceholder,
                  j = void 0;

              if (("undefined" == typeof s ? "undefined" : l(s)) === v.strFunction) {
                if (O = s(M, {
                  currentCaretPosition: w,
                  previousConformedValue: S,
                  placeholderChar: b
                }), O === !1) return;
                var V = (0, p.processCaretTraps)(O),
                    A = V.maskWithoutCaretTraps,
                    E = V.indexes;
                O = A, j = E, T = (0, p.convertMaskToPlaceholder)(O, b);
              } else O = s;

              var N = {
                previousConformedValue: S,
                guide: d,
                placeholderChar: b,
                pipe: m,
                placeholder: T,
                currentCaretPosition: w,
                keepCharPositions: k
              },
                  F = (0, c.default)(M, O, N),
                  I = F.conformedValue,
                  L = ("undefined" == typeof m ? "undefined" : l(m)) === v.strFunction,
                  R = {};
              L && (R = m(I, u({
                rawValue: M
              }, N)), R === !1 ? R = {
                value: S,
                rejected: !0
              } : (0, p.isString)(R) && (R = {
                value: R
              }));
              var J = L ? R.value : I,
                  W = (0, f.default)({
                previousConformedValue: S,
                previousPlaceholder: _,
                conformedValue: J,
                placeholder: T,
                rawValue: M,
                currentCaretPosition: w,
                placeholderChar: b,
                indexesOfPipedChars: R.indexesOfPipedChars,
                caretTrapIndexes: j
              }),
                  q = J === T && 0 === W,
                  z = P ? T : h,
                  B = q ? z : J;
              r.previousConformedValue = B, r.previousPlaceholder = T, o.value !== B && (o.value = B, i(o, W));
            }
          }
        }
      };
    }

    function i(e, r) {
      document.activeElement === e && (g ? b(function () {
        return e.setSelectionRange(r, r, m);
      }, 0) : e.setSelectionRange(r, r, m));
    }

    function a(e) {
      if ((0, p.isString)(e)) return e;
      if ((0, p.isNumber)(e)) return String(e);
      if (void 0 === e || null === e) return h;
      throw new Error("The 'value' provided to Text Mask needs to be a string or a number. The value received was:\n\n " + JSON.stringify(e));
    }

    Object.defineProperty(r, "__esModule", {
      value: !0
    });

    var u = Object.assign || function (e) {
      for (var r = 1; r < arguments.length; r++) {
        var t = arguments[r];

        for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
      }

      return e;
    },
        l = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
      return typeof e;
    } : function (e) {
      return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e;
    };

    r.default = o;
    var s = t(4),
        f = n(s),
        d = t(2),
        c = n(d),
        p = t(3),
        v = t(1),
        h = "",
        m = "none",
        y = "object",
        g = "undefined" != typeof navigator && /Android/i.test(navigator.userAgent),
        b = "undefined" != typeof requestAnimationFrame ? requestAnimationFrame : setTimeout;
  }]);
});

/***/ }),

/***/ "./node_modules/webpack/buildin/global.js":
/*!***********************************!*\
  !*** (webpack)/buildin/global.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

var g; // This works in non-strict mode

g = function () {
  return this;
}();

try {
  // This works if eval is allowed (see CSP)
  g = g || new Function("return this")();
} catch (e) {
  // This works if the window reference is available
  if (typeof window === "object") g = window;
} // g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}


module.exports = g;

/***/ }),

/***/ "./node_modules/whatwg-fetch/fetch.js":
/*!********************************************!*\
  !*** ./node_modules/whatwg-fetch/fetch.js ***!
  \********************************************/
/*! exports provided: Headers, Request, Response, DOMException, fetch */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Headers", function() { return Headers; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Request", function() { return Request; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Response", function() { return Response; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DOMException", function() { return DOMException; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetch", function() { return fetch; });
var support = {
  searchParams: 'URLSearchParams' in self,
  iterable: 'Symbol' in self && 'iterator' in Symbol,
  blob: 'FileReader' in self && 'Blob' in self && function () {
    try {
      new Blob();
      return true;
    } catch (e) {
      return false;
    }
  }(),
  formData: 'FormData' in self,
  arrayBuffer: 'ArrayBuffer' in self
};

function isDataView(obj) {
  return obj && DataView.prototype.isPrototypeOf(obj);
}

if (support.arrayBuffer) {
  var viewClasses = ['[object Int8Array]', '[object Uint8Array]', '[object Uint8ClampedArray]', '[object Int16Array]', '[object Uint16Array]', '[object Int32Array]', '[object Uint32Array]', '[object Float32Array]', '[object Float64Array]'];

  var isArrayBufferView = ArrayBuffer.isView || function (obj) {
    return obj && viewClasses.indexOf(Object.prototype.toString.call(obj)) > -1;
  };
}

function normalizeName(name) {
  if (typeof name !== 'string') {
    name = String(name);
  }

  if (/[^a-z0-9\-#$%&'*+.^_`|~]/i.test(name)) {
    throw new TypeError('Invalid character in header field name');
  }

  return name.toLowerCase();
}

function normalizeValue(value) {
  if (typeof value !== 'string') {
    value = String(value);
  }

  return value;
} // Build a destructive iterator for the value list


function iteratorFor(items) {
  var iterator = {
    next: function () {
      var value = items.shift();
      return {
        done: value === undefined,
        value: value
      };
    }
  };

  if (support.iterable) {
    iterator[Symbol.iterator] = function () {
      return iterator;
    };
  }

  return iterator;
}

function Headers(headers) {
  this.map = {};

  if (headers instanceof Headers) {
    headers.forEach(function (value, name) {
      this.append(name, value);
    }, this);
  } else if (Array.isArray(headers)) {
    headers.forEach(function (header) {
      this.append(header[0], header[1]);
    }, this);
  } else if (headers) {
    Object.getOwnPropertyNames(headers).forEach(function (name) {
      this.append(name, headers[name]);
    }, this);
  }
}

Headers.prototype.append = function (name, value) {
  name = normalizeName(name);
  value = normalizeValue(value);
  var oldValue = this.map[name];
  this.map[name] = oldValue ? oldValue + ', ' + value : value;
};

Headers.prototype['delete'] = function (name) {
  delete this.map[normalizeName(name)];
};

Headers.prototype.get = function (name) {
  name = normalizeName(name);
  return this.has(name) ? this.map[name] : null;
};

Headers.prototype.has = function (name) {
  return this.map.hasOwnProperty(normalizeName(name));
};

Headers.prototype.set = function (name, value) {
  this.map[normalizeName(name)] = normalizeValue(value);
};

Headers.prototype.forEach = function (callback, thisArg) {
  for (var name in this.map) {
    if (this.map.hasOwnProperty(name)) {
      callback.call(thisArg, this.map[name], name, this);
    }
  }
};

Headers.prototype.keys = function () {
  var items = [];
  this.forEach(function (value, name) {
    items.push(name);
  });
  return iteratorFor(items);
};

Headers.prototype.values = function () {
  var items = [];
  this.forEach(function (value) {
    items.push(value);
  });
  return iteratorFor(items);
};

Headers.prototype.entries = function () {
  var items = [];
  this.forEach(function (value, name) {
    items.push([name, value]);
  });
  return iteratorFor(items);
};

if (support.iterable) {
  Headers.prototype[Symbol.iterator] = Headers.prototype.entries;
}

function consumed(body) {
  if (body.bodyUsed) {
    return Promise.reject(new TypeError('Already read'));
  }

  body.bodyUsed = true;
}

function fileReaderReady(reader) {
  return new Promise(function (resolve, reject) {
    reader.onload = function () {
      resolve(reader.result);
    };

    reader.onerror = function () {
      reject(reader.error);
    };
  });
}

function readBlobAsArrayBuffer(blob) {
  var reader = new FileReader();
  var promise = fileReaderReady(reader);
  reader.readAsArrayBuffer(blob);
  return promise;
}

function readBlobAsText(blob) {
  var reader = new FileReader();
  var promise = fileReaderReady(reader);
  reader.readAsText(blob);
  return promise;
}

function readArrayBufferAsText(buf) {
  var view = new Uint8Array(buf);
  var chars = new Array(view.length);

  for (var i = 0; i < view.length; i++) {
    chars[i] = String.fromCharCode(view[i]);
  }

  return chars.join('');
}

function bufferClone(buf) {
  if (buf.slice) {
    return buf.slice(0);
  } else {
    var view = new Uint8Array(buf.byteLength);
    view.set(new Uint8Array(buf));
    return view.buffer;
  }
}

function Body() {
  this.bodyUsed = false;

  this._initBody = function (body) {
    this._bodyInit = body;

    if (!body) {
      this._bodyText = '';
    } else if (typeof body === 'string') {
      this._bodyText = body;
    } else if (support.blob && Blob.prototype.isPrototypeOf(body)) {
      this._bodyBlob = body;
    } else if (support.formData && FormData.prototype.isPrototypeOf(body)) {
      this._bodyFormData = body;
    } else if (support.searchParams && URLSearchParams.prototype.isPrototypeOf(body)) {
      this._bodyText = body.toString();
    } else if (support.arrayBuffer && support.blob && isDataView(body)) {
      this._bodyArrayBuffer = bufferClone(body.buffer); // IE 10-11 can't handle a DataView body.

      this._bodyInit = new Blob([this._bodyArrayBuffer]);
    } else if (support.arrayBuffer && (ArrayBuffer.prototype.isPrototypeOf(body) || isArrayBufferView(body))) {
      this._bodyArrayBuffer = bufferClone(body);
    } else {
      this._bodyText = body = Object.prototype.toString.call(body);
    }

    if (!this.headers.get('content-type')) {
      if (typeof body === 'string') {
        this.headers.set('content-type', 'text/plain;charset=UTF-8');
      } else if (this._bodyBlob && this._bodyBlob.type) {
        this.headers.set('content-type', this._bodyBlob.type);
      } else if (support.searchParams && URLSearchParams.prototype.isPrototypeOf(body)) {
        this.headers.set('content-type', 'application/x-www-form-urlencoded;charset=UTF-8');
      }
    }
  };

  if (support.blob) {
    this.blob = function () {
      var rejected = consumed(this);

      if (rejected) {
        return rejected;
      }

      if (this._bodyBlob) {
        return Promise.resolve(this._bodyBlob);
      } else if (this._bodyArrayBuffer) {
        return Promise.resolve(new Blob([this._bodyArrayBuffer]));
      } else if (this._bodyFormData) {
        throw new Error('could not read FormData body as blob');
      } else {
        return Promise.resolve(new Blob([this._bodyText]));
      }
    };

    this.arrayBuffer = function () {
      if (this._bodyArrayBuffer) {
        return consumed(this) || Promise.resolve(this._bodyArrayBuffer);
      } else {
        return this.blob().then(readBlobAsArrayBuffer);
      }
    };
  }

  this.text = function () {
    var rejected = consumed(this);

    if (rejected) {
      return rejected;
    }

    if (this._bodyBlob) {
      return readBlobAsText(this._bodyBlob);
    } else if (this._bodyArrayBuffer) {
      return Promise.resolve(readArrayBufferAsText(this._bodyArrayBuffer));
    } else if (this._bodyFormData) {
      throw new Error('could not read FormData body as text');
    } else {
      return Promise.resolve(this._bodyText);
    }
  };

  if (support.formData) {
    this.formData = function () {
      return this.text().then(decode);
    };
  }

  this.json = function () {
    return this.text().then(JSON.parse);
  };

  return this;
} // HTTP methods whose capitalization should be normalized


var methods = ['DELETE', 'GET', 'HEAD', 'OPTIONS', 'POST', 'PUT'];

function normalizeMethod(method) {
  var upcased = method.toUpperCase();
  return methods.indexOf(upcased) > -1 ? upcased : method;
}

function Request(input, options) {
  options = options || {};
  var body = options.body;

  if (input instanceof Request) {
    if (input.bodyUsed) {
      throw new TypeError('Already read');
    }

    this.url = input.url;
    this.credentials = input.credentials;

    if (!options.headers) {
      this.headers = new Headers(input.headers);
    }

    this.method = input.method;
    this.mode = input.mode;
    this.signal = input.signal;

    if (!body && input._bodyInit != null) {
      body = input._bodyInit;
      input.bodyUsed = true;
    }
  } else {
    this.url = String(input);
  }

  this.credentials = options.credentials || this.credentials || 'same-origin';

  if (options.headers || !this.headers) {
    this.headers = new Headers(options.headers);
  }

  this.method = normalizeMethod(options.method || this.method || 'GET');
  this.mode = options.mode || this.mode || null;
  this.signal = options.signal || this.signal;
  this.referrer = null;

  if ((this.method === 'GET' || this.method === 'HEAD') && body) {
    throw new TypeError('Body not allowed for GET or HEAD requests');
  }

  this._initBody(body);
}

Request.prototype.clone = function () {
  return new Request(this, {
    body: this._bodyInit
  });
};

function decode(body) {
  var form = new FormData();
  body.trim().split('&').forEach(function (bytes) {
    if (bytes) {
      var split = bytes.split('=');
      var name = split.shift().replace(/\+/g, ' ');
      var value = split.join('=').replace(/\+/g, ' ');
      form.append(decodeURIComponent(name), decodeURIComponent(value));
    }
  });
  return form;
}

function parseHeaders(rawHeaders) {
  var headers = new Headers(); // Replace instances of \r\n and \n followed by at least one space or horizontal tab with a space
  // https://tools.ietf.org/html/rfc7230#section-3.2

  var preProcessedHeaders = rawHeaders.replace(/\r?\n[\t ]+/g, ' ');
  preProcessedHeaders.split(/\r?\n/).forEach(function (line) {
    var parts = line.split(':');
    var key = parts.shift().trim();

    if (key) {
      var value = parts.join(':').trim();
      headers.append(key, value);
    }
  });
  return headers;
}

Body.call(Request.prototype);
function Response(bodyInit, options) {
  if (!options) {
    options = {};
  }

  this.type = 'default';
  this.status = options.status === undefined ? 200 : options.status;
  this.ok = this.status >= 200 && this.status < 300;
  this.statusText = 'statusText' in options ? options.statusText : 'OK';
  this.headers = new Headers(options.headers);
  this.url = options.url || '';

  this._initBody(bodyInit);
}
Body.call(Response.prototype);

Response.prototype.clone = function () {
  return new Response(this._bodyInit, {
    status: this.status,
    statusText: this.statusText,
    headers: new Headers(this.headers),
    url: this.url
  });
};

Response.error = function () {
  var response = new Response(null, {
    status: 0,
    statusText: ''
  });
  response.type = 'error';
  return response;
};

var redirectStatuses = [301, 302, 303, 307, 308];

Response.redirect = function (url, status) {
  if (redirectStatuses.indexOf(status) === -1) {
    throw new RangeError('Invalid status code');
  }

  return new Response(null, {
    status: status,
    headers: {
      location: url
    }
  });
};

var DOMException = self.DOMException;

try {
  new DOMException();
} catch (err) {
  DOMException = function (message, name) {
    this.message = message;
    this.name = name;
    var error = Error(message);
    this.stack = error.stack;
  };

  DOMException.prototype = Object.create(Error.prototype);
  DOMException.prototype.constructor = DOMException;
}

function fetch(input, init) {
  return new Promise(function (resolve, reject) {
    var request = new Request(input, init);

    if (request.signal && request.signal.aborted) {
      return reject(new DOMException('Aborted', 'AbortError'));
    }

    var xhr = new XMLHttpRequest();

    function abortXhr() {
      xhr.abort();
    }

    xhr.onload = function () {
      var options = {
        status: xhr.status,
        statusText: xhr.statusText,
        headers: parseHeaders(xhr.getAllResponseHeaders() || '')
      };
      options.url = 'responseURL' in xhr ? xhr.responseURL : options.headers.get('X-Request-URL');
      var body = 'response' in xhr ? xhr.response : xhr.responseText;
      resolve(new Response(body, options));
    };

    xhr.onerror = function () {
      reject(new TypeError('Network request failed'));
    };

    xhr.ontimeout = function () {
      reject(new TypeError('Network request failed'));
    };

    xhr.onabort = function () {
      reject(new DOMException('Aborted', 'AbortError'));
    };

    xhr.open(request.method, request.url, true);

    if (request.credentials === 'include') {
      xhr.withCredentials = true;
    } else if (request.credentials === 'omit') {
      xhr.withCredentials = false;
    }

    if ('responseType' in xhr && support.blob) {
      xhr.responseType = 'blob';
    }

    request.headers.forEach(function (value, name) {
      xhr.setRequestHeader(name, value);
    });

    if (request.signal) {
      request.signal.addEventListener('abort', abortXhr);

      xhr.onreadystatechange = function () {
        // DONE (success or failure)
        if (xhr.readyState === 4) {
          request.signal.removeEventListener('abort', abortXhr);
        }
      };
    }

    xhr.send(typeof request._bodyInit === 'undefined' ? null : request._bodyInit);
  });
}
fetch.polyfill = true;

if (!self.fetch) {
  self.fetch = fetch;
  self.Headers = Headers;
  self.Request = Request;
  self.Response = Response;
}

/***/ }),

/***/ "./src/public/js/core/data-manager.js":
/*!********************************************!*\
  !*** ./src/public/js/core/data-manager.js ***!
  \********************************************/
/*! exports provided: DataManager */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataManager", function() { return DataManager; });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var DataManager =
/*#__PURE__*/
function () {
  function DataManager() {
    _classCallCheck(this, DataManager);
  }

  _createClass(DataManager, null, [{
    key: "storeBanks",
    value: function storeBanks(collection) {
      localStorage.setItem("swyp-businesses", JSON.stringify(collection));
    }
  }, {
    key: "getBanks",
    value: function getBanks() {
      return JSON.parse(localStorage.getItem("swyp-businesses"));
    }
  }, {
    key: "findBank",
    value: function findBank(slug) {
      var businesses = JSON.parse(localStorage.getItem("swyp-businesses"));
      return businesses.find(function (biz) {
        return biz.slug === slug;
      });
    }
  }, {
    key: "storeForms",
    value: function storeForms(collection) {
      localStorage.setItem("swyp-forms", JSON.stringify(collection));
    }
  }, {
    key: "getForms",
    value: function getForms() {
      return JSON.parse(localStorage.getItem("swyp-forms"));
    }
  }, {
    key: "findForm",
    value: function findForm(slug) {
      try {
        var forms = JSON.parse(localStorage.getItem("swyp-forms"));
        return forms.find(function (form) {
          return form.slug === slug;
        });
      } catch (error) {
        return null;
      }
    }
  }]);

  return DataManager;
}();

/***/ }),

/***/ "./src/public/js/core/http.js":
/*!************************************!*\
  !*** ./src/public/js/core/http.js ***!
  \************************************/
/*! exports provided: Http */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Http", function() { return Http; });
/* harmony import */ var whatwg_fetch__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! whatwg-fetch */ "./node_modules/whatwg-fetch/fetch.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);


var baseUrl = "https://business-backend-service.herokuapp.com/api/v1/"; // const baseUrl = "http://localhost:4000/api/v1/";

var Http = {
  get: function get(url) {
    return fetch("".concat(baseUrl).concat(url)).then(function (response) {
      return processResponse(response);
    });
  },
  post: function post(url, data) {
    return fetch("".concat(baseUrl).concat(url), {
      method: "POST",
      headers: {
        "Content-type": "application/json"
      },
      body: JSON.stringify(data)
    }).then(function (response) {
      return processResponse(response);
    });
  },
  put: function put(url, data) {
    return fetch("".concat(baseUrl).concat(url), {
      method: "PUT",
      headers: {
        "Content-type": "application/json"
      },
      body: JSON.stringify(data)
    }).then(function (response) {
      return processResponse(response);
    });
  },
  // upLoadForm(url, formData) {
  //   return fetch(`${baseUrl}${url}`, {
  //     method: "POST",
  //     body: formData
  //   }).then(response => processResponse(response));
  // }
  upLoadForm: function upLoadForm(url, formData, id, type) {
    var pb = "<div class=\"pb-cont\">\n                <span class=\"placeholder\">Uploading...</span>\n                <div class=\"pb-inside\"></div>\n              </div>";

    if (document.querySelector("div.uploadstatus[data-q-id=\"".concat(id, "\"]")) !== null) {
      var uploadIndicator = document.querySelector("div.uploadstatus[data-q-id=\"".concat(id, "\"]")); // var t = document.createElement('span');
      // t.innerHTML = eve.loaded;

      uploadIndicator.innerHTML = pb;
    }

    return axios__WEBPACK_IMPORTED_MODULE_1___default()({
      url: "".concat(baseUrl).concat(url),
      method: "POST",
      data: formData,
      onUploadProgress: function onUploadProgress(eve) {
        console.log("logging progress event", eve);
        var total = eve.total;
        var loaded = eve.loaded;
        var fraction = eve.loaded / eve.total;
        var perc = Math.floor(fraction * 100) + "%";

        if (type == "videos") {
          var target = ".uploadButton[data-qType=video] .cam-progress"; // if (loaded >= total) {
          //   document.querySelector("video#live").style.display = "none";
          //   document.querySelector("#record").style.display = "none";
          //   var mediaPermissionBtn = document.querySelector(
          //     ".askPermissionBtn"
          //   );
          //   document
          //     .querySelector(".answerWrapper")
          //     .appendChild(mediaPermissionBtn);
          // }
        } else {
          var target = ".uploadButton[data-qType=picture] .cam-progress";
        }

        var pb = document.querySelector(".pb-cont .pb-inside") || document.querySelector(target);

        if (loaded >= total) {
          pb.style.width = "100%";
        } else {
          pb.style.width = perc;
        }
      }
    }).then(function (response) {
      return processResponse2(response);
    });
  }
};

var processResponse = function processResponse(response) {
  if (response.ok) {
    return response.json();
  }

  var error = new Error("NetworkError");
  error.detials = response.json();
  throw error;
};

var processResponse2 = function processResponse2(response) {
  if (response.status == 200) {
    var ress = new Promise(function (res, rej) {
      res(response.data);
    });
    return ress;
  }

  var error = new Error("NetworkError");
  error.detials = response.statusText;
  throw error;
};

/***/ }),

/***/ "./src/public/js/core/index.js":
/*!*************************************!*\
  !*** ./src/public/js/core/index.js ***!
  \*************************************/
/*! exports provided: DataManager, Http */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _data_manager__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./data-manager */ "./src/public/js/core/data-manager.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "DataManager", function() { return _data_manager__WEBPACK_IMPORTED_MODULE_0__["DataManager"]; });

/* harmony import */ var _http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./http */ "./src/public/js/core/http.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Http", function() { return _http__WEBPACK_IMPORTED_MODULE_1__["Http"]; });




/***/ }),

/***/ "./src/public/js/core/response-canvas/callback.js":
/*!********************************************************!*\
  !*** ./src/public/js/core/response-canvas/callback.js ***!
  \********************************************************/
/*! exports provided: Callback */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Callback", function() { return Callback; });
/* harmony import */ var _notifier__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./notifier */ "./src/public/js/core/response-canvas/notifier.js");
/* harmony import */ var _helper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./helper */ "./src/public/js/core/response-canvas/helper.js");
/* harmony import */ var _state__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./state */ "./src/public/js/core/response-canvas/state.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }




var Callback =
/*#__PURE__*/
function () {
  function Callback() {
    _classCallCheck(this, Callback);
  }

  _createClass(Callback, null, [{
    key: "intersectTargetAbove",
    // if TOP edge of target crosses threshold,
    // bottom must be > 0 which means it is on "screen" (shifted by offset)
    value: function intersectTargetAbove(entries) {
      _helper__WEBPACK_IMPORTED_MODULE_1__["Helper"].updateDirection();
      entries.forEach(function (entry) {
        var isIntersecting = entry.isIntersecting,
            boundingClientRect = entry.boundingClientRect,
            target = entry.target; // bottom is how far bottom edge of target element is from top of viewport

        var bottom = boundingClientRect.bottom,
            height = boundingClientRect.height;
        var bottomAdjusted = bottom - _state__WEBPACK_IMPORTED_MODULE_2__["State"].offsetMargin;
        var targetIndex = _helper__WEBPACK_IMPORTED_MODULE_1__["Helper"].getIndex(target);
        var targetState = _state__WEBPACK_IMPORTED_MODULE_2__["State"].targetStates[targetIndex];

        if (bottomAdjusted >= -_state__WEBPACK_IMPORTED_MODULE_2__["State"].ZERO_MOE) {
          if (isIntersecting && _state__WEBPACK_IMPORTED_MODULE_2__["State"].direction === "down" && targetState.mode !== "enter") {
            _notifier__WEBPACK_IMPORTED_MODULE_0__["Notifier"].notifyTargetEnter(target, _state__WEBPACK_IMPORTED_MODULE_2__["State"].direction);
          } else if (!isIntersecting && _state__WEBPACK_IMPORTED_MODULE_2__["State"].direction === "up" && targetState.mode === "enter") {
            _notifier__WEBPACK_IMPORTED_MODULE_0__["Notifier"].notifyTargetExit(target, _state__WEBPACK_IMPORTED_MODULE_2__["State"].direction);
          } else if (!isIntersecting && bottomAdjusted >= height && _state__WEBPACK_IMPORTED_MODULE_2__["State"].direction === "down" && targetState.mode === "enter") {
            _notifier__WEBPACK_IMPORTED_MODULE_0__["Notifier"].notifyTargetExit(target, _state__WEBPACK_IMPORTED_MODULE_2__["State"].direction);
          }
        }
      });
    }
  }, {
    key: "intersectTargetBelow",
    value: function intersectTargetBelow(entries) {
      _helper__WEBPACK_IMPORTED_MODULE_1__["Helper"].updateDirection();
      entries.forEach(function (entry) {
        var isIntersecting = entry.isIntersecting,
            boundingClientRect = entry.boundingClientRect,
            target = entry.target;
        var bottom = boundingClientRect.bottom,
            height = boundingClientRect.height;
        var bottomAdjusted = bottom - _state__WEBPACK_IMPORTED_MODULE_2__["State"].offsetMargin;
        var targetIndex = _helper__WEBPACK_IMPORTED_MODULE_1__["Helper"].getIndex(target);
        var targetState = _state__WEBPACK_IMPORTED_MODULE_2__["State"].targetStates[targetIndex];

        if (bottomAdjusted >= -_state__WEBPACK_IMPORTED_MODULE_2__["State"].ZERO_MOE && bottomAdjusted < height && isIntersecting && _state__WEBPACK_IMPORTED_MODULE_2__["State"].direction === "up" && targetState.mode !== "enter") {
          _notifier__WEBPACK_IMPORTED_MODULE_0__["Notifier"].notifyTargetEnter(target, _state__WEBPACK_IMPORTED_MODULE_2__["State"].direction);
        } else if (bottomAdjusted <= _state__WEBPACK_IMPORTED_MODULE_2__["State"].ZERO_MOE && !isIntersecting && _state__WEBPACK_IMPORTED_MODULE_2__["State"].direction === "down" && targetState.mode === "enter") {
          _notifier__WEBPACK_IMPORTED_MODULE_0__["Notifier"].notifyTargetExit(target, _state__WEBPACK_IMPORTED_MODULE_2__["State"].direction);
        }
      });
    }
    /*
    if there is a scroll event where a target never intersects (therefore
    skipping an enter/exit trigger), use this fallback to detect if it is
    in view
    */

  }, {
    key: "intersectViewportAbove",
    value: function intersectViewportAbove(entries) {
      _helper__WEBPACK_IMPORTED_MODULE_1__["Helper"].updateDirection();
      entries.forEach(function (entry) {
        var isIntersecting = entry.isIntersecting,
            target = entry.target;
        var index = _helper__WEBPACK_IMPORTED_MODULE_1__["Helper"].getIndex(target);
        var targetState = _state__WEBPACK_IMPORTED_MODULE_2__["State"].targetStates[index];

        if (isIntersecting && _state__WEBPACK_IMPORTED_MODULE_2__["State"].direction === "down" && targetState.state !== "enter" && targetState.direction !== "down") {
          _notifier__WEBPACK_IMPORTED_MODULE_0__["Notifier"].notifyTargetEnter(target, "down");
          _notifier__WEBPACK_IMPORTED_MODULE_0__["Notifier"].notifyTargetExit(target, "down");
        }
      });
    }
  }, {
    key: "intersectViewportBelow",
    value: function intersectViewportBelow(entries) {
      _helper__WEBPACK_IMPORTED_MODULE_1__["Helper"].updateDirection();
      entries.forEach(function (entry) {
        var isIntersecting = entry.isIntersecting,
            target = entry.target;
        var index = _helper__WEBPACK_IMPORTED_MODULE_1__["Helper"].getIndex(target);
        var targetState = _state__WEBPACK_IMPORTED_MODULE_2__["State"].targetStates[index];

        if (isIntersecting && _state__WEBPACK_IMPORTED_MODULE_2__["State"].direction === "up" && targetState.state !== "enter" && targetState.direction !== "up") {
          _notifier__WEBPACK_IMPORTED_MODULE_0__["Notifier"].notifyTargetEnter(target, "up");
          _notifier__WEBPACK_IMPORTED_MODULE_0__["Notifier"].notifyTargetExit(target, "up");
        }
      });
    }
  }]);

  return Callback;
}();

/***/ }),

/***/ "./src/public/js/core/response-canvas/canvas.js":
/*!******************************************************!*\
  !*** ./src/public/js/core/response-canvas/canvas.js ***!
  \******************************************************/
/*! exports provided: Canvas */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Canvas", function() { return Canvas; });
/* harmony import */ var _observer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./observer */ "./src/public/js/core/response-canvas/observer.js");
/* harmony import */ var _debugger__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./debugger */ "./src/public/js/core/response-canvas/debugger.js");
/* harmony import */ var _state__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./state */ "./src/public/js/core/response-canvas/state.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }




var Canvas =
/*#__PURE__*/
function () {
  function Canvas() {
    _classCallCheck(this, Canvas);
  }

  _createClass(Canvas, null, [{
    key: "unmountObservers",
    value: function unmountObservers() {
      _state__WEBPACK_IMPORTED_MODULE_2__["State"].unregisterObservers();
      return this;
    }
  }, {
    key: "setUp",
    value: function setUp(_ref) {
      var targets = _ref.targets,
          canvasClass = _ref.canvasClass,
          _ref$offset = _ref.offset,
          offset = _ref$offset === void 0 ? 0.20 : _ref$offset,
          _ref$debug = _ref.debug,
          debug = _ref$debug === void 0 ? false : _ref$debug;
      _state__WEBPACK_IMPORTED_MODULE_2__["State"].canvasClass = canvasClass;
      _state__WEBPACK_IMPORTED_MODULE_2__["State"].offsetValue = offset;
      _state__WEBPACK_IMPORTED_MODULE_2__["State"].targets = targets;
      _state__WEBPACK_IMPORTED_MODULE_2__["State"].debugMode = debug;
      setUpDebugMode();
      _state__WEBPACK_IMPORTED_MODULE_2__["State"].addIndexDateToTargets();
      _state__WEBPACK_IMPORTED_MODULE_2__["State"].setTargetStates();
      initialize();
      return this;
    }
  }, {
    key: "onTargetEnter",
    value: function onTargetEnter(cb) {
      _state__WEBPACK_IMPORTED_MODULE_2__["State"].registerTargetEnter(cb);
      return this;
    }
  }, {
    key: "onTargetExit",
    value: function onTargetExit(cb) {
      _state__WEBPACK_IMPORTED_MODULE_2__["State"].registerTargetExit(cb);
      return this;
    }
  }]);

  return Canvas;
}();

function initialize() {
  // Warning order of initialization is important
  _state__WEBPACK_IMPORTED_MODULE_2__["State"].setViewPortHeight();
  _state__WEBPACK_IMPORTED_MODULE_2__["State"].setPageHeight();
  _state__WEBPACK_IMPORTED_MODULE_2__["State"].setOffsetMargin();
  _state__WEBPACK_IMPORTED_MODULE_2__["State"].setTargetsOffsetHeight();
  _state__WEBPACK_IMPORTED_MODULE_2__["State"].setTargetsOffsetTop();
  _observer__WEBPACK_IMPORTED_MODULE_0__["Observer"].registerAll();

  if (_state__WEBPACK_IMPORTED_MODULE_2__["State"].debugMode) {
    _debugger__WEBPACK_IMPORTED_MODULE_1__["Debugger"].updateOffset(_state__WEBPACK_IMPORTED_MODULE_2__["State"].offsetMargin);
  }
}

function setUpDebugMode() {
  var offsetValue = _state__WEBPACK_IMPORTED_MODULE_2__["State"].offsetValue,
      targets = _state__WEBPACK_IMPORTED_MODULE_2__["State"].targets,
      debugMode = _state__WEBPACK_IMPORTED_MODULE_2__["State"].debugMode;
  if (debugMode) _debugger__WEBPACK_IMPORTED_MODULE_1__["Debugger"].setup({
    offsetValue: offsetValue,
    targets: targets
  });
}

/***/ }),

/***/ "./src/public/js/core/response-canvas/debugger.js":
/*!********************************************************!*\
  !*** ./src/public/js/core/response-canvas/debugger.js ***!
  \********************************************************/
/*! exports provided: Debugger */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Debugger", function() { return Debugger; });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var id = "1";
var Debugger =
/*#__PURE__*/
function () {
  function Debugger() {
    _classCallCheck(this, Debugger);
  }

  _createClass(Debugger, null, [{
    key: "setup",
    value: function setup(_ref) {
      var offsetValue = _ref.offsetValue,
          targets = _ref.targets;
      var targetClass = targets[0].getAttribute("class");
      setupOffset({
        id: id,
        offsetValue: offsetValue,
        targetClass: targetClass
      });
    }
  }, {
    key: "updateOffset",
    value: function updateOffset(offsetMargin) {
      var idVal = getOffsetId();
      var el = document.querySelector("#".concat(idVal));
      el.style.top = "".concat(offsetMargin, "px");
    }
  }, {
    key: "notifyStep",
    value: function notifyStep(state) {
      var idVal = getStepId();
      var elA = document.querySelector("#".concat(idVal, "_above"));
      var elB = document.querySelector("#".concat(idVal, "_below"));
      var display = state === "enter" ? "block" : "none";
      if (elA) elA.style.display = display;
      if (elB) elB.style.display = display;
    }
  }]);

  return Debugger;
}();

function getStepId(i) {
  return "scrollama__debug-step--".concat(id, "-").concat(i);
}

function getOffsetId() {
  return "scrollama__debug-offset--".concat(id);
}

function setupOffset(_ref2) {
  var offsetValue = _ref2.offsetValue,
      targetClass = _ref2.targetClass;
  var el = document.createElement("div");
  el.setAttribute("id", getOffsetId());
  el.setAttribute("class", "scrollama__debug-offset");
  el.style.position = "fixed";
  el.style.left = "0";
  el.style.width = "100%";
  el.style.height = "0px";
  el.style.borderTop = "2px dashed black";
  el.style.zIndex = "9999";
  var text = document.createElement("p");
  text.innerText = "\".".concat(targetClass, "\" trigger: ").concat(offsetValue);
  text.style.fontSize = "12px";
  text.style.fontFamily = "monospace";
  text.style.color = "black";
  text.style.margin = "0";
  text.style.padding = "6px";
  el.appendChild(text);
  document.body.appendChild(el);
}

/***/ }),

/***/ "./src/public/js/core/response-canvas/helper.js":
/*!******************************************************!*\
  !*** ./src/public/js/core/response-canvas/helper.js ***!
  \******************************************************/
/*! exports provided: Helper */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Helper", function() { return Helper; });
/* harmony import */ var _state__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./state */ "./src/public/js/core/response-canvas/state.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }


var Helper =
/*#__PURE__*/
function () {
  function Helper() {
    _classCallCheck(this, Helper);
  }

  _createClass(Helper, null, [{
    key: "getPageHeight",
    value: function getPageHeight() {
      var html = document.documentElement;
      var body = document.body;
      return Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);
    }
  }, {
    key: "getDistanceToTop",
    value: function getDistanceToTop(elem) {
      var distance = 0;

      if (elem.offsetParent) {
        do {
          distance += elem.offsetTop;
          elem = elem.offsetParent;
        } while (elem);
      }

      return distance < 0 ? 0 : distance;
    }
  }, {
    key: "getIndex",
    value: function getIndex(element) {
      return +element.getAttribute("data-intersect-index");
    }
  }, {
    key: "updateDirection",
    value: function updateDirection() {
      var className = _state__WEBPACK_IMPORTED_MODULE_0__["State"].canvasClass;
      var scrolledOffset = document.querySelector(".".concat(className)) ? document.querySelector(".".concat(className)).scrollTop : window.pageYOffset;

      if (scrolledOffset > _state__WEBPACK_IMPORTED_MODULE_0__["State"].previousYOffset) {
        _state__WEBPACK_IMPORTED_MODULE_0__["State"].direction = "down";
      } else if (scrolledOffset < _state__WEBPACK_IMPORTED_MODULE_0__["State"].previousYOffset) {
        _state__WEBPACK_IMPORTED_MODULE_0__["State"].direction = "up";
      }

      _state__WEBPACK_IMPORTED_MODULE_0__["State"].previousYOffset = scrolledOffset;
    }
  }]);

  return Helper;
}();

/***/ }),

/***/ "./src/public/js/core/response-canvas/index.js":
/*!*****************************************************!*\
  !*** ./src/public/js/core/response-canvas/index.js ***!
  \*****************************************************/
/*! exports provided: Canvas */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _canvas__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./canvas */ "./src/public/js/core/response-canvas/canvas.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Canvas", function() { return _canvas__WEBPACK_IMPORTED_MODULE_0__["Canvas"]; });



/***/ }),

/***/ "./src/public/js/core/response-canvas/notifier.js":
/*!********************************************************!*\
  !*** ./src/public/js/core/response-canvas/notifier.js ***!
  \********************************************************/
/*! exports provided: Notifier */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Notifier", function() { return Notifier; });
/* harmony import */ var _helper__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./helper */ "./src/public/js/core/response-canvas/helper.js");
/* harmony import */ var _state__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./state */ "./src/public/js/core/response-canvas/state.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



var Notifier = function Notifier() {
  _classCallCheck(this, Notifier);
};

_defineProperty(Notifier, "notifyTargetEnter", function (element, direction) {
  var check = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
  var index = _helper__WEBPACK_IMPORTED_MODULE_0__["Helper"].getIndex(element);
  var response = {
    element: element,
    index: index,
    direction: direction
  };
  _state__WEBPACK_IMPORTED_MODULE_1__["State"].updateTargetStateDirection(index, direction);
  _state__WEBPACK_IMPORTED_MODULE_1__["State"].updateTargetStateMode(index, "enter");

  if (check && direction === "down") {
    notifyOthers(index, "above");
  }

  if (check && direction === "up") {
    notifyOthers(index, "below");
  }

  _state__WEBPACK_IMPORTED_MODULE_1__["State"].callTargetEnter(response);
});

_defineProperty(Notifier, "notifyTargetExit", function (element, direction) {
  var index = _helper__WEBPACK_IMPORTED_MODULE_0__["Helper"].getIndex(element);
  var response = {
    element: element,
    index: index,
    direction: direction
  };
  _state__WEBPACK_IMPORTED_MODULE_1__["State"].updateTargetStateDirection(index, direction);
  _state__WEBPACK_IMPORTED_MODULE_1__["State"].updateTargetStateMode(index, "exit");
  _state__WEBPACK_IMPORTED_MODULE_1__["State"].callTargetExit(response);
});

function notifyOthers(index, location) {
  var targets = _state__WEBPACK_IMPORTED_MODULE_1__["State"].targets,
      targetStates = _state__WEBPACK_IMPORTED_MODULE_1__["State"].targetStates;

  if (location === "above") {
    // check if targets below were skipped and notified first
    for (var i = 0; i < index; i++) {
      var targetState = targetStates[i];
      if (targetState.mode === "enter") Notifier.notifyTargetEnter(targets[i], "down");

      if (targetState.direction === "up") {
        Notifier.notifyTargetEnter(targets[i], "down", false);
        Notifier.notifyTargetExit(targets[i], "down");
      }
    }
  } else if (location === "below") {
    // check if targets above were skipped and notified first
    var len = targetStates.length;

    for (var _i = len - 1; _i > index; _i--) {
      var _targetState = targetStates[_i];
      if (_targetState.state === "enter") Notifier.notifyTargetExit(targets[_i], "up");

      if (_targetState.direction === "down") {
        Notifier.notifyTargetEnter(targets[_i], "up", false);
        Notifier.notifyTargetExit(targets[_i], "up");
      }
    }
  }
}

/***/ }),

/***/ "./src/public/js/core/response-canvas/observer.js":
/*!********************************************************!*\
  !*** ./src/public/js/core/response-canvas/observer.js ***!
  \********************************************************/
/*! exports provided: Observer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Observer", function() { return Observer; });
/* harmony import */ var _state__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./state */ "./src/public/js/core/response-canvas/state.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }


var Observer =
/*#__PURE__*/
function () {
  function Observer() {
    _classCallCheck(this, Observer);
  }

  _createClass(Observer, null, [{
    key: "registerAll",
    value: function registerAll() {
      _state__WEBPACK_IMPORTED_MODULE_0__["State"].registerObserverOnViewPortAbove();
      _state__WEBPACK_IMPORTED_MODULE_0__["State"].registerObserverOnViewPortBelow();
      _state__WEBPACK_IMPORTED_MODULE_0__["State"].registerObserversAbove();
      _state__WEBPACK_IMPORTED_MODULE_0__["State"].registerObserversBelow();
    }
  }]);

  return Observer;
}();

/***/ }),

/***/ "./src/public/js/core/response-canvas/state.js":
/*!*****************************************************!*\
  !*** ./src/public/js/core/response-canvas/state.js ***!
  \*****************************************************/
/*! exports provided: State */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "State", function() { return State; });
/* harmony import */ var _callback__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./callback */ "./src/public/js/core/response-canvas/callback.js");
/* harmony import */ var _helper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./helper */ "./src/public/js/core/response-canvas/helper.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }



var stateValues = {
  observers: {
    above: [],
    below: [],
    viewportAbove: [],
    viewportBelow: []
  },
  callbacks: {
    targetEnter: null,
    targetExit: null
  },
  targetsOffsetHeight: [],
  targetsOffsetTop: [],
  previousYOffset: -1,
  canvasClass: null,
  viewPortHeight: 0,
  targetStates: [],
  debugMode: false,
  offsetMargin: 0,
  direction: null,
  offsetValue: 0,
  pageHeight: 0,
  ZERO_MOE: 1,
  targets: []
};
var State =
/*#__PURE__*/
function () {
  function State() {
    _classCallCheck(this, State);
  }

  _createClass(State, null, [{
    key: "addIndexDateToTargets",
    value: function addIndexDateToTargets() {
      stateValues.targets.forEach(function (el, index) {
        return el.setAttribute("data-intersect-index", index);
      });
    }
  }, {
    key: "setViewPortHeight",
    value: function setViewPortHeight() {
      stateValues.viewPortHeight = window.innerHeight;
    }
  }, {
    key: "setPageHeight",
    value: function setPageHeight() {
      stateValues.pageHeight = _helper__WEBPACK_IMPORTED_MODULE_1__["Helper"].getPageHeight();
    }
  }, {
    key: "setOffsetMargin",
    value: function setOffsetMargin() {
      var offsetValue = stateValues.offsetValue,
          viewPortHeight = stateValues.viewPortHeight;
      stateValues.offsetMargin = offsetValue * viewPortHeight;
    }
  }, {
    key: "setTargetStates",
    value: function setTargetStates() {
      stateValues.targetStates = stateValues.targets.map(function () {
        return {
          direction: null,
          state: null
        };
      });
    }
  }, {
    key: "setTargetsOffsetTop",
    value: function setTargetsOffsetTop() {
      var targets = stateValues.targets;
      if (!targets[0]) throw new Error("Canvas doesn't have target element to monitor");
      stateValues.targetsOffsetTop = targets.map(_helper__WEBPACK_IMPORTED_MODULE_1__["Helper"].getDistanceToTop);
    }
  }, {
    key: "setTargetsOffsetHeight",
    value: function setTargetsOffsetHeight() {
      var targets = stateValues.targets;
      if (!targets[0]) throw new Error("Canvas doesn't have target element to monitor");
      stateValues.targetsOffsetHeight = targets.map(function (el) {
        return el.offsetHeight;
      });
    }
  }, {
    key: "registerTargetEnter",
    value: function registerTargetEnter(cb) {
      if (typeof cb !== "function") throw new Error("Argument passed is not a function");
      stateValues.callbacks.targetEnter = cb;
    }
  }, {
    key: "registerTargetExit",
    value: function registerTargetExit(cb) {
      if (typeof cb !== "function") throw new Error("Argument passed is not a function");
      stateValues.callbacks.targetExit = cb;
    }
  }, {
    key: "callTargetExit",
    value: function callTargetExit(response) {
      var callbacks = stateValues.callbacks,
          targetStates = stateValues.targetStates;

      if (callbacks.targetExit && typeof callbacks.targetExit === "function") {
        callbacks.targetExit(response, targetStates);
      }
    }
  }, {
    key: "callTargetEnter",
    value: function callTargetEnter(response) {
      var callbacks = stateValues.callbacks,
          targetStates = stateValues.targetStates;

      if (callbacks.targetEnter && typeof callbacks.targetEnter === "function") {
        callbacks.targetEnter(response, targetStates);
      }
    }
  }, {
    key: "registerObserversAbove",
    value: function registerObserversAbove() {
      var targetsOffsetHeight = stateValues.targetsOffsetHeight,
          viewPortHeight = stateValues.viewPortHeight,
          offsetMargin = stateValues.offsetMargin;
      var observers = stateValues.observers,
          targets = stateValues.targets;
      if (observers.above[0]) observers.above.forEach(function (observer) {
        return observer.disconnect();
      });
      observers.above = targets.map(function (target, index) {
        var marginTop = targetsOffsetHeight[index];
        var marginBottom = -viewPortHeight + offsetMargin;
        var rootMargin = "".concat(marginTop, "px 0px ").concat(marginBottom, "px 0px");
        var options = {
          root: null,
          rootMargin: rootMargin,
          threshold: 0
        };
        var observer = new IntersectionObserver(_callback__WEBPACK_IMPORTED_MODULE_0__["Callback"].intersectTargetAbove, options);
        observer.observe(target);
        return observer;
      });
    }
  }, {
    key: "registerObserversBelow",
    value: function registerObserversBelow() {
      var targetsOffsetHeight = stateValues.targetsOffsetHeight,
          viewPortHeight = stateValues.viewPortHeight,
          offsetMargin = stateValues.offsetMargin;
      var observers = stateValues.observers,
          targets = stateValues.targets,
          pageHeight = stateValues.pageHeight;
      if (observers.below[0]) observers.below.forEach(function (observer) {
        return observer.disconnect();
      });
      observers.below = targets.map(function (target, index) {
        var marginTop = -offsetMargin;
        var marginBottom = pageHeight - viewPortHeight + targetsOffsetHeight[index] + offsetMargin;
        var rootMargin = "".concat(marginTop, "px 0px ").concat(marginBottom, "px 0px");
        var options = {
          root: null,
          rootMargin: rootMargin,
          threshold: 0
        };
        var observer = new IntersectionObserver(_callback__WEBPACK_IMPORTED_MODULE_0__["Callback"].intersectTargetBelow, options);
        observer.observe(target);
        return observer;
      });
    }
  }, {
    key: "registerObserverOnViewPortAbove",
    value: function registerObserverOnViewPortAbove() {
      var targetsOffsetHeight = stateValues.targetsOffsetHeight,
          viewPortHeight = stateValues.viewPortHeight,
          offsetMargin = stateValues.offsetMargin;
      var observers = stateValues.observers,
          targets = stateValues.targets;
      if (observers.viewportAbove[0]) observers.viewportAbove.forEach(function (observer) {
        return observer.disconnect();
      });
      observers.viewportAbove = targets.map(function (target, index) {
        var marginTop = targetsOffsetHeight[index];
        var marginBottom = -(viewPortHeight - offsetMargin + targetsOffsetHeight[index]);
        var rootMargin = "".concat(marginTop, "px 0px ").concat(marginBottom, "px 0px");
        var options = {
          root: null,
          rootMargin: rootMargin,
          threshold: 0
        };
        var observer = new IntersectionObserver(_callback__WEBPACK_IMPORTED_MODULE_0__["Callback"].intersectViewportAbove, options);
        observer.observe(target);
        return observer;
      });
    }
  }, {
    key: "registerObserverOnViewPortBelow",
    value: function registerObserverOnViewPortBelow() {
      var targetsOffsetHeight = stateValues.targetsOffsetHeight,
          pageHeight = stateValues.pageHeight,
          offsetMargin = stateValues.offsetMargin;
      var observers = stateValues.observers,
          targets = stateValues.targets,
          targetsOffsetTop = stateValues.targetsOffsetTop;
      if (observers.viewportBelow[0]) ;
      observers.viewportBelow.forEach(function (observer) {
        return observer.disconnect();
      });
      observers.viewportBelow = targets.map(function (target, index) {
        var marginTop = -(offsetMargin + targetsOffsetHeight[index]);
        var marginBottom = pageHeight - targetsOffsetTop[index] - targetsOffsetHeight[index] - offsetMargin;
        var rootMargin = "".concat(marginTop, "px 0px ").concat(marginBottom, "px 0px");
        var options = {
          root: null,
          rootMargin: rootMargin,
          threshold: 0
        };
        var observer = new IntersectionObserver(_callback__WEBPACK_IMPORTED_MODULE_0__["Callback"].intersectViewportBelow, options);
        observer.observe(target);
        return observer;
      });
    }
  }, {
    key: "unregisterObservers",
    value: function unregisterObservers() {
      var observers = stateValues.observers;
      if (observers.above[0]) observers.above.forEach(function (observer) {
        return observer.disconnect();
      });
      if (observers.below[0]) observers.below.forEach(function (observer) {
        return observer.disconnect();
      });
      if (observers.viewportAbove[0]) observers.viewportAbove.forEach(function (observer) {
        return observer.disconnect();
      });
      if (observers.viewportBelow[0]) observers.viewportBelow.forEach(function (observer) {
        return observer.disconnect();
      });
    }
  }, {
    key: "updateTargetStateMode",
    value: function updateTargetStateMode(index, mode) {
      if (!stateValues.targetStates[index]) throw new Error("The element with the provided index has no state data");
      stateValues.targetStates[index].mode = mode;
    }
  }, {
    key: "updateTargetStateDirection",
    value: function updateTargetStateDirection(index, direction) {
      if (!stateValues.targetStates[index]) throw new Error("The element with the provided index has no state data");
      stateValues.targetStates[index].direction = direction;
    }
  }, {
    key: "targets",
    get: function get() {
      return stateValues.targets;
    },
    set: function set(nodeList) {
      var list = [];

      if (nodeList instanceof NodeList) {
        [].forEach.call(nodeList, function (node) {
          return list.push(node);
        });
        stateValues.targets = list;
      }
    }
  }, {
    key: "canvasClass",
    get: function get() {
      return stateValues.canvasClass;
    },
    set: function set() {
      var string = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "canvas";
      stateValues.canvasClass = string;
    }
  }, {
    key: "ZERO_MOE",
    get: function get() {
      return stateValues.ZERO_MOE;
    }
  }, {
    key: "debugMode",
    get: function get() {
      return stateValues.debugMode;
    },
    set: function set(bool) {
      if (![true, false].includes(bool)) throw new Error("Argument ".concat(bool, " is not a valid value"));
      stateValues.debugMode = bool;
    }
  }, {
    key: "direction",
    get: function get() {
      return stateValues.direction;
    },
    set: function set(value) {
      if (!["up", "down"].includes(value)) throw new Error("Argument ".concat(value, " is not a valid direction"));
      stateValues.direction = value;
    }
  }, {
    key: "previousYOffset",
    get: function get() {
      return stateValues.previousYOffset;
    },
    set: function set(numb) {
      if (isNaN(numb)) throw new Error("Argument ".concat(numb, " is not a valid value"));
      stateValues.previousYOffset = numb;
    }
  }, {
    key: "viewPortHeight",
    get: function get() {
      return stateValues.viewPortHeight;
    }
  }, {
    key: "pageHeight",
    get: function get() {
      return stateValues.pageHeight;
    }
  }, {
    key: "offsetValue",
    get: function get() {
      return stateValues.offsetValue;
    },
    set: function set(numb) {
      if (isNaN(numb)) throw new Error("Argument ".concat(numb, " is not a valid value"));
      stateValues.offsetValue = Math.min(Math.max(0, numb), 1);
    }
  }, {
    key: "offsetMargin",
    get: function get() {
      return stateValues.offsetMargin;
    }
  }, {
    key: "targetStates",
    get: function get() {
      return stateValues.targetStates;
    }
  }, {
    key: "targetsOffsetTop",
    get: function get() {
      return stateValues.targetsOffsetTop;
    }
  }]);

  return State;
}();

/***/ }),

/***/ "./src/public/js/response-canvas/index.js":
/*!************************************************!*\
  !*** ./src/public/js/response-canvas/index.js ***!
  \************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _response_canvas__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./response-canvas */ "./src/public/js/response-canvas/response-canvas.js");

new _response_canvas__WEBPACK_IMPORTED_MODULE_0__["ResponseCanvas"](document.querySelector("body"));

/***/ }),

/***/ "./src/public/js/response-canvas/media-manager/picture.js":
/*!****************************************************************!*\
  !*** ./src/public/js/response-canvas/media-manager/picture.js ***!
  \****************************************************************/
/*! exports provided: PictureManager */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PictureManager", function() { return PictureManager; });
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./util */ "./src/public/js/response-canvas/media-manager/util.js");
/* harmony import */ var _page_event_handlers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../page-event-handlers */ "./src/public/js/response-canvas/page-event-handlers.js");
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



var PictureManager =
/*#__PURE__*/
function () {
  function PictureManager(componenntUI, id) {
    var _this = this;

    _classCallCheck(this, PictureManager);

    _defineProperty(this, "getFacingMode", function () {
      return _util__WEBPACK_IMPORTED_MODULE_0__["constraints"].video.facingMode;
    });

    _defineProperty(this, "setFacingMode", function (mode) {
      _util__WEBPACK_IMPORTED_MODULE_0__["constraints"].video.facingMode = mode;
    });

    _defineProperty(this, "handleCameraToggling", function () {
      _this.stopMediaTracks(_this.stream);

      if (_this.getFacingMode() === "user") {
        _this.setFacingMode("environment");
      } else {
        _this.setFacingMode("user");
      }

      _this.iniate(_util__WEBPACK_IMPORTED_MODULE_0__["constraints"]);
    });

    _defineProperty(this, "handleVideoCanPlay", function (ev) {
      _this.takePictureBtn.style.opacity = 1;

      if (!_this.streaming) {
        _this.height = _this.video.videoWidth / (_this.video.videoWidth / _this.video.videoHeight);
        _this.tak;

        _this.canvas.setAttribute("height", _this.height);

        _this.video.setAttribute("height", _this.height);

        _this.canvas.setAttribute("width", _this.width);

        _this.video.setAttribute("width", _this.width);

        _this.streaming = false;
      }
    });

    _defineProperty(this, "takePicture", function () {
      var context = _this.canvas.getContext("2d");

      if (_this.width && _this.height) {
        _this.canvas.height = _this.height;
        _this.canvas.width = _this.width;
        context.drawImage(_this.video, 0, 0, _this.width, _this.height);

        var data = _this.canvas.toDataURL("image/png");

        _this.photo.setAttribute("src", data);

        _this.pictureTaken = true; // this.photoContainer.classList.remove("des-content");
        // this.retakeButton.classList.remove("des-content");

        _this.uploadBtn.classList.remove("hide-elem", "des-content"); // this.videoContainer.classList.add("des-content");

      } else {
        _this.clearPhoto();
      }
    });

    _defineProperty(this, "startAgain", function () {
      _this.clearPhoto();

      _this.videoContainer.classList.remove("des-content");

      _this.photoContainer.classList.add("des-content");

      _this.retakeButton.classList.add("des-content");

      _this.uploadBtn.classList.add("des-content");
    });

    this.uploadBtn = document.querySelector(".uploadButton[id=\"".concat(id, "\"]"));
    this.retakeButton = document.querySelector(".resetButton[id=\"".concat(id, "\"]"));
    this.photoContainer = componenntUI.querySelector(".picture__output");
    this.toggleCameraButton = componenntUI.querySelector("button#pToggleCam");
    this.videoContainer = componenntUI.querySelector(".picture__feed");
    this.video = componenntUI.getElementById("livefeed");
    this.takePictureBtn = componenntUI.getElementById("capture");
    this.canvas = componenntUI.getElementById("canvas");
    this.photo = componenntUI.getElementById("photo");
    this.streaming = false;
    this.pictureTaken = false;
    this.width = 340;
    this.questionId = id;
    this.stream = null;
    this.height = 0;
    this.iniate(_util__WEBPACK_IMPORTED_MODULE_0__["constraints"]);
  }

  _createClass(PictureManager, [{
    key: "iniate",
    value: function iniate(constraints) {
      var _this2 = this;

      // this.retakeButton.addEventListener("click", this.startAgain);
      this.toggleCameraButton.addEventListener("click", this.handleCameraToggling);

      var settings = _objectSpread({}, constraints);

      delete settings.audio; // register events

      this.takePictureBtn.addEventListener("click", this.takePicture, false); // request stream data from media devices on auser system

      navigator.mediaDevices.getUserMedia(settings).then(function (stream) {
        _this2.video.srcObject = stream;
        _this2.stream = stream;

        _this2.video.play();
      })["catch"](function (err) {
        return alert("Sorry we can't access your camera");
      });
      this.video.addEventListener("canplay", this.handleVideoCanPlay); // reset picture canvas

      this.clearPhoto();
    }
    /**
     * this function stopMediaTracks loops through each media track in the stream, and stop each of them.  Ezugudor addendum
     * @param { MediaStream } stream
     */

  }, {
    key: "stopMediaTracks",
    value: function stopMediaTracks(stream) {
      stream.getTracks().forEach(function (track) {
        track.stop();
      });
    }
    /**
     * function gets the facing mode of the camera - Ezugudor
     */

  }, {
    key: "clearPhoto",

    /**
     * Prepare the canvas for another picture
     */
    value: function clearPhoto() {
      var context = this.canvas.getContext("2d");
      context.fillStyle = "#AAA";
      context.fillRect(0, 0, this.canvas.width, this.canvas.height);
      var data = this.canvas.toDataURL("image/png");
      this.photo.setAttribute("src", data);
    }
  }, {
    key: "getAsset",

    /**
     * return asset that was recorded
     */
    value: function getAsset() {
      if (this.pictureTaken) {
        var data = this.canvas.toDataURL("image/png");
        var blobBin = atob(data.split(",")[1]);
        var array = [];

        for (var i = 0; i < blobBin.length; i++) {
          array.push(blobBin.charCodeAt(i));
        }

        return new Blob([new Uint8Array(array)], {
          type: "image/png"
        });
      }

      return null;
    }
  }]);

  return PictureManager;
}();

/***/ }),

/***/ "./src/public/js/response-canvas/media-manager/util.js":
/*!*************************************************************!*\
  !*** ./src/public/js/response-canvas/media-manager/util.js ***!
  \*************************************************************/
/*! exports provided: constraints */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "constraints", function() { return constraints; });
var constraints = {
  video: {
    width: {
      ideal: 600
    },
    height: {
      ideal: 600
    },
    facingMode: "user"
  },
  audio: true
};

/***/ }),

/***/ "./src/public/js/response-canvas/media-manager/video.js":
/*!**************************************************************!*\
  !*** ./src/public/js/response-canvas/media-manager/video.js ***!
  \**************************************************************/
/*! exports provided: VideoManager */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VideoManager", function() { return VideoManager; });
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./util */ "./src/public/js/response-canvas/media-manager/util.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


var VideoManager =
/*#__PURE__*/
function () {
  function VideoManager(componentUI, id) {
    var _this = this;

    _classCallCheck(this, VideoManager);

    _defineProperty(this, "getFacingMode", function () {
      return _util__WEBPACK_IMPORTED_MODULE_0__["constraints"].video.facingMode;
    });

    _defineProperty(this, "setFacingMode", function (mode) {
      _util__WEBPACK_IMPORTED_MODULE_0__["constraints"].video.facingMode = mode;
    });

    _defineProperty(this, "handleCameraToggling", function () {
      _this.stopMediaTracks(_this.stream);

      if (_this.getFacingMode() === "user") {
        _this.setFacingMode("environment");
      } else {
        _this.setFacingMode("user");
      }

      _this.init(_util__WEBPACK_IMPORTED_MODULE_0__["constraints"]);
    });

    _defineProperty(this, "handleMediaSourceOpen", function () {
      _this.sourceBuffer = _this.mediaSource.addSourceBuffer('video/webm; codecs="vp8"');
    });

    _defineProperty(this, "handleRecording", function () {
      var text = _this.recordButton.textContent;

      if (text === "Start Recording") {
        _this.startRecording();
      } else if (text === "Record Again") {
        _this.recordAgain();
      } else {
        _this.stopRecording();
      }
    });

    _defineProperty(this, "playRecordedVideo", function () {
      var buffer = new Blob(_this.recordedBlob, {
        type: "video/webm"
      });
      _this.recordedVideo.src = null;
      _this.recordedVideo.srcObject = null;
      _this.recordedVideo.src = window.URL.createObjectURL(buffer);
      _this.recordedVideo.controls = true;

      _this.recordedVideo.play();
    });

    _defineProperty(this, "handleDataAvailable", function (event) {
      if (event.data && event.data.size > 0) {
        _this.recordedBlob.push(event.data);
      }
    });

    this.uploadBtn = document.querySelector(".uploadButton[id=\"".concat(id, "\"]"));
    this.recordedVideo = componentUI.querySelector("video#playback");
    this.recordButton = componentUI.querySelector("button#record");
    this.toggleCameraButton = componentUI.querySelector("button#toggleCam");
    this.errorMsgElement = componentUI.querySelector("span#errorMsg");
    this.liveVideo = componentUI.querySelector("video#live");
    this.secondsSpan = componentUI.querySelector(".seconds");
    this.minutesSpan = componentUI.querySelector(".minutes");
    this.hoursSpan = componentUI.querySelector(".hours");
    this.mediaSource = new MediaSource();
    this.componentUI = componentUI;
    this.timeInterval = null;
    this.mediaRecorder = null;
    this.recordedBlob = null;
    this.sourceBuffer = null;
    this.stream = null;
    this.init(_util__WEBPACK_IMPORTED_MODULE_0__["constraints"]);
  }
  /**
   * Ask permission to user media and set up camera
   * start streaming media data from users devices
   */


  _createClass(VideoManager, [{
    key: "init",
    value: function init(constraints) {
      var _this2 = this;

      this.mediaSource.addEventListener("sourceopen", this.handleMediaSourceOpen);
      this.recordButton.addEventListener("click", this.handleRecording);
      this.toggleCameraButton.addEventListener("click", this.handleCameraToggling);

      try {
        navigator.mediaDevices.getUserMedia(constraints).then(function (stream) {
          _this2.stream = stream;
          _this2.liveVideo.srcObject = stream;
          _this2.recordButton.disabled = false;
        });
      } catch (ex) {}
    }
    /**
     * this function stopMediaTracks loops through each media track in the stream, and stop each of them.  Ezugudor addendum
     * @param { MediaStream } stream
     */

  }, {
    key: "stopMediaTracks",
    value: function stopMediaTracks(stream) {
      stream.getTracks().forEach(function (track) {
        track.stop();
      });
    }
    /**
     * function gets the facing mode of the camera - Ezugudor
     */

  }, {
    key: "recordAgain",

    /**
     * prepare environment to record another video
     */
    value: function recordAgain() {
      this.recordedVideo.classList.add("des-content", "hide-elem"); //below commented out so we dont get playback error in the recorded video element
      // this.recordedVideo.src = null;
      // this.recordedVideo.srcObject = null;

      this.liveVideo.classList.remove("des-content", "hide-elem");
      this.uploadBtn.classList.add("des-content", "hide-elem");
      this.secondsSpan.innerHTML = "00";
      this.minutesSpan.innerHTML = "0";
      this.hoursSpan.innerHTML = "0";
      this.startRecording();
    }
    /**
     * Save the data coming from users media devices camera/mic
     */

  }, {
    key: "startRecording",
    value: function startRecording() {
      this.recordedBlob = [];
      var options = {
        mimeType: "video/webm;codec=vp9"
      };

      if (!MediaRecorder.isTypeSupported(options.mimeType)) {
        options = {
          mimeType: "video/webm;codec=vp8"
        };

        if (!MediaRecorder.isTypeSupported(options.mimeType)) {
          options = {
            mimeType: "video/webm"
          };
        }
      }

      try {
        this.mediaRecorder = new MediaRecorder(this.stream, options);
        this.recordButton.textContent = "Stop Recording";
        this.mediaRecorder.ondataavailable = this.handleDataAvailable;
        this.mediaRecorder.start(10); // collect 10ms of data;

        this.startCountDown();
      } catch (ex) {}
    }
    /**
     * Save raw data from user's devices to memory
     */

  }, {
    key: "stopRecording",

    /**
     * stop collecting media data from user's device
     */
    value: function stopRecording() {
      this.mediaRecorder.stop();
      this.recordButton.textContent = "Record Again";
      this.recordedVideo.classList.remove("des-content", "hide-elem");
      this.uploadBtn.classList.remove("des-content", "hide-elem"); // this.liveVideo.classList.add("des-content");

      this.playRecordedVideo();
      this.clearTimer();
    }
    /**
     * display elapsed time
     */

  }, {
    key: "startCountDown",
    value: function startCountDown() {
      var _this3 = this;

      var startTime = new Date();
      this.timeInterval = setInterval(function () {
        var time = new Date() - startTime;
        var seconds = Math.floor(time / 1000 % 60);
        var munites = Math.floor(time / 1000 / 60 % 60);
        var hours = Math.floor(time / (1000 * 60 * 60) % 24);
        var days = Math.floor(time / (1000 * 60 * 60 * 24));
        _this3.secondsSpan.innerHTML = ("0" + seconds).slice(-2);
        _this3.minutesSpan.innerHTML = munites;
        _this3.hoursSpan.innerHTML = hours;
      }, 1000);
    }
  }, {
    key: "clearTimer",
    value: function clearTimer() {
      clearInterval(this.timeInterval);
    }
    /**
     * return asset that was recorded
     */

  }, {
    key: "getAsset",
    value: function getAsset() {
      if (this.recordedBlob.length > 0) return new Blob(this.recordedBlob, {
        type: "video/webm"
      });
      return null;
    }
  }]);

  return VideoManager;
}();

/***/ }),

/***/ "./src/public/js/response-canvas/page-event-handlers.js":
/*!**************************************************************!*\
  !*** ./src/public/js/response-canvas/page-event-handlers.js ***!
  \**************************************************************/
/*! exports provided: EventHandler */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EventHandler", function() { return EventHandler; });
/* harmony import */ var _templates_response_canvas_uploaded_indicator_hbs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../templates/response-canvas/uploaded-indicator.hbs */ "./src/public/js/templates/response-canvas/uploaded-indicator.hbs");
/* harmony import */ var _templates_response_canvas_uploaded_indicator_hbs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_uploaded_indicator_hbs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _templates_response_canvas_upload_indicator_hbs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../templates/response-canvas/upload-indicator.hbs */ "./src/public/js/templates/response-canvas/upload-indicator.hbs");
/* harmony import */ var _templates_response_canvas_upload_indicator_hbs__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_upload_indicator_hbs__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _templates_response_canvas_validation_msg_hbs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../templates/response-canvas/validation-msg.hbs */ "./src/public/js/templates/response-canvas/validation-msg.hbs");
/* harmony import */ var _templates_response_canvas_validation_msg_hbs__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_validation_msg_hbs__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _templates_response_canvas_record_video_hbs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../templates/response-canvas/record-video.hbs */ "./src/public/js/templates/response-canvas/record-video.hbs");
/* harmony import */ var _templates_response_canvas_record_video_hbs__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_record_video_hbs__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _templates_response_canvas_take_picture_hbs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../templates/response-canvas/take-picture.hbs */ "./src/public/js/templates/response-canvas/take-picture.hbs");
/* harmony import */ var _templates_response_canvas_take_picture_hbs__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_take_picture_hbs__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _templates_response_canvas_header_hbs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../templates/response-canvas/header.hbs */ "./src/public/js/templates/response-canvas/header.hbs");
/* harmony import */ var _templates_response_canvas_header_hbs__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_header_hbs__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _templates_response_canvas_footer_hbs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../templates/response-canvas/footer.hbs */ "./src/public/js/templates/response-canvas/footer.hbs");
/* harmony import */ var _templates_response_canvas_footer_hbs__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_footer_hbs__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _media_manager_picture__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./media-manager/picture */ "./src/public/js/response-canvas/media-manager/picture.js");
/* harmony import */ var _media_manager_video__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./media-manager/video */ "./src/public/js/response-canvas/media-manager/video.js");
/* harmony import */ var _page_utils__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./page-utils */ "./src/public/js/response-canvas/page-utils.js");
/* harmony import */ var _validator__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./validator */ "./src/public/js/response-canvas/validator.js");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../utils */ "./src/public/js/utils/index.js");
/* harmony import */ var _core__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../core */ "./src/public/js/core/index.js");
/* harmony import */ var _media_manager_util__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./media-manager/util */ "./src/public/js/response-canvas/media-manager/util.js");
/* harmony import */ var vanilla_text_mask__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! vanilla-text-mask */ "./node_modules/vanilla-text-mask/dist/vanillaTextMask.js");
/* harmony import */ var vanilla_text_mask__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(vanilla_text_mask__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var text_mask_addons__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! text-mask-addons */ "./node_modules/text-mask-addons/dist/textMaskAddons.js");
/* harmony import */ var text_mask_addons__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(text_mask_addons__WEBPACK_IMPORTED_MODULE_15__);
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

















var EventHandler =
/*#__PURE__*/
function () {
  function EventHandler(pageView, questions, _formData) {
    var _this = this;

    _classCallCheck(this, EventHandler);

    _defineProperty(this, "handleInput", function (event) {
      var _event$target = event.target,
          id = _event$target.id,
          value = _event$target.value;

      var nextButtonWrapper = _this.pageView.querySelector("div.nextButtonWrapper[id=\"".concat(id, "\"]"));

      if (nextButtonWrapper) {
        if (value) {
          nextButtonWrapper.classList.remove("inactiveNextButtonWrapper");
        } else {
          nextButtonWrapper.classList.add("inactiveNextButtonWrapper");
        }
      }

      _this.addAnswer(id, value);
    });

    _defineProperty(this, "handleFileDrop", function (e) {
      e.preventDefault();
      var data = e.target.dataset;

      _this.handleStaticAssetUpload(e.dataTransfer.files[0], data.qType, dataqId);
    });

    _defineProperty(this, "expandDropDownList", function (event) {
      var id = event.target.dataset.qId;

      var node = _this.pageView.querySelector("div.dropdownOptions[data-q-id=\"".concat(id, "\"]"));

      if (node.classList.contains("inactiveWrapper")) {
        node.classList.remove("inactiveWrapper");
        node.classList.add("activeWrapper");
      }
    });

    _defineProperty(this, "filterDropdownOptions", function (event) {
      var _event$target2 = event.target,
          id = _event$target2.id,
          value = _event$target2.value,
          dataset = _event$target2.dataset;
      var root = [];

      if (dataset.isCompact) {
        var pID = dataset.parentId;
        root = _this.questions.find(function (el) {
          return el.id === pID;
        }).children;
      } else {
        root = _this.questions;
      }

      var question = root.find(function (el) {
        return el.id === id;
      });
      var type = question.type,
          children = question.children,
          controlType = question.controlType;
      var options = [];
      var template = "";

      if (controlType === "branch") {
        options = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].buildOptionsFromBranches(children, value);
        template = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].buildOptionTemplate(options, id, type, controlType, dataset.parentId);
      } else if (controlType === "state") {
        options = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].buildOptionFromArray(_page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].getStates(), value, controlType);
        template = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].buildOptionTemplate(options, id, type, controlType, dataset.parentId);
      } else if (controlType === "lga") {
        options = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].buildOptionFromArray(_page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].getDefaultStateLGA(), value, controlType);
        template = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].buildOptionTemplate(options, id, type, controlType, dataset.parentId);
      } else {
        options = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].buildOptionFromArray(children, value, controlType);
        template = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].buildOptionTemplate(options, id, type);
      }

      var optionsNode = Object(_utils__WEBPACK_IMPORTED_MODULE_11__["parseHtml"])(template);

      var optionContainer = _this.pageView.querySelector("div.optionsBox[data-q-id=\"".concat(id, "\"]"));

      _this.addOptionListeners(optionsNode, id);

      optionContainer.innerHTML = "";
      optionContainer.appendChild(optionsNode);
    });

    _defineProperty(this, "toggleDropDownList", function (event) {
      var id = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

      if (!id) {
        id = event.target.dataset.qId;
      }

      var node = _this.pageView.querySelector("div.dropdownOptions[data-q-id=\"".concat(id, "\"]"));

      if (node.classList.contains("inactiveWrapper")) {
        node.classList.remove("inactiveWrapper");
        node.classList.add("activeWrapper");
      } else {
        node.classList.add("inactiveWrapper");
        node.classList.remove("activeWrapper");
      }
    });

    _defineProperty(this, "handleOptionSelection", function (event) {
      var node = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].getRootNode(event.target);
      var type = node.dataset.optionType;
      var id = node.dataset.optionId;
      var parentID = node.dataset.parentId;
      var resourceId = node.dataset.resourceId;
      var controlType = node.dataset.controlType; //  auto-populate dependent dropdowns

      if (controlType == "state") {
        // use the parent id to find the lga element that has the same parent id and populate it
        var options = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].buildOptionFromArray(_page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].getSelectedStateLGA(resourceId));

        var optionContainer = _this.pageView.querySelector("div.optionsBox[data-parent-id=\"".concat(parentID, "\"][data-control-type=\"lga\"]"));

        var containerID = optionContainer.dataset.qId;
        var selectedLGATemplate = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].buildOptionTemplate(options, containerID, type, "lga", parentID);
        var optionsNode = Object(_utils__WEBPACK_IMPORTED_MODULE_11__["parseHtml"])(selectedLGATemplate); //get the first option and insert in the element input element

        var firstSelectedOption = optionsNode.querySelector(".dropDownOptionText").innerText;
        _this.pageView.querySelector("input[data-q-id=\"".concat(containerID, "\"]")).value = firstSelectedOption;

        _this.addOptionListeners(optionsNode, containerID);

        optionContainer.innerHTML = "";
        optionContainer.appendChild(optionsNode);
      }

      if (id) {
        var allOptions = _this.pageView.querySelectorAll("div[data-option-id=\"".concat(id, "\"]"));

        allOptions.forEach(function (option) {
          var checkIconBox = option.querySelector("div.iconWrapper");

          if (checkIconBox.classList.contains("activIconWrapper")) {
            checkIconBox.classList.add("inactivIconWrapper");
            checkIconBox.classList.remove("activIconWrapper");
          }
        });
        var picked = node.querySelector("div.iconWrapper");
        var pickedTextBox = node.querySelector("div.text");
        picked.classList.remove("inactivIconWrapper");
        picked.classList.add("activIconWrapper");

        _this.addAnswer(id, pickedTextBox.textContent, parentID);

        _this.toggleErrorUIFor(id, []);

        _this.toggleSubmitErrorUI(false);

        _this.updateHeader(id);

        _this.updateFooter();

        if (type === "dropdown") {
          var input = _this.pageView.querySelector("input[data-q-id=\"".concat(id, "\"]"));

          input.value = pickedTextBox.textContent;

          _this.toggleDropDownList(null, id);
        }

        _this.goToNextQuestion(id, "down");

        _this.updateFocus(id);
      }
    });

    _defineProperty(this, "goToNextQuestion", function (currentQuestionId) {
      var direction = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "down";
      // Todo **************************************
      // logic should change if user is scrolling up
      var nextQuestion = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].getNextQuestion(_this.questions, currentQuestionId);

      var canvas = _this.pageView.querySelector(".canvas");

      var currentQuestionUI = _this.pageView.querySelector(".elementWrapper[data-q-id=\"".concat(currentQuestionId, "\"]"));

      var nextQuestionUI = _this.pageView.querySelector(".elementWrapper[data-q-id=\"".concat(nextQuestion.id, "\"]"));

      nextQuestionUI.scrollIntoView(); // let distanceToScroll = 237;
      // if (nextQuestion) {
      //   const nextQuestionUI = this.pageView.querySelector(
      //     `.elementWrapper[data-q-id="${nextQuestion.id}"]`
      //   );
      //   const distanceToCurrentQ = PageUtils.getDistanceToTop(currentQuestionUI);
      //   const distanceToNextQ = PageUtils.getDistanceToTop(nextQuestionUI);
      //   distanceToScroll = distanceToNextQ - distanceToCurrentQ;
      // }
      // if (direction === "down") {
      //   canvas.scrollBy(0, distanceToScroll);
      // } else if (direction === "up") {
      //   canvas.scrollBy(0, -distanceToScroll);
      // }
    });

    _defineProperty(this, "getDirection", function (currentQuestionId, nextQnDistance) {
      var direction = "down";

      var currentQuestionUI = _this.pageView.querySelector(".elementWrapper[data-q-id=\"".concat(currentQuestionId, "\"]"));

      var currentQnDistance = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].getDistanceToTop(currentQuestionUI);
      direction = currentQnDistance > nextQnDistance ? "up" : "down";
      return direction;
    });

    _defineProperty(this, "goToErrorQuestion", function () {
      var currentQuestionId = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      var direction = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "up";

      var singleUnansweredQnID = _this.getUnAnswered()[0].id;

      var unAnsweredQuestionUI = _this.pageView.querySelector(".elementWrapper[data-q-id=\"".concat(singleUnansweredQnID, "\"]"));

      unAnsweredQuestionUI.scrollIntoView();
    });

    _defineProperty(this, "goToLast", function () {
      var direction = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "down";

      var canvas = _this.pageView.querySelector(".canvas");

      var submitBtnUI = _this.pageView.querySelector("button[id=\"submitresponse\"]");

      var submitBtnDistance = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].getDistanceToTop(submitBtnUI);

      if (direction === "down") {
        submitBtnUI.scrollIntoView(); // canvas.scrollBy(0, submitBtnDistance);
      } else if (direction === "up") {
        submitBtnUI.scrollIntoView(); // canvas.scrollBy(0, -submitBtnDistance);
      }
    });

    _defineProperty(this, "handleStaticAssetUpload", function (file, type, id) {
      var uploadIndicator = _this.pageView.querySelector("div.uploadstatus[data-q-id=\"".concat(id, "\"]"));

      var templateOption = {
        imgUrl: "/img/uploading.svg",
        labelText: "Uploading...",
        id: id
      };
      var template = _templates_response_canvas_upload_indicator_hbs__WEBPACK_IMPORTED_MODULE_1___default()(templateOption);
      uploadIndicator.innerHTML = "";
      uploadIndicator.appendChild(Object(_utils__WEBPACK_IMPORTED_MODULE_11__["parseHtml"])(template));

      _this.uploadFile(file, type, id).then(function (res) {
        var labelVal = type == "signature" ? "Change Signature" : "Change Passport";
        templateOption = {
          imgUrl: res.assetUrl,
          labelText: labelVal,
          id: id
        };
        template = _templates_response_canvas_uploaded_indicator_hbs__WEBPACK_IMPORTED_MODULE_0___default()(templateOption);
        uploadIndicator.innerHTML = "";
        uploadIndicator.appendChild(Object(_utils__WEBPACK_IMPORTED_MODULE_11__["parseHtml"])(template));

        _this.addAnswer(id, res.assetUrl);

        _this.processAnswer(null, id, res.assetUrl);
      })["catch"](function (err) {
        alert("".concat(type, " upload faild please try again."));
      });
    });

    _defineProperty(this, "askMediaPermission", function (ev) {
      var id = ev.currentTarget.id;
      var qtype = ev.currentTarget.dataset.qtype;

      var componentParent = _this.pageView.querySelector("div.mediaRecorder[id=\"".concat(id, "\"]"));

      var componentUI = document.querySelector(".elementWrapper[data-q-id=\"".concat(id, "\"]"));
      var component = componentParent.querySelector("div.answerWrapper");
      component.innerHTML = "";

      if (qtype === "video") {
        var newNodeTree = Object(_utils__WEBPACK_IMPORTED_MODULE_11__["parseHtml"])(_templates_response_canvas_record_video_hbs__WEBPACK_IMPORTED_MODULE_3___default()());
        var videoManager = new _media_manager_video__WEBPACK_IMPORTED_MODULE_8__["VideoManager"](newNodeTree, id);
        component.appendChild(newNodeTree);

        _this.videoManagers.set(id, videoManager);

        _this.uploadBtn = componentUI.querySelector(".uploadButton");
        _this.recordedVideo = componentUI.querySelector("video#playback");
        _this.recordButton = componentUI.querySelector("button#record");
        _this.retakeButton = componentUI.querySelector(".resetButton");
        _this.toggleCameraButton = componentUI.querySelector("button#toggleCam");
        _this.liveVideo = componentUI.querySelector("video#live");
        _this.countDown = componentUI.querySelector("#clockdiv");

        _this.uploadBtn.classList.add("hide-elem");

        _this.toggleCameraButton.classList.remove("hide-elem");

        _this.liveVideo.classList.remove("hide-elem");

        _this.recordButton.classList.remove("hide-elem");

        _this.retakeButton.classList.add("hide-elem");

        _this.countDown.classList.remove("hide-elem");
      } else if (qtype === "picture") {
        // console.log('the picture template',parseHtml(takePicture()))
        var _newNodeTree = Object(_utils__WEBPACK_IMPORTED_MODULE_11__["parseHtml"])(_templates_response_canvas_take_picture_hbs__WEBPACK_IMPORTED_MODULE_4___default()());

        var pictureManager = new _media_manager_picture__WEBPACK_IMPORTED_MODULE_7__["PictureManager"](_newNodeTree, id);
        component.appendChild(_newNodeTree);

        _this.pictureManagers.set(id, pictureManager);

        console.log("logging data in askmedia", componentUI);
        _this.uploadBtn = componentUI.querySelector(".uploadButton");
        _this.retakeButton = componentUI.querySelector(".resetButton");
        _this.videoContainer = componentUI.querySelector(".picture__feed");
        _this.photoContainer = componentUI.querySelector(".picture__output");
        _this.toggleCameraButton = componentUI.querySelector("button#pToggleCam");
        _this.takePictureBtn = componentUI.querySelector("#capture"); // this.uploadBtn.classList.add("hide-elem");

        _this.toggleCameraButton.classList.remove("hide-elem");

        _this.retakeButton.classList.add("hide-elem");

        _this.videoContainer.classList.remove("hide-elem");

        _this.takePictureBtn.classList.remove("hide-elem");
      }
    });

    _defineProperty(this, "handleGeneratedAssetUpload", function (ev) {
      var span = ev.currentTarget.querySelector(".nextButtonText");
      span.innerHTML = "Uploading...";
      var id = ev.currentTarget.id;
      var componentUI = document.querySelector(".elementWrapper[data-q-id=\"".concat(id, "\"]"));
      var qtype = ev.currentTarget.dataset.qtype; // get toggleable UIs

      if (qtype === "video") {
        _this.uploadBtn = componentUI.querySelector(".uploadButton");
        _this.recordedVideo = componentUI.querySelector("video#playback");
        _this.recordButton = componentUI.querySelector("button#record");
        _this.toggleCameraButton = componentUI.querySelector("button#toggleCam");
        _this.liveVideo = componentUI.querySelector("video#live");
        _this.countDown = componentUI.querySelector("#clockdiv");
        _this.retakeButton = componentUI.querySelector(".resetButton");
      } else if (qtype === "picture") {
        _this.uploadBtn = componentUI.querySelector(".uploadButton");
        _this.retakeButton = componentUI.querySelector(".resetButton");
        _this.videoContainer = componentUI.querySelector(".picture__feed");
        _this.photoContainer = componentUI.querySelector(".picture__output");
        _this.toggleCameraButton = componentUI.querySelector("button#pToggleCam");
        _this.takePictureBtn = componentUI.querySelector("#capture");
      }

      var assetManager = null;
      var name = null;
      var errorMessage = null;

      if (qtype === "video") {
        assetManager = _this.videoManagers.get(id);
        errorMessage = "You have not recorded any video yet.";
        name = "video.webm";
      } else if (qtype === "picture") {
        assetManager = _this.pictureManagers.get(id);
        name = "image.png";
        errorMessage = "You have not taken any picture yet.";
      }

      if (assetManager) {
        var asset = assetManager.getAsset();

        if (asset) {
          _this.uploadFile(asset, qtype, name).then(function (res) {
            //fix from Ezugudor
            _this.turnOffMediaCam(assetManager);

            span.innerHTML = "Upload";

            _this.addAnswer(id, res.assetUrl);

            _this.processAnswer(null, id, res.assetUrl); // hide some of this  componets UI


            if (qtype === "video") {
              _this.uploadBtn.classList.add("hide-elem");

              _this.toggleCameraButton.classList.add("hide-elem");

              _this.liveVideo.classList.add("hide-elem");

              _this.recordButton.classList.add("hide-elem");

              _this.countDown.classList.add("hide-elem");

              _this.retakeButton.classList.remove("hide-elem");
            } else if (qtype === "picture") {
              _this.uploadBtn.classList.add("hide-elem");

              _this.toggleCameraButton.classList.add("hide-elem");

              _this.retakeButton.classList.remove("hide-elem");

              _this.videoContainer.classList.add("hide-elem");

              _this.takePictureBtn.classList.add("hide-elem");
            }
          })["catch"](function (err) {
            span.innerHTML = "Upload";
            alert("".concat(qtype, " upload failed please try again."));
          });
        } else {
          span.innerHTML = "Upload";
          alert(errorMessage);
        }
      }
    });

    _defineProperty(this, "turnOffMediaCam", function (assetManager) {
      var videoHandle = assetManager.video || assetManager.liveVideo; // iterate through the media elements and turn each off (video and audio)

      for (var elem in videoHandle.srcObject.getTracks()) {
        videoHandle.srcObject.getTracks()[elem].stop();
      }
    });

    _defineProperty(this, "showAskPermisisonBtn", function (id) {
      var componentParent = _this.pageView.querySelector("div.mediaRecorder[id=\"".concat(id, "\"]"));

      var component = componentParent.querySelector("div.answerWrapper");
      var permissionBtn = component.querySelector(".askPermissionBtn");
      permissionBtn.querySelector(".nextButtonText").innerHTML = "Re-take"; // component.innerHTML = "";

      permissionBtn.style.display = "block";
    });

    _defineProperty(this, "uploadFile", function (file, type) {
      var name = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

      var _PageUtils$getDefault = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].getDefaultRespondant(),
          firstname = _PageUtils$getDefault.firstname,
          lastname = _PageUtils$getDefault.lastname;

      type = String("".concat(type, "s")).toLowerCase();
      var fullname = "".concat(firstname).concat(lastname);
      var bankslug = _this.pageView.dataset.bankslug;
      var url = "upload/".concat(type, "/").concat(bankslug, "/").concat(fullname, "_").concat(type);
      var formData = new FormData();

      if (name) {
        formData.append("asset", file, name);
      } else {
        formData.append("asset", file);
      }

      return _core__WEBPACK_IMPORTED_MODULE_12__["Http"].upLoadForm(url, formData, name, type);
    });

    _defineProperty(this, "processAnswer", function (event, id, value) {
      if (!id) {
        // process answer only when a user press enter while interacting with an input element
        var target = event.target;
        if (event.shiftKey && event.key === "Enter") return;
        if (event.key && event.key !== "Enter") return;

        if (event.key && event.key === "Enter") {
          target = event.target;
        } // ensure that the event target is an input element


        if (target.nodeName === "SPAN" || target.nodeName === "BUTTON") {
          target = _this.pageView.querySelector(".input[data-q-id=\"".concat(target.id, "\"]"));
        }

        value = target.value;
        id = target.id;
      }

      var hasError = _this.validateAnswer(value, id);

      if (!hasError) {
        _this.updateHeader(id);

        _this.updateFooter();

        if (!_this.correctionMode) {
          _this.goToNextQuestion(id, "down", 340);
        } else {
          if (_this.getUnAnswered().length > 0) {
            _this.goToErrorQuestion(id);
          } else {
            _this.goToLast();

            _this.correctionMode = false;
            return; //so it doesnt focus on any other
          }
        }
      } else {
        /** If a user clears an already answered question and hits the enter button,
         * remove the question from the answered array and update acordingly */
        var newAnswers = _this.answers.filter(function (e) {
          return e.questionId !== id;
        });

        _this.answers = newAnswers;

        _this.updateHeader(id);

        _this.updateFooter();
      }

      _this.updateFocus(id);
    });

    _defineProperty(this, "validateAnswer", function (answer, questionId) {
      var question = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].questionFinder(_this.questions, questionId);
      var validationRules = question.validationRules,
          type = question.type;
      if (type === "statement") return;
      var result = _validator__WEBPACK_IMPORTED_MODULE_10__["Validator"].validate(validationRules, type, answer);
      var hasError = result.hasError,
          messages = result.messages;

      _this.toggleErrorUIFor(questionId, messages, hasError);

      _this.toggleSubmitErrorUI(hasError);

      return result.hasError;
    });

    this.questions = questions;
    this.formData = _formData;
    this.pageView = pageView;
    this.pictureManagers = new Map();
    this.videoManagers = new Map();
    this.answers = [];
    this.correctionMode = false; //picture UI components

    this.uploadBtn = null;
    this.retakeButton = null;
    this.photoContainer = null;
    this.toggleCameraButton = null;
    this.videoContainer = null;
    this.video = null;
    this.takePictureBtn = null;
    this.canvas = null;
    this.photo = null; // video UI components

    this.recordedVideo = null;
    this.recordButton = null;
    this.liveVideo = null;
    this.countDown = null; //imask plugin

    setTimeout(function () {
      if (!document.querySelectorAll("input[type=text].date-element")) {
        return false;
      }

      var element = document.querySelectorAll("input[type=text].date-element")[0]; //this is wrong, once a new input[type=text] element is added will break.

      var aYearFromNow = new Date();
      var nextYear = aYearFromNow.getFullYear() + 1;
      var phoneMask = [/\d/, /\d/, "/", /\d/, /\d/, "/", /[1-2]/, /\d/, /\d/, /\d/];
      var autoCorrectedDatePipe = Object(text_mask_addons__WEBPACK_IMPORTED_MODULE_15__["createAutoCorrectedDatePipe"])("dd mm yyyy", {
        minYear: 1900,
        maxYear: nextYear
      });
      var _iteratorNormalCompletion = true;
      var _didIteratorError = false;
      var _iteratorError = undefined;

      try {
        for (var _iterator = document.querySelectorAll("input[type=text].date-element")[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
          var input = _step.value;
          vanilla_text_mask__WEBPACK_IMPORTED_MODULE_14__["maskInput"]({
            inputElement: input,
            mask: phoneMask,
            pipe: autoCorrectedDatePipe,
            keepCharPositions: true
          });
        } // console.log('timeout')

      } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion && _iterator["return"] != null) {
            _iterator["return"]();
          }
        } finally {
          if (_didIteratorError) {
            throw _iteratorError;
          }
        }
      }
    }, 1000);
  }
  /**
   * collect text entered into an input element by a user
   * @param {InputEvent} event the input event
   */


  _createClass(EventHandler, [{
    key: "highlightDropArea",

    /**
     * Give a visual cue to user when they drag a object around an area where files
     * can be dropped for uploading
     * @param {DragEvent} event
     */
    value: function highlightDropArea(event) {
      event.target.classList.add("highlightArea");
    }
    /**
     * Give a visual cue to user when they drag a object out of an area where files
     * can be dropped for uploading
     *@param {DragEvent} event
     */

  }, {
    key: "unhighlightDropArea",
    value: function unhighlightDropArea(event) {
      event.target.classList.remove("highlightArea");
    }
    /**
     * Handle object drop event in a region that allow drag and drop
     * @param {DropEvent} e
     */

  }, {
    key: "addOptionListeners",

    /**
     *  Register an event listener that monitors when a user clicks an option in a multi
     * option interaction element
     * @param {documentFragment} node Document Object containing input elements for user
     * to interact with
     * @param {string} id  id of the interaction element whose option the user clicked
     * on
     */
    value: function addOptionListeners(node, id) {
      var _this2 = this;

      var options = node.querySelectorAll("div[data-option-id=\"".concat(id, "\"]"));

      if (options.length) {
        options.forEach(function (option) {
          return option.onclick = _this2.handleOptionSelection;
        });
      }
    }
    /**
     * Handle user option selection, add check icon on the select option
     * @param {mouseEvent} event the clicked event
     */

  }, {
    key: "addAnswer",

    /**
     * add users answer to a question to the list of answers
     * @param {string} questionId
     * @param {string} answerText text entered by user or file url from s3 bucket
     */
    value: function addAnswer(questionId, answerText, parentID) {
      // const question = this.questions.find(el => el.id === questionId);
      var question = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].questionFinder(this.questions, questionId);
      var answer = {
        questionType: question.type,
        question: question.name,
        questionId: question.id,
        answer: answerText
      };
      var existingIndex = this.answers.findIndex(function (res) {
        return res.question === answer.question;
      });

      if (existingIndex >= 0) {
        this.answers[existingIndex] = answer;
      } else {
        this.answers.push(answer);
      }
    }
    /**
     * remove users answer to a question from the list of answers
     * @param {string} questionId
     */

  }, {
    key: "removeAnswer",
    value: function removeAnswer(questionId) {
      // const question = this.questions.find(el => el.id === questionId);
      var question = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].questionFinder(this.questions, questionId); // const existingIndex = this.answers.findIndex(
      //   res => res.question === answer.question
      // );
      // if (existingIndex >= 0) {
      //   this.answers[existingIndex] = answer;
      // } else {
      //   this.answers.push(answer);
      // }
    }
    /**
     * Show/Hid UI with error messages for a question with wrong answer
     * @param {string} questionId id of question whose answer failed validation
     * @param {array} messages messages to display to user
     */

  }, {
    key: "toggleErrorUIFor",
    value: function toggleErrorUIFor(questionId) {
      var messages = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
      var msgContainer = this.pageView.querySelector("[data-validation-msg-for=\"".concat(questionId, "\"]"));

      if (messages.length) {
        msgContainer.classList.remove("inactiveValidation");
        var msgString = _templates_response_canvas_validation_msg_hbs__WEBPACK_IMPORTED_MODULE_2___default()({
          messages: messages
        });
        var oldNode = msgContainer.firstChild;
        var msgNode = Object(_utils__WEBPACK_IMPORTED_MODULE_11__["parseHtml"])(msgString);
        msgContainer.replaceChild(msgNode, oldNode);
      } else {
        if (!msgContainer.classList.contains("inactiveValidation")) {
          msgContainer.classList.add("inactiveValidation");
        }
      }
    }
    /**
     * Show error message to user if any question's  answer does not pass all validation rules
     * @param {bool} haveError
     */

  }, {
    key: "toggleSubmitErrorUI",
    value: function toggleSubmitErrorUI(haveError) {
      var msgContainer = this.pageView.querySelector("#submit-error-msg");

      if (haveError) {
        msgContainer.classList.remove("inactiveValidation");
        var messages = ["Your answers failed validation!", "Please scroll up and correct your answers"];
        var msgString = _templates_response_canvas_validation_msg_hbs__WEBPACK_IMPORTED_MODULE_2___default()({
          messages: messages
        });
        var oldNode = msgContainer.firstChild;
        var msgNode = Object(_utils__WEBPACK_IMPORTED_MODULE_11__["parseHtml"])(msgString);
        msgContainer.replaceChild(msgNode, oldNode);
      } else {
        if (!msgContainer.classList.contains("inactiveValidation")) {
          msgContainer.classList.add("inactiveValidation");
        }
      }
    }
    /**
     * update the section information on the canvas header
     * @param {number} currentQuestionId
     */

  }, {
    key: "updateHeader",
    value: function updateHeader(currentQuestionId) {
      var sectionData = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].getNextSectionData(this.formData.elements, currentQuestionId);

      if (sectionData) {
        var headerString = _templates_response_canvas_header_hbs__WEBPACK_IMPORTED_MODULE_5___default()(sectionData);
        var newHeaderNode = Object(_utils__WEBPACK_IMPORTED_MODULE_11__["parseHtml"])(headerString);
        var oldHeaderNode = this.pageView.querySelector("section.canvas__headerSection");
        this.pageView.replaceChild(newHeaderNode, oldHeaderNode);
      }
    }
    /**
     * Replace old copy of footer node with a new copy that
     * reflect the current state of the application
     */

  }, {
    key: "updateFooter",
    value: function updateFooter() {
      this.toggleFooterForMobile(false);
      var total = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].getTotalQuestions(this.questions);
      var totalAnswered = this.answers.length;
      var footerString = _templates_response_canvas_footer_hbs__WEBPACK_IMPORTED_MODULE_6___default()({
        total: total,
        totalAnswered: totalAnswered
      });
      var newFooterNode = Object(_utils__WEBPACK_IMPORTED_MODULE_11__["parseHtml"])(footerString);
      this.addNavigationListeners(newFooterNode);
      var oldFooterNode = this.pageView.querySelector("section.canvas__footerSection");
      this.pageView.replaceChild(newFooterNode, oldFooterNode);
    }
    /**
     * Register click event handler on navigation button
     *  @param {documentFragment} node Document Object containing input elements for user
     * to interact with
     */

  }, {
    key: "addNavigationListeners",
    value: function addNavigationListeners(node) {
      var _this3 = this;

      var upBtn = node.querySelector("button[id=\"navigateUP\"]");
      var downBtn = node.querySelector("button[id=\"navigateDown\"]");

      if (upBtn && downBtn) {
        upBtn.onclick = function () {
          var questionUI = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].getCurrentQuestionUI();

          _this3.goToNextQuestion(questionUI.dataset.qId, "up");
        };

        downBtn.onclick = function () {
          var questionUI = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].getCurrentQuestionUI();

          _this3.goToNextQuestion(questionUI.dataset.qId, "down");
        };
      }
    }
    /**
     * Bring a new question into user's focus
     * @param {string} currentQuestionId id of the question user has just answered
     * @param direction direction to scroll to get to the next question
     */

  }, {
    key: "updateFocus",

    /**
     * Put focus on the next question for user to answer
     * @param {string} questionId id of the question a user just finished answering
     */
    value: function updateFocus(questionId) {
      var firstQuestion = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      var nextQuestion = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].getNextQuestion(this.questions, questionId);
      if (!nextQuestion && firstQuestion === false) return;
      var id = questionId;

      if (!firstQuestion) {
        id = nextQuestion.id;
      }

      if (_page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].compactTypes().includes(nextQuestion.type)) {
        var firstNextCompact = nextQuestion.children.find(function (elem, index) {
          return index === 0;
        });
        id = firstNextCompact.id;
      }

      var input = this.pageView.querySelector(".input[data-q-id=\"".concat(id, "\"]"));

      if (input) {
        input.focus();
        this.toggleFooterForMobile(true);
      }
    }
    /**
     * handle uploading static assets like passport and signature
     */

  }, {
    key: "toggleFooterForMobile",

    /**
     * show/hid progress on the footer depending on the kind of question
     * a user is answering while using a mobile phone
     * @param {Boolean} show
     */
    value: function toggleFooterForMobile() {
      var show = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
      var footer = this.pageView.querySelector("section.canvas__footerSection");

      if (show && !footer.classList.contains("des-content")) {
        footer.classList.add("des-content");
      }

      if (!show && footer.classList.contains("des-content")) {
        footer.classList.remove("des-content");
      }
    }
    /**
     * Observes when users click the ok button on each input based interaction elements
     * @param {MouseEvent} event
     * to signify that they have finished answering the question
     */

  }, {
    key: "getAnswers",

    /**
     * return all the answer given by a user
     */
    value: function getAnswers() {
      return this.answers;
    }
    /**
     * return all questions  including the ones within a compact question all flattened  - Ezugudor
     */

  }, {
    key: "flattenedQuestions",
    value: function flattenedQuestions() {
      var questions = [];
      this.questions.forEach(function (question) {
        if (_page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].compactTypes().includes(question.type)) {
          question.children.forEach(function (cElem) {
            questions.push(cElem);
          });
        } else {
          questions.push(question);
        }
      });
      return questions;
    }
    /**
     * return all unanswered questions by a user - Ezugudor
     */

  }, {
    key: "getUnAnswered",
    value: function getUnAnswered() {
      var _this4 = this;

      var arr = this.flattenedQuestions().filter(function (question) {
        return !_this4.answers.map(function (answer) {
          return answer.questionId;
        }).includes(question.id);
      });
      var result = arr.sort(function (a, b) {
        return a.position - b.position;
      });
      return result;
    }
  }]);

  return EventHandler;
}();

/***/ }),

/***/ "./src/public/js/response-canvas/page-ui-builder.js":
/*!**********************************************************!*\
  !*** ./src/public/js/response-canvas/page-ui-builder.js ***!
  \**********************************************************/
/*! exports provided: PageUIBuilder */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PageUIBuilder", function() { return PageUIBuilder; });
/* harmony import */ var _templates_response_canvas_multi_choice_question_hbs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../templates/response-canvas/multi-choice-question.hbs */ "./src/public/js/templates/response-canvas/multi-choice-question.hbs");
/* harmony import */ var _templates_response_canvas_multi_choice_question_hbs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_multi_choice_question_hbs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _templates_response_canvas_upload_indicator_hbs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../templates/response-canvas/upload-indicator.hbs */ "./src/public/js/templates/response-canvas/upload-indicator.hbs");
/* harmony import */ var _templates_response_canvas_upload_indicator_hbs__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_upload_indicator_hbs__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _templates_response_canvas_media_permission_hbs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../templates/response-canvas/media-permission.hbs */ "./src/public/js/templates/response-canvas/media-permission.hbs");
/* harmony import */ var _templates_response_canvas_media_permission_hbs__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_media_permission_hbs__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _templates_response_canvas_dropdown_question_hbs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../templates/response-canvas/dropdown-question.hbs */ "./src/public/js/templates/response-canvas/dropdown-question.hbs");
/* harmony import */ var _templates_response_canvas_dropdown_question_hbs__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_dropdown_question_hbs__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _templates_response_canvas_compact_question_header_hbs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../templates/response-canvas/compact-question-header.hbs */ "./src/public/js/templates/response-canvas/compact-question-header.hbs");
/* harmony import */ var _templates_response_canvas_compact_question_header_hbs__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_compact_question_header_hbs__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _templates_response_canvas_simple_question_hbs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../templates/response-canvas/simple-question.hbs */ "./src/public/js/templates/response-canvas/simple-question.hbs");
/* harmony import */ var _templates_response_canvas_simple_question_hbs__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_simple_question_hbs__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _templates_response_canvas_statement_hbs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../templates/response-canvas/statement.hbs */ "./src/public/js/templates/response-canvas/statement.hbs");
/* harmony import */ var _templates_response_canvas_statement_hbs__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_statement_hbs__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _templates_response_canvas_long_question_hbs__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../templates/response-canvas/long-question.hbs */ "./src/public/js/templates/response-canvas/long-question.hbs");
/* harmony import */ var _templates_response_canvas_long_question_hbs__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_long_question_hbs__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _templates_response_canvas_picture_hbs__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../templates/response-canvas/picture.hbs */ "./src/public/js/templates/response-canvas/picture.hbs");
/* harmony import */ var _templates_response_canvas_picture_hbs__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_picture_hbs__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _page_utils__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./page-utils */ "./src/public/js/response-canvas/page-utils.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }











/**
 * transform form data into interactive UI
 */

var PageUIBuilder = function PageUIBuilder() {
  _classCallCheck(this, PageUIBuilder);
};
/**
 * Get the right input type property
 * @param {string} el
 */

_defineProperty(PageUIBuilder, "buildUIFor", function (questionData) {
  var parentID = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  var validationRules = questionData.validationRules;
  var option = {
    isRequired: confirmRequirement(validationRules),
    placeholder: generatePlaceholder(questionData),
    type: generateType(questionData),
    description: questionData.description,
    position: questionData.qPosition,
    children: questionData.children,
    question: questionData.name,
    uploadIndicator: "",
    id: questionData.id,
    optionsString: "",
    compact: false
  };
  var options = null;

  if (questionData.isCompact) {
    option.compact = true;
  }

  switch (questionData.type) {
    case "introduction":
      return null;

    case "section":
      return null;

    case "statement":
      return _templates_response_canvas_statement_hbs__WEBPACK_IMPORTED_MODULE_6___default()(option);

    case "picture":
      option.text = "Turn on Camera";
      return _templates_response_canvas_media_permission_hbs__WEBPACK_IMPORTED_MODULE_2___default()(option);

    case "video":
      option.text = "Start Recording";
      return _templates_response_canvas_media_permission_hbs__WEBPACK_IMPORTED_MODULE_2___default()(option);

    case "signature":
    case "passport":
      option.labelText = "Or Click to upload";
      option.uploadIndicator = _templates_response_canvas_upload_indicator_hbs__WEBPACK_IMPORTED_MODULE_1___default()(option);
      return _templates_response_canvas_picture_hbs__WEBPACK_IMPORTED_MODULE_8___default()(option);

    case "dob":
    case "date":
      option["class"] = "date-element";
      return _templates_response_canvas_simple_question_hbs__WEBPACK_IMPORTED_MODULE_5___default()(option);

    case "multichoice":
      options = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].buildOptionFromArray(option.children);
      option.optionsString = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].buildOptionTemplate(options, option.id, option.type);
      return _templates_response_canvas_multi_choice_question_hbs__WEBPACK_IMPORTED_MODULE_0___default()(option);

    case "dropdown":
      if (!questionData.isCompact) {
        options = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].buildOptionFromArray(option.children);
        option.optionsString = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].buildOptionTemplate(options, option.id, option.type);
        return _templates_response_canvas_dropdown_question_hbs__WEBPACK_IMPORTED_MODULE_3___default()(option);
      } else {
        option.controlType = questionData.controlType;
        option.parent = parentID; // check the type of dropdown, if state,branch,lga,branch name etc and auto fill as needed

        if (questionData.controlType == "state") {
          //auto generate all the states
          options = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].buildOptionFromArray(_page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].getStates());
          option.optionsString = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].buildOptionTemplate(options, option.id, option.type, questionData.controlType, parentID);
          return _templates_response_canvas_dropdown_question_hbs__WEBPACK_IMPORTED_MODULE_3___default()(option);
        } else if (questionData.controlType == "branch") {// auto generate all the branch in this business account
        } else if (questionData.controlType == "lga") {
          // this will vary depending on the selected state
          //auto generate all the states
          options = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].buildOptionFromArray(_page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].getDefaultStateLGA());
          option.optionsString = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].buildOptionTemplate(options, option.id, option.type, questionData.controlType, parentID);
          return _templates_response_canvas_dropdown_question_hbs__WEBPACK_IMPORTED_MODULE_3___default()(option);
        }

        return _templates_response_canvas_dropdown_question_hbs__WEBPACK_IMPORTED_MODULE_3___default()(option);
      }

    case "address":
    case "branch": // let subQns = '<div class="compactContainer" style="background:blue">';
    //           subQns += questionData.children.map(child => {
    //             return this.buildUIFor(child);
    //           }).join("");
    //    subQns+= "</div>";
    // return compactHeaderTemplate(option) + subQns;

    case "creditcards":
      options = generateCardOptions();
      option.optionsString = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].buildOptionTemplate(options, option.id, option.type);
      return _templates_response_canvas_multi_choice_question_hbs__WEBPACK_IMPORTED_MODULE_0___default()(option);

    case "yesorno":
      options = generateYesOrNoOptions();
      option.optionsString = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].buildOptionTemplate(options, option.id, option.type);
      return _templates_response_canvas_multi_choice_question_hbs__WEBPACK_IMPORTED_MODULE_0___default()(option);

    case "gender":
      options = generateGenderOptions();
      option.optionsString = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].buildOptionTemplate(options, option.id, option.type);
      return _templates_response_canvas_multi_choice_question_hbs__WEBPACK_IMPORTED_MODULE_0___default()(option);

    case "longtext":
      return _templates_response_canvas_long_question_hbs__WEBPACK_IMPORTED_MODULE_7___default()(option);

    default:
      return _templates_response_canvas_simple_question_hbs__WEBPACK_IMPORTED_MODULE_5___default()(option);
  }
});

var generateType = function generateType(el) {
  switch (el.type) {
    case "shorttext":
    case "firstname":
    case "lastname":
    case "address":
      return "text";

    case "date":
    case "dob":
      // return "date";
      return "text";

    case "tel":
      return "tel";

    case "email":
      return "email";

    case "mobile":
    case "tel":
    case "bvn":
      return "number";

    default:
      return el.type;
  }
};
/**
 * Return the right input placeholver property
 * @param {obje3ct} el
 */


var generatePlaceholder = function generatePlaceholder(el) {
  switch (el.type) {
    case "address":
      return "Like 16 Karimu Ikotun VI, Lagos";

    case "mobile":
      return "Like 08030659872";

    case "tel":
      return "Like 01729011";

    case "email":
      return "Like jendoe@cool.com";

    case "bvn":
      return "Like 22123803000";

    case "dob":
      return "DD/MM/YYYY e.g 09/12/1982";

    case "date":
      return "DD/MM/YYYY e.g 09/12/1982";

    case "dropdown":
      return "Start typing to filter options";

    default:
      return "Enter Your Answer Here";
  }
};
/**
 * return array of options to display for card questions
 */


var generateCardOptions = function generateCardOptions() {
  return [{
    label: "A",
    text: "Master"
  }, {
    label: "B",
    text: "Valve"
  }, {
    label: "C",
    text: "Visa"
  }];
};
/**
 * return array of options to display for yes/no questions
 */


var generateYesOrNoOptions = function generateYesOrNoOptions() {
  return [{
    label: "Y",
    text: "Yes"
  }, {
    label: "N",
    text: "No"
  }];
};
/**
 * return array of options to display for yes/no questions
 */


var generateGenderOptions = function generateGenderOptions() {
  return [{
    label: "M",
    text: "Male"
  }, {
    label: "F",
    text: "Female"
  }];
};
/**
 * check if a question has required as part of its validation rules
 * @param {object} validationRules
 */


var confirmRequirement = function confirmRequirement(validationRules) {
  if (!validationRules) return;
  var requiredRule = validationRules.find(function (rule) {
    return rule.name === "required";
  });
  return requiredRule !== undefined;
};

/***/ }),

/***/ "./src/public/js/response-canvas/page-utils.js":
/*!*****************************************************!*\
  !*** ./src/public/js/response-canvas/page-utils.js ***!
  \*****************************************************/
/*! exports provided: PageUtils */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PageUtils", function() { return PageUtils; });
/* harmony import */ var _templates_response_canvas_dropdown_options_hbs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../templates/response-canvas/dropdown-options.hbs */ "./src/public/js/templates/response-canvas/dropdown-options.hbs");
/* harmony import */ var _templates_response_canvas_dropdown_options_hbs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_dropdown_options_hbs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _templates_response_canvas_option_hbs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../templates/response-canvas/option.hbs */ "./src/public/js/templates/response-canvas/option.hbs");
/* harmony import */ var _templates_response_canvas_option_hbs__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_option_hbs__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _utils_index__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../utils/index */ "./src/public/js/utils/index.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }




/**
 * used to for building label for question with options
 */

var alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
var PageUtils =
/*#__PURE__*/
function () {
  function PageUtils() {
    _classCallCheck(this, PageUtils);
  }

  _createClass(PageUtils, null, [{
    key: "getQuestions",

    /**
     * Extract out form element that need users interaction
     * @param {array} formInputs form elements
     */
    value: function getQuestions(formInputs) {
      return formInputs.filter(function (element) {
        return element.type !== "section" && element.type !== "introduction";
      }).map(function (question, index) {
        question.qPosition = index + 1;
        return question;
      });
    }
  }, {
    key: "containsIntro",

    /**
     * check if a form has intro section
     * @param {*} formInputs form elements
     */
    value: function containsIntro(formInputs) {
      var intro = formInputs.find(function (el) {
        return el.type === "introduction";
      });
      if (intro) return true;
      return false;
    }
  }, {
    key: "compactTypes",
    value: function compactTypes() {
      return ["address", "branch"];
    }
    /**
     * pick the ids of all form elements that needs user's interaction
     * @param {*} questions
     */

  }, {
    key: "pickIds",
    value: function pickIds(questions) {
      var _this = this;

      // console.log(this.compactTypes)
      var allIDs = [];

      var name = function name(questions) {
        questions.forEach(function (el) {
          if (_this.compactTypes().includes(el.type)) {
            name(el.children);
          } else {
            allIDs.push(el.id);
          }
        });
      };

      name(questions);
      return allIDs;
    }
    /**
     * get the intro section of a form
     * @param {*} formInputs
     */

  }, {
    key: "getIntroData",
    value: function getIntroData(formInputs) {
      return formInputs.find(function (el) {
        return el.type === "introduction";
      });
    }
    /**
     * extract the first section data to display on the page header
     */

  }, {
    key: "getFirstSection",
    value: function getFirstSection(formQuestions) {
      return formQuestions.slice(0, 3).find(function (question) {
        return question.type === "section";
      });
    }
    /**
     * Extract the next section data to display on the page header
     *  @param {*} formInputs
     */

  }, {
    key: "getNextSection",
    value: function getNextSection(formQuestions, currentQuestionId) {
      var currentQuestionIndex = formQuestions.findIndex(function (question) {
        return question.id === currentQuestionId;
      });
      var nextQuestion = formQuestions[currentQuestionIndex + 1];
      return (nextQuestion && nextQuestion.type) === "section" ? nextQuestion : null;
    }
    /**
     * return the parent node for document fragment acting as a question option
     * that recieved a click event
     * @param {DocumentNode} node
     */

  }, {
    key: "getRootNode",
    value: function getRootNode(node) {
      while (node.dataset.optionId === undefined && node.nodeName !== "SECTION") {
        node = node.parentElement;
      }

      return node;
    }
    /**
     * transform array of text to arry of option object
     * @param {array} array array with text to be transformed into option object
     * @param {string} filterText value by which array element would be filted
     */

  }, {
    key: "buildOptionFromArray",
    value: function buildOptionFromArray(array) {
      var filterText = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      var controlType = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

      if (filterText) {
        array = array.filter(function (item) {
          if (controlType) {
            return item.name.toLowerCase().indexOf(filterText.toLowerCase()) !== -1;
          } else {
            return item.toLowerCase().indexOf(filterText.toLowerCase()) !== -1;
          }
        });
      }

      return array.map(function (el, index) {
        return {
          label: alphabet[index],
          text: el
        };
      });
    }
    /**
     * transform array of text to arry of option object
     * @param {array} array array with text to be transformed into option object
     * @param {string} filterText value by which array element would be filted
     */

  }, {
    key: "buildStateOptionFromArray",
    value: function buildStateOptionFromArray(array) {
      var filterText = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

      if (filterText) {
        array = array.filter(function (item) {
          return item.stateName.toLowerCase().indexOf(filterText.toLowerCase()) !== -1;
        });
      }

      return array.map(function (el, index) {
        return {
          label: alphabet[index],
          text: el
        };
      });
    }
    /**
     * Build UI for each multi choice question's option
     * @param {array} options option object
     * @param {string} questionId id of question
     * @param {boolean} questionType type of question e.g dropdown/card etc.
     */

  }, {
    key: "buildStateOptionTemplate",
    value: function buildStateOptionTemplate() {
      var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
      var questionId = arguments.length > 1 ? arguments[1] : undefined;
      var questionType = arguments.length > 2 ? arguments[2] : undefined;

      // IF = no need for labels
      if (questionType === "dropdown" || questionType === "branch" || questionType === "state" || questionType === "country" || questionType === "lga") {
        return options.map(function (_ref) {
          var text = _ref.text;
          return _templates_response_canvas_dropdown_options_hbs__WEBPACK_IMPORTED_MODULE_0___default()({
            text: text,
            id: questionId
          });
        }).join("");
      }

      return options.map(function (_ref2) {
        var label = _ref2.label,
            text = _ref2.text;
        return _templates_response_canvas_option_hbs__WEBPACK_IMPORTED_MODULE_1___default()({
          label: label,
          text: text,
          id: questionId
        });
      }).join("");
    }
    /**
     * Transform the data in a branch list to option objects
     * @param {Array} branches list of a bank branch
     * @param {string} filterText value by which array element would be filted
     */

  }, {
    key: "buildOptionsFromBranches",
    value: function buildOptionsFromBranches(branches) {
      var filterText = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      branches = branches.map(function (branch) {
        return "".concat(branch.name, " | ").concat(branch.address);
      });

      if (filterText) {
        branches = branches.filter(function (branch) {
          return branch.toLowerCase().indexOf(filterText.toLowerCase()) !== -1;
        });
      }

      return branches.map(function (text, index) {
        return {
          label: alphabet[index],
          text: text
        };
      });
    }
  }, {
    key: "stripStates",
    value: function stripStates(str) {
      return str == "FCT" ? str : str.substring(0, str.length - 5);
    }
    /**
     * Build UI for each multi choice question's option
     * @param {array} options option object
     * @param {string} questionId id of question
     * @param {boolean} questionType type of question e.g dropdown/card etc.
     */

  }, {
    key: "buildOptionTemplate",
    value: function buildOptionTemplate() {
      var _this2 = this;

      var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
      var questionId = arguments.length > 1 ? arguments[1] : undefined;
      var questionType = arguments.length > 2 ? arguments[2] : undefined;
      var dropdownType = arguments.length > 3 ? arguments[3] : undefined;
      var parentID = arguments.length > 4 ? arguments[4] : undefined;

      // IF = no need for labels
      if (questionType === "dropdown" || questionType === "branch" || questionType === "country" || questionType === "lga") {
        if (dropdownType == "state") {
          return options.map(function (_ref3) {
            var text = _ref3.text;
            return _templates_response_canvas_dropdown_options_hbs__WEBPACK_IMPORTED_MODULE_0___default()({
              text: _this2.stripStates(text.name),
              resourceId: text.id,
              id: questionId,
              parent: parentID,
              dropdownType: dropdownType
            });
          }).join("");
        } else if (dropdownType == "lga") {
          return options.map(function (_ref4) {
            var text = _ref4.text;
            return _templates_response_canvas_dropdown_options_hbs__WEBPACK_IMPORTED_MODULE_0___default()({
              text: text.name,
              resourceId: text.id,
              id: questionId,
              parent: parentID,
              dropdownType: dropdownType
            });
          }).join("");
        } else {
          return options.map(function (_ref5) {
            var text = _ref5.text;
            return _templates_response_canvas_dropdown_options_hbs__WEBPACK_IMPORTED_MODULE_0___default()({
              text: text,
              id: questionId,
              parent: parentID
            });
          }).join("");
        }
      }

      return options.map(function (_ref6) {
        var label = _ref6.label,
            text = _ref6.text;
        return _templates_response_canvas_option_hbs__WEBPACK_IMPORTED_MODULE_1___default()({
          label: label,
          text: text,
          id: questionId
        });
      }).join("");
    }
    /**
     * Return the next data to be be displayed on the page header
     */

  }, {
    key: "getNextSectionData",
    value: function getNextSectionData(formQuestions, currentQuestionId) {
      var currentQuestionIndex = formQuestions.findIndex(function (question) {
        return question.id === currentQuestionId;
      });
      var nextQuestion = formQuestions[currentQuestionIndex + 1];
      return (nextQuestion && nextQuestion.type) === "section" ? nextQuestion : null;
    }
    /**
     * return the next question in the series
     * @param {array} formQuestions
     * @param {string} currentQuestionId
     */

  }, {
    key: "getNextQuestion",
    value: function getNextQuestion(formQuestions, currentQuestionId) {
      var _this3 = this;

      var nextQuestion = {};
      formQuestions.forEach(function (question, index) {
        if (_this3.compactTypes().includes(question.type)) {
          question.children.forEach(function (cElem, cIndex) {
            if (cElem.id === currentQuestionId) {
              nextQuestion = question.children[cIndex + 1];
            }
          });
        } else {
          if (question.id === currentQuestionId) {
            nextQuestion = formQuestions[index + 1];
          }
        }
      });
      return nextQuestion;
    }
    /**
     * get the distance between the top of the canvas to an element
     * @param {DocumentNode} elem
     */

  }, {
    key: "getDistanceToTop",
    value: function getDistanceToTop(elem) {
      var distance = 0;

      if (elem.offsetParent) {
        do {
          distance += elem.offsetTop;
          elem = elem.offsetParent;
        } while (elem);
      }

      return distance < 0 ? 0 : distance;
    }
    /**
     * get the branch a user want their answers sent to
     * @param {array} answers all the answers a user has given
     */

  }, {
    key: "getBranchData",
    value: function getBranchData(answers) {
      var branch = answers.find(function (answer) {
        return answer.questionType === "branch";
      });
      return branch ? removeAddresspart(branch.answer) : "HQ";
    }
    /**
     * return the node of the current question a user is answering
     */

  }, {
    key: "getCurrentQuestionUI",
    value: function getCurrentQuestionUI() {
      return Array.from(document.querySelectorAll(".elementWrapper")).find(function (node) {
        return !node.classList.contains("dull");
      });
    }
    /**
     * since we are not collect users data this is our default form respondant value for now
     */

  }, {
    key: "getDefaultRespondant",
    value: function getDefaultRespondant() {
      return {
        id: "5b51e1706fa9c80029476871",
        firstname: "Ezugudor",
        lastname: "Okpara",
        phone: "08030659872"
      };
    }
  }]);

  return PageUtils;
}();

_defineProperty(PageUtils, "getStates", function () {
  return _utils_index__WEBPACK_IMPORTED_MODULE_2__["countryStates"].map(function (state) {
    return {
      name: state.state.name,
      id: state.state.id
    };
  });
});

_defineProperty(PageUtils, "getDefaultStateLGA", function () {
  return _utils_index__WEBPACK_IMPORTED_MODULE_2__["countryStates"][0].state.locals;
});

_defineProperty(PageUtils, "getTotalQuestions", function (questions) {
  var total = 0;
  questions.forEach(function (question) {
    if (question.type !== "statement") {
      if (PageUtils.compactTypes().includes(question.type)) {
        total += question.children.length;
      } else {
        total++;
      }
    }
  });
  return total;
});

_defineProperty(PageUtils, "questionFinder", function (questions, questionId) {
  return questions.map(function (elem) {
    if (PageUtils.compactTypes().includes(elem.type)) {
      return elem.children.find(function (cElem) {
        if (cElem.id == questionId) return cElem;
      });
    } else {
      if (elem.id === questionId) return elem;
    }
  }).find(function (e) {
    return e !== undefined;
  });
});

_defineProperty(PageUtils, "getSelectedStateLGA", function (stateID) {
  return _utils_index__WEBPACK_IMPORTED_MODULE_2__["countryStates"].find(function (state) {
    return state.state.id == stateID;
  }).state.locals;
});

var removeAddresspart = function removeAddresspart(branchAnswer) {
  var index = branchAnswer.indexOf(" ");
  return branchAnswer.substring(0, index);
};

/***/ }),

/***/ "./src/public/js/response-canvas/response-canvas.js":
/*!**********************************************************!*\
  !*** ./src/public/js/response-canvas/response-canvas.js ***!
  \**********************************************************/
/*! exports provided: ResponseCanvas */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResponseCanvas", function() { return ResponseCanvas; });
/* harmony import */ var _templates_response_canvas_submit_hbs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../templates/response-canvas/submit.hbs */ "./src/public/js/templates/response-canvas/submit.hbs");
/* harmony import */ var _templates_response_canvas_submit_hbs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_submit_hbs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _templates_response_canvas_header_hbs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../templates/response-canvas/header.hbs */ "./src/public/js/templates/response-canvas/header.hbs");
/* harmony import */ var _templates_response_canvas_header_hbs__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_header_hbs__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _templates_response_canvas_footer_hbs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../templates/response-canvas/footer.hbs */ "./src/public/js/templates/response-canvas/footer.hbs");
/* harmony import */ var _templates_response_canvas_footer_hbs__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_footer_hbs__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _templates_response_canvas_question_only_hbs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../templates/response-canvas/question-only.hbs */ "./src/public/js/templates/response-canvas/question-only.hbs");
/* harmony import */ var _templates_response_canvas_question_only_hbs__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_question_only_hbs__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _templates_response_canvas_canvas_hbs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../templates/response-canvas/canvas.hbs */ "./src/public/js/templates/response-canvas/canvas.hbs");
/* harmony import */ var _templates_response_canvas_canvas_hbs__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_canvas_hbs__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _templates_response_canvas_new_intro_hbs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../templates/response-canvas/new-intro.hbs */ "./src/public/js/templates/response-canvas/new-intro.hbs");
/* harmony import */ var _templates_response_canvas_new_intro_hbs__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_new_intro_hbs__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../utils */ "./src/public/js/utils/index.js");
/* harmony import */ var _page_event_handlers__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./page-event-handlers */ "./src/public/js/response-canvas/page-event-handlers.js");
/* harmony import */ var _page_ui_builder__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./page-ui-builder */ "./src/public/js/response-canvas/page-ui-builder.js");
/* harmony import */ var _core_response_canvas__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../core/response-canvas */ "./src/public/js/core/response-canvas/index.js");
/* harmony import */ var _core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../core */ "./src/public/js/core/index.js");
/* harmony import */ var _page_utils__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./page-utils */ "./src/public/js/response-canvas/page-utils.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }













/**
 * Manages the response page where user are given oppurtunity to provide data
 * to banks by interacting with various UI elements
 */

var ResponseCanvas =
/*#__PURE__*/
function () {
  function ResponseCanvas(_container) {
    var _this = this;

    _classCallCheck(this, ResponseCanvas);

    _defineProperty(this, "confirmRequirement", function (validationRules) {
      var requiredRule = validationRules.find(function (rule) {
        return rule.name === "required";
      });
      return requiredRule !== undefined;
    });

    _defineProperty(this, "handleGettingSetarted", function () {
      _this.startFormFilling = true;

      _this.presentView();
    });

    _defineProperty(this, "targetEnterCanvas", function (res) {
      var element = res.element;
      var container = element.querySelector("section");
      element.classList.remove("dull");
      container.classList.remove("inactiveElement");
      container.classList.add("activeElement");
    });

    _defineProperty(this, "targetExitCanvas", function (res) {
      var element = res.element;
      var container = element.querySelector("section");
      element.classList.add("dull");
      container.classList.remove("activeElement");
      container.classList.add("inactiveElement");
    });

    _defineProperty(this, "processResponse", function () {
      var haveError = false;

      var answers = _this.eventHandler.getAnswers();

      _this.questions.forEach(function (question) {
        if (_page_utils__WEBPACK_IMPORTED_MODULE_11__["PageUtils"].compactTypes().includes(question.type)) {
          question.children.forEach(function (cElem) {
            var response = answers.find(function (r) {
              return r.questionId === cElem.id;
            });
            var answer = response ? response.answer : "";

            var notValid = _this.eventHandler.validateAnswer(answer, cElem.id);

            if (notValid) haveError = true;
          });
        } else {
          var response = answers.find(function (r) {
            return r.questionId === question.id;
          });
          var answer = response ? response.answer : "";

          var notValid = _this.eventHandler.validateAnswer(answer, question.id);

          if (notValid) haveError = true;
        }
      });

      if (!haveError) {
        var params = {
          user: _page_utils__WEBPACK_IMPORTED_MODULE_11__["PageUtils"].getDefaultRespondant(),
          branch: _page_utils__WEBPACK_IMPORTED_MODULE_11__["PageUtils"].getBranchData(answers),
          content: answers,
          form: _this.formData.id,
          shortId: _this.formData.shortId
        };
        _core__WEBPACK_IMPORTED_MODULE_10__["Http"].post("responses", params).then(function (data) {
          var _this$container$datas = _this.container.dataset,
              formslug = _this$container$datas.formslug,
              bankslug = _this$container$datas.bankslug;
          location.replace("".concat(location.origin, "/thankyou?bank=").concat(bankslug, "&form=").concat(formslug));
        })["catch"](function (err) {
          alert("Sorry we are experiencing some issues handling your response");
        });
      } else {
        _this.eventHandler.correctionMode = true;

        _this.eventHandler.toggleSubmitErrorUI(haveError);

        _this.eventHandler.goToErrorQuestion();
      }
    });

    this.startFormFilling = false;
    this.container = _container; // generate the business color hues and store for themeing

    var _bankslug = this.container.dataset.bankslug;
    this.info = _core__WEBPACK_IMPORTED_MODULE_10__["DataManager"].findBank(_bankslug);
    Object(_utils__WEBPACK_IMPORTED_MODULE_6__["themeMaker"])(this.info.color);
    this.fetchForm().then(function (form) {
      _this.questions = _page_utils__WEBPACK_IMPORTED_MODULE_11__["PageUtils"].getQuestions(form.elements);
      _this.eventHandler = new _page_event_handlers__WEBPACK_IMPORTED_MODULE_7__["EventHandler"](_container, _this.questions, form);
      _this.formData = form;

      _this.presentView();

      _this._container = _container; // this.logoMbi = document.querySelector("#banklogo-mobi");

      _this.logoDes = document.querySelector("#banklogo-des");
      _this.introTitle = document.querySelector(".bank-name");
      var headingText = "".concat(_this.info.name, " Forms"); // this.logoMbi.src = this.info.logo;

      if (_this.logoDes) {
        _this.logoDes.src = _this.info.logo;
      }

      if (_this.introTitle) {
        _this.introTitle.innerHTML = headingText;
      }
    });
  }
  /**
   * fetch form created by the bank either locally or from the server
   */


  _createClass(ResponseCanvas, [{
    key: "fetchForm",
    value: function fetchForm() {
      // Todo ***********************************************************************
      // need to investigate with there is an error when data is not on local storage
      var formslug = this.container.dataset.formslug;
      var localFormData = _core__WEBPACK_IMPORTED_MODULE_10__["DataManager"].findForm(formslug);

      if (localFormData) {
        return Promise.resolve(localFormData);
      }

      return this.fetchRemotely();
    }
    /**
     * Go to back end service to fetch form data as it is not available locally
     */

  }, {
    key: "fetchRemotely",
    value: function fetchRemotely() {
      var data = this.container.dataset;
      var bankslug = data.bankslug,
          formslug = data.formslug;
      var parent = Object(_utils__WEBPACK_IMPORTED_MODULE_6__["capitalizeFirstLatter"])(data.formtypeparent);
      var formType = Object(_utils__WEBPACK_IMPORTED_MODULE_6__["capitalizeFirstLatter"])(data.formtype);
      var url = "forms/".concat(bankslug, "/").concat(parent, "/").concat(formType, "/").concat(formslug);
      return _core__WEBPACK_IMPORTED_MODULE_10__["Http"].get(url)["catch"](function (err) {
        alert("Sorry we are experiencing issues fetching content from our servers");
      });
    }
    /**
     * Ansamble various ui element and add them to the document at once
     */

  }, {
    key: "presentView",
    value: function presentView() {
      var nodeString = "";
      var canvasReady = false;

      if (!this.startFormFilling && _page_utils__WEBPACK_IMPORTED_MODULE_11__["PageUtils"].containsIntro(this.formData.elements)) {
        nodeString += this.buildIntroUI();
      } else {
        nodeString += this.buildCanvasUI();
        canvasReady = true;
      }

      var questionIDs = _page_utils__WEBPACK_IMPORTED_MODULE_11__["PageUtils"].pickIds(this.questions);
      var node = Object(_utils__WEBPACK_IMPORTED_MODULE_6__["parseHtml"])(nodeString);
      this.addEventListeners(node, questionIDs);
      this.container.replaceChild(node, this.container.firstChild);

      if (canvasReady) {
        this.initiateCanvas();
      }
    }
    /**
     * Build the intro section ui if its part of a form
     */

  }, {
    key: "buildIntroUI",
    value: function buildIntroUI() {
      var name = this.formData.name;
      var introData = _page_utils__WEBPACK_IMPORTED_MODULE_11__["PageUtils"].getIntroData(this.formData.elements);
      return _templates_response_canvas_new_intro_hbs__WEBPACK_IMPORTED_MODULE_5___default()({
        name: name,
        components: introData.children
      });
    }
    /**
     * Translate the various template that makes up the canvas area
     * into a a single string that can be converted into a node
     */

  }, {
    key: "buildCanvasUI",
    value: function buildCanvasUI() {
      var total = _page_utils__WEBPACK_IMPORTED_MODULE_11__["PageUtils"].getTotalQuestions(this.questions);
      var sectionData = _page_utils__WEBPACK_IMPORTED_MODULE_11__["PageUtils"].getFirstSection(this.formData.elements);
      var submitSectionString = _templates_response_canvas_submit_hbs__WEBPACK_IMPORTED_MODULE_0___default()();
      var totalAnswered = this.eventHandler.getAnswers().length;
      var footerString = _templates_response_canvas_footer_hbs__WEBPACK_IMPORTED_MODULE_2___default()({
        total: total,
        totalAnswered: totalAnswered
      });
      var headerData = sectionData ? sectionData : {
        name: this.formData.name
      };
      var headerString = _templates_response_canvas_header_hbs__WEBPACK_IMPORTED_MODULE_1___default()(headerData); // convert the built question UI to string so as to strip off the ","

      var questions = "";
      this.buildQuestionsUI().forEach(function (question) {
        questions += question;
      });
      var canvasString = _templates_response_canvas_canvas_hbs__WEBPACK_IMPORTED_MODULE_4___default()({
        questions: this.buildQuestionsUI().join(""),
        submitSection: submitSectionString
      });
      return "".concat(headerString).concat(canvasString).concat(footerString);
    }
    /**
     * check if a question has required as part of its validation rules
     * @param {object} validationRules
     */

  }, {
    key: "buildQuestionsUI",

    /**
     * Build a suitable ui for each question data to allow users
     * Answer the question being asked
     */
    value: function buildQuestionsUI() {
      var _this2 = this;

      return this.questions.map(function (question, index) {
        if (_page_utils__WEBPACK_IMPORTED_MODULE_11__["PageUtils"].compactTypes().includes(question.type)) {
          // show the question parent name first
          var options = {
            question: question.name,
            position: question.position,
            isRequired: _this2.confirmRequirement(question.validationRules),
            description: question.description
          };
          var subQns = _templates_response_canvas_question_only_hbs__WEBPACK_IMPORTED_MODULE_3___default()(options);
          subQns += "<div class=\"compactContainer elementWrapper elementWrapperDropdown\" style=\"background:#fff;\" data-q-id=\"".concat(question.id, "\" data-intersect-index=\"").concat(index, "\">");
          subQns += question.children.map(function (child) {
            return _page_ui_builder__WEBPACK_IMPORTED_MODULE_8__["PageUIBuilder"].buildUIFor(child, question.id);
          }).join("");
          subQns += "</div>";
          return subQns;
        } else {
          return _page_ui_builder__WEBPACK_IMPORTED_MODULE_8__["PageUIBuilder"].buildUIFor(question);
        }
      });
    }
    /**
     * Add event listeners to items of interest in the document
     * @param {documentFragment} node Document Object containing input elements for user
     * to interact with
     * @param {array} elementIds array of string containing unique id for input
     * interaction item
     */

  }, {
    key: "addEventListeners",
    value: function addEventListeners(node) {
      var _this3 = this;

      var elementIds = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
      this.addGetStartedListener(node);
      this.addNavigationListeners(node);
      elementIds.forEach(function (id) {
        _this3.addInputListener(node, id);

        _this3.addOptionListeners(node, id);

        _this3.addDragDropListeners(node, id);

        _this3.addMediaPermissionHandler(node, id);
      });
      this.addOnSubmitListener(node);
    }
    /**
     * Observe when user click get started button and show response canvas
     * @param {documentFragment} node Document Object containing input elements for user
     * to interact with
     */

  }, {
    key: "addGetStartedListener",
    value: function addGetStartedListener(node) {
      var getStartedBtn = node.querySelector("#getStarted");

      if (getStartedBtn) {
        getStartedBtn.addEventListener("click", this.handleGettingSetarted, false);
      }
    }
    /**
     * Register click event handler on navigation button
     *  @param {documentFragment} node Document Object containing input elements for user
     * to interact with
     */

  }, {
    key: "addNavigationListeners",
    value: function addNavigationListeners(node) {
      var _this4 = this;

      var upBtn = node.querySelector("button[id=\"navigateUP\"]");
      var downBtn = node.querySelector("button[id=\"navigateDown\"]");

      if (upBtn && downBtn) {
        upBtn.onclick = function () {
          var questionUI = _page_utils__WEBPACK_IMPORTED_MODULE_11__["PageUtils"].getCurrentQuestionUI();

          _this4.eventHandler.goToNextQuestion(questionUI.dataset.qId, "up");
        };

        downBtn.onclick = function () {
          var questionUI = _page_utils__WEBPACK_IMPORTED_MODULE_11__["PageUtils"].getCurrentQuestionUI();

          _this4.eventHandler.goToNextQuestion(questionUI.dataset.qId, "down");
        };
      }
    }
    /**
     * Register an event listener that monitors when a user start interacting with an input
     * interection eleemnt
     *  @param {documentFragment} node Document Object containing input elements for user
     * to interact with
     * @param {string} id  id of the interaction element whose input element is to be
     * observed
     */

  }, {
    key: "addInputListener",
    value: function addInputListener(node, id) {
      var _this5 = this;

      //this functions breaks single responsibility principle. fix it
      var btn = node.querySelector("button.nextButton[id=\"".concat(id, "\"]"));
      var input = node.querySelector(".input[data-q-id=\"".concat(id, "\"]"));

      if (input) {
        if (input.dataset.qType === "dropdown") {
          var dropDownIcon = node.querySelector("span[data-q-id=\"".concat(id, "\"]"));
          dropDownIcon.onclick = this.eventHandler.toggleDropDownList;
          input.addEventListener("input", this.eventHandler.expandDropDownList, false);
          input.oninput = this.eventHandler.filterDropdownOptions;
        } else if (input.type === "file") {
          var type = input.dataset.qType;
          var _id = input.dataset.qId;

          input.oninput = function (e) {
            _this5.eventHandler.handleStaticAssetUpload(e.target.files[0], type, _id);
          };
        } else {
          input.oninput = this.eventHandler.handleInput;
          input.onkeyup = this.eventHandler.processAnswer;
        }
      }

      if (btn) {
        if (btn.classList.contains("statement")) {
          btn.onclick = function (e) {
            return _this5.eventHandler.goToNextQuestion(e.target.id, "down");
          };
        } else {
          btn.onclick = this.eventHandler.processAnswer;
        }
      }
    }
    /**
     * Register event handler for button elements on media component UI
     *  @param {documentFragment} node Document Object containing input elements for user
     * @param {string} id  id of the question whose askPermissionBtn we are interested in
     */

  }, {
    key: "addMediaPermissionHandler",
    value: function addMediaPermissionHandler(node, id) {
      var _this6 = this;

      var permissionBtns = node.querySelectorAll("button.askPermissionBtn[id=\"".concat(id, "\"]"));
      var uploadBtn = node.querySelector("button.uploadButton[id=\"".concat(id, "\"]"));

      if (permissionBtns) {
        permissionBtns.forEach(function (permissionBtn) {
          return permissionBtn.onclick = _this6.eventHandler.askMediaPermission;
        });
      }

      if (uploadBtn) {
        uploadBtn.onclick = function (e) {
          return _this6.eventHandler.handleGeneratedAssetUpload(e);
        };
      }
    }
    /**
     *  Register an event listener that monitors when a user clicks an option in a multi
     * option interaction element
     * @param {documentFragment} node Document Object containing input elements for user
     * to interact with
     * @param {string} id  id of the interaction element whose option the user clicked
     * on
     */

  }, {
    key: "addOptionListeners",
    value: function addOptionListeners(node, id) {
      var _this7 = this;

      var options = node.querySelectorAll("div[data-option-id=\"".concat(id, "\"]"));

      if (options.length) {
        options.forEach(function (option) {
          return option.onclick = _this7.eventHandler.handleOptionSelection;
        });
      }
    }
    /**
     * Register an click handler that processes users response
     *@param {documentFragment} node Document Object containing input elements for user
     * to interact with
     */

  }, {
    key: "addOnSubmitListener",
    value: function addOnSubmitListener(node) {
      var btn = node.querySelector("button[id=\"submitresponse\"]");
      if (btn) btn.onclick = this.processResponse;
    }
    /**
     * Register event to handle user drag and drop action
     */

  }, {
    key: "addDragDropListeners",
    value: function addDragDropListeners(node, id) {
      var _this8 = this;

      var dropArea = node.querySelector("div.dropArea[data-q-id=\"".concat(id, "\"]"));

      function preventDefault(e) {
        e.preventDefault();
        e.stopPropagation();
      }

      if (dropArea) {
        ["dragenter", "dragover", "dragleave", "drop"].forEach(function (eventName) {
          dropArea.addEventListener(eventName, preventDefault, false);
        });
        ["dragenter", "dragover"].forEach(function (eventName) {
          dropArea.addEventListener(eventName, _this8.eventHandler.highlightDropArea, false);
        });
        ["dragleave", "drop"].forEach(function (eventName) {
          dropArea.addEventListener(eventName, _this8.eventHandler.unhighlightDropArea, false);
        });
        dropArea.addEventListener("drop", this.eventHandler.handleFileDrop, false);
      }
    }
    /**
     * Observe when a user click get started and show and initialize the canvas
     */

  }, {
    key: "initiateCanvas",

    /**
     * set up intersection observers to control appearance of
     * elements entering the canvas area
     */
    // initiateCanvas() {
    //   const targets = document.querySelectorAll('[data-question="true"]');
    //   const canvasClass = "canvas";
    //   Canvas.setUp({ targets, canvasClass })
    //     .onTargetEnter(this.targetEnterCanvas)
    //     .onTargetExit(this.targetExitCanvas);
    //   this.canvas = document.querySelector(`.${canvasClass}`);
    //   // temporarily hut the unidentified issues causing intersection
    //   // observe to enter infinit loop
    //   this.canvas.scrollBy(0, 50);
    //   setTimeout(() => {
    //     this.canvas.scrollBy(0, -20);
    //     // focus on first question if it is an input element
    //     this.eventHandler.updateFocus(this.questions[0].id, true);
    //   }, 10);
    // }
    value: function initiateCanvas() {
      var _this9 = this;

      var targets = document.querySelectorAll('[data-question="true"]');
      var canvasClass = "canvas"; // Canvas.setUp({ targets, canvasClass })
      //   .onTargetEnter(this.targetEnterCanvas)
      //   .onTargetExit(this.targetExitCanvas);

      this.canvas = document.querySelector(".".concat(canvasClass));
      var viewPortWidth = document.documentElement.clientWidth;
      var viewPortHeight = document.documentElement.clientHeight;
      console.log("viewport width", viewPortWidth);
      console.log("viewport height", viewPortHeight);
      var rootMargin = // viewPortHeight > 700 ? "-183px 0px -500px" : "-183px 0px -350px";
      viewPortWidth > 700 ? "-26% 0px -70%" : "-26% 0px -70%";
      var options = {
        root: this.canvas,
        rootMargin: rootMargin,
        threshold: 0
      };

      var callback = function callback(entries) {
        entries.forEach(function (entry, index) {
          if (entry.isIntersecting) {
            // entry.target.classList.remove('dull')
            var target = entry.target;
            var res = {
              element: target,
              index: index
            };

            _this9.targetEnterCanvas(res);
          } else {
            // entry.target.classList.add('dull');
            // entry.target.addEventListener("click",e=>{
            //     e.preventDefault();
            // });
            var _target = entry.target;
            var _res = {
              element: _target,
              index: index
            };

            _this9.targetExitCanvas(_res);
          } // console.log(
          //   `${entry.target.textContent} entered`,
          //   entry.isIntersecting
          // );

        });
      };

      var observer = new IntersectionObserver(callback, options);
      targets.forEach(function (target) {
        observer.observe(target);
      }); // temporarily hut the unidentified issues causing intersection
      // observe to enter infinit loop
      // this.canvas.scrollBy(0, 50);

      setTimeout(function () {
        // this.canvas.scrollBy(0, -20);
        // focus on first question if it is an input element
        _this9.eventHandler.updateFocus(_this9.questions[0].id, true);
      }, 10);
    }
    /**s
     * Make question element in the canvas ready to be interacted with
     * @param res passed down from intersection observer
     */

  }]);

  return ResponseCanvas;
}();

/***/ }),

/***/ "./src/public/js/response-canvas/validator.js":
/*!****************************************************!*\
  !*** ./src/public/js/response-canvas/validator.js ***!
  \****************************************************/
/*! exports provided: Validator */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Validator", function() { return Validator; });
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../utils */ "./src/public/js/utils/index.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }


/**
 * object to hold error messages for rules that each value
 * Beign validated fail
 */

var validationError = {
  hasError: false,
  messages: []
};
/**
 * question types that require digit values
 */

var digitTypes = ["tel", "number", "account", "bvn", "mobile"];
/**
 * class with interfaces to validate values users pass to response canvas
 * interactive elements
 */

var Validator =
/*#__PURE__*/
function () {
  function Validator() {
    _classCallCheck(this, Validator);
  }

  _createClass(Validator, null, [{
    key: "validate",

    /**
     * 
     * @param {object} rules // object containing containing rules that value entered must meet
     * @param {string} questionType string of charactered by user
     * @param {string} answer string of charactered entered by user
     */
    value: function validate(rules, questionType, answer) {
      // empty validation error to remove hold over values
      validationError.hasError = false;
      validationError.messages = [];
      rules.forEach(function (rule) {
        var methodName = "validate".concat(Object(_utils__WEBPACK_IMPORTED_MODULE_0__["capitalizeFirstLatter"])(rule.name), "Rule");

        if (Validator.hasOwnProperty(methodName)) {
          Validator[methodName](rule, questionType, answer);
        }
      });
      Validator.validateType(questionType, answer);
      return validationError;
    }
    /**
     * validate that value meets the mininum value required for the value
     * @param {object} rule includes the amount of value that satisfies the minimum
     * characters for answer
     * @param {string} questionType string of charactered by user
     * @param {string} answer string of characters entered by user that needs to be validated
     */

  }, {
    key: "validateMinRule",
    value: function validateMinRule(rule, questionType, answer) {
      if (answer.length < rule.value) {
        buildErrorMessage(questionType, rule);
      }
    }
    /**
     * validate that value entered by users meet the maximum rule required
     * @param {object} rule includes the amount of value that satisfies the maximum
     * characters for answer
     * @param {string} questionType string of charactered by user
     * @param {string} answer  string of characters entered by user that needs to be validated
     */

  }, {
    key: "validateMaxRule",
    value: function validateMaxRule(rule, questionType, answer) {
      if (answer.length > rule.value) {
        buildErrorMessage(questionType, rule);
      }
    }
    /**
     * validate that users answer required questions
     * @param {object} rule includes the amount of answer that satisfies the maximum requirement
     * @param {string} questionType string of charactered by user
     * @param {string} answer string of characters entered by user that needs to be validated
     */

  }, {
    key: "validateRequiredRule",
    value: function validateRequiredRule(rule, questionType, answer) {
      if (!answer) {
        buildErrorMessage(questionType, rule);
      }
    }
    /**
     * validate that users answer is a valid email address
     * @param {object} rule includes the amount of answer that satisfies the maximum requirement
     * @param {string} questionType string of charactered by user
     * @param {string} answer string of characters entered by user that needs to be validated
     */

  }, {
    key: "validateEmailRule",
    value: function validateEmailRule(rule, questionType, answer) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

      if (!re.test(String(answer).toLowerCase())) {
        buildErrorMessage(questionType, rule);
      }
    }
    /**
    * validate that users answer is of the right type
    * @param {string} questionType string of charactered by user
    * @param {string} anwer string of characters entered by user that needs to be validated
    */

  }, {
    key: "validateType",
    value: function validateType(questionType, anwer) {
      if (digitTypes.includes(questionType) && isNaN(anwer)) {
        buildErrorMessage(questionType, {
          name: "need-digits"
        });
      }
    }
  }]);

  return Validator;
}();
/**
 * Add suitable error message to be show users
 * @param {string} questionType the type of question user is answering
 * @param {object} rule // object containing containing rules that answer entered must meet
 */

var buildErrorMessage = function buildErrorMessage(questionType, rule) {
  var message = "";

  switch (rule.name) {
    case "need-digits":
      message = "You need to enter digits not letters";
      break;

    case "email":
      message = "You need to enter a valid email address";
      break;

    case "min":
      message = "You need to enter at least ".concat(rule.value, " ").concat(lettersOrDigits(questionType));
      break;

    case "max":
      message = "You can't enter more than ".concat(rule.value, " ").concat(lettersOrDigits(questionType));
      break;

    case "required":
      message = "This question is required";
      break;

    default:
      message = "";
  }

  validationError.messages.push(message);
  validationError.hasError = true;
};
/**
 * figure out whether to include digits or letters to error message
 * @param {string} questionType the type of question user is answering
 */


var lettersOrDigits = function lettersOrDigits(questionType) {
  if (digitTypes.includes(questionType)) {
    return "digits";
  }

  return "letters";
};

/***/ }),

/***/ "./src/public/js/templates/response-canvas/canvas.hbs":
/*!************************************************************!*\
  !*** ./src/public/js/templates/response-canvas/canvas.hbs ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "<section class=\"interectionSection\">\r\n  <div class=\"canvas\">\r\n    <main>\r\n      "
    + ((stack1 = ((helper = (helper = helpers.questions || (depth0 != null ? depth0.questions : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"questions","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\r\n    </main>\r\n    "
    + ((stack1 = ((helper = (helper = helpers.submitSection || (depth0 != null ? depth0.submitSection : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"submitSection","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\r\n  </div>\r\n</section>";
},"useData":true});

/***/ }),

/***/ "./src/public/js/templates/response-canvas/compact-question-header.hbs":
/*!*****************************************************************************!*\
  !*** ./src/public/js/templates/response-canvas/compact-question-header.hbs ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "                    <div class=\"questionPosition\" data-q-position="
    + alias4(((helper = (helper = helpers.position || (depth0 != null ? depth0.position : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"position","hash":{},"data":data}) : helper)))
    + ">\r\n                      "
    + alias4(((helper = (helper = helpers.position || (depth0 != null ? depth0.position : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"position","hash":{},"data":data}) : helper)))
    + "\r\n                    </div>\r\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "                <div class=\"requiredQuestion\">*</div>\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"elementWrapper dull elementWrapperDropdown\" data-q-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" data-question=\"true\">\r\n  <section class=\"inactiveElement\">\r\n    <div class=\"elementParent\">\r\n      <div class=\"elementHouse\">\r\n        <div>\r\n          <div class=\"elementQuestionWrapper\">\r\n            <div class=\"elementQuestionContents\">\r\n              <div class=\"questionDecorationWrapper\">\r\n                <span class=\"questionDecoration\">\r\n"
    + ((stack1 = helpers.unless.call(alias1,(depth0 != null ? depth0.compact : depth0),{"name":"unless","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                  <div class=\"questionIcon\">\r\n                    <span>\r\n                      <i class=\"fa fa-arrow-right\"></i>\r\n                    </span>\r\n                  </div>\r\n                </span>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"questionTextWrapper\">\r\n            <span>\r\n              <span>"
    + alias4(((helper = (helper = helpers.question || (depth0 != null ? depth0.question : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"question","hash":{},"data":data}) : helper)))
    + "</span>\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.isRequired : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "            </span>\r\n          </div>\r\n          <p class=\"description\">"
    + alias4(((helper = (helper = helpers.description || (depth0 != null ? depth0.description : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data}) : helper)))
    + "</p>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </section>\r\n</div>";
},"useData":true});

/***/ }),

/***/ "./src/public/js/templates/response-canvas/dropdown-options.hbs":
/*!**********************************************************************!*\
  !*** ./src/public/js/templates/response-canvas/dropdown-options.hbs ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var helper;

  return " data-parent-id=\""
    + container.escapeExpression(((helper = (helper = helpers.parent || (depth0 != null ? depth0.parent : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"parent","hash":{},"data":data}) : helper)))
    + "\" ";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"dropdownOptionWrapper\" data-resource-id=\""
    + alias4(((helper = (helper = helpers.resourceId || (depth0 != null ? depth0.resourceId : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"resourceId","hash":{},"data":data}) : helper)))
    + "\" data-option-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" data-option-type=\"dropdown\" data-q-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" data-control-type=\""
    + alias4(((helper = (helper = helpers.dropdownType || (depth0 != null ? depth0.dropdownType : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"dropdownType","hash":{},"data":data}) : helper)))
    + "\"  "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.parent : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ">\r\n  <div class=\"optionContents\">\r\n    <div class=\"optionBox\">\r\n      <div class=\"dropDownOption\">\r\n        <div class=\"optionTextWrapper\">\r\n          <div class=\"optionTextBox\">\r\n            <div class=\"dropDownOptionText text\">"
    + alias4(((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"text","hash":{},"data":data}) : helper)))
    + "</div>\r\n          </div>\r\n          <div class=\"iconWrapper inactivIconWrapper\">\r\n            <div class=\"optionIconBox\">\r\n              <span>\r\n                <img src=\"/img/picked-green.svg\" alt=\"picked\" />\r\n              </span>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>";
},"useData":true});

/***/ }),

/***/ "./src/public/js/templates/response-canvas/dropdown-question.hbs":
/*!***********************************************************************!*\
  !*** ./src/public/js/templates/response-canvas/dropdown-question.hbs ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return " data-parent-id=\""
    + alias4(((helper = (helper = helpers.parent || (depth0 != null ? depth0.parent : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"parent","hash":{},"data":data}) : helper)))
    + "\"\r\n  data-control-type=\""
    + alias4(((helper = (helper = helpers.controlType || (depth0 != null ? depth0.controlType : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"controlType","hash":{},"data":data}) : helper)))
    + "\" ";
},"3":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "                  <div class=\"questionPosition\" data-q-position="
    + alias4(((helper = (helper = helpers.position || (depth0 != null ? depth0.position : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"position","hash":{},"data":data}) : helper)))
    + ">\r\n                        "
    + alias4(((helper = (helper = helpers.position || (depth0 != null ? depth0.position : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"position","hash":{},"data":data}) : helper)))
    + "\r\n                  </div>\r\n";
},"5":function(container,depth0,helpers,partials,data) {
    return "                <div class=\"requiredQuestion\">*</div>\r\n";
},"7":function(container,depth0,helpers,partials,data) {
    var helper;

  return " data-parent-id=\""
    + container.escapeExpression(((helper = (helper = helpers.parent || (depth0 != null ? depth0.parent : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"parent","hash":{},"data":data}) : helper)))
    + "\" data-is-compact=\"true\" ";
},"9":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return " data-parent-id=\""
    + alias4(((helper = (helper = helpers.parent || (depth0 != null ? depth0.parent : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"parent","hash":{},"data":data}) : helper)))
    + "\"\r\n                    data-control-type=\""
    + alias4(((helper = (helper = helpers.controlType || (depth0 != null ? depth0.controlType : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"controlType","hash":{},"data":data}) : helper)))
    + "\" ";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"elementWrapper dull elementWrapperDropdown\" "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.parent : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + " data-q-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" data-question=\"true\">\r\n  <section class=\"inactiveElement\">\r\n    <div class=\"elementParent\">\r\n      <div class=\"elementHouse\">\r\n        <div>\r\n          <div class=\"elementQuestionWrapper\">\r\n            <div class=\"elementQuestionContents\">\r\n              <div class=\"questionDecorationWrapper\">\r\n                <span class=\"questionDecoration\">\r\n"
    + ((stack1 = helpers.unless.call(alias1,(depth0 != null ? depth0.compact : depth0),{"name":"unless","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                  <div class=\"questionIcon\">\r\n                    <span>\r\n                      <i class=\"fa fa-arrow-right\"></i>\r\n                    </span>\r\n                  </div>\r\n                </span>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"questionTextWrapper\">\r\n            <span>\r\n              <span>"
    + alias4(((helper = (helper = helpers.question || (depth0 != null ? depth0.question : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"question","hash":{},"data":data}) : helper)))
    + "</span>\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.isRequired : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "            </span>\r\n          </div>\r\n          <p class=\"description\">"
    + alias4(((helper = (helper = helpers.description || (depth0 != null ? depth0.description : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data}) : helper)))
    + "</p>\r\n        </div>\r\n        <div>\r\n          <div class=\"answerWrapper\">\r\n            <div class=\"answerContent\" tabIndex=\"-1\">\r\n              <div class=\"answerBox\">\r\n                <div class=\"answerText\">\r\n                  <input class=\"answer input\" placeholder=\""
    + alias4(((helper = (helper = helpers.placeholder || (depth0 != null ? depth0.placeholder : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"placeholder","hash":{},"data":data}) : helper)))
    + "\" data-q-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" data-q-type=\"dropdown\"\r\n                    id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.parent : depth0),{"name":"if","hash":{},"fn":container.program(7, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + " />\r\n                  <div class=\"answerDecorationWrapper\">\r\n                    <div class=\"answerDecoration\">\r\n                      <span data-q-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\r\n                        <i class=\"fa fa-angle-down\" data-q-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\"></i>\r\n                      </span>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                <div class=\"inactiveWrapper dropdownOptions\" data-q-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\r\n                  <div class=\"optionsBox\" data-q-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.parent : depth0),{"name":"if","hash":{},"fn":container.program(9, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ">\r\n                    "
    + ((stack1 = ((helper = (helper = helpers.optionsString || (depth0 != null ? depth0.optionsString : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"optionsString","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"validationWrapper inactiveValidation\" data-validation-msg-for=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </section>\r\n</div>";
},"useData":true});

/***/ }),

/***/ "./src/public/js/templates/response-canvas/footer.hbs":
/*!************************************************************!*\
  !*** ./src/public/js/templates/response-canvas/footer.hbs ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<section class=\"canvas__footerSection\">\r\n  <div class=\"canvas__footer\">\r\n    <div class=\"canvas__footerContent\">\r\n      <div class=\"canvas__footerProgress\">\r\n        <p class=\"progress__text\">\r\n          "
    + alias4(((helper = (helper = helpers.totalAnswered || (depth0 != null ? depth0.totalAnswered : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"totalAnswered","hash":{},"data":data}) : helper)))
    + " of "
    + alias4(((helper = (helper = helpers.total || (depth0 != null ? depth0.total : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total","hash":{},"data":data}) : helper)))
    + " Questions\r\n        </p>\r\n        <progress value=\""
    + alias4(((helper = (helper = helpers.totalAnswered || (depth0 != null ? depth0.totalAnswered : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"totalAnswered","hash":{},"data":data}) : helper)))
    + "\" max=\""
    + alias4(((helper = (helper = helpers.total || (depth0 != null ? depth0.total : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total","hash":{},"data":data}) : helper)))
    + "\"></progress>\r\n      </div>\r\n      <div class=\"canvas__footerNavigiation\">\r\n        <button class=\"canvas__btn canvas__btn--primary btn--navigation\" id=\"navigateUP\">\r\n          <i class=\"fa fa-angle-up\"></i>\r\n        </button>\r\n        <button class=\"canvas__btn canvas__btn--primary btn--navigation\" id=\"navigateDown\">\r\n          <i class=\"fa fa-angle-down\"></i>\r\n        </button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</section>";
},"useData":true});

/***/ }),

/***/ "./src/public/js/templates/response-canvas/header.hbs":
/*!************************************************************!*\
  !*** ./src/public/js/templates/response-canvas/header.hbs ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper;

  return "<section class=\"canvas__headerSection\" style=\"margin-bottom:10px;\">\r\n  <div class=\"canvas__header\">\r\n    <h1 class=\"canvas__sectionTitle\" style=\"font-size: 20px;color:#000 !important;\">"
    + container.escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"name","hash":{},"data":data}) : helper)))
    + "</h1>\r\n  </div>\r\n</section>";
},"useData":true});

/***/ }),

/***/ "./src/public/js/templates/response-canvas/long-question.hbs":
/*!*******************************************************************!*\
  !*** ./src/public/js/templates/response-canvas/long-question.hbs ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "                  <div class=\"questionPosition\" data-q-position="
    + alias4(((helper = (helper = helpers.position || (depth0 != null ? depth0.position : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"position","hash":{},"data":data}) : helper)))
    + ">\r\n                        "
    + alias4(((helper = (helper = helpers.position || (depth0 != null ? depth0.position : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"position","hash":{},"data":data}) : helper)))
    + "\r\n                  </div>\r\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "                  <div class=\"requiredQuestion\">*</div>\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"elementWrapper dull\" data-q-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" data-question=\"true\">\r\n  <section class=\"inactiveElement\">\r\n    <div class=\"elementParent\">\r\n      <div class=\"elementHouse\">\r\n        <div>\r\n          <div>\r\n            <div class=\"elementQuestionWrapper\">\r\n              <div class=\"elementQuestionContents\">\r\n                <div class=\"questionDecorationWrapper\">\r\n                  <span class=\"questionDecoration\">\r\n"
    + ((stack1 = helpers.unless.call(alias1,(depth0 != null ? depth0.compact : depth0),{"name":"unless","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                    <div class=\"questionIcon\">\r\n                      <span>\r\n                        <i class=\"fa fa-arrow-right\"></i>\r\n                      </span>\r\n                    </div>\r\n                  </span>\r\n                </div>\r\n                <div class=\"questionTextWrapper\">\r\n                  <label for=\"id=\" "
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\"\"> "
    + alias4(((helper = (helper = helpers.question || (depth0 != null ? depth0.question : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"question","hash":{},"data":data}) : helper)))
    + "?</label>\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.isRequired : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                </div>\r\n                <p class=\"description\">"
    + alias4(((helper = (helper = helpers.description || (depth0 != null ? depth0.description : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data}) : helper)))
    + "</p>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div>\r\n\r\n\r\n            <div class=\"answerWrapper\">\r\n              <textarea data-q-position=\""
    + alias4(((helper = (helper = helpers.position || (depth0 != null ? depth0.position : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"position","hash":{},"data":data}) : helper)))
    + "\" placeholder=\""
    + alias4(((helper = (helper = helpers.placeholder || (depth0 != null ? depth0.placeholder : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"placeholder","hash":{},"data":data}) : helper)))
    + "\" class=\"textAreaAnswer input\"\r\n                id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" data-q-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" required=\""
    + alias4(((helper = (helper = helpers.isRequired || (depth0 != null ? depth0.isRequired : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"isRequired","hash":{},"data":data}) : helper)))
    + "\"></textarea>\r\n            </div>\r\n            <p class=\"anwerHint\">\r\n              <strong>SHIFT</strong> + <strong>ENTER</strong> To make a new line\r\n            </p>\r\n\r\n\r\n\r\n\r\n\r\n            <div class=\"validationWrapper inactiveValidation\" data-validation-msg-for=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </section>\r\n</div>\r\n";
},"useData":true});

/***/ }),

/***/ "./src/public/js/templates/response-canvas/media-permission.hbs":
/*!**********************************************************************!*\
  !*** ./src/public/js/templates/response-canvas/media-permission.hbs ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "                      <div class=\"questionPosition\" data-q-position="
    + alias4(((helper = (helper = helpers.position || (depth0 != null ? depth0.position : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"position","hash":{},"data":data}) : helper)))
    + ">\r\n                        "
    + alias4(((helper = (helper = helpers.position || (depth0 != null ? depth0.position : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"position","hash":{},"data":data}) : helper)))
    + "\r\n                      </div>\r\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "                    <div class=\"requiredQuestion\">*</div>\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"elementWrapper dull elementWrapperMedia\" data-q-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" data-question=\"true\">\r\n  <section class=\"inactiveElement\">\r\n    <div class=\"elementParent recording-canvas\">\r\n      <div class=\"elementHouse\">\r\n        <div>\r\n          <div>\r\n            <div class=\"elementQuestionWrapper\">\r\n              <div class=\"elementQuestionContents\">\r\n                <div class=\"questionDecorationWrapper\">\r\n                  <span class=\"questionDecoration\">\r\n"
    + ((stack1 = helpers.unless.call(alias1,(depth0 != null ? depth0.compact : depth0),{"name":"unless","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                    <div class=\"questionIcon\">\r\n                      <span>\r\n                        <i class=\"fa fa-arrow-right\"></i>\r\n                      </span>\r\n                    </div>\r\n                  </span>\r\n                </div>\r\n                <div class=\"questionTextWrapper\">\r\n                  <label for=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\"> "
    + alias4(((helper = (helper = helpers.question || (depth0 != null ? depth0.question : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"question","hash":{},"data":data}) : helper)))
    + "</label>\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.isRequired : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                </div>\r\n                <p class=\"description\">"
    + alias4(((helper = (helper = helpers.description || (depth0 != null ? depth0.description : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data}) : helper)))
    + "</p>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"mediaRecorder\" id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\r\n            <div class=\"answerWrapper\">\r\n              <button class=\"askPermissionBtn\" id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" data-qType=\""
    + alias4(((helper = (helper = helpers.type || (depth0 != null ? depth0.type : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"type","hash":{},"data":data}) : helper)))
    + "\">\r\n                <span class=\"nextButtonText\">"
    + alias4(((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"text","hash":{},"data":data}) : helper)))
    + "</span>\r\n              </button>\r\n            </div>\r\n            <div class=\"nextButtonWrapper in-mobile\" id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\r\n              <button class=\"uploadButton hide-elem\" id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" data-qType=\""
    + alias4(((helper = (helper = helpers.type || (depth0 != null ? depth0.type : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"type","hash":{},"data":data}) : helper)))
    + "\">\r\n                <span class=\"cam-progress\"></span>\r\n                <span class=\"nextButtonText upload-btn-txt\" id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">Upload</span>\r\n              </button>\r\n              <button class=\"resetButton askPermissionBtn hide-elem\" id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" data-qType=\""
    + alias4(((helper = (helper = helpers.type || (depth0 != null ? depth0.type : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"type","hash":{},"data":data}) : helper)))
    + "\">\r\n                <span class=\"nextButtonText\" id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">Re-Take</span>\r\n              </button>\r\n            </div>\r\n            <div class=\"validationWrapper inactiveValidation\" data-validation-msg-for=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </section>\r\n</div>";
},"useData":true});

/***/ }),

/***/ "./src/public/js/templates/response-canvas/multi-choice-question.hbs":
/*!***************************************************************************!*\
  !*** ./src/public/js/templates/response-canvas/multi-choice-question.hbs ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "                    <div class=\"questionPosition\" data-q-position="
    + alias4(((helper = (helper = helpers.position || (depth0 != null ? depth0.position : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"position","hash":{},"data":data}) : helper)))
    + ">\r\n                      "
    + alias4(((helper = (helper = helpers.position || (depth0 != null ? depth0.position : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"position","hash":{},"data":data}) : helper)))
    + "\r\n                    </div>\r\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "                <div class=\"requiredQuestion\">*</div>\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"elementWrapper dull\" data-q-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" data-question=\"true\">\r\n  <section class=\"inactiveElement\">\r\n    <div class=\"elementParent\">\r\n      <div class=\"elementHouse\">\r\n        <div>\r\n          <div class=\"elementQuestionWrapper\">\r\n            <div class=\"elementQuestionContents\">\r\n              <div class=\"questionDecorationWrapper\">\r\n                <span class=\"questionDecoration\">\r\n"
    + ((stack1 = helpers.unless.call(alias1,(depth0 != null ? depth0.compact : depth0),{"name":"unless","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                  <div class=\"questionIcon\">\r\n                    <span>\r\n                      <i class=\"fa fa-arrow-right\"></i>\r\n                    </span>\r\n                  </div>\r\n                </span>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"questionTextWrapper\">\r\n            <span>\r\n              <span>"
    + alias4(((helper = (helper = helpers.question || (depth0 != null ? depth0.question : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"question","hash":{},"data":data}) : helper)))
    + "</span>\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.isRequired : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "            </span>\r\n          </div>\r\n          <p class=\"description\">"
    + alias4(((helper = (helper = helpers.description || (depth0 != null ? depth0.description : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data}) : helper)))
    + "</p>\r\n        </div>\r\n        <div>\r\n          <div class=\"answerWrapper\">\r\n            <div class=\"answerContent\">\r\n              <div>\r\n                "
    + ((stack1 = ((helper = (helper = helpers.optionsString || (depth0 != null ? depth0.optionsString : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"optionsString","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"validationWrapper inactiveValidation\" data-validation-msg-for=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </section>\r\n</div>";
},"useData":true});

/***/ }),

/***/ "./src/public/js/templates/response-canvas/new-intro.hbs":
/*!***************************************************************!*\
  !*** ./src/public/js/templates/response-canvas/new-intro.hbs ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var alias1=container.lambda, alias2=container.escapeExpression;

  return "            <!--<div class=\"col-sm-2 bottom\">-->\r\n            <!--<div class=\"col-sm-2 bottom\">-->\r\n            <div class=\"col-3 col--mobile\">\r\n                <span class=\"requirement sign\">\r\n                    <img class=\"intro__iconImage\" src=\"/img/"
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + ".svg\" alt=\""
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + " Icon\"/>\r\n                </span>\r\n                <h5>"
    + alias2(alias1((depth0 != null ? depth0.description : depth0), depth0))
    + "</h5>\r\n            </div>\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return "<div style=\"overflow: scroll !important;\">\r\n<section class=\"container-fluid stretch\">\r\n\r\n\r\n\r\n    <div class=\"row header-wrap\">\r\n    <a href=\"/"
    + container.escapeExpression(((helper = (helper = helpers.bankSlug || (depth0 != null ? depth0.bankSlug : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"bankSlug","hash":{},"data":data}) : helper)))
    + "\">\r\n    <div class=\"image-left dynamic-image-parent\">\r\n        <!--<img class=\"img-responsive logo-circle\" src=\"/img/unionbank.png\" alt=\"Union Bank\">-->\r\n         <img src=\"/img/logo.png\" alt=\"Swyp Logo\" class=\"header-desktop__logo bank-page-dynamic\" id=\"banklogo-des\">\r\n         <!--<img src=\"https://swyp-assets.s3.eu-central-1.amazonaws.com/logos/1567606884630_gtbanklogo.png\" alt=\"Swyp Logo\" class=\"header-desktop__logo bank-page-dynamic\" id=\"banklogo-des\">-->\r\n    </div>\r\n    </a>\r\n\r\n    <div class=\"bank-name\">Registration form</div>\r\n    </div>\r\n\r\n</section>\r\n\r\n\r\n\r\n<!--<a href=\"/\">-->\r\n    <!--<div class=\"header-desktop__logo-box dynamic-image-left\">-->\r\n        <!--<img src=\"/img/logo.png\" alt=\"Swyp Logo\" class=\"header-desktop__logo dynamic\" id=\"banklogo-des\">-->\r\n    <!--</div>-->\r\n    <!--<h1 class=\"bank-name\" id=\"heading\" data-slug=\"\"><br>-->\r\n    <!--</h1>-->\r\n<!--</a>-->\r\n\r\n\r\n\r\n<section class=\"container-fluid\">\r\n\r\n    <div class=\"row cover-center\">\r\n        <h3>Kindly fill out all the fields with accurate information to aid smooth processing of your form.\r\n            You will need below documents before you can complete the form.</h3>\r\n    </div>\r\n\r\n\r\n    <div class=\"col-sm-12 form-header\">\r\n        <h3 class=\"text-flow\">Form Requirements</h3>\r\n    </div>\r\n\r\n\r\n    <div class=\"row parent-wrap\">\r\n\r\n        <!---->\r\n            <!--<div class=\"intro__requirement\">-->\r\n                <!--<div class=\"intro__icon\">-->\r\n                    <!--<img class=\"intro__iconImage\" src=\"/img/.svg\" alt=\" Icon\" />-->\r\n                <!--</div>-->\r\n                <!--<div class=\"intro__requirementTextWrapper\">-->\r\n                    <!--<h3 class=\"intro__requirementText\">-->\r\n                        <!---->\r\n                    <!--</h3>-->\r\n                <!--</div>-->\r\n            <!--</div>-->\r\n        <!---->\r\n\r\n        <!--<div class=\"col-sm-2 bottom\">-->\r\n        <!--<span class=\"requirement util\"><i class=\"fa fa-check\" aria-hidden=\"true\"></i></span>-->\r\n        <!--<h5>Utility Bill</h5>-->\r\n        <!--</div>-->\r\n\r\n\r\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.components : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n\r\n\r\n\r\n\r\n        <!--<div class=\"col-sm-2 bottom\">-->\r\n            <!--<span class=\"requirement sign\"><i class=\"fa fa-check\" aria-hidden=\"true\"></i></span>-->\r\n            <!--<h5>Signature</h5>-->\r\n        <!--</div>-->\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n        <!--<div class=\"col-sm-2 bottom\">-->\r\n            <!--<span class=\"requirement util\"><i class=\"fa fa-check\" aria-hidden=\"true\"></i></span>-->\r\n            <!--<h5>Utility Bill</h5>-->\r\n        <!--</div>-->\r\n        <!---->\r\n        <!--<div class=\"col-sm-2 bottom\">-->\r\n            <!--<span class=\"requirement bcert\"><i class=\"fa fa-check\" aria-hidden=\"true\"></i></span>-->\r\n            <!--<h5>Birth Certificate</h5>-->\r\n        <!--</div>-->\r\n        <!---->\r\n        <!--<div class=\"col-sm-2 bottom\">-->\r\n            <!--<span class=\"requirement bcert\"><i class=\"fa fa-check\" aria-hidden=\"true\"></i></span>-->\r\n            <!--<h5>Visa</h5>-->\r\n        <!--</div>-->\r\n        <!---->\r\n        <!--<div class=\"col-sm-2 bottom\">-->\r\n            <!--<span class=\"requirement bcert\"><i class=\"fa fa-check\" aria-hidden=\"true\"></i></span>-->\r\n            <!--<h5>Bills</h5>-->\r\n        <!--</div>-->\r\n        <!---->\r\n        <!--<div class=\"col-sm-2 bottom\">-->\r\n            <!--<span class=\"requirement bcert\"><i class=\"fa fa-check\" aria-hidden=\"true\"></i></span>-->\r\n            <!--<h5>CAC</h5>-->\r\n        <!--</div>-->\r\n\r\n\r\n\r\n    </div>\r\n\r\n    <button class=\"buttons btn-link canvas__btn canvas__btn--primary canvas__btn--action\" id=\"getStarted\">Proceed to Form\r\n        <!--<a href=\"\" class=\"btn-link canvas__btn canvas__btn--primary canvas__btn--action\" id=\"getStarted\">Proceed to Form</a>-->\r\n    </button>\r\n\r\n    <!--<button class=\"canvas__btn canvas__btn--primary canvas__btn--action\" id=\"getStarted\">-->\r\n        <!--I am Ready-->\r\n    <!--</button>-->\r\n\r\n\r\n</section>\r\n</div>\r\n";
},"useData":true});

/***/ }),

/***/ "./src/public/js/templates/response-canvas/option.hbs":
/*!************************************************************!*\
  !*** ./src/public/js/templates/response-canvas/option.hbs ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"optionRoot\" data-option-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\r\n  <div class=\"optionWrapper\">\r\n    <div class=\"optionContents\">\r\n      <div class=\"option\">\r\n        <div class=\"optionContent\">\r\n          <div class=\"optionDecoration\" data-input=\"true\">\r\n            <div class=\"optionLabelBox\">\r\n              <div class=\"optionLabelDecoration\">\r\n                <span class=\"optionLabelIcon\">"
    + alias4(((helper = (helper = helpers.label || (depth0 != null ? depth0.label : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data}) : helper)))
    + "</span>\r\n              </div>\r\n            </div>\r\n            <div class=\"optionTextBox\">\r\n              <div class=\"optionText text\">"
    + alias4(((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"text","hash":{},"data":data}) : helper)))
    + "</div>\r\n            </div>\r\n            <div class=\"iconWrapper inactivIconWrapper\">\r\n              <div class=\"optionIconBox\">\r\n                <span>\r\n                  <img src=\"/img/picked-green.svg\" alt=\"picked\" />\r\n                </span>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>";
},"useData":true});

/***/ }),

/***/ "./src/public/js/templates/response-canvas/picture.hbs":
/*!*************************************************************!*\
  !*** ./src/public/js/templates/response-canvas/picture.hbs ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "                      <div class=\"questionPosition\" data-q-position="
    + alias4(((helper = (helper = helpers.position || (depth0 != null ? depth0.position : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"position","hash":{},"data":data}) : helper)))
    + ">\r\n                        "
    + alias4(((helper = (helper = helpers.position || (depth0 != null ? depth0.position : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"position","hash":{},"data":data}) : helper)))
    + "\r\n                      </div>\r\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "                    <div class=\"requiredQuestion\">*</div>\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"elementWrapper dull\" data-q-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" data-question=\"true\">\r\n  <section class=\"inactiveElement\">\r\n    <div class=\"elementParent\">\r\n      <div class=\"elementHouse\">\r\n        <div>\r\n          <div>\r\n            <div class=\"elementQuestionWrapper\">\r\n              <div class=\"elementQuestionContents\">\r\n                <div class=\"questionDecorationWrapper\">\r\n                  <span class=\"questionDecoration\">\r\n"
    + ((stack1 = helpers.unless.call(alias1,(depth0 != null ? depth0.compact : depth0),{"name":"unless","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                    <div class=\"questionIcon\">\r\n                      <span>\r\n                        <i class=\"fa fa-arrow-right\"></i>\r\n                      </span>\r\n                    </div>\r\n                  </span>\r\n                </div>\r\n                <div class=\"questionTextWrapper\">\r\n                  <span>"
    + alias4(((helper = (helper = helpers.question || (depth0 != null ? depth0.question : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"question","hash":{},"data":data}) : helper)))
    + "</span>\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.isRequired : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                </div>\r\n                <p class=\"description\">"
    + alias4(((helper = (helper = helpers.description || (depth0 != null ? depth0.description : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data}) : helper)))
    + "</p>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div>\r\n            <div class=\"answerWrapper\">\r\n              <div class=\"dropArea\" data-q-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" data-q-type=\""
    + alias4(((helper = (helper = helpers.type || (depth0 != null ? depth0.type : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"type","hash":{},"data":data}) : helper)))
    + "\">\r\n                <div class=\"dropAreaContent\">\r\n                  <input type=\"file\" id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" data-q-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" class=\"logo input\" data-q-type=\""
    + alias4(((helper = (helper = helpers.type || (depth0 != null ? depth0.type : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"type","hash":{},"data":data}) : helper)))
    + "\" />\r\n                  <div class=\"uploadstatus\" data-q-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\r\n                    "
    + ((stack1 = ((helper = (helper = helpers.uploadIndicator || (depth0 != null ? depth0.uploadIndicator : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"uploadIndicator","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"validationWrapper inactiveValidation\" data-validation-msg-for=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </section>\r\n</div>";
},"useData":true});

/***/ }),

/***/ "./src/public/js/templates/response-canvas/question-only.hbs":
/*!*******************************************************************!*\
  !*** ./src/public/js/templates/response-canvas/question-only.hbs ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "                  <div class=\"questionPosition\" data-q-position="
    + alias4(((helper = (helper = helpers.position || (depth0 != null ? depth0.position : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"position","hash":{},"data":data}) : helper)))
    + ">\r\n                    "
    + alias4(((helper = (helper = helpers.position || (depth0 != null ? depth0.position : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"position","hash":{},"data":data}) : helper)))
    + "\r\n                  </div>\r\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "              <div class=\"requiredQuestion\">*</div>\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<section class=\"activeElement\">\r\n  <div class=\"elementParent\">\r\n    <div class=\"elementHouse\">\r\n      <div>\r\n        <div class=\"elementQuestionWrapper\">\r\n          <div class=\"elementQuestionContents\">\r\n            <div class=\"questionDecorationWrapper\">\r\n              <span class=\"questionDecoration\">\r\n"
    + ((stack1 = helpers.unless.call(alias1,(depth0 != null ? depth0.compact : depth0),{"name":"unless","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                <div class=\"questionIcon\">\r\n                  <span>\r\n                    <i class=\"fa fa-arrow-right\"></i>\r\n                  </span>\r\n                </div>\r\n              </span>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"questionTextWrapper\">\r\n          <span>\r\n            <span>"
    + alias4(((helper = (helper = helpers.question || (depth0 != null ? depth0.question : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"question","hash":{},"data":data}) : helper)))
    + "</span>\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.isRequired : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "          </span>\r\n        </div>\r\n        <p class=\"description\">"
    + alias4(((helper = (helper = helpers.description || (depth0 != null ? depth0.description : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data}) : helper)))
    + "</p>\r\n      </div>\r\n\r\n    </div>\r\n  </div>\r\n</section>";
},"useData":true});

/***/ }),

/***/ "./src/public/js/templates/response-canvas/record-video.hbs":
/*!******************************************************************!*\
  !*** ./src/public/js/templates/response-canvas/record-video.hbs ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class=\"video-recorder\">\r\n  <div id=\"clockdiv\">\r\n    <div style=\"display:none;\">\r\n      <span class=\"hours\">0</span>\r\n    </div>\r\n    <div style=\"display:none;\">\r\n      <span class=\"minutes\">0</span>\r\n    </div>\r\n    <div>\r\n      <span class=\"seconds\">00</span>\r\n    </div>\r\n  </div>\r\n  <div class=\"video__players\">\r\n\r\n    <div class=\"video__liveCont\">\r\n      <video id=\"live\" class=\"video__playerLive\" playsinline autoplay muted></video>\r\n      <button class=\"video__toggleCam hidden-lg\" id=\"toggleCam\" alt=\"Toggle Cam\"><i\r\n          class=\"ion ion-ios-reverse-camera\"></i></button>\r\n    </div>\r\n    <video id=\"playback\" class=\"video__player des-content\" playsinline></video>\r\n    <div class=\"clearfix\"></div>\r\n  </div>\r\n  <div class=\"video__controls\">\r\n    <button id=\"record\" class=\"video__control\">Start Recording</button>\r\n  </div>\r\n</div>";
},"useData":true});

/***/ }),

/***/ "./src/public/js/templates/response-canvas/simple-question.hbs":
/*!*********************************************************************!*\
  !*** ./src/public/js/templates/response-canvas/simple-question.hbs ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "                      <div class=\"questionPosition\" data-q-position="
    + alias4(((helper = (helper = helpers.position || (depth0 != null ? depth0.position : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"position","hash":{},"data":data}) : helper)))
    + ">\r\n                        "
    + alias4(((helper = (helper = helpers.position || (depth0 != null ? depth0.position : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"position","hash":{},"data":data}) : helper)))
    + "\r\n                      </div>\r\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "                    <div class=\"requiredQuestion\">*</div>\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<!--<div class=\"elementWrapper dull\" data-q-id=\"\" data-question=\"true\" style=\"background: #022fc7;\">-->\r\n<div class=\"elementWrapper dull\" data-q-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" data-question=\"true\">\r\n  <section class=\"inactiveElement\">\r\n    <div class=\"elementParent\">\r\n      <div class=\"elementHouse\">\r\n        <div>\r\n          <div>\r\n            <div class=\"elementQuestionWrapper\">\r\n              <div class=\"elementQuestionContents\">\r\n                <div class=\"questionDecorationWrapper\">\r\n                  <span class=\"questionDecoration\">\r\n"
    + ((stack1 = helpers.unless.call(alias1,(depth0 != null ? depth0.compact : depth0),{"name":"unless","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                    <div class=\"questionIcon\">\r\n                      <span>\r\n                        <i class=\"fa fa-arrow-right\"></i>\r\n                      </span>\r\n                    </div>\r\n                  </span>\r\n                </div>\r\n                <div class=\"questionTextWrapper\">\r\n                  <label for=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\"> "
    + alias4(((helper = (helper = helpers.question || (depth0 != null ? depth0.question : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"question","hash":{},"data":data}) : helper)))
    + "</label>\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.isRequired : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                </div>\r\n                <p class=\"description\">"
    + alias4(((helper = (helper = helpers.description || (depth0 != null ? depth0.description : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data}) : helper)))
    + "</p>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div>\r\n            <div class=\"answerWrapper\">\r\n              <input placeholder=\""
    + alias4(((helper = (helper = helpers.placeholder || (depth0 != null ? depth0.placeholder : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"placeholder","hash":{},"data":data}) : helper)))
    + "\" type=\""
    + alias4(((helper = (helper = helpers.type || (depth0 != null ? depth0.type : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"type","hash":{},"data":data}) : helper)))
    + "\" id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" class=\"answer input "
    + alias4(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + "\"\r\n                data-q-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" required=\""
    + alias4(((helper = (helper = helpers.isRequired || (depth0 != null ? depth0.isRequired : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"isRequired","hash":{},"data":data}) : helper)))
    + "\" />\r\n            </div>\r\n            <div class=\"validationWrapper inactiveValidation\" data-validation-msg-for=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </section>\r\n</div>";
},"useData":true});

/***/ }),

/***/ "./src/public/js/templates/response-canvas/statement.hbs":
/*!***************************************************************!*\
  !*** ./src/public/js/templates/response-canvas/statement.hbs ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"2":function(container,depth0,helpers,partials,data) {
    return "                      <li>"
    + container.escapeExpression(container.lambda(depth0, depth0))
    + "</li>\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"elementWrapper dull\" data-q-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" data-question=\"true\">\r\n  <section class=\"inactiveElement\">\r\n    <div class=\"activeElementParent\">\r\n      <div class=\"elementParent\">\r\n        <div class=\"elementHouse\">\r\n          <div>\r\n            <div>\r\n              <div class=\"elementQuestionWrapper\">\r\n                <div class=\"elementQuestionContents\">\r\n                  <div class=\"questionDecorationWrapper\">\r\n                    <span class=\"questionDecoration\">\r\n                      <div class=\"questionIcon\">\r\n                        <span>\r\n                          <i class=\"fa fa-tasks\"></i>\r\n                        </span>\r\n                      </div>\r\n                    </span>\r\n                  </div>\r\n                  <div class=\"questionTextWrapper\">\r\n                    <span style=\"font-size: 2rem;text-align: justify\">\r\n                      "
    + alias4(((helper = (helper = helpers.question || (depth0 != null ? depth0.question : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"question","hash":{},"data":data}) : helper)))
    + "<strong></strong>\r\n                    </span>\r\n                    <ul>\r\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.children : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                    </ul>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"childHouse\">\r\n              <div class=\"nextButtonWrapper\">\r\n                <div class=\"buttonInstruction\">\r\n                  Press <strong>Enter</strong> to continue\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </section>\r\n</div>";
},"useData":true});

/***/ }),

/***/ "./src/public/js/templates/response-canvas/submit.hbs":
/*!************************************************************!*\
  !*** ./src/public/js/templates/response-canvas/submit.hbs ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper;

  return "<div class=\"elementWrapper\" data-q-id=\""
    + container.escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"id","hash":{},"data":data}) : helper)))
    + "\" data-question=\"false\">\r\n  <section class=\"submitSection activeElement\">\r\n    <div class=\"submit\">\r\n      <div class=\"itemsWrapper\">\r\n        <div class=\"items\">\r\n          <div class=\"contentWrapper\">\r\n            <div class=\"contents\">\r\n              <button class=\"submitButton\" id=\"submitresponse\">\r\n                <div class=\"sc-gqjmRU jhOZlx\">Submit</div>\r\n              </button>\r\n              <div class=\"sc-chPdSV bmzmvX\" color=\"#4FB0AE\">\r\n                <div class=\"sc-kgoBCf kvyvll\">\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"validationWrapper inactiveValidation\" id=\"submit-error-msg\">\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </section>\r\n</div>\r\n";
},"useData":true});

/***/ }),

/***/ "./src/public/js/templates/response-canvas/take-picture.hbs":
/*!******************************************************************!*\
  !*** ./src/public/js/templates/response-canvas/take-picture.hbs ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class=\"picture\">\r\n  <div class=\"picture__feed\" style=\"position: relative;\">\r\n    <video class=\"picture__live\" id=\"livefeed\">Video stream</video>\r\n    <button class=\"picture__capture\" id=\"capture\">Take Photo</button>\r\n    <button class=\"video__toggleCam hidden-lg\" id=\"pToggleCam\" alt=\"Toggle Cam\"><i class=\"ion ion-ios-reverse-camera\"></i></button>\r\n  </div>\r\n  <canvas class=\"picture__canvas\" id=\"canvas\"></canvas>\r\n  <div class=\"picture__output des-content\">\r\n    <img alt=\"photo from camera\" class=\"picture__photo\" id=\"photo\" style=\"box-shadow:none;border:none;margin-bottom:10px\">\r\n  </div>\r\n</div>\r\n";
},"useData":true});

/***/ }),

/***/ "./src/public/js/templates/response-canvas/upload-indicator.hbs":
/*!**********************************************************************!*\
  !*** ./src/public/js/templates/response-canvas/upload-indicator.hbs ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var helper;

  return "  <div class=\"uploadingImgWrapper\">\r\n    <img src=\""
    + container.escapeExpression(((helper = (helper = helpers.imgUrl || (depth0 != null ? depth0.imgUrl : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"imgUrl","hash":{},"data":data}) : helper)))
    + "\" alt=\"upload status\" class=\"img\" />\r\n  </div>\r\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "  <p class=\"uploadInstruction\">Drag and drop</p>\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div>\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.imgUrl : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data})) != null ? stack1 : "")
    + "  <label for=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" class=\"inputLabel\">\r\n    "
    + alias4(((helper = (helper = helpers.labelText || (depth0 != null ? depth0.labelText : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"labelText","hash":{},"data":data}) : helper)))
    + "\r\n  </label>\r\n</div>";
},"useData":true});

/***/ }),

/***/ "./src/public/js/templates/response-canvas/uploaded-indicator.hbs":
/*!************************************************************************!*\
  !*** ./src/public/js/templates/response-canvas/uploaded-indicator.hbs ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var helper;

  return "  <div class=\"uploadedImgWrapper\">\r\n    <img src=\""
    + container.escapeExpression(((helper = (helper = helpers.imgUrl || (depth0 != null ? depth0.imgUrl : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"imgUrl","hash":{},"data":data}) : helper)))
    + "\" alt=\"upload status\" class=\"uploadedImg\" />\r\n  </div>\r\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "  <p class=\"uploadInstruction\">Drag and drop</p>\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div>\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.imgUrl : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data})) != null ? stack1 : "")
    + "  <label for=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" class=\"inputLabel\">\r\n    "
    + alias4(((helper = (helper = helpers.labelText || (depth0 != null ? depth0.labelText : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"labelText","hash":{},"data":data}) : helper)))
    + "\r\n  </label>\r\n</div>";
},"useData":true});

/***/ }),

/***/ "./src/public/js/templates/response-canvas/validation-msg.hbs":
/*!********************************************************************!*\
  !*** ./src/public/js/templates/response-canvas/validation-msg.hbs ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    return "    <p>\r\n      <i class=\"fa fa-arrow-right\"></i>\r\n      "
    + container.escapeExpression(container.lambda(depth0, depth0))
    + "\r\n    </p>\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"validation\">\r\n  <div class=\"validationText\">\r\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.messages : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "  </div>\r\n</div>";
},"useData":true});

/***/ }),

/***/ "./src/public/js/utils/countryStates.js":
/*!**********************************************!*\
  !*** ./src/public/js/utils/countryStates.js ***!
  \**********************************************/
/*! exports provided: countryStates */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "countryStates", function() { return countryStates; });
var countryStates = [{
  state: {
    name: "Abia State",
    id: 1,
    locals: [{
      name: "Aba South",
      id: 1
    }, {
      name: "Arochukwu",
      id: 2
    }, {
      name: "Bende",
      id: 3
    }, {
      name: "Ikwuano",
      id: 4
    }, {
      name: "Isiala Ngwa North",
      id: 5
    }, {
      name: "Isiala Ngwa South",
      id: 6
    }, {
      name: "Isuikwuato",
      id: 7
    }, {
      name: "Obi Ngwa",
      id: 8
    }, {
      name: "Ohafia",
      id: 9
    }, {
      name: "Osisioma",
      id: 10
    }, {
      name: "Ugwunagbo",
      id: 11
    }, {
      name: "Ukwa East",
      id: 12
    }, {
      name: "Ukwa West",
      id: 13
    }, {
      name: "Umuahia North",
      id: 14
    }, {
      name: "Umuahia South",
      id: 15
    }, {
      name: "Umu Nneochi",
      id: 16
    }]
  }
}, {
  state: {
    name: "Adamawa State",
    id: 2,
    locals: [{
      name: "Fufure",
      id: 1
    }, {
      name: "Ganye",
      id: 2
    }, {
      name: "Gayuk",
      id: 3
    }, {
      name: "Gombi",
      id: 4
    }, {
      name: "Grie",
      id: 5
    }, {
      name: "Hong",
      id: 6
    }, {
      name: "Jada",
      id: 7
    }, {
      name: "Lamurde",
      id: 8
    }, {
      name: "Madagali",
      id: 9
    }, {
      name: "Maiha",
      id: 10
    }, {
      name: "Mayo Belwa",
      id: 11
    }, {
      name: "Michika",
      id: 12
    }, {
      name: "Mubi North",
      id: 13
    }, {
      name: "Mubi South",
      id: 14
    }, {
      name: "Numan",
      id: 15
    }, {
      name: "Shelleng",
      id: 16
    }, {
      name: "Song",
      id: 17
    }, {
      name: "Toungo",
      id: 18
    }, {
      name: "Yola North",
      id: 19
    }, {
      name: "Yola South",
      id: 20
    }]
  }
}, {
  state: {
    name: "Akwa Ibom State",
    id: 3,
    locals: [{
      name: "Eastern Obolo",
      id: 1
    }, {
      name: "Eket",
      id: 2
    }, {
      name: "Esit Eket",
      id: 3
    }, {
      name: "Essien Udim",
      id: 4
    }, {
      name: "Etim Ekpo",
      id: 5
    }, {
      name: "Etinan",
      id: 6
    }, {
      name: "Ibeno",
      id: 7
    }, {
      name: "Ibesikpo Asutan",
      id: 8
    }, {
      name: "Ibiono-Ibom",
      id: 9
    }, {
      name: "Ika",
      id: 10
    }, {
      name: "Ikono",
      id: 11
    }, {
      name: "Ikot Abasi",
      id: 12
    }, {
      name: "Ikot Ekpene",
      id: 13
    }, {
      name: "Ini",
      id: 14
    }, {
      name: "Itu",
      id: 15
    }, {
      name: "Mbo",
      id: 16
    }, {
      name: "Mkpat-Enin",
      id: 17
    }, {
      name: "Nsit-Atai",
      id: 18
    }, {
      name: "Nsit-Ibom",
      id: 19
    }, {
      name: "Nsit-Ubium",
      id: 20
    }, {
      name: "Obot Akara",
      id: 21
    }, {
      name: "Okobo",
      id: 22
    }, {
      name: "Onna",
      id: 23
    }, {
      name: "Oron",
      id: 24
    }, {
      name: "Oruk Anam",
      id: 25
    }, {
      name: "Udung-Uko",
      id: 26
    }, {
      name: "Ukanafun",
      id: 27
    }, {
      name: "Uruan",
      id: 28
    }, {
      name: "Urue-Offong/Oruko",
      id: 29
    }, {
      name: "Uyo",
      id: 30
    }]
  }
}, {
  state: {
    name: "Anambra State",
    id: 4,
    locals: [{
      name: "Anambra East",
      id: 1
    }, {
      name: "Anambra West",
      id: 2
    }, {
      name: "Anaocha",
      id: 3
    }, {
      name: "Awka North",
      id: 4
    }, {
      name: "Awka South",
      id: 5
    }, {
      name: "Ayamelum",
      id: 6
    }, {
      name: "Dunukofia",
      id: 7
    }, {
      name: "Ekwusigo",
      id: 8
    }, {
      name: "Idemili North",
      id: 9
    }, {
      name: "Idemili South",
      id: 10
    }, {
      name: "Ihiala",
      id: 11
    }, {
      name: "Njikoka",
      id: 12
    }, {
      name: "Nnewi North",
      id: 13
    }, {
      name: "Nnewi South",
      id: 14
    }, {
      name: "Ogbaru",
      id: 15
    }, {
      name: "Onitsha North",
      id: 16
    }, {
      name: "Onitsha South",
      id: 17
    }, {
      name: "Orumba North",
      id: 18
    }, {
      name: "Orumba South",
      id: 19
    }, {
      name: "Oyi",
      id: 20
    }]
  }
}, {
  state: {
    name: "Bauchi State",
    id: 5,
    locals: [{
      name: "Bauchi",
      id: 1
    }, {
      name: "Bogoro",
      id: 2
    }, {
      name: "Damban",
      id: 3
    }, {
      name: "Darazo",
      id: 4
    }, {
      name: "Dass",
      id: 5
    }, {
      name: "Gamawa",
      id: 6
    }, {
      name: "Ganjuwa",
      id: 7
    }, {
      name: "Giade",
      id: 8
    }, {
      name: "Itas/Gadau",
      id: 9
    }, {
      name: "Jama'are",
      id: 10
    }, {
      name: "Katagum",
      id: 11
    }, {
      name: "Kirfi",
      id: 12
    }, {
      name: "Misau",
      id: 13
    }, {
      name: "Ningi",
      id: 14
    }, {
      name: "Shira",
      id: 15
    }, {
      name: "Tafawa Balewa",
      id: 16
    }, {
      name: "Toro",
      id: 17
    }, {
      name: "Warji",
      id: 18
    }, {
      name: "Zaki",
      id: 19
    }]
  }
}, {
  state: {
    name: "Bayelsa State",
    id: 6,
    locals: [{
      name: "Ekeremor",
      id: 1
    }, {
      name: "Kolokuma/Opokuma",
      id: 2
    }, {
      name: "Nembe",
      id: 3
    }, {
      name: "Ogbia",
      id: 4
    }, {
      name: "Sagbama",
      id: 5
    }, {
      name: "Southern Ijaw",
      id: 6
    }, {
      name: "Yenagoa",
      id: 7
    }]
  }
}, {
  state: {
    name: "Benue State",
    id: 7,
    locals: [{
      name: "Apa",
      id: 1
    }, {
      name: "Ado",
      id: 2
    }, {
      name: "Buruku",
      id: 3
    }, {
      name: "Gboko",
      id: 4
    }, {
      name: "Guma",
      id: 5
    }, {
      name: "Gwer East",
      id: 6
    }, {
      name: "Gwer West",
      id: 7
    }, {
      name: "Katsina-Ala",
      id: 8
    }, {
      name: "Konshisha",
      id: 9
    }, {
      name: "Kwande",
      id: 10
    }, {
      name: "Logo",
      id: 11
    }, {
      name: "Makurdi",
      id: 12
    }, {
      name: "Obi",
      id: 13
    }, {
      name: "Ogbadibo",
      id: 14
    }, {
      name: "Ohimini",
      id: 15
    }, {
      name: "Oju",
      id: 16
    }, {
      name: "Okpokwu",
      id: 17
    }, {
      name: "Oturkpo",
      id: 18
    }, {
      name: "Tarka",
      id: 19
    }, {
      name: "Ukum",
      id: 20
    }, {
      name: "Ushongo",
      id: 21
    }, {
      name: "Vandeikya",
      id: 22
    }]
  }
}, {
  state: {
    name: "Borno State",
    id: 8,
    locals: [{
      name: "Askira/Uba",
      id: 1
    }, {
      name: "Bama",
      id: 2
    }, {
      name: "Bayo",
      id: 3
    }, {
      name: "Biu",
      id: 4
    }, {
      name: "Chibok",
      id: 5
    }, {
      name: "Damboa",
      id: 6
    }, {
      name: "Dikwa",
      id: 7
    }, {
      name: "Gubio",
      id: 8
    }, {
      name: "Guzamala",
      id: 9
    }, {
      name: "Gwoza",
      id: 10
    }, {
      name: "Hawul",
      id: 11
    }, {
      name: "Jere",
      id: 12
    }, {
      name: "Kaga",
      id: 13
    }, {
      name: "Kala/Balge",
      id: 14
    }, {
      name: "Konduga",
      id: 15
    }, {
      name: "Kukawa",
      id: 16
    }, {
      name: "Kwaya Kusar",
      id: 17
    }, {
      name: "Mafa",
      id: 18
    }, {
      name: "Magumeri",
      id: 19
    }, {
      name: "Maiduguri",
      id: 20
    }, {
      name: "Marte",
      id: 21
    }, {
      name: "Mobbar",
      id: 22
    }, {
      name: "Monguno",
      id: 23
    }, {
      name: "Ngala",
      id: 24
    }, {
      name: "Nganzai",
      id: 25
    }, {
      name: "Shani",
      id: 26
    }]
  }
}, {
  state: {
    name: "Cross River State",
    id: 9,
    locals: [{
      name: "Akamkpa",
      id: 1
    }, {
      name: "Akpabuyo",
      id: 2
    }, {
      name: "Bakassi",
      id: 3
    }, {
      name: "Bekwarra",
      id: 4
    }, {
      name: "Biase",
      id: 5
    }, {
      name: "Boki",
      id: 6
    }, {
      name: "Calabar Municipal",
      id: 7
    }, {
      name: "Calabar South",
      id: 8
    }, {
      name: "Etung",
      id: 9
    }, {
      name: "Ikom",
      id: 10
    }, {
      name: "Obanliku",
      id: 11
    }, {
      name: "Obubra",
      id: 12
    }, {
      name: "Obudu",
      id: 13
    }, {
      name: "Odukpani",
      id: 14
    }, {
      name: "Ogoja",
      id: 15
    }, {
      name: "Yakuur",
      id: 16
    }, {
      name: "Yala",
      id: 17
    }]
  }
}, {
  state: {
    name: "Delta State",
    id: 10,
    locals: [{
      name: "Aniocha South",
      id: 1
    }, {
      name: "Bomadi",
      id: 2
    }, {
      name: "Burutu",
      id: 3
    }, {
      name: "Ethiope East",
      id: 4
    }, {
      name: "Ethiope West",
      id: 5
    }, {
      name: "Ika North East",
      id: 6
    }, {
      name: "Ika South",
      id: 7
    }, {
      name: "Isoko North",
      id: 8
    }, {
      name: "Isoko South",
      id: 9
    }, {
      name: "Ndokwa East",
      id: 10
    }, {
      name: "Ndokwa West",
      id: 11
    }, {
      name: "Okpe",
      id: 12
    }, {
      name: "Oshimili North",
      id: 13
    }, {
      name: "Oshimili South",
      id: 14
    }, {
      name: "Patani",
      id: 15
    }, {
      name: "Sapele",
      id: 16
    }, {
      name: "Udu",
      id: 17
    }, {
      name: "Ughelli North",
      id: 18
    }, {
      name: "Ughelli South",
      id: 19
    }, {
      name: "Ukwuani",
      id: 20
    }, {
      name: "Uvwie",
      id: 21
    }, {
      name: "Warri North",
      id: 22
    }, {
      name: "Warri South",
      id: 23
    }, {
      name: "Warri South West",
      id: 24
    }]
  }
}, {
  state: {
    name: "Ebonyi State",
    id: 11,
    locals: [{
      name: "Afikpo North",
      id: 1
    }, {
      name: "Afikpo South",
      id: 2
    }, {
      name: "Ebonyi",
      id: 3
    }, {
      name: "Ezza North",
      id: 4
    }, {
      name: "Ezza South",
      id: 5
    }, {
      name: "Ikwo",
      id: 6
    }, {
      name: "Ishielu",
      id: 7
    }, {
      name: "Ivo",
      id: 8
    }, {
      name: "Izzi",
      id: 9
    }, {
      name: "Ohaozara",
      id: 10
    }, {
      name: "Ohaukwu",
      id: 11
    }, {
      name: "Onicha",
      id: 12
    }]
  }
}, {
  state: {
    name: "Edo State",
    id: 12,
    locals: [{
      name: "Egor",
      id: 1
    }, {
      name: "Esan Central",
      id: 2
    }, {
      name: "Esan North-East",
      id: 3
    }, {
      name: "Esan South-East",
      id: 4
    }, {
      name: "Esan West",
      id: 5
    }, {
      name: "Etsako Central",
      id: 6
    }, {
      name: "Etsako East",
      id: 7
    }, {
      name: "Etsako West",
      id: 8
    }, {
      name: "Igueben",
      id: 9
    }, {
      name: "Ikpoba Okha",
      id: 10
    }, {
      name: "Orhionmwon",
      id: 11
    }, {
      name: "Oredo",
      id: 12
    }, {
      name: "Ovia North-East",
      id: 13
    }, {
      name: "Ovia South-West",
      id: 14
    }, {
      name: "Owan East",
      id: 15
    }, {
      name: "Owan West",
      id: 16
    }, {
      name: "Uhunmwonde",
      id: 17
    }]
  }
}, {
  state: {
    name: "Ekiti State",
    id: 13,
    locals: [{
      name: "Efon",
      id: 1
    }, {
      name: "Ekiti East",
      id: 2
    }, {
      name: "Ekiti South-West",
      id: 3
    }, {
      name: "Ekiti West",
      id: 4
    }, {
      name: "Emure",
      id: 5
    }, {
      name: "Gbonyin",
      id: 6
    }, {
      name: "Ido Osi",
      id: 7
    }, {
      name: "Ijero",
      id: 8
    }, {
      name: "Ikere",
      id: 9
    }, {
      name: "Ikole",
      id: 10
    }, {
      name: "Ilejemeje",
      id: 11
    }, {
      name: "Irepodun/Ifelodun",
      id: 12
    }, {
      name: "Ise/Orun",
      id: 13
    }, {
      name: "Moba",
      id: 14
    }, {
      name: "Oye",
      id: 15
    }]
  }
}, {
  state: {
    name: "Enugu State",
    id: 14,
    locals: [{
      name: "Awgu",
      id: 1
    }, {
      name: "Enugu East",
      id: 2
    }, {
      name: "Enugu North",
      id: 3
    }, {
      name: "Enugu South",
      id: 4
    }, {
      name: "Ezeagu",
      id: 5
    }, {
      name: "Igbo Etiti",
      id: 6
    }, {
      name: "Igbo Eze North",
      id: 7
    }, {
      name: "Igbo Eze South",
      id: 8
    }, {
      name: "Isi Uzo",
      id: 9
    }, {
      name: "Nkanu East",
      id: 10
    }, {
      name: "Nkanu West",
      id: 11
    }, {
      name: "Nsukka",
      id: 12
    }, {
      name: "Oji River",
      id: 13
    }, {
      name: "Udenu",
      id: 14
    }, {
      name: "Udi",
      id: 15
    }, {
      name: "Uzo Uwani",
      id: 16
    }]
  }
}, {
  state: {
    name: "FCT",
    id: 15,
    locals: [{
      name: "Bwari",
      id: 1
    }, {
      name: "Gwagwalada",
      id: 2
    }, {
      name: "Kuje",
      id: 3
    }, {
      name: "Kwali",
      id: 4
    }, {
      name: "Municipal Area Council",
      id: 5
    }]
  }
}, {
  state: {
    name: "Gombe State",
    id: 16,
    locals: [{
      name: "Balanga",
      id: 1
    }, {
      name: "Billiri",
      id: 2
    }, {
      name: "Dukku",
      id: 3
    }, {
      name: "Funakaye",
      id: 4
    }, {
      name: "Gombe",
      id: 5
    }, {
      name: "Kaltungo",
      id: 6
    }, {
      name: "Kwami",
      id: 7
    }, {
      name: "Nafada",
      id: 8
    }, {
      name: "Shongom",
      id: 9
    }, {
      name: "Yamaltu/Deba",
      id: 10
    }]
  }
}, {
  state: {
    name: "Imo State",
    id: 17,
    locals: [{
      name: "Ahiazu Mbaise",
      id: 1
    }, {
      name: "Ehime Mbano",
      id: 2
    }, {
      name: "Ezinihitte",
      id: 3
    }, {
      name: "Ideato North",
      id: 4
    }, {
      name: "Ideato South",
      id: 5
    }, {
      name: "Ihitte/Uboma",
      id: 6
    }, {
      name: "Ikeduru",
      id: 7
    }, {
      name: "Isiala Mbano",
      id: 8
    }, {
      name: "Isu",
      id: 9
    }, {
      name: "Mbaitoli",
      id: 10
    }, {
      name: "Ngor Okpala",
      id: 11
    }, {
      name: "Njaba",
      id: 12
    }, {
      name: "Nkwerre",
      id: 13
    }, {
      name: "Nwangele",
      id: 14
    }, {
      name: "Obowo",
      id: 15
    }, {
      name: "Oguta",
      id: 16
    }, {
      name: "Ohaji/Egbema",
      id: 17
    }, {
      name: "Okigwe",
      id: 18
    }, {
      name: "Orlu",
      id: 19
    }, {
      name: "Orsu",
      id: 20
    }, {
      name: "Oru East",
      id: 21
    }, {
      name: "Oru West",
      id: 22
    }, {
      name: "Owerri Municipal",
      id: 23
    }, {
      name: "Owerri North",
      id: 24
    }, {
      name: "Owerri West",
      id: 25
    }, {
      name: "Unuimo",
      id: 26
    }]
  }
}, {
  state: {
    name: "Jigawa State",
    id: 18,
    locals: [{
      name: "Babura",
      id: 1
    }, {
      name: "Biriniwa",
      id: 2
    }, {
      name: "Birnin Kudu",
      id: 3
    }, {
      name: "Buji",
      id: 4
    }, {
      name: "Dutse",
      id: 5
    }, {
      name: "Gagarawa",
      id: 6
    }, {
      name: "Garki",
      id: 7
    }, {
      name: "Gumel",
      id: 8
    }, {
      name: "Guri",
      id: 9
    }, {
      name: "Gwaram",
      id: 10
    }, {
      name: "Gwiwa",
      id: 11
    }, {
      name: "Hadejia",
      id: 12
    }, {
      name: "Jahun",
      id: 13
    }, {
      name: "Kafin Hausa",
      id: 14
    }, {
      name: "Kazaure",
      id: 15
    }, {
      name: "Kiri Kasama",
      id: 16
    }, {
      name: "Kiyawa",
      id: 17
    }, {
      name: "Kaugama",
      id: 18
    }, {
      name: "Maigatari",
      id: 19
    }, {
      name: "Malam Madori",
      id: 20
    }, {
      name: "Miga",
      id: 21
    }, {
      name: "Ringim",
      id: 22
    }, {
      name: "Roni",
      id: 23
    }, {
      name: "Sule Tankarkar",
      id: 24
    }, {
      name: "Taura",
      id: 25
    }, {
      name: "Yankwashi",
      id: 26
    }]
  }
}, {
  state: {
    name: "Kaduna State",
    id: 19,
    locals: [{
      name: "Chikun",
      id: 1
    }, {
      name: "Giwa",
      id: 2
    }, {
      name: "Igabi",
      id: 3
    }, {
      name: "Ikara",
      id: 4
    }, {
      name: "Jaba",
      id: 5
    }, {
      name: "Jema'a",
      id: 6
    }, {
      name: "Kachia",
      id: 7
    }, {
      name: "Kaduna North",
      id: 8
    }, {
      name: "Kaduna South",
      id: 9
    }, {
      name: "Kagarko",
      id: 10
    }, {
      name: "Kajuru",
      id: 11
    }, {
      name: "Kaura",
      id: 12
    }, {
      name: "Kauru",
      id: 13
    }, {
      name: "Kubau",
      id: 14
    }, {
      name: "Kudan",
      id: 15
    }, {
      name: "Lere",
      id: 16
    }, {
      name: "Makarfi",
      id: 17
    }, {
      name: "Sabon Gari",
      id: 18
    }, {
      name: "Sanga",
      id: 19
    }, {
      name: "Soba",
      id: 20
    }, {
      name: "Zangon Kataf",
      id: 21
    }, {
      name: "Zaria",
      id: 22
    }]
  }
}, {
  state: {
    name: "Kano State",
    id: 20,
    locals: [{
      name: "Albasu",
      id: 1
    }, {
      name: "Bagwai",
      id: 2
    }, {
      name: "Bebeji",
      id: 3
    }, {
      name: "Bichi",
      id: 4
    }, {
      name: "Bunkure",
      id: 5
    }, {
      name: "Dala",
      id: 6
    }, {
      name: "Dambatta",
      id: 7
    }, {
      name: "Dawakin Kudu",
      id: 8
    }, {
      name: "Dawakin Tofa",
      id: 9
    }, {
      name: "Doguwa",
      id: 10
    }, {
      name: "Fagge",
      id: 11
    }, {
      name: "Gabasawa",
      id: 12
    }, {
      name: "Garko",
      id: 13
    }, {
      name: "Garun Mallam",
      id: 14
    }, {
      name: "Gaya",
      id: 15
    }, {
      name: "Gezawa",
      id: 16
    }, {
      name: "Gwale",
      id: 17
    }, {
      name: "Gwarzo",
      id: 18
    }, {
      name: "Kabo",
      id: 19
    }, {
      name: "Kano Municipal",
      id: 20
    }, {
      name: "Karaye",
      id: 21
    }, {
      name: "Kibiya",
      id: 22
    }, {
      name: "Kiru",
      id: 23
    }, {
      name: "Kumbotso",
      id: 24
    }, {
      name: "Kunchi",
      id: 25
    }, {
      name: "Kura",
      id: 26
    }, {
      name: "Madobi",
      id: 27
    }, {
      name: "Makoda",
      id: 28
    }, {
      name: "Minjibir",
      id: 29
    }, {
      name: "Nasarawa",
      id: 30
    }, {
      name: "Rano",
      id: 31
    }, {
      name: "Rimin Gado",
      id: 32
    }, {
      name: "Rogo",
      id: 33
    }, {
      name: "Shanono",
      id: 34
    }, {
      name: "Sumaila",
      id: 35
    }, {
      name: "Takai",
      id: 36
    }, {
      name: "Tarauni",
      id: 37
    }, {
      name: "Tofa",
      id: 38
    }, {
      name: "Tsanyawa",
      id: 39
    }, {
      name: "Tudun Wada",
      id: 40
    }, {
      name: "Ungogo",
      id: 41
    }, {
      name: "Warawa",
      id: 42
    }, {
      name: "Wudil",
      id: 43
    }]
  }
}, {
  state: {
    name: "Katsina State",
    id: 21,
    locals: [{
      name: "Batagarawa",
      id: 1
    }, {
      name: "Batsari",
      id: 2
    }, {
      name: "Baure",
      id: 3
    }, {
      name: "Bindawa",
      id: 4
    }, {
      name: "Charanchi",
      id: 5
    }, {
      name: "Dandume",
      id: 6
    }, {
      name: "Danja",
      id: 7
    }, {
      name: "Dan Musa",
      id: 8
    }, {
      name: "Daura",
      id: 9
    }, {
      name: "Dutsi",
      id: 10
    }, {
      name: "Dutsin Ma",
      id: 11
    }, {
      name: "Faskari",
      id: 12
    }, {
      name: "Funtua",
      id: 13
    }, {
      name: "Ingawa",
      id: 14
    }, {
      name: "Jibia",
      id: 15
    }, {
      name: "Kafur",
      id: 16
    }, {
      name: "Kaita",
      id: 17
    }, {
      name: "Kankara",
      id: 18
    }, {
      name: "Kankia",
      id: 19
    }, {
      name: "Katsina",
      id: 20
    }, {
      name: "Kurfi",
      id: 21
    }, {
      name: "Kusada",
      id: 22
    }, {
      name: "Mai'Adua",
      id: 23
    }, {
      name: "Malumfashi",
      id: 24
    }, {
      name: "Mani",
      id: 25
    }, {
      name: "Mashi",
      id: 26
    }, {
      name: "Matazu",
      id: 27
    }, {
      name: "Musawa",
      id: 28
    }, {
      name: "Rimi",
      id: 29
    }, {
      name: "Sabuwa",
      id: 30
    }, {
      name: "Safana",
      id: 31
    }, {
      name: "Sandamu",
      id: 32
    }, {
      name: "Zango",
      id: 33
    }]
  }
}, {
  state: {
    name: "Kebbi State",
    id: 22,
    locals: [{
      name: "Arewa Dandi",
      id: 1
    }, {
      name: "Argungu",
      id: 2
    }, {
      name: "Augie",
      id: 3
    }, {
      name: "Bagudo",
      id: 4
    }, {
      name: "Birnin Kebbi",
      id: 5
    }, {
      name: "Bunza",
      id: 6
    }, {
      name: "Dandi",
      id: 7
    }, {
      name: "Fakai",
      id: 8
    }, {
      name: "Gwandu",
      id: 9
    }, {
      name: "Jega",
      id: 10
    }, {
      name: "Kalgo",
      id: 11
    }, {
      name: "Koko/Besse",
      id: 12
    }, {
      name: "Maiyama",
      id: 13
    }, {
      name: "Ngaski",
      id: 14
    }, {
      name: "Sakaba",
      id: 15
    }, {
      name: "Shanga",
      id: 16
    }, {
      name: "Suru",
      id: 17
    }, {
      name: "Wasagu/Danko",
      id: 18
    }, {
      name: "Yauri",
      id: 19
    }, {
      name: "Zuru",
      id: 20
    }]
  }
}, {
  state: {
    name: "Kogi State",
    id: 23,
    locals: [{
      name: "Ajaokuta",
      id: 1
    }, {
      name: "Ankpa",
      id: 2
    }, {
      name: "Bassa",
      id: 3
    }, {
      name: "Dekina",
      id: 4
    }, {
      name: "Ibaji",
      id: 5
    }, {
      name: "Idah",
      id: 6
    }, {
      name: "Igalamela Odolu",
      id: 7
    }, {
      name: "Ijumu",
      id: 8
    }, {
      name: "Kabba/Bunu",
      id: 9
    }, {
      name: "Kogi",
      id: 10
    }, {
      name: "Lokoja",
      id: 11
    }, {
      name: "Mopa Muro",
      id: 12
    }, {
      name: "Ofu",
      id: 13
    }, {
      name: "Ogori/Magongo",
      id: 14
    }, {
      name: "Okehi",
      id: 15
    }, {
      name: "Okene",
      id: 16
    }, {
      name: "Olamaboro",
      id: 17
    }, {
      name: "Omala",
      id: 18
    }, {
      name: "Yagba East",
      id: 19
    }, {
      name: "Yagba West",
      id: 20
    }]
  }
}, {
  state: {
    name: "Kwara State",
    id: 24,
    locals: [{
      name: "Baruten",
      id: 1
    }, {
      name: "Edu",
      id: 2
    }, {
      name: "Ekiti",
      id: 3
    }, {
      name: "Ifelodun",
      id: 4
    }, {
      name: "Ilorin East",
      id: 5
    }, {
      name: "Ilorin South",
      id: 6
    }, {
      name: "Ilorin West",
      id: 7
    }, {
      name: "Irepodun",
      id: 8
    }, {
      name: "Isin",
      id: 9
    }, {
      name: "Kaiama",
      id: 10
    }, {
      name: "Moro",
      id: 11
    }, {
      name: "Offa",
      id: 12
    }, {
      name: "Oke Ero",
      id: 13
    }, {
      name: "Oyun",
      id: 14
    }, {
      name: "Pategi",
      id: 15
    }]
  }
}, {
  state: {
    name: "Lagos State",
    id: 25,
    locals: [{
      name: "Ajeromi-Ifelodun",
      id: 1
    }, {
      name: "Alimosho",
      id: 2
    }, {
      name: "Amuwo-Odofin",
      id: 3
    }, {
      name: "Apapa",
      id: 4
    }, {
      name: "Badagry",
      id: 5
    }, {
      name: "Epe",
      id: 6
    }, {
      name: "Eti Osa",
      id: 7
    }, {
      name: "Ibeju-Lekki",
      id: 8
    }, {
      name: "Ifako-Ijaiye",
      id: 9
    }, {
      name: "Ikeja",
      id: 10
    }, {
      name: "Ikorodu",
      id: 11
    }, {
      name: "Kosofe",
      id: 12
    }, {
      name: "Lagos Island",
      id: 13
    }, {
      name: "Lagos Mainland",
      id: 14
    }, {
      name: "Mushin",
      id: 15
    }, {
      name: "Ojo",
      id: 16
    }, {
      name: "Oshodi-Isolo",
      id: 17
    }, {
      name: "Shomolu",
      id: 18
    }, {
      name: "Surulere",
      id: 19
    }]
  }
}, {
  state: {
    name: "Nasarawa State",
    id: 26,
    locals: [{
      name: "Awe",
      id: 1
    }, {
      name: "Doma",
      id: 2
    }, {
      name: "Karu",
      id: 3
    }, {
      name: "Keana",
      id: 4
    }, {
      name: "Keffi",
      id: 5
    }, {
      name: "Kokona",
      id: 6
    }, {
      name: "Lafia",
      id: 7
    }, {
      name: "Nasarawa",
      id: 8
    }, {
      name: "Nasarawa Egon",
      id: 9
    }, {
      name: "Obi",
      id: 10
    }, {
      name: "Toto",
      id: 11
    }, {
      name: "Wamba",
      id: 12
    }]
  }
}, {
  state: {
    name: "Niger State",
    id: 27,
    locals: [{
      name: "Agwara",
      id: 1
    }, {
      name: "Bida",
      id: 2
    }, {
      name: "Borgu",
      id: 3
    }, {
      name: "Bosso",
      id: 4
    }, {
      name: "Chanchaga",
      id: 5
    }, {
      name: "Edati",
      id: 6
    }, {
      name: "Gbako",
      id: 7
    }, {
      name: "Gurara",
      id: 8
    }, {
      name: "Katcha",
      id: 9
    }, {
      name: "Kontagora",
      id: 10
    }, {
      name: "Lapai",
      id: 11
    }, {
      name: "Lavun",
      id: 12
    }, {
      name: "Magama",
      id: 13
    }, {
      name: "Mariga",
      id: 14
    }, {
      name: "Mashegu",
      id: 15
    }, {
      name: "Mokwa",
      id: 16
    }, {
      name: "Moya",
      id: 17
    }, {
      name: "Paikoro",
      id: 18
    }, {
      name: "Rafi",
      id: 19
    }, {
      name: "Rijau",
      id: 20
    }, {
      name: "Shiroro",
      id: 21
    }, {
      name: "Suleja",
      id: 22
    }, {
      name: "Tafa",
      id: 23
    }, {
      name: "Wushishi",
      id: 24
    }]
  }
}, {
  state: {
    name: "Ogun State",
    id: 28,
    locals: [{
      name: "Abeokuta South",
      id: 1
    }, {
      name: "Ado-Odo/Ota",
      id: 2
    }, {
      name: "Egbado North",
      id: 3
    }, {
      name: "Egbado South",
      id: 4
    }, {
      name: "Ewekoro",
      id: 5
    }, {
      name: "Ifo",
      id: 6
    }, {
      name: "Ijebu East",
      id: 7
    }, {
      name: "Ijebu North",
      id: 8
    }, {
      name: "Ijebu North East",
      id: 9
    }, {
      name: "Ijebu Ode",
      id: 10
    }, {
      name: "Ikenne",
      id: 11
    }, {
      name: "Imeko Afon",
      id: 12
    }, {
      name: "Ipokia",
      id: 13
    }, {
      name: "Obafemi Owode",
      id: 14
    }, {
      name: "Odeda",
      id: 15
    }, {
      name: "Odogbolu",
      id: 16
    }, {
      name: "Ogun Waterside",
      id: 17
    }, {
      name: "Remo North",
      id: 18
    }, {
      name: "Shagamu",
      id: 19
    }]
  }
}, {
  state: {
    name: "Ondo State",
    id: 29,
    locals: [{
      name: "Akoko North-West",
      id: 1
    }, {
      name: "Akoko South-West",
      id: 2
    }, {
      name: "Akoko South-East",
      id: 3
    }, {
      name: "Akure North",
      id: 4
    }, {
      name: "Akure South",
      id: 5
    }, {
      name: "Ese Odo",
      id: 6
    }, {
      name: "Idanre",
      id: 7
    }, {
      name: "Ifedore",
      id: 8
    }, {
      name: "Ilaje",
      id: 9
    }, {
      name: "Ile Oluji/Okeigbo",
      id: 10
    }, {
      name: "Irele",
      id: 11
    }, {
      name: "Odigbo",
      id: 12
    }, {
      name: "Okitipupa",
      id: 13
    }, {
      name: "Ondo East",
      id: 14
    }, {
      name: "Ondo West",
      id: 15
    }, {
      name: "Ose",
      id: 16
    }, {
      name: "Owo",
      id: 17
    }]
  }
}, {
  state: {
    name: "Osun State",
    id: 30,
    locals: [{
      name: "Atakunmosa West",
      id: 1
    }, {
      name: "Aiyedaade",
      id: 2
    }, {
      name: "Aiyedire",
      id: 3
    }, {
      name: "Boluwaduro",
      id: 4
    }, {
      name: "Boripe",
      id: 5
    }, {
      name: "Ede North",
      id: 6
    }, {
      name: "Ede South",
      id: 7
    }, {
      name: "Ife Central",
      id: 8
    }, {
      name: "Ife East",
      id: 9
    }, {
      name: "Ife North",
      id: 10
    }, {
      name: "Ife South",
      id: 11
    }, {
      name: "Egbedore",
      id: 12
    }, {
      name: "Ejigbo",
      id: 13
    }, {
      name: "Ifedayo",
      id: 14
    }, {
      name: "Ifelodun",
      id: 15
    }, {
      name: "Ila",
      id: 16
    }, {
      name: "Ilesa East",
      id: 17
    }, {
      name: "Ilesa West",
      id: 18
    }, {
      name: "Irepodun",
      id: 19
    }, {
      name: "Irewole",
      id: 20
    }, {
      name: "Isokan",
      id: 21
    }, {
      name: "Iwo",
      id: 22
    }, {
      name: "Obokun",
      id: 23
    }, {
      name: "Odo Otin",
      id: 24
    }, {
      name: "Ola Oluwa",
      id: 25
    }, {
      name: "Olorunda",
      id: 26
    }, {
      name: "Oriade",
      id: 27
    }, {
      name: "Orolu",
      id: 28
    }, {
      name: "Osogbo",
      id: 29
    }]
  }
}, {
  state: {
    name: "Oyo State",
    id: 31,
    locals: [{
      name: "Akinyele",
      id: 1
    }, {
      name: "Atiba",
      id: 2
    }, {
      name: "Atisbo",
      id: 3
    }, {
      name: "Egbeda",
      id: 4
    }, {
      name: "Ibadan North",
      id: 5
    }, {
      name: "Ibadan North-East",
      id: 6
    }, {
      name: "Ibadan North-West",
      id: 7
    }, {
      name: "Ibadan South-East",
      id: 8
    }, {
      name: "Ibadan South-West",
      id: 9
    }, {
      name: "Ibarapa Central",
      id: 10
    }, {
      name: "Ibarapa East",
      id: 11
    }, {
      name: "Ibarapa North",
      id: 12
    }, {
      name: "Ido",
      id: 13
    }, {
      name: "Irepo",
      id: 14
    }, {
      name: "Iseyin",
      id: 15
    }, {
      name: "Itesiwaju",
      id: 16
    }, {
      name: "Iwajowa",
      id: 17
    }, {
      name: "Kajola",
      id: 18
    }, {
      name: "Lagelu",
      id: 19
    }, {
      name: "Ogbomosho North",
      id: 20
    }, {
      name: "Ogbomosho South",
      id: 21
    }, {
      name: "Ogo Oluwa",
      id: 22
    }, {
      name: "Olorunsogo",
      id: 23
    }, {
      name: "Oluyole",
      id: 24
    }, {
      name: "Ona Ara",
      id: 25
    }, {
      name: "Orelope",
      id: 26
    }, {
      name: "Ori Ire",
      id: 27
    }, {
      name: "Oyo",
      id: 28
    }, {
      name: "Oyo East",
      id: 29
    }, {
      name: "Saki East",
      id: 30
    }, {
      name: "Saki West",
      id: 31
    }, {
      name: "Surulere",
      id: 32
    }]
  }
}, {
  state: {
    name: "Plateau State",
    id: 32,
    locals: [{
      name: "Barkin Ladi",
      id: 1
    }, {
      name: "Bassa",
      id: 2
    }, {
      name: "Jos East",
      id: 3
    }, {
      name: "Jos North",
      id: 4
    }, {
      name: "Jos South",
      id: 5
    }, {
      name: "Kanam",
      id: 6
    }, {
      name: "Kanke",
      id: 7
    }, {
      name: "Langtang South",
      id: 8
    }, {
      name: "Langtang North",
      id: 9
    }, {
      name: "Mangu",
      id: 10
    }, {
      name: "Mikang",
      id: 11
    }, {
      name: "Pankshin",
      id: 12
    }, {
      name: "Qua'an Pan",
      id: 13
    }, {
      name: "Riyom",
      id: 14
    }, {
      name: "Shendam",
      id: 15
    }, {
      name: "Wase",
      id: 16
    }]
  }
}, {
  state: {
    name: "Rivers State",
    id: 33,
    locals: [{
      name: "Ahoada East",
      id: 1
    }, {
      name: "Ahoada West",
      id: 2
    }, {
      name: "Akuku-Toru",
      id: 3
    }, {
      name: "Andoni",
      id: 4
    }, {
      name: "Asari-Toru",
      id: 5
    }, {
      name: "Bonny",
      id: 6
    }, {
      name: "Degema",
      id: 7
    }, {
      name: "Eleme",
      id: 8
    }, {
      name: "Emuoha",
      id: 9
    }, {
      name: "Etche",
      id: 10
    }, {
      name: "Gokana",
      id: 11
    }, {
      name: "Ikwerre",
      id: 12
    }, {
      name: "Khana",
      id: 13
    }, {
      name: "Obio/Akpor",
      id: 14
    }, {
      name: "Ogba/Egbema/Ndoni",
      id: 15
    }, {
      name: "Ogu/Bolo",
      id: 16
    }, {
      name: "Okrika",
      id: 17
    }, {
      name: "Omuma",
      id: 18
    }, {
      name: "Opobo/Nkoro",
      id: 19
    }, {
      name: "Oyigbo",
      id: 20
    }, {
      name: "Port Harcourt",
      id: 21
    }, {
      name: "Tai",
      id: 22
    }]
  }
}, {
  state: {
    name: "Sokoto State",
    id: 34,
    locals: [{
      name: "Bodinga",
      id: 1
    }, {
      name: "Dange Shuni",
      id: 2
    }, {
      name: "Gada",
      id: 3
    }, {
      name: "Goronyo",
      id: 4
    }, {
      name: "Gudu",
      id: 5
    }, {
      name: "Gwadabawa",
      id: 6
    }, {
      name: "Illela",
      id: 7
    }, {
      name: "Isa",
      id: 8
    }, {
      name: "Kebbe",
      id: 9
    }, {
      name: "Kware",
      id: 10
    }, {
      name: "Rabah",
      id: 11
    }, {
      name: "Sabon Birni",
      id: 12
    }, {
      name: "Shagari",
      id: 13
    }, {
      name: "Silame",
      id: 14
    }, {
      name: "Sokoto North",
      id: 15
    }, {
      name: "Sokoto South",
      id: 16
    }, {
      name: "Tambuwal",
      id: 17
    }, {
      name: "Tangaza",
      id: 18
    }, {
      name: "Tureta",
      id: 19
    }, {
      name: "Wamako",
      id: 20
    }, {
      name: "Wurno",
      id: 21
    }, {
      name: "Yabo",
      id: 22
    }]
  }
}, {
  state: {
    name: "Taraba State",
    id: 35,
    locals: [{
      name: "Bali",
      id: 1
    }, {
      name: "Donga",
      id: 2
    }, {
      name: "Gashaka",
      id: 3
    }, {
      name: "Gassol",
      id: 4
    }, {
      name: "Ibi",
      id: 5
    }, {
      name: "Jalingo",
      id: 6
    }, {
      name: "Karim Lamido",
      id: 7
    }, {
      name: "Kumi",
      id: 8
    }, {
      name: "Lau",
      id: 9
    }, {
      name: "Sardauna",
      id: 10
    }, {
      name: "Takum",
      id: 11
    }, {
      name: "Ussa",
      id: 12
    }, {
      name: "Wukari",
      id: 13
    }, {
      name: "Yorro",
      id: 14
    }, {
      name: "Zing",
      id: 15
    }]
  }
}, {
  state: {
    name: "Yobe State",
    id: 36,
    locals: [{
      name: "Bursari",
      id: 1
    }, {
      name: "Damaturu",
      id: 2
    }, {
      name: "Fika",
      id: 3
    }, {
      name: "Fune",
      id: 4
    }, {
      name: "Geidam",
      id: 5
    }, {
      name: "Gujba",
      id: 6
    }, {
      name: "Gulani",
      id: 7
    }, {
      name: "Jakusko",
      id: 8
    }, {
      name: "Karasuwa",
      id: 9
    }, {
      name: "Machina",
      id: 10
    }, {
      name: "Nangere",
      id: 11
    }, {
      name: "Nguru",
      id: 12
    }, {
      name: "Potiskum",
      id: 13
    }, {
      name: "Tarmuwa",
      id: 14
    }, {
      name: "Yunusari",
      id: 15
    }, {
      name: "Yusufari",
      id: 16
    }]
  }
}, {
  state: {
    name: "Zamfara State",
    id: 37,
    locals: [{
      name: "Bakura",
      id: 1
    }, {
      name: "Birnin Magaji/Kiyaw",
      id: 2
    }, {
      name: "Bukkuyum",
      id: 3
    }, {
      name: "Bungudu",
      id: 4
    }, {
      name: "Gummi",
      id: 5
    }, {
      name: "Gusau",
      id: 6
    }, {
      name: "Kaura Namoda",
      id: 7
    }, {
      name: "Maradun",
      id: 8
    }, {
      name: "Maru",
      id: 9
    }, {
      name: "Shinkafi",
      id: 10
    }, {
      name: "Talata Mafara",
      id: 11
    }, {
      name: "Chafe",
      id: 12
    }, {
      name: "Zurmi",
      id: 13
    }]
  }
}];

/***/ }),

/***/ "./src/public/js/utils/helpers.js":
/*!****************************************!*\
  !*** ./src/public/js/utils/helpers.js ***!
  \****************************************/
/*! exports provided: capitalizeFirstLatter, chunkData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "capitalizeFirstLatter", function() { return capitalizeFirstLatter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "chunkData", function() { return chunkData; });
var capitalizeFirstLatter = function capitalizeFirstLatter(s) {
  return s[0].toUpperCase() + s.slice(1);
};
var chunkData = function chunkData(arr) {
  var size = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 3;
  var result = [];
  var currentChunk = [];
  arr.forEach(function (item) {
    if (currentChunk.length === size) {
      result.push(currentChunk);
      currentChunk = [];
    }

    currentChunk.push(item);
  });
  result.push(currentChunk);
  return result;
};

/***/ }),

/***/ "./src/public/js/utils/hexToHSL.js":
/*!*****************************************!*\
  !*** ./src/public/js/utils/hexToHSL.js ***!
  \*****************************************/
/*! exports provided: hexToHSL */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "hexToHSL", function() { return hexToHSL; });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var hexToHSL =
/*#__PURE__*/
function () {
  function hexToHSL(hex) {
    _classCallCheck(this, hexToHSL);

    this.hex = hex;
    this.H_Value = "";
    this.S_Value = "";
    this.L_Value = "";
    this.init();
  }

  _createClass(hexToHSL, [{
    key: "init",
    value: function init() {
      var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(this.hex);
      var r = parseInt(result[1], 16);
      var g = parseInt(result[2], 16);
      var b = parseInt(result[3], 16);
      r /= 255, g /= 255, b /= 255;
      var max = Math.max(r, g, b),
          min = Math.min(r, g, b);
      var h,
          s,
          l = (max + min) / 2;

      if (max == min) {
        h = s = 0; // achromatic
      } else {
        var d = max - min;
        s = l > 0.5 ? d / (2 - max - min) : d / (max + min);

        switch (max) {
          case r:
            h = (g - b) / d + (g < b ? 6 : 0);
            break;

          case g:
            h = (b - r) / d + 2;
            break;

          case b:
            h = (r - g) / d + 4;
            break;
        }

        h /= 6;
      }

      s = s * 100;
      s = Math.round(s);
      l = l * 100;
      l = Math.round(l);
      h = Math.round(360 * h);
      this.H_Value = h;
      this.S_Value = s;
      this.L_Value = l;
    }
  }, {
    key: "getColor",
    value: function getColor() {
      var s = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.S_Value;
      var l = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.L_Value;
      var h = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : this.H_Value;
      var colorInHSL = 'hsl(' + h + ', ' + s + '%, ' + l + '%)';
      return colorInHSL;
    }
  }, {
    key: "getHValue",
    value: function getHValue() {
      return this.H_Value;
    }
  }, {
    key: "getSValue",
    value: function getSValue() {
      return this.S_Value;
    }
  }, {
    key: "getLValue",
    value: function getLValue() {
      return this.L_Value;
    }
  }]);

  return hexToHSL;
}();

/***/ }),

/***/ "./src/public/js/utils/index.js":
/*!**************************************!*\
  !*** ./src/public/js/utils/index.js ***!
  \**************************************/
/*! exports provided: parseHtml, capitalizeFirstLatter, chunkData, countryStates, hexToHSL, themeMaker */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _parse_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./parse-html */ "./src/public/js/utils/parse-html.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "parseHtml", function() { return _parse_html__WEBPACK_IMPORTED_MODULE_0__["parseHtml"]; });

/* harmony import */ var _helpers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./helpers */ "./src/public/js/utils/helpers.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "capitalizeFirstLatter", function() { return _helpers__WEBPACK_IMPORTED_MODULE_1__["capitalizeFirstLatter"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "chunkData", function() { return _helpers__WEBPACK_IMPORTED_MODULE_1__["chunkData"]; });

/* harmony import */ var _countryStates__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./countryStates */ "./src/public/js/utils/countryStates.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "countryStates", function() { return _countryStates__WEBPACK_IMPORTED_MODULE_2__["countryStates"]; });

/* harmony import */ var _hexToHSL__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./hexToHSL */ "./src/public/js/utils/hexToHSL.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "hexToHSL", function() { return _hexToHSL__WEBPACK_IMPORTED_MODULE_3__["hexToHSL"]; });

/* harmony import */ var _themeMaker__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./themeMaker */ "./src/public/js/utils/themeMaker.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "themeMaker", function() { return _themeMaker__WEBPACK_IMPORTED_MODULE_4__["themeMaker"]; });







/***/ }),

/***/ "./src/public/js/utils/parse-html.js":
/*!*******************************************!*\
  !*** ./src/public/js/utils/parse-html.js ***!
  \*******************************************/
/*! exports provided: parseHtml */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "parseHtml", function() { return parseHtml; });
var contextRange = document.createRange();
contextRange.setStart(document.body, 0);
var parseHtml = function parseHtml(str) {
  return contextRange.createContextualFragment(str);
};

/***/ }),

/***/ "./src/public/js/utils/themeMaker.js":
/*!*******************************************!*\
  !*** ./src/public/js/utils/themeMaker.js ***!
  \*******************************************/
/*! exports provided: themeMaker */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "themeMaker", function() { return themeMaker; });
/* harmony import */ var _hexToHSL__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./hexToHSL */ "./src/public/js/utils/hexToHSL.js");

var themeMaker = function themeMaker(hexColor) {
  // let color =  new ToHSL("var(--color1)");
  var color = new _hexToHSL__WEBPACK_IMPORTED_MODULE_0__["hexToHSL"](hexColor);
  var color1 = color.getColor();
  document.documentElement.style.setProperty("--color1", color1);
  var color2_S = color.getSValue() - 60;
  var color2_L = color.getLValue();
  var color2 = color.getColor(color2_S, color2_L);
  document.documentElement.style.setProperty("--color2", color2);
  var color3_S = color.getSValue() - 5;
  var color3_L = color.getLValue() + 23;
  var color3 = color.getColor(color3_S, color3_L);
  document.documentElement.style.setProperty("--color3", color3);
  var color4_S = color.getSValue() - 14;
  var color4_L = color.getLValue() + 40;
  var color4 = color.getColor(color4_S, color4_L);
  document.documentElement.style.setProperty("--color4", color4);
};

/***/ })

/******/ });
//# sourceMappingURL=response-canvas.js.map