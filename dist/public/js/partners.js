/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/public/js/partners/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/axios/index.js":
/*!*************************************!*\
  !*** ./node_modules/axios/index.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! ./lib/axios */ "./node_modules/axios/lib/axios.js");

/***/ }),

/***/ "./node_modules/axios/lib/adapters/xhr.js":
/*!************************************************!*\
  !*** ./node_modules/axios/lib/adapters/xhr.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");

var settle = __webpack_require__(/*! ./../core/settle */ "./node_modules/axios/lib/core/settle.js");

var buildURL = __webpack_require__(/*! ./../helpers/buildURL */ "./node_modules/axios/lib/helpers/buildURL.js");

var parseHeaders = __webpack_require__(/*! ./../helpers/parseHeaders */ "./node_modules/axios/lib/helpers/parseHeaders.js");

var isURLSameOrigin = __webpack_require__(/*! ./../helpers/isURLSameOrigin */ "./node_modules/axios/lib/helpers/isURLSameOrigin.js");

var createError = __webpack_require__(/*! ../core/createError */ "./node_modules/axios/lib/core/createError.js");

module.exports = function xhrAdapter(config) {
  return new Promise(function dispatchXhrRequest(resolve, reject) {
    var requestData = config.data;
    var requestHeaders = config.headers;

    if (utils.isFormData(requestData)) {
      delete requestHeaders['Content-Type']; // Let the browser set it
    }

    var request = new XMLHttpRequest(); // HTTP basic authentication

    if (config.auth) {
      var username = config.auth.username || '';
      var password = config.auth.password || '';
      requestHeaders.Authorization = 'Basic ' + btoa(username + ':' + password);
    }

    request.open(config.method.toUpperCase(), buildURL(config.url, config.params, config.paramsSerializer), true); // Set the request timeout in MS

    request.timeout = config.timeout; // Listen for ready state

    request.onreadystatechange = function handleLoad() {
      if (!request || request.readyState !== 4) {
        return;
      } // The request errored out and we didn't get a response, this will be
      // handled by onerror instead
      // With one exception: request that using file: protocol, most browsers
      // will return status as 0 even though it's a successful request


      if (request.status === 0 && !(request.responseURL && request.responseURL.indexOf('file:') === 0)) {
        return;
      } // Prepare the response


      var responseHeaders = 'getAllResponseHeaders' in request ? parseHeaders(request.getAllResponseHeaders()) : null;
      var responseData = !config.responseType || config.responseType === 'text' ? request.responseText : request.response;
      var response = {
        data: responseData,
        status: request.status,
        statusText: request.statusText,
        headers: responseHeaders,
        config: config,
        request: request
      };
      settle(resolve, reject, response); // Clean up request

      request = null;
    }; // Handle browser request cancellation (as opposed to a manual cancellation)


    request.onabort = function handleAbort() {
      if (!request) {
        return;
      }

      reject(createError('Request aborted', config, 'ECONNABORTED', request)); // Clean up request

      request = null;
    }; // Handle low level network errors


    request.onerror = function handleError() {
      // Real errors are hidden from us by the browser
      // onerror should only fire if it's a network error
      reject(createError('Network Error', config, null, request)); // Clean up request

      request = null;
    }; // Handle timeout


    request.ontimeout = function handleTimeout() {
      reject(createError('timeout of ' + config.timeout + 'ms exceeded', config, 'ECONNABORTED', request)); // Clean up request

      request = null;
    }; // Add xsrf header
    // This is only done if running in a standard browser environment.
    // Specifically not if we're in a web worker, or react-native.


    if (utils.isStandardBrowserEnv()) {
      var cookies = __webpack_require__(/*! ./../helpers/cookies */ "./node_modules/axios/lib/helpers/cookies.js"); // Add xsrf header


      var xsrfValue = (config.withCredentials || isURLSameOrigin(config.url)) && config.xsrfCookieName ? cookies.read(config.xsrfCookieName) : undefined;

      if (xsrfValue) {
        requestHeaders[config.xsrfHeaderName] = xsrfValue;
      }
    } // Add headers to the request


    if ('setRequestHeader' in request) {
      utils.forEach(requestHeaders, function setRequestHeader(val, key) {
        if (typeof requestData === 'undefined' && key.toLowerCase() === 'content-type') {
          // Remove Content-Type if data is undefined
          delete requestHeaders[key];
        } else {
          // Otherwise add header to the request
          request.setRequestHeader(key, val);
        }
      });
    } // Add withCredentials to request if needed


    if (config.withCredentials) {
      request.withCredentials = true;
    } // Add responseType to request if needed


    if (config.responseType) {
      try {
        request.responseType = config.responseType;
      } catch (e) {
        // Expected DOMException thrown by browsers not compatible XMLHttpRequest Level 2.
        // But, this can be suppressed for 'json' type as it can be parsed by default 'transformResponse' function.
        if (config.responseType !== 'json') {
          throw e;
        }
      }
    } // Handle progress if needed


    if (typeof config.onDownloadProgress === 'function') {
      request.addEventListener('progress', config.onDownloadProgress);
    } // Not all browsers support upload events


    if (typeof config.onUploadProgress === 'function' && request.upload) {
      request.upload.addEventListener('progress', config.onUploadProgress);
    }

    if (config.cancelToken) {
      // Handle cancellation
      config.cancelToken.promise.then(function onCanceled(cancel) {
        if (!request) {
          return;
        }

        request.abort();
        reject(cancel); // Clean up request

        request = null;
      });
    }

    if (requestData === undefined) {
      requestData = null;
    } // Send the request


    request.send(requestData);
  });
};

/***/ }),

/***/ "./node_modules/axios/lib/axios.js":
/*!*****************************************!*\
  !*** ./node_modules/axios/lib/axios.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./utils */ "./node_modules/axios/lib/utils.js");

var bind = __webpack_require__(/*! ./helpers/bind */ "./node_modules/axios/lib/helpers/bind.js");

var Axios = __webpack_require__(/*! ./core/Axios */ "./node_modules/axios/lib/core/Axios.js");

var mergeConfig = __webpack_require__(/*! ./core/mergeConfig */ "./node_modules/axios/lib/core/mergeConfig.js");

var defaults = __webpack_require__(/*! ./defaults */ "./node_modules/axios/lib/defaults.js");
/**
 * Create an instance of Axios
 *
 * @param {Object} defaultConfig The default config for the instance
 * @return {Axios} A new instance of Axios
 */


function createInstance(defaultConfig) {
  var context = new Axios(defaultConfig);
  var instance = bind(Axios.prototype.request, context); // Copy axios.prototype to instance

  utils.extend(instance, Axios.prototype, context); // Copy context to instance

  utils.extend(instance, context);
  return instance;
} // Create the default instance to be exported


var axios = createInstance(defaults); // Expose Axios class to allow class inheritance

axios.Axios = Axios; // Factory for creating new instances

axios.create = function create(instanceConfig) {
  return createInstance(mergeConfig(axios.defaults, instanceConfig));
}; // Expose Cancel & CancelToken


axios.Cancel = __webpack_require__(/*! ./cancel/Cancel */ "./node_modules/axios/lib/cancel/Cancel.js");
axios.CancelToken = __webpack_require__(/*! ./cancel/CancelToken */ "./node_modules/axios/lib/cancel/CancelToken.js");
axios.isCancel = __webpack_require__(/*! ./cancel/isCancel */ "./node_modules/axios/lib/cancel/isCancel.js"); // Expose all/spread

axios.all = function all(promises) {
  return Promise.all(promises);
};

axios.spread = __webpack_require__(/*! ./helpers/spread */ "./node_modules/axios/lib/helpers/spread.js");
module.exports = axios; // Allow use of default import syntax in TypeScript

module.exports.default = axios;

/***/ }),

/***/ "./node_modules/axios/lib/cancel/Cancel.js":
/*!*************************************************!*\
  !*** ./node_modules/axios/lib/cancel/Cancel.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/**
 * A `Cancel` is an object that is thrown when an operation is canceled.
 *
 * @class
 * @param {string=} message The message.
 */

function Cancel(message) {
  this.message = message;
}

Cancel.prototype.toString = function toString() {
  return 'Cancel' + (this.message ? ': ' + this.message : '');
};

Cancel.prototype.__CANCEL__ = true;
module.exports = Cancel;

/***/ }),

/***/ "./node_modules/axios/lib/cancel/CancelToken.js":
/*!******************************************************!*\
  !*** ./node_modules/axios/lib/cancel/CancelToken.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var Cancel = __webpack_require__(/*! ./Cancel */ "./node_modules/axios/lib/cancel/Cancel.js");
/**
 * A `CancelToken` is an object that can be used to request cancellation of an operation.
 *
 * @class
 * @param {Function} executor The executor function.
 */


function CancelToken(executor) {
  if (typeof executor !== 'function') {
    throw new TypeError('executor must be a function.');
  }

  var resolvePromise;
  this.promise = new Promise(function promiseExecutor(resolve) {
    resolvePromise = resolve;
  });
  var token = this;
  executor(function cancel(message) {
    if (token.reason) {
      // Cancellation has already been requested
      return;
    }

    token.reason = new Cancel(message);
    resolvePromise(token.reason);
  });
}
/**
 * Throws a `Cancel` if cancellation has been requested.
 */


CancelToken.prototype.throwIfRequested = function throwIfRequested() {
  if (this.reason) {
    throw this.reason;
  }
};
/**
 * Returns an object that contains a new `CancelToken` and a function that, when called,
 * cancels the `CancelToken`.
 */


CancelToken.source = function source() {
  var cancel;
  var token = new CancelToken(function executor(c) {
    cancel = c;
  });
  return {
    token: token,
    cancel: cancel
  };
};

module.exports = CancelToken;

/***/ }),

/***/ "./node_modules/axios/lib/cancel/isCancel.js":
/*!***************************************************!*\
  !*** ./node_modules/axios/lib/cancel/isCancel.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function isCancel(value) {
  return !!(value && value.__CANCEL__);
};

/***/ }),

/***/ "./node_modules/axios/lib/core/Axios.js":
/*!**********************************************!*\
  !*** ./node_modules/axios/lib/core/Axios.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");

var buildURL = __webpack_require__(/*! ../helpers/buildURL */ "./node_modules/axios/lib/helpers/buildURL.js");

var InterceptorManager = __webpack_require__(/*! ./InterceptorManager */ "./node_modules/axios/lib/core/InterceptorManager.js");

var dispatchRequest = __webpack_require__(/*! ./dispatchRequest */ "./node_modules/axios/lib/core/dispatchRequest.js");

var mergeConfig = __webpack_require__(/*! ./mergeConfig */ "./node_modules/axios/lib/core/mergeConfig.js");
/**
 * Create a new instance of Axios
 *
 * @param {Object} instanceConfig The default config for the instance
 */


function Axios(instanceConfig) {
  this.defaults = instanceConfig;
  this.interceptors = {
    request: new InterceptorManager(),
    response: new InterceptorManager()
  };
}
/**
 * Dispatch a request
 *
 * @param {Object} config The config specific for this request (merged with this.defaults)
 */


Axios.prototype.request = function request(config) {
  /*eslint no-param-reassign:0*/
  // Allow for axios('example/url'[, config]) a la fetch API
  if (typeof config === 'string') {
    config = arguments[1] || {};
    config.url = arguments[0];
  } else {
    config = config || {};
  }

  config = mergeConfig(this.defaults, config);
  config.method = config.method ? config.method.toLowerCase() : 'get'; // Hook up interceptors middleware

  var chain = [dispatchRequest, undefined];
  var promise = Promise.resolve(config);
  this.interceptors.request.forEach(function unshiftRequestInterceptors(interceptor) {
    chain.unshift(interceptor.fulfilled, interceptor.rejected);
  });
  this.interceptors.response.forEach(function pushResponseInterceptors(interceptor) {
    chain.push(interceptor.fulfilled, interceptor.rejected);
  });

  while (chain.length) {
    promise = promise.then(chain.shift(), chain.shift());
  }

  return promise;
};

Axios.prototype.getUri = function getUri(config) {
  config = mergeConfig(this.defaults, config);
  return buildURL(config.url, config.params, config.paramsSerializer).replace(/^\?/, '');
}; // Provide aliases for supported request methods


utils.forEach(['delete', 'get', 'head', 'options'], function forEachMethodNoData(method) {
  /*eslint func-names:0*/
  Axios.prototype[method] = function (url, config) {
    return this.request(utils.merge(config || {}, {
      method: method,
      url: url
    }));
  };
});
utils.forEach(['post', 'put', 'patch'], function forEachMethodWithData(method) {
  /*eslint func-names:0*/
  Axios.prototype[method] = function (url, data, config) {
    return this.request(utils.merge(config || {}, {
      method: method,
      url: url,
      data: data
    }));
  };
});
module.exports = Axios;

/***/ }),

/***/ "./node_modules/axios/lib/core/InterceptorManager.js":
/*!***********************************************************!*\
  !*** ./node_modules/axios/lib/core/InterceptorManager.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");

function InterceptorManager() {
  this.handlers = [];
}
/**
 * Add a new interceptor to the stack
 *
 * @param {Function} fulfilled The function to handle `then` for a `Promise`
 * @param {Function} rejected The function to handle `reject` for a `Promise`
 *
 * @return {Number} An ID used to remove interceptor later
 */


InterceptorManager.prototype.use = function use(fulfilled, rejected) {
  this.handlers.push({
    fulfilled: fulfilled,
    rejected: rejected
  });
  return this.handlers.length - 1;
};
/**
 * Remove an interceptor from the stack
 *
 * @param {Number} id The ID that was returned by `use`
 */


InterceptorManager.prototype.eject = function eject(id) {
  if (this.handlers[id]) {
    this.handlers[id] = null;
  }
};
/**
 * Iterate over all the registered interceptors
 *
 * This method is particularly useful for skipping over any
 * interceptors that may have become `null` calling `eject`.
 *
 * @param {Function} fn The function to call for each interceptor
 */


InterceptorManager.prototype.forEach = function forEach(fn) {
  utils.forEach(this.handlers, function forEachHandler(h) {
    if (h !== null) {
      fn(h);
    }
  });
};

module.exports = InterceptorManager;

/***/ }),

/***/ "./node_modules/axios/lib/core/createError.js":
/*!****************************************************!*\
  !*** ./node_modules/axios/lib/core/createError.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var enhanceError = __webpack_require__(/*! ./enhanceError */ "./node_modules/axios/lib/core/enhanceError.js");
/**
 * Create an Error with the specified message, config, error code, request and response.
 *
 * @param {string} message The error message.
 * @param {Object} config The config.
 * @param {string} [code] The error code (for example, 'ECONNABORTED').
 * @param {Object} [request] The request.
 * @param {Object} [response] The response.
 * @returns {Error} The created error.
 */


module.exports = function createError(message, config, code, request, response) {
  var error = new Error(message);
  return enhanceError(error, config, code, request, response);
};

/***/ }),

/***/ "./node_modules/axios/lib/core/dispatchRequest.js":
/*!********************************************************!*\
  !*** ./node_modules/axios/lib/core/dispatchRequest.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");

var transformData = __webpack_require__(/*! ./transformData */ "./node_modules/axios/lib/core/transformData.js");

var isCancel = __webpack_require__(/*! ../cancel/isCancel */ "./node_modules/axios/lib/cancel/isCancel.js");

var defaults = __webpack_require__(/*! ../defaults */ "./node_modules/axios/lib/defaults.js");

var isAbsoluteURL = __webpack_require__(/*! ./../helpers/isAbsoluteURL */ "./node_modules/axios/lib/helpers/isAbsoluteURL.js");

var combineURLs = __webpack_require__(/*! ./../helpers/combineURLs */ "./node_modules/axios/lib/helpers/combineURLs.js");
/**
 * Throws a `Cancel` if cancellation has been requested.
 */


function throwIfCancellationRequested(config) {
  if (config.cancelToken) {
    config.cancelToken.throwIfRequested();
  }
}
/**
 * Dispatch a request to the server using the configured adapter.
 *
 * @param {object} config The config that is to be used for the request
 * @returns {Promise} The Promise to be fulfilled
 */


module.exports = function dispatchRequest(config) {
  throwIfCancellationRequested(config); // Support baseURL config

  if (config.baseURL && !isAbsoluteURL(config.url)) {
    config.url = combineURLs(config.baseURL, config.url);
  } // Ensure headers exist


  config.headers = config.headers || {}; // Transform request data

  config.data = transformData(config.data, config.headers, config.transformRequest); // Flatten headers

  config.headers = utils.merge(config.headers.common || {}, config.headers[config.method] || {}, config.headers || {});
  utils.forEach(['delete', 'get', 'head', 'post', 'put', 'patch', 'common'], function cleanHeaderConfig(method) {
    delete config.headers[method];
  });
  var adapter = config.adapter || defaults.adapter;
  return adapter(config).then(function onAdapterResolution(response) {
    throwIfCancellationRequested(config); // Transform response data

    response.data = transformData(response.data, response.headers, config.transformResponse);
    return response;
  }, function onAdapterRejection(reason) {
    if (!isCancel(reason)) {
      throwIfCancellationRequested(config); // Transform response data

      if (reason && reason.response) {
        reason.response.data = transformData(reason.response.data, reason.response.headers, config.transformResponse);
      }
    }

    return Promise.reject(reason);
  });
};

/***/ }),

/***/ "./node_modules/axios/lib/core/enhanceError.js":
/*!*****************************************************!*\
  !*** ./node_modules/axios/lib/core/enhanceError.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/**
 * Update an Error with the specified config, error code, and response.
 *
 * @param {Error} error The error to update.
 * @param {Object} config The config.
 * @param {string} [code] The error code (for example, 'ECONNABORTED').
 * @param {Object} [request] The request.
 * @param {Object} [response] The response.
 * @returns {Error} The error.
 */

module.exports = function enhanceError(error, config, code, request, response) {
  error.config = config;

  if (code) {
    error.code = code;
  }

  error.request = request;
  error.response = response;
  error.isAxiosError = true;

  error.toJSON = function () {
    return {
      // Standard
      message: this.message,
      name: this.name,
      // Microsoft
      description: this.description,
      number: this.number,
      // Mozilla
      fileName: this.fileName,
      lineNumber: this.lineNumber,
      columnNumber: this.columnNumber,
      stack: this.stack,
      // Axios
      config: this.config,
      code: this.code
    };
  };

  return error;
};

/***/ }),

/***/ "./node_modules/axios/lib/core/mergeConfig.js":
/*!****************************************************!*\
  !*** ./node_modules/axios/lib/core/mergeConfig.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ../utils */ "./node_modules/axios/lib/utils.js");
/**
 * Config-specific merge-function which creates a new config-object
 * by merging two configuration objects together.
 *
 * @param {Object} config1
 * @param {Object} config2
 * @returns {Object} New object resulting from merging config2 to config1
 */


module.exports = function mergeConfig(config1, config2) {
  // eslint-disable-next-line no-param-reassign
  config2 = config2 || {};
  var config = {};
  utils.forEach(['url', 'method', 'params', 'data'], function valueFromConfig2(prop) {
    if (typeof config2[prop] !== 'undefined') {
      config[prop] = config2[prop];
    }
  });
  utils.forEach(['headers', 'auth', 'proxy'], function mergeDeepProperties(prop) {
    if (utils.isObject(config2[prop])) {
      config[prop] = utils.deepMerge(config1[prop], config2[prop]);
    } else if (typeof config2[prop] !== 'undefined') {
      config[prop] = config2[prop];
    } else if (utils.isObject(config1[prop])) {
      config[prop] = utils.deepMerge(config1[prop]);
    } else if (typeof config1[prop] !== 'undefined') {
      config[prop] = config1[prop];
    }
  });
  utils.forEach(['baseURL', 'transformRequest', 'transformResponse', 'paramsSerializer', 'timeout', 'withCredentials', 'adapter', 'responseType', 'xsrfCookieName', 'xsrfHeaderName', 'onUploadProgress', 'onDownloadProgress', 'maxContentLength', 'validateStatus', 'maxRedirects', 'httpAgent', 'httpsAgent', 'cancelToken', 'socketPath'], function defaultToConfig2(prop) {
    if (typeof config2[prop] !== 'undefined') {
      config[prop] = config2[prop];
    } else if (typeof config1[prop] !== 'undefined') {
      config[prop] = config1[prop];
    }
  });
  return config;
};

/***/ }),

/***/ "./node_modules/axios/lib/core/settle.js":
/*!***********************************************!*\
  !*** ./node_modules/axios/lib/core/settle.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var createError = __webpack_require__(/*! ./createError */ "./node_modules/axios/lib/core/createError.js");
/**
 * Resolve or reject a Promise based on response status.
 *
 * @param {Function} resolve A function that resolves the promise.
 * @param {Function} reject A function that rejects the promise.
 * @param {object} response The response.
 */


module.exports = function settle(resolve, reject, response) {
  var validateStatus = response.config.validateStatus;

  if (!validateStatus || validateStatus(response.status)) {
    resolve(response);
  } else {
    reject(createError('Request failed with status code ' + response.status, response.config, null, response.request, response));
  }
};

/***/ }),

/***/ "./node_modules/axios/lib/core/transformData.js":
/*!******************************************************!*\
  !*** ./node_modules/axios/lib/core/transformData.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");
/**
 * Transform the data for a request or a response
 *
 * @param {Object|String} data The data to be transformed
 * @param {Array} headers The headers for the request or response
 * @param {Array|Function} fns A single function or Array of functions
 * @returns {*} The resulting transformed data
 */


module.exports = function transformData(data, headers, fns) {
  /*eslint no-param-reassign:0*/
  utils.forEach(fns, function transform(fn) {
    data = fn(data, headers);
  });
  return data;
};

/***/ }),

/***/ "./node_modules/axios/lib/defaults.js":
/*!********************************************!*\
  !*** ./node_modules/axios/lib/defaults.js ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(process) {

var utils = __webpack_require__(/*! ./utils */ "./node_modules/axios/lib/utils.js");

var normalizeHeaderName = __webpack_require__(/*! ./helpers/normalizeHeaderName */ "./node_modules/axios/lib/helpers/normalizeHeaderName.js");

var DEFAULT_CONTENT_TYPE = {
  'Content-Type': 'application/x-www-form-urlencoded'
};

function setContentTypeIfUnset(headers, value) {
  if (!utils.isUndefined(headers) && utils.isUndefined(headers['Content-Type'])) {
    headers['Content-Type'] = value;
  }
}

function getDefaultAdapter() {
  var adapter; // Only Node.JS has a process variable that is of [[Class]] process

  if (typeof process !== 'undefined' && Object.prototype.toString.call(process) === '[object process]') {
    // For node use HTTP adapter
    adapter = __webpack_require__(/*! ./adapters/http */ "./node_modules/axios/lib/adapters/xhr.js");
  } else if (typeof XMLHttpRequest !== 'undefined') {
    // For browsers use XHR adapter
    adapter = __webpack_require__(/*! ./adapters/xhr */ "./node_modules/axios/lib/adapters/xhr.js");
  }

  return adapter;
}

var defaults = {
  adapter: getDefaultAdapter(),
  transformRequest: [function transformRequest(data, headers) {
    normalizeHeaderName(headers, 'Accept');
    normalizeHeaderName(headers, 'Content-Type');

    if (utils.isFormData(data) || utils.isArrayBuffer(data) || utils.isBuffer(data) || utils.isStream(data) || utils.isFile(data) || utils.isBlob(data)) {
      return data;
    }

    if (utils.isArrayBufferView(data)) {
      return data.buffer;
    }

    if (utils.isURLSearchParams(data)) {
      setContentTypeIfUnset(headers, 'application/x-www-form-urlencoded;charset=utf-8');
      return data.toString();
    }

    if (utils.isObject(data)) {
      setContentTypeIfUnset(headers, 'application/json;charset=utf-8');
      return JSON.stringify(data);
    }

    return data;
  }],
  transformResponse: [function transformResponse(data) {
    /*eslint no-param-reassign:0*/
    if (typeof data === 'string') {
      try {
        data = JSON.parse(data);
      } catch (e) {
        /* Ignore */
      }
    }

    return data;
  }],

  /**
   * A timeout in milliseconds to abort a request. If set to 0 (default) a
   * timeout is not created.
   */
  timeout: 0,
  xsrfCookieName: 'XSRF-TOKEN',
  xsrfHeaderName: 'X-XSRF-TOKEN',
  maxContentLength: -1,
  validateStatus: function validateStatus(status) {
    return status >= 200 && status < 300;
  }
};
defaults.headers = {
  common: {
    'Accept': 'application/json, text/plain, */*'
  }
};
utils.forEach(['delete', 'get', 'head'], function forEachMethodNoData(method) {
  defaults.headers[method] = {};
});
utils.forEach(['post', 'put', 'patch'], function forEachMethodWithData(method) {
  defaults.headers[method] = utils.merge(DEFAULT_CONTENT_TYPE);
});
module.exports = defaults;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./node_modules/axios/lib/helpers/bind.js":
/*!************************************************!*\
  !*** ./node_modules/axios/lib/helpers/bind.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function bind(fn, thisArg) {
  return function wrap() {
    var args = new Array(arguments.length);

    for (var i = 0; i < args.length; i++) {
      args[i] = arguments[i];
    }

    return fn.apply(thisArg, args);
  };
};

/***/ }),

/***/ "./node_modules/axios/lib/helpers/buildURL.js":
/*!****************************************************!*\
  !*** ./node_modules/axios/lib/helpers/buildURL.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");

function encode(val) {
  return encodeURIComponent(val).replace(/%40/gi, '@').replace(/%3A/gi, ':').replace(/%24/g, '$').replace(/%2C/gi, ',').replace(/%20/g, '+').replace(/%5B/gi, '[').replace(/%5D/gi, ']');
}
/**
 * Build a URL by appending params to the end
 *
 * @param {string} url The base of the url (e.g., http://www.google.com)
 * @param {object} [params] The params to be appended
 * @returns {string} The formatted url
 */


module.exports = function buildURL(url, params, paramsSerializer) {
  /*eslint no-param-reassign:0*/
  if (!params) {
    return url;
  }

  var serializedParams;

  if (paramsSerializer) {
    serializedParams = paramsSerializer(params);
  } else if (utils.isURLSearchParams(params)) {
    serializedParams = params.toString();
  } else {
    var parts = [];
    utils.forEach(params, function serialize(val, key) {
      if (val === null || typeof val === 'undefined') {
        return;
      }

      if (utils.isArray(val)) {
        key = key + '[]';
      } else {
        val = [val];
      }

      utils.forEach(val, function parseValue(v) {
        if (utils.isDate(v)) {
          v = v.toISOString();
        } else if (utils.isObject(v)) {
          v = JSON.stringify(v);
        }

        parts.push(encode(key) + '=' + encode(v));
      });
    });
    serializedParams = parts.join('&');
  }

  if (serializedParams) {
    var hashmarkIndex = url.indexOf('#');

    if (hashmarkIndex !== -1) {
      url = url.slice(0, hashmarkIndex);
    }

    url += (url.indexOf('?') === -1 ? '?' : '&') + serializedParams;
  }

  return url;
};

/***/ }),

/***/ "./node_modules/axios/lib/helpers/combineURLs.js":
/*!*******************************************************!*\
  !*** ./node_modules/axios/lib/helpers/combineURLs.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/**
 * Creates a new URL by combining the specified URLs
 *
 * @param {string} baseURL The base URL
 * @param {string} relativeURL The relative URL
 * @returns {string} The combined URL
 */

module.exports = function combineURLs(baseURL, relativeURL) {
  return relativeURL ? baseURL.replace(/\/+$/, '') + '/' + relativeURL.replace(/^\/+/, '') : baseURL;
};

/***/ }),

/***/ "./node_modules/axios/lib/helpers/cookies.js":
/*!***************************************************!*\
  !*** ./node_modules/axios/lib/helpers/cookies.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");

module.exports = utils.isStandardBrowserEnv() ? // Standard browser envs support document.cookie
function standardBrowserEnv() {
  return {
    write: function write(name, value, expires, path, domain, secure) {
      var cookie = [];
      cookie.push(name + '=' + encodeURIComponent(value));

      if (utils.isNumber(expires)) {
        cookie.push('expires=' + new Date(expires).toGMTString());
      }

      if (utils.isString(path)) {
        cookie.push('path=' + path);
      }

      if (utils.isString(domain)) {
        cookie.push('domain=' + domain);
      }

      if (secure === true) {
        cookie.push('secure');
      }

      document.cookie = cookie.join('; ');
    },
    read: function read(name) {
      var match = document.cookie.match(new RegExp('(^|;\\s*)(' + name + ')=([^;]*)'));
      return match ? decodeURIComponent(match[3]) : null;
    },
    remove: function remove(name) {
      this.write(name, '', Date.now() - 86400000);
    }
  };
}() : // Non standard browser env (web workers, react-native) lack needed support.
function nonStandardBrowserEnv() {
  return {
    write: function write() {},
    read: function read() {
      return null;
    },
    remove: function remove() {}
  };
}();

/***/ }),

/***/ "./node_modules/axios/lib/helpers/isAbsoluteURL.js":
/*!*********************************************************!*\
  !*** ./node_modules/axios/lib/helpers/isAbsoluteURL.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/**
 * Determines whether the specified URL is absolute
 *
 * @param {string} url The URL to test
 * @returns {boolean} True if the specified URL is absolute, otherwise false
 */

module.exports = function isAbsoluteURL(url) {
  // A URL is considered absolute if it begins with "<scheme>://" or "//" (protocol-relative URL).
  // RFC 3986 defines scheme name as a sequence of characters beginning with a letter and followed
  // by any combination of letters, digits, plus, period, or hyphen.
  return /^([a-z][a-z\d\+\-\.]*:)?\/\//i.test(url);
};

/***/ }),

/***/ "./node_modules/axios/lib/helpers/isURLSameOrigin.js":
/*!***********************************************************!*\
  !*** ./node_modules/axios/lib/helpers/isURLSameOrigin.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");

module.exports = utils.isStandardBrowserEnv() ? // Standard browser envs have full support of the APIs needed to test
// whether the request URL is of the same origin as current location.
function standardBrowserEnv() {
  var msie = /(msie|trident)/i.test(navigator.userAgent);
  var urlParsingNode = document.createElement('a');
  var originURL;
  /**
  * Parse a URL to discover it's components
  *
  * @param {String} url The URL to be parsed
  * @returns {Object}
  */

  function resolveURL(url) {
    var href = url;

    if (msie) {
      // IE needs attribute set twice to normalize properties
      urlParsingNode.setAttribute('href', href);
      href = urlParsingNode.href;
    }

    urlParsingNode.setAttribute('href', href); // urlParsingNode provides the UrlUtils interface - http://url.spec.whatwg.org/#urlutils

    return {
      href: urlParsingNode.href,
      protocol: urlParsingNode.protocol ? urlParsingNode.protocol.replace(/:$/, '') : '',
      host: urlParsingNode.host,
      search: urlParsingNode.search ? urlParsingNode.search.replace(/^\?/, '') : '',
      hash: urlParsingNode.hash ? urlParsingNode.hash.replace(/^#/, '') : '',
      hostname: urlParsingNode.hostname,
      port: urlParsingNode.port,
      pathname: urlParsingNode.pathname.charAt(0) === '/' ? urlParsingNode.pathname : '/' + urlParsingNode.pathname
    };
  }

  originURL = resolveURL(window.location.href);
  /**
  * Determine if a URL shares the same origin as the current location
  *
  * @param {String} requestURL The URL to test
  * @returns {boolean} True if URL shares the same origin, otherwise false
  */

  return function isURLSameOrigin(requestURL) {
    var parsed = utils.isString(requestURL) ? resolveURL(requestURL) : requestURL;
    return parsed.protocol === originURL.protocol && parsed.host === originURL.host;
  };
}() : // Non standard browser envs (web workers, react-native) lack needed support.
function nonStandardBrowserEnv() {
  return function isURLSameOrigin() {
    return true;
  };
}();

/***/ }),

/***/ "./node_modules/axios/lib/helpers/normalizeHeaderName.js":
/*!***************************************************************!*\
  !*** ./node_modules/axios/lib/helpers/normalizeHeaderName.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ../utils */ "./node_modules/axios/lib/utils.js");

module.exports = function normalizeHeaderName(headers, normalizedName) {
  utils.forEach(headers, function processHeader(value, name) {
    if (name !== normalizedName && name.toUpperCase() === normalizedName.toUpperCase()) {
      headers[normalizedName] = value;
      delete headers[name];
    }
  });
};

/***/ }),

/***/ "./node_modules/axios/lib/helpers/parseHeaders.js":
/*!********************************************************!*\
  !*** ./node_modules/axios/lib/helpers/parseHeaders.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js"); // Headers whose duplicates are ignored by node
// c.f. https://nodejs.org/api/http.html#http_message_headers


var ignoreDuplicateOf = ['age', 'authorization', 'content-length', 'content-type', 'etag', 'expires', 'from', 'host', 'if-modified-since', 'if-unmodified-since', 'last-modified', 'location', 'max-forwards', 'proxy-authorization', 'referer', 'retry-after', 'user-agent'];
/**
 * Parse headers into an object
 *
 * ```
 * Date: Wed, 27 Aug 2014 08:58:49 GMT
 * Content-Type: application/json
 * Connection: keep-alive
 * Transfer-Encoding: chunked
 * ```
 *
 * @param {String} headers Headers needing to be parsed
 * @returns {Object} Headers parsed into an object
 */

module.exports = function parseHeaders(headers) {
  var parsed = {};
  var key;
  var val;
  var i;

  if (!headers) {
    return parsed;
  }

  utils.forEach(headers.split('\n'), function parser(line) {
    i = line.indexOf(':');
    key = utils.trim(line.substr(0, i)).toLowerCase();
    val = utils.trim(line.substr(i + 1));

    if (key) {
      if (parsed[key] && ignoreDuplicateOf.indexOf(key) >= 0) {
        return;
      }

      if (key === 'set-cookie') {
        parsed[key] = (parsed[key] ? parsed[key] : []).concat([val]);
      } else {
        parsed[key] = parsed[key] ? parsed[key] + ', ' + val : val;
      }
    }
  });
  return parsed;
};

/***/ }),

/***/ "./node_modules/axios/lib/helpers/spread.js":
/*!**************************************************!*\
  !*** ./node_modules/axios/lib/helpers/spread.js ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/**
 * Syntactic sugar for invoking a function and expanding an array for arguments.
 *
 * Common use case would be to use `Function.prototype.apply`.
 *
 *  ```js
 *  function f(x, y, z) {}
 *  var args = [1, 2, 3];
 *  f.apply(null, args);
 *  ```
 *
 * With `spread` this example can be re-written.
 *
 *  ```js
 *  spread(function(x, y, z) {})([1, 2, 3]);
 *  ```
 *
 * @param {Function} callback
 * @returns {Function}
 */

module.exports = function spread(callback) {
  return function wrap(arr) {
    return callback.apply(null, arr);
  };
};

/***/ }),

/***/ "./node_modules/axios/lib/utils.js":
/*!*****************************************!*\
  !*** ./node_modules/axios/lib/utils.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var bind = __webpack_require__(/*! ./helpers/bind */ "./node_modules/axios/lib/helpers/bind.js");

var isBuffer = __webpack_require__(/*! is-buffer */ "./node_modules/axios/node_modules/is-buffer/index.js");
/*global toString:true*/
// utils is a library of generic helper functions non-specific to axios


var toString = Object.prototype.toString;
/**
 * Determine if a value is an Array
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an Array, otherwise false
 */

function isArray(val) {
  return toString.call(val) === '[object Array]';
}
/**
 * Determine if a value is an ArrayBuffer
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an ArrayBuffer, otherwise false
 */


function isArrayBuffer(val) {
  return toString.call(val) === '[object ArrayBuffer]';
}
/**
 * Determine if a value is a FormData
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an FormData, otherwise false
 */


function isFormData(val) {
  return typeof FormData !== 'undefined' && val instanceof FormData;
}
/**
 * Determine if a value is a view on an ArrayBuffer
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a view on an ArrayBuffer, otherwise false
 */


function isArrayBufferView(val) {
  var result;

  if (typeof ArrayBuffer !== 'undefined' && ArrayBuffer.isView) {
    result = ArrayBuffer.isView(val);
  } else {
    result = val && val.buffer && val.buffer instanceof ArrayBuffer;
  }

  return result;
}
/**
 * Determine if a value is a String
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a String, otherwise false
 */


function isString(val) {
  return typeof val === 'string';
}
/**
 * Determine if a value is a Number
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Number, otherwise false
 */


function isNumber(val) {
  return typeof val === 'number';
}
/**
 * Determine if a value is undefined
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if the value is undefined, otherwise false
 */


function isUndefined(val) {
  return typeof val === 'undefined';
}
/**
 * Determine if a value is an Object
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an Object, otherwise false
 */


function isObject(val) {
  return val !== null && typeof val === 'object';
}
/**
 * Determine if a value is a Date
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Date, otherwise false
 */


function isDate(val) {
  return toString.call(val) === '[object Date]';
}
/**
 * Determine if a value is a File
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a File, otherwise false
 */


function isFile(val) {
  return toString.call(val) === '[object File]';
}
/**
 * Determine if a value is a Blob
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Blob, otherwise false
 */


function isBlob(val) {
  return toString.call(val) === '[object Blob]';
}
/**
 * Determine if a value is a Function
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Function, otherwise false
 */


function isFunction(val) {
  return toString.call(val) === '[object Function]';
}
/**
 * Determine if a value is a Stream
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Stream, otherwise false
 */


function isStream(val) {
  return isObject(val) && isFunction(val.pipe);
}
/**
 * Determine if a value is a URLSearchParams object
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a URLSearchParams object, otherwise false
 */


function isURLSearchParams(val) {
  return typeof URLSearchParams !== 'undefined' && val instanceof URLSearchParams;
}
/**
 * Trim excess whitespace off the beginning and end of a string
 *
 * @param {String} str The String to trim
 * @returns {String} The String freed of excess whitespace
 */


function trim(str) {
  return str.replace(/^\s*/, '').replace(/\s*$/, '');
}
/**
 * Determine if we're running in a standard browser environment
 *
 * This allows axios to run in a web worker, and react-native.
 * Both environments support XMLHttpRequest, but not fully standard globals.
 *
 * web workers:
 *  typeof window -> undefined
 *  typeof document -> undefined
 *
 * react-native:
 *  navigator.product -> 'ReactNative'
 * nativescript
 *  navigator.product -> 'NativeScript' or 'NS'
 */


function isStandardBrowserEnv() {
  if (typeof navigator !== 'undefined' && (navigator.product === 'ReactNative' || navigator.product === 'NativeScript' || navigator.product === 'NS')) {
    return false;
  }

  return typeof window !== 'undefined' && typeof document !== 'undefined';
}
/**
 * Iterate over an Array or an Object invoking a function for each item.
 *
 * If `obj` is an Array callback will be called passing
 * the value, index, and complete array for each item.
 *
 * If 'obj' is an Object callback will be called passing
 * the value, key, and complete object for each property.
 *
 * @param {Object|Array} obj The object to iterate
 * @param {Function} fn The callback to invoke for each item
 */


function forEach(obj, fn) {
  // Don't bother if no value provided
  if (obj === null || typeof obj === 'undefined') {
    return;
  } // Force an array if not already something iterable


  if (typeof obj !== 'object') {
    /*eslint no-param-reassign:0*/
    obj = [obj];
  }

  if (isArray(obj)) {
    // Iterate over array values
    for (var i = 0, l = obj.length; i < l; i++) {
      fn.call(null, obj[i], i, obj);
    }
  } else {
    // Iterate over object keys
    for (var key in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, key)) {
        fn.call(null, obj[key], key, obj);
      }
    }
  }
}
/**
 * Accepts varargs expecting each argument to be an object, then
 * immutably merges the properties of each object and returns result.
 *
 * When multiple objects contain the same key the later object in
 * the arguments list will take precedence.
 *
 * Example:
 *
 * ```js
 * var result = merge({foo: 123}, {foo: 456});
 * console.log(result.foo); // outputs 456
 * ```
 *
 * @param {Object} obj1 Object to merge
 * @returns {Object} Result of all merge properties
 */


function merge()
/* obj1, obj2, obj3, ... */
{
  var result = {};

  function assignValue(val, key) {
    if (typeof result[key] === 'object' && typeof val === 'object') {
      result[key] = merge(result[key], val);
    } else {
      result[key] = val;
    }
  }

  for (var i = 0, l = arguments.length; i < l; i++) {
    forEach(arguments[i], assignValue);
  }

  return result;
}
/**
 * Function equal to merge with the difference being that no reference
 * to original objects is kept.
 *
 * @see merge
 * @param {Object} obj1 Object to merge
 * @returns {Object} Result of all merge properties
 */


function deepMerge()
/* obj1, obj2, obj3, ... */
{
  var result = {};

  function assignValue(val, key) {
    if (typeof result[key] === 'object' && typeof val === 'object') {
      result[key] = deepMerge(result[key], val);
    } else if (typeof val === 'object') {
      result[key] = deepMerge({}, val);
    } else {
      result[key] = val;
    }
  }

  for (var i = 0, l = arguments.length; i < l; i++) {
    forEach(arguments[i], assignValue);
  }

  return result;
}
/**
 * Extends object a by mutably adding to it the properties of object b.
 *
 * @param {Object} a The object to be extended
 * @param {Object} b The object to copy properties from
 * @param {Object} thisArg The object to bind function to
 * @return {Object} The resulting value of object a
 */


function extend(a, b, thisArg) {
  forEach(b, function assignValue(val, key) {
    if (thisArg && typeof val === 'function') {
      a[key] = bind(val, thisArg);
    } else {
      a[key] = val;
    }
  });
  return a;
}

module.exports = {
  isArray: isArray,
  isArrayBuffer: isArrayBuffer,
  isBuffer: isBuffer,
  isFormData: isFormData,
  isArrayBufferView: isArrayBufferView,
  isString: isString,
  isNumber: isNumber,
  isObject: isObject,
  isUndefined: isUndefined,
  isDate: isDate,
  isFile: isFile,
  isBlob: isBlob,
  isFunction: isFunction,
  isStream: isStream,
  isURLSearchParams: isURLSearchParams,
  isStandardBrowserEnv: isStandardBrowserEnv,
  forEach: forEach,
  merge: merge,
  deepMerge: deepMerge,
  extend: extend,
  trim: trim
};

/***/ }),

/***/ "./node_modules/axios/node_modules/is-buffer/index.js":
/*!************************************************************!*\
  !*** ./node_modules/axios/node_modules/is-buffer/index.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/*!
 * Determine if an object is a Buffer
 *
 * @author   Feross Aboukhadijeh <https://feross.org>
 * @license  MIT
 */
module.exports = function isBuffer(obj) {
  return obj != null && obj.constructor != null && typeof obj.constructor.isBuffer === 'function' && obj.constructor.isBuffer(obj);
};

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars.runtime.js":
/*!****************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars.runtime.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true; // istanbul ignore next

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    'default': obj
  };
} // istanbul ignore next


function _interopRequireWildcard(obj) {
  if (obj && obj.__esModule) {
    return obj;
  } else {
    var newObj = {};

    if (obj != null) {
      for (var key in obj) {
        if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key];
      }
    }

    newObj['default'] = obj;
    return newObj;
  }
}

var _handlebarsBase = __webpack_require__(/*! ./handlebars/base */ "./node_modules/handlebars/dist/cjs/handlebars/base.js");

var base = _interopRequireWildcard(_handlebarsBase); // Each of these augment the Handlebars object. No need to setup here.
// (This is done to easily share code between commonjs and browse envs)


var _handlebarsSafeString = __webpack_require__(/*! ./handlebars/safe-string */ "./node_modules/handlebars/dist/cjs/handlebars/safe-string.js");

var _handlebarsSafeString2 = _interopRequireDefault(_handlebarsSafeString);

var _handlebarsException = __webpack_require__(/*! ./handlebars/exception */ "./node_modules/handlebars/dist/cjs/handlebars/exception.js");

var _handlebarsException2 = _interopRequireDefault(_handlebarsException);

var _handlebarsUtils = __webpack_require__(/*! ./handlebars/utils */ "./node_modules/handlebars/dist/cjs/handlebars/utils.js");

var Utils = _interopRequireWildcard(_handlebarsUtils);

var _handlebarsRuntime = __webpack_require__(/*! ./handlebars/runtime */ "./node_modules/handlebars/dist/cjs/handlebars/runtime.js");

var runtime = _interopRequireWildcard(_handlebarsRuntime);

var _handlebarsNoConflict = __webpack_require__(/*! ./handlebars/no-conflict */ "./node_modules/handlebars/dist/cjs/handlebars/no-conflict.js");

var _handlebarsNoConflict2 = _interopRequireDefault(_handlebarsNoConflict); // For compatibility and usage outside of module systems, make the Handlebars object a namespace


function create() {
  var hb = new base.HandlebarsEnvironment();
  Utils.extend(hb, base);
  hb.SafeString = _handlebarsSafeString2['default'];
  hb.Exception = _handlebarsException2['default'];
  hb.Utils = Utils;
  hb.escapeExpression = Utils.escapeExpression;
  hb.VM = runtime;

  hb.template = function (spec) {
    return runtime.template(spec, hb);
  };

  return hb;
}

var inst = create();
inst.create = create;

_handlebarsNoConflict2['default'](inst);

inst['default'] = inst;
exports['default'] = inst;
module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/base.js":
/*!*************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/base.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.HandlebarsEnvironment = HandlebarsEnvironment; // istanbul ignore next

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    'default': obj
  };
}

var _utils = __webpack_require__(/*! ./utils */ "./node_modules/handlebars/dist/cjs/handlebars/utils.js");

var _exception = __webpack_require__(/*! ./exception */ "./node_modules/handlebars/dist/cjs/handlebars/exception.js");

var _exception2 = _interopRequireDefault(_exception);

var _helpers = __webpack_require__(/*! ./helpers */ "./node_modules/handlebars/dist/cjs/handlebars/helpers.js");

var _decorators = __webpack_require__(/*! ./decorators */ "./node_modules/handlebars/dist/cjs/handlebars/decorators.js");

var _logger = __webpack_require__(/*! ./logger */ "./node_modules/handlebars/dist/cjs/handlebars/logger.js");

var _logger2 = _interopRequireDefault(_logger);

var VERSION = '4.1.2';
exports.VERSION = VERSION;
var COMPILER_REVISION = 7;
exports.COMPILER_REVISION = COMPILER_REVISION;
var REVISION_CHANGES = {
  1: '<= 1.0.rc.2',
  // 1.0.rc.2 is actually rev2 but doesn't report it
  2: '== 1.0.0-rc.3',
  3: '== 1.0.0-rc.4',
  4: '== 1.x.x',
  5: '== 2.0.0-alpha.x',
  6: '>= 2.0.0-beta.1',
  7: '>= 4.0.0'
};
exports.REVISION_CHANGES = REVISION_CHANGES;
var objectType = '[object Object]';

function HandlebarsEnvironment(helpers, partials, decorators) {
  this.helpers = helpers || {};
  this.partials = partials || {};
  this.decorators = decorators || {};

  _helpers.registerDefaultHelpers(this);

  _decorators.registerDefaultDecorators(this);
}

HandlebarsEnvironment.prototype = {
  constructor: HandlebarsEnvironment,
  logger: _logger2['default'],
  log: _logger2['default'].log,
  registerHelper: function registerHelper(name, fn) {
    if (_utils.toString.call(name) === objectType) {
      if (fn) {
        throw new _exception2['default']('Arg not supported with multiple helpers');
      }

      _utils.extend(this.helpers, name);
    } else {
      this.helpers[name] = fn;
    }
  },
  unregisterHelper: function unregisterHelper(name) {
    delete this.helpers[name];
  },
  registerPartial: function registerPartial(name, partial) {
    if (_utils.toString.call(name) === objectType) {
      _utils.extend(this.partials, name);
    } else {
      if (typeof partial === 'undefined') {
        throw new _exception2['default']('Attempting to register a partial called "' + name + '" as undefined');
      }

      this.partials[name] = partial;
    }
  },
  unregisterPartial: function unregisterPartial(name) {
    delete this.partials[name];
  },
  registerDecorator: function registerDecorator(name, fn) {
    if (_utils.toString.call(name) === objectType) {
      if (fn) {
        throw new _exception2['default']('Arg not supported with multiple decorators');
      }

      _utils.extend(this.decorators, name);
    } else {
      this.decorators[name] = fn;
    }
  },
  unregisterDecorator: function unregisterDecorator(name) {
    delete this.decorators[name];
  }
};
var log = _logger2['default'].log;
exports.log = log;
exports.createFrame = _utils.createFrame;
exports.logger = _logger2['default'];

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/decorators.js":
/*!*******************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/decorators.js ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.registerDefaultDecorators = registerDefaultDecorators; // istanbul ignore next

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    'default': obj
  };
}

var _decoratorsInline = __webpack_require__(/*! ./decorators/inline */ "./node_modules/handlebars/dist/cjs/handlebars/decorators/inline.js");

var _decoratorsInline2 = _interopRequireDefault(_decoratorsInline);

function registerDefaultDecorators(instance) {
  _decoratorsInline2['default'](instance);
}

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/decorators/inline.js":
/*!**************************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/decorators/inline.js ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _utils = __webpack_require__(/*! ../utils */ "./node_modules/handlebars/dist/cjs/handlebars/utils.js");

exports['default'] = function (instance) {
  instance.registerDecorator('inline', function (fn, props, container, options) {
    var ret = fn;

    if (!props.partials) {
      props.partials = {};

      ret = function (context, options) {
        // Create a new partials stack frame prior to exec.
        var original = container.partials;
        container.partials = _utils.extend({}, original, props.partials);
        var ret = fn(context, options);
        container.partials = original;
        return ret;
      };
    }

    props.partials[options.args[0]] = options.fn;
    return ret;
  });
};

module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/exception.js":
/*!******************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/exception.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
var errorProps = ['description', 'fileName', 'lineNumber', 'message', 'name', 'number', 'stack'];

function Exception(message, node) {
  var loc = node && node.loc,
      line = undefined,
      column = undefined;

  if (loc) {
    line = loc.start.line;
    column = loc.start.column;
    message += ' - ' + line + ':' + column;
  }

  var tmp = Error.prototype.constructor.call(this, message); // Unfortunately errors are not enumerable in Chrome (at least), so `for prop in tmp` doesn't work.

  for (var idx = 0; idx < errorProps.length; idx++) {
    this[errorProps[idx]] = tmp[errorProps[idx]];
  }
  /* istanbul ignore else */


  if (Error.captureStackTrace) {
    Error.captureStackTrace(this, Exception);
  }

  try {
    if (loc) {
      this.lineNumber = line; // Work around issue under safari where we can't directly set the column value

      /* istanbul ignore next */

      if (Object.defineProperty) {
        Object.defineProperty(this, 'column', {
          value: column,
          enumerable: true
        });
      } else {
        this.column = column;
      }
    }
  } catch (nop) {
    /* Ignore if the browser is very particular */
  }
}

Exception.prototype = new Error();
exports['default'] = Exception;
module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/helpers.js":
/*!****************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/helpers.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.registerDefaultHelpers = registerDefaultHelpers; // istanbul ignore next

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    'default': obj
  };
}

var _helpersBlockHelperMissing = __webpack_require__(/*! ./helpers/block-helper-missing */ "./node_modules/handlebars/dist/cjs/handlebars/helpers/block-helper-missing.js");

var _helpersBlockHelperMissing2 = _interopRequireDefault(_helpersBlockHelperMissing);

var _helpersEach = __webpack_require__(/*! ./helpers/each */ "./node_modules/handlebars/dist/cjs/handlebars/helpers/each.js");

var _helpersEach2 = _interopRequireDefault(_helpersEach);

var _helpersHelperMissing = __webpack_require__(/*! ./helpers/helper-missing */ "./node_modules/handlebars/dist/cjs/handlebars/helpers/helper-missing.js");

var _helpersHelperMissing2 = _interopRequireDefault(_helpersHelperMissing);

var _helpersIf = __webpack_require__(/*! ./helpers/if */ "./node_modules/handlebars/dist/cjs/handlebars/helpers/if.js");

var _helpersIf2 = _interopRequireDefault(_helpersIf);

var _helpersLog = __webpack_require__(/*! ./helpers/log */ "./node_modules/handlebars/dist/cjs/handlebars/helpers/log.js");

var _helpersLog2 = _interopRequireDefault(_helpersLog);

var _helpersLookup = __webpack_require__(/*! ./helpers/lookup */ "./node_modules/handlebars/dist/cjs/handlebars/helpers/lookup.js");

var _helpersLookup2 = _interopRequireDefault(_helpersLookup);

var _helpersWith = __webpack_require__(/*! ./helpers/with */ "./node_modules/handlebars/dist/cjs/handlebars/helpers/with.js");

var _helpersWith2 = _interopRequireDefault(_helpersWith);

function registerDefaultHelpers(instance) {
  _helpersBlockHelperMissing2['default'](instance);

  _helpersEach2['default'](instance);

  _helpersHelperMissing2['default'](instance);

  _helpersIf2['default'](instance);

  _helpersLog2['default'](instance);

  _helpersLookup2['default'](instance);

  _helpersWith2['default'](instance);
}

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/helpers/block-helper-missing.js":
/*!*************************************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/helpers/block-helper-missing.js ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _utils = __webpack_require__(/*! ../utils */ "./node_modules/handlebars/dist/cjs/handlebars/utils.js");

exports['default'] = function (instance) {
  instance.registerHelper('blockHelperMissing', function (context, options) {
    var inverse = options.inverse,
        fn = options.fn;

    if (context === true) {
      return fn(this);
    } else if (context === false || context == null) {
      return inverse(this);
    } else if (_utils.isArray(context)) {
      if (context.length > 0) {
        if (options.ids) {
          options.ids = [options.name];
        }

        return instance.helpers.each(context, options);
      } else {
        return inverse(this);
      }
    } else {
      if (options.data && options.ids) {
        var data = _utils.createFrame(options.data);

        data.contextPath = _utils.appendContextPath(options.data.contextPath, options.name);
        options = {
          data: data
        };
      }

      return fn(context, options);
    }
  });
};

module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/helpers/each.js":
/*!*********************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/helpers/each.js ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true; // istanbul ignore next

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    'default': obj
  };
}

var _utils = __webpack_require__(/*! ../utils */ "./node_modules/handlebars/dist/cjs/handlebars/utils.js");

var _exception = __webpack_require__(/*! ../exception */ "./node_modules/handlebars/dist/cjs/handlebars/exception.js");

var _exception2 = _interopRequireDefault(_exception);

exports['default'] = function (instance) {
  instance.registerHelper('each', function (context, options) {
    if (!options) {
      throw new _exception2['default']('Must pass iterator to #each');
    }

    var fn = options.fn,
        inverse = options.inverse,
        i = 0,
        ret = '',
        data = undefined,
        contextPath = undefined;

    if (options.data && options.ids) {
      contextPath = _utils.appendContextPath(options.data.contextPath, options.ids[0]) + '.';
    }

    if (_utils.isFunction(context)) {
      context = context.call(this);
    }

    if (options.data) {
      data = _utils.createFrame(options.data);
    }

    function execIteration(field, index, last) {
      if (data) {
        data.key = field;
        data.index = index;
        data.first = index === 0;
        data.last = !!last;

        if (contextPath) {
          data.contextPath = contextPath + field;
        }
      }

      ret = ret + fn(context[field], {
        data: data,
        blockParams: _utils.blockParams([context[field], field], [contextPath + field, null])
      });
    }

    if (context && typeof context === 'object') {
      if (_utils.isArray(context)) {
        for (var j = context.length; i < j; i++) {
          if (i in context) {
            execIteration(i, i, i === context.length - 1);
          }
        }
      } else {
        var priorKey = undefined;

        for (var key in context) {
          if (context.hasOwnProperty(key)) {
            // We're running the iterations one step out of sync so we can detect
            // the last iteration without have to scan the object twice and create
            // an itermediate keys array.
            if (priorKey !== undefined) {
              execIteration(priorKey, i - 1);
            }

            priorKey = key;
            i++;
          }
        }

        if (priorKey !== undefined) {
          execIteration(priorKey, i - 1, true);
        }
      }
    }

    if (i === 0) {
      ret = inverse(this);
    }

    return ret;
  });
};

module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/helpers/helper-missing.js":
/*!*******************************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/helpers/helper-missing.js ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true; // istanbul ignore next

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    'default': obj
  };
}

var _exception = __webpack_require__(/*! ../exception */ "./node_modules/handlebars/dist/cjs/handlebars/exception.js");

var _exception2 = _interopRequireDefault(_exception);

exports['default'] = function (instance) {
  instance.registerHelper('helperMissing', function ()
  /* [args, ]options */
  {
    if (arguments.length === 1) {
      // A missing field in a {{foo}} construct.
      return undefined;
    } else {
      // Someone is actually trying to call something, blow up.
      throw new _exception2['default']('Missing helper: "' + arguments[arguments.length - 1].name + '"');
    }
  });
};

module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/helpers/if.js":
/*!*******************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/helpers/if.js ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _utils = __webpack_require__(/*! ../utils */ "./node_modules/handlebars/dist/cjs/handlebars/utils.js");

exports['default'] = function (instance) {
  instance.registerHelper('if', function (conditional, options) {
    if (_utils.isFunction(conditional)) {
      conditional = conditional.call(this);
    } // Default behavior is to render the positive path if the value is truthy and not empty.
    // The `includeZero` option may be set to treat the condtional as purely not empty based on the
    // behavior of isEmpty. Effectively this determines if 0 is handled by the positive path or negative.


    if (!options.hash.includeZero && !conditional || _utils.isEmpty(conditional)) {
      return options.inverse(this);
    } else {
      return options.fn(this);
    }
  });
  instance.registerHelper('unless', function (conditional, options) {
    return instance.helpers['if'].call(this, conditional, {
      fn: options.inverse,
      inverse: options.fn,
      hash: options.hash
    });
  });
};

module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/helpers/log.js":
/*!********************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/helpers/log.js ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

exports['default'] = function (instance) {
  instance.registerHelper('log', function ()
  /* message, options */
  {
    var args = [undefined],
        options = arguments[arguments.length - 1];

    for (var i = 0; i < arguments.length - 1; i++) {
      args.push(arguments[i]);
    }

    var level = 1;

    if (options.hash.level != null) {
      level = options.hash.level;
    } else if (options.data && options.data.level != null) {
      level = options.data.level;
    }

    args[0] = level;
    instance.log.apply(instance, args);
  });
};

module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/helpers/lookup.js":
/*!***********************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/helpers/lookup.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

exports['default'] = function (instance) {
  instance.registerHelper('lookup', function (obj, field) {
    if (!obj) {
      return obj;
    }

    if (field === 'constructor' && !obj.propertyIsEnumerable(field)) {
      return undefined;
    }

    return obj[field];
  });
};

module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/helpers/with.js":
/*!*********************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/helpers/with.js ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _utils = __webpack_require__(/*! ../utils */ "./node_modules/handlebars/dist/cjs/handlebars/utils.js");

exports['default'] = function (instance) {
  instance.registerHelper('with', function (context, options) {
    if (_utils.isFunction(context)) {
      context = context.call(this);
    }

    var fn = options.fn;

    if (!_utils.isEmpty(context)) {
      var data = options.data;

      if (options.data && options.ids) {
        data = _utils.createFrame(options.data);
        data.contextPath = _utils.appendContextPath(options.data.contextPath, options.ids[0]);
      }

      return fn(context, {
        data: data,
        blockParams: _utils.blockParams([context], [data && data.contextPath])
      });
    } else {
      return options.inverse(this);
    }
  });
};

module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/logger.js":
/*!***************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/logger.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _utils = __webpack_require__(/*! ./utils */ "./node_modules/handlebars/dist/cjs/handlebars/utils.js");

var logger = {
  methodMap: ['debug', 'info', 'warn', 'error'],
  level: 'info',
  // Maps a given level value to the `methodMap` indexes above.
  lookupLevel: function lookupLevel(level) {
    if (typeof level === 'string') {
      var levelMap = _utils.indexOf(logger.methodMap, level.toLowerCase());

      if (levelMap >= 0) {
        level = levelMap;
      } else {
        level = parseInt(level, 10);
      }
    }

    return level;
  },
  // Can be overridden in the host environment
  log: function log(level) {
    level = logger.lookupLevel(level);

    if (typeof console !== 'undefined' && logger.lookupLevel(logger.level) <= level) {
      var method = logger.methodMap[level];

      if (!console[method]) {
        // eslint-disable-line no-console
        method = 'log';
      }

      for (var _len = arguments.length, message = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        message[_key - 1] = arguments[_key];
      }

      console[method].apply(console, message); // eslint-disable-line no-console
    }
  }
};
exports['default'] = logger;
module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/no-conflict.js":
/*!********************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/no-conflict.js ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {/* global window */


exports.__esModule = true;

exports['default'] = function (Handlebars) {
  /* istanbul ignore next */
  var root = typeof global !== 'undefined' ? global : window,
      $Handlebars = root.Handlebars;
  /* istanbul ignore next */

  Handlebars.noConflict = function () {
    if (root.Handlebars === Handlebars) {
      root.Handlebars = $Handlebars;
    }

    return Handlebars;
  };
};

module.exports = exports['default'];
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/runtime.js":
/*!****************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/runtime.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.checkRevision = checkRevision;
exports.template = template;
exports.wrapProgram = wrapProgram;
exports.resolvePartial = resolvePartial;
exports.invokePartial = invokePartial;
exports.noop = noop; // istanbul ignore next

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    'default': obj
  };
} // istanbul ignore next


function _interopRequireWildcard(obj) {
  if (obj && obj.__esModule) {
    return obj;
  } else {
    var newObj = {};

    if (obj != null) {
      for (var key in obj) {
        if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key];
      }
    }

    newObj['default'] = obj;
    return newObj;
  }
}

var _utils = __webpack_require__(/*! ./utils */ "./node_modules/handlebars/dist/cjs/handlebars/utils.js");

var Utils = _interopRequireWildcard(_utils);

var _exception = __webpack_require__(/*! ./exception */ "./node_modules/handlebars/dist/cjs/handlebars/exception.js");

var _exception2 = _interopRequireDefault(_exception);

var _base = __webpack_require__(/*! ./base */ "./node_modules/handlebars/dist/cjs/handlebars/base.js");

function checkRevision(compilerInfo) {
  var compilerRevision = compilerInfo && compilerInfo[0] || 1,
      currentRevision = _base.COMPILER_REVISION;

  if (compilerRevision !== currentRevision) {
    if (compilerRevision < currentRevision) {
      var runtimeVersions = _base.REVISION_CHANGES[currentRevision],
          compilerVersions = _base.REVISION_CHANGES[compilerRevision];
      throw new _exception2['default']('Template was precompiled with an older version of Handlebars than the current runtime. ' + 'Please update your precompiler to a newer version (' + runtimeVersions + ') or downgrade your runtime to an older version (' + compilerVersions + ').');
    } else {
      // Use the embedded version info since the runtime doesn't know about this revision yet
      throw new _exception2['default']('Template was precompiled with a newer version of Handlebars than the current runtime. ' + 'Please update your runtime to a newer version (' + compilerInfo[1] + ').');
    }
  }
}

function template(templateSpec, env) {
  /* istanbul ignore next */
  if (!env) {
    throw new _exception2['default']('No environment passed to template');
  }

  if (!templateSpec || !templateSpec.main) {
    throw new _exception2['default']('Unknown template object: ' + typeof templateSpec);
  }

  templateSpec.main.decorator = templateSpec.main_d; // Note: Using env.VM references rather than local var references throughout this section to allow
  // for external users to override these as psuedo-supported APIs.

  env.VM.checkRevision(templateSpec.compiler);

  function invokePartialWrapper(partial, context, options) {
    if (options.hash) {
      context = Utils.extend({}, context, options.hash);

      if (options.ids) {
        options.ids[0] = true;
      }
    }

    partial = env.VM.resolvePartial.call(this, partial, context, options);
    var result = env.VM.invokePartial.call(this, partial, context, options);

    if (result == null && env.compile) {
      options.partials[options.name] = env.compile(partial, templateSpec.compilerOptions, env);
      result = options.partials[options.name](context, options);
    }

    if (result != null) {
      if (options.indent) {
        var lines = result.split('\n');

        for (var i = 0, l = lines.length; i < l; i++) {
          if (!lines[i] && i + 1 === l) {
            break;
          }

          lines[i] = options.indent + lines[i];
        }

        result = lines.join('\n');
      }

      return result;
    } else {
      throw new _exception2['default']('The partial ' + options.name + ' could not be compiled when running in runtime-only mode');
    }
  } // Just add water


  var container = {
    strict: function strict(obj, name) {
      if (!(name in obj)) {
        throw new _exception2['default']('"' + name + '" not defined in ' + obj);
      }

      return obj[name];
    },
    lookup: function lookup(depths, name) {
      var len = depths.length;

      for (var i = 0; i < len; i++) {
        if (depths[i] && depths[i][name] != null) {
          return depths[i][name];
        }
      }
    },
    lambda: function lambda(current, context) {
      return typeof current === 'function' ? current.call(context) : current;
    },
    escapeExpression: Utils.escapeExpression,
    invokePartial: invokePartialWrapper,
    fn: function fn(i) {
      var ret = templateSpec[i];
      ret.decorator = templateSpec[i + '_d'];
      return ret;
    },
    programs: [],
    program: function program(i, data, declaredBlockParams, blockParams, depths) {
      var programWrapper = this.programs[i],
          fn = this.fn(i);

      if (data || depths || blockParams || declaredBlockParams) {
        programWrapper = wrapProgram(this, i, fn, data, declaredBlockParams, blockParams, depths);
      } else if (!programWrapper) {
        programWrapper = this.programs[i] = wrapProgram(this, i, fn);
      }

      return programWrapper;
    },
    data: function data(value, depth) {
      while (value && depth--) {
        value = value._parent;
      }

      return value;
    },
    merge: function merge(param, common) {
      var obj = param || common;

      if (param && common && param !== common) {
        obj = Utils.extend({}, common, param);
      }

      return obj;
    },
    // An empty object to use as replacement for null-contexts
    nullContext: Object.seal({}),
    noop: env.VM.noop,
    compilerInfo: templateSpec.compiler
  };

  function ret(context) {
    var options = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];
    var data = options.data;

    ret._setup(options);

    if (!options.partial && templateSpec.useData) {
      data = initData(context, data);
    }

    var depths = undefined,
        blockParams = templateSpec.useBlockParams ? [] : undefined;

    if (templateSpec.useDepths) {
      if (options.depths) {
        depths = context != options.depths[0] ? [context].concat(options.depths) : options.depths;
      } else {
        depths = [context];
      }
    }

    function main(context
    /*, options*/
    ) {
      return '' + templateSpec.main(container, context, container.helpers, container.partials, data, blockParams, depths);
    }

    main = executeDecorators(templateSpec.main, main, container, options.depths || [], data, blockParams);
    return main(context, options);
  }

  ret.isTop = true;

  ret._setup = function (options) {
    if (!options.partial) {
      container.helpers = container.merge(options.helpers, env.helpers);

      if (templateSpec.usePartial) {
        container.partials = container.merge(options.partials, env.partials);
      }

      if (templateSpec.usePartial || templateSpec.useDecorators) {
        container.decorators = container.merge(options.decorators, env.decorators);
      }
    } else {
      container.helpers = options.helpers;
      container.partials = options.partials;
      container.decorators = options.decorators;
    }
  };

  ret._child = function (i, data, blockParams, depths) {
    if (templateSpec.useBlockParams && !blockParams) {
      throw new _exception2['default']('must pass block params');
    }

    if (templateSpec.useDepths && !depths) {
      throw new _exception2['default']('must pass parent depths');
    }

    return wrapProgram(container, i, templateSpec[i], data, 0, blockParams, depths);
  };

  return ret;
}

function wrapProgram(container, i, fn, data, declaredBlockParams, blockParams, depths) {
  function prog(context) {
    var options = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];
    var currentDepths = depths;

    if (depths && context != depths[0] && !(context === container.nullContext && depths[0] === null)) {
      currentDepths = [context].concat(depths);
    }

    return fn(container, context, container.helpers, container.partials, options.data || data, blockParams && [options.blockParams].concat(blockParams), currentDepths);
  }

  prog = executeDecorators(fn, prog, container, depths, data, blockParams);
  prog.program = i;
  prog.depth = depths ? depths.length : 0;
  prog.blockParams = declaredBlockParams || 0;
  return prog;
}

function resolvePartial(partial, context, options) {
  if (!partial) {
    if (options.name === '@partial-block') {
      partial = options.data['partial-block'];
    } else {
      partial = options.partials[options.name];
    }
  } else if (!partial.call && !options.name) {
    // This is a dynamic partial that returned a string
    options.name = partial;
    partial = options.partials[partial];
  }

  return partial;
}

function invokePartial(partial, context, options) {
  // Use the current closure context to save the partial-block if this partial
  var currentPartialBlock = options.data && options.data['partial-block'];
  options.partial = true;

  if (options.ids) {
    options.data.contextPath = options.ids[0] || options.data.contextPath;
  }

  var partialBlock = undefined;

  if (options.fn && options.fn !== noop) {
    (function () {
      options.data = _base.createFrame(options.data); // Wrapper function to get access to currentPartialBlock from the closure

      var fn = options.fn;

      partialBlock = options.data['partial-block'] = function partialBlockWrapper(context) {
        var options = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1]; // Restore the partial-block from the closure for the execution of the block
        // i.e. the part inside the block of the partial call.

        options.data = _base.createFrame(options.data);
        options.data['partial-block'] = currentPartialBlock;
        return fn(context, options);
      };

      if (fn.partials) {
        options.partials = Utils.extend({}, options.partials, fn.partials);
      }
    })();
  }

  if (partial === undefined && partialBlock) {
    partial = partialBlock;
  }

  if (partial === undefined) {
    throw new _exception2['default']('The partial ' + options.name + ' could not be found');
  } else if (partial instanceof Function) {
    return partial(context, options);
  }
}

function noop() {
  return '';
}

function initData(context, data) {
  if (!data || !('root' in data)) {
    data = data ? _base.createFrame(data) : {};
    data.root = context;
  }

  return data;
}

function executeDecorators(fn, prog, container, depths, data, blockParams) {
  if (fn.decorator) {
    var props = {};
    prog = fn.decorator(prog, props, container, depths && depths[0], data, blockParams, depths);
    Utils.extend(prog, props);
  }

  return prog;
}

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/safe-string.js":
/*!********************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/safe-string.js ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Build out our basic SafeString type


exports.__esModule = true;

function SafeString(string) {
  this.string = string;
}

SafeString.prototype.toString = SafeString.prototype.toHTML = function () {
  return '' + this.string;
};

exports['default'] = SafeString;
module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/utils.js":
/*!**************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/utils.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.extend = extend;
exports.indexOf = indexOf;
exports.escapeExpression = escapeExpression;
exports.isEmpty = isEmpty;
exports.createFrame = createFrame;
exports.blockParams = blockParams;
exports.appendContextPath = appendContextPath;
var escape = {
  '&': '&amp;',
  '<': '&lt;',
  '>': '&gt;',
  '"': '&quot;',
  "'": '&#x27;',
  '`': '&#x60;',
  '=': '&#x3D;'
};
var badChars = /[&<>"'`=]/g,
    possible = /[&<>"'`=]/;

function escapeChar(chr) {
  return escape[chr];
}

function extend(obj
/* , ...source */
) {
  for (var i = 1; i < arguments.length; i++) {
    for (var key in arguments[i]) {
      if (Object.prototype.hasOwnProperty.call(arguments[i], key)) {
        obj[key] = arguments[i][key];
      }
    }
  }

  return obj;
}

var toString = Object.prototype.toString;
exports.toString = toString; // Sourced from lodash
// https://github.com/bestiejs/lodash/blob/master/LICENSE.txt

/* eslint-disable func-style */

var isFunction = function isFunction(value) {
  return typeof value === 'function';
}; // fallback for older versions of Chrome and Safari

/* istanbul ignore next */


if (isFunction(/x/)) {
  exports.isFunction = isFunction = function (value) {
    return typeof value === 'function' && toString.call(value) === '[object Function]';
  };
}

exports.isFunction = isFunction;
/* eslint-enable func-style */

/* istanbul ignore next */

var isArray = Array.isArray || function (value) {
  return value && typeof value === 'object' ? toString.call(value) === '[object Array]' : false;
};

exports.isArray = isArray; // Older IE versions do not directly support indexOf so we must implement our own, sadly.

function indexOf(array, value) {
  for (var i = 0, len = array.length; i < len; i++) {
    if (array[i] === value) {
      return i;
    }
  }

  return -1;
}

function escapeExpression(string) {
  if (typeof string !== 'string') {
    // don't escape SafeStrings, since they're already safe
    if (string && string.toHTML) {
      return string.toHTML();
    } else if (string == null) {
      return '';
    } else if (!string) {
      return string + '';
    } // Force a string conversion as this will be done by the append regardless and
    // the regex test will do this transparently behind the scenes, causing issues if
    // an object's to string has escaped characters in it.


    string = '' + string;
  }

  if (!possible.test(string)) {
    return string;
  }

  return string.replace(badChars, escapeChar);
}

function isEmpty(value) {
  if (!value && value !== 0) {
    return true;
  } else if (isArray(value) && value.length === 0) {
    return true;
  } else {
    return false;
  }
}

function createFrame(object) {
  var frame = extend({}, object);
  frame._parent = object;
  return frame;
}

function blockParams(params, ids) {
  params.path = ids;
  return params;
}

function appendContextPath(contextPath, id) {
  return (contextPath ? contextPath + '.' : '') + id;
}

/***/ }),

/***/ "./node_modules/handlebars/runtime.js":
/*!********************************************!*\
  !*** ./node_modules/handlebars/runtime.js ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Create a simple path alias to allow browserify to resolve
// the runtime on a supported path.
module.exports = __webpack_require__(/*! ./dist/cjs/handlebars.runtime */ "./node_modules/handlebars/dist/cjs/handlebars.runtime.js")['default'];

/***/ }),

/***/ "./node_modules/process/browser.js":
/*!*****************************************!*\
  !*** ./node_modules/process/browser.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// shim for using process in browser
var process = module.exports = {}; // cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
  throw new Error('setTimeout has not been defined');
}

function defaultClearTimeout() {
  throw new Error('clearTimeout has not been defined');
}

(function () {
  try {
    if (typeof setTimeout === 'function') {
      cachedSetTimeout = setTimeout;
    } else {
      cachedSetTimeout = defaultSetTimout;
    }
  } catch (e) {
    cachedSetTimeout = defaultSetTimout;
  }

  try {
    if (typeof clearTimeout === 'function') {
      cachedClearTimeout = clearTimeout;
    } else {
      cachedClearTimeout = defaultClearTimeout;
    }
  } catch (e) {
    cachedClearTimeout = defaultClearTimeout;
  }
})();

function runTimeout(fun) {
  if (cachedSetTimeout === setTimeout) {
    //normal enviroments in sane situations
    return setTimeout(fun, 0);
  } // if setTimeout wasn't available but was latter defined


  if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
    cachedSetTimeout = setTimeout;
    return setTimeout(fun, 0);
  }

  try {
    // when when somebody has screwed with setTimeout but no I.E. maddness
    return cachedSetTimeout(fun, 0);
  } catch (e) {
    try {
      // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
      return cachedSetTimeout.call(null, fun, 0);
    } catch (e) {
      // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
      return cachedSetTimeout.call(this, fun, 0);
    }
  }
}

function runClearTimeout(marker) {
  if (cachedClearTimeout === clearTimeout) {
    //normal enviroments in sane situations
    return clearTimeout(marker);
  } // if clearTimeout wasn't available but was latter defined


  if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
    cachedClearTimeout = clearTimeout;
    return clearTimeout(marker);
  }

  try {
    // when when somebody has screwed with setTimeout but no I.E. maddness
    return cachedClearTimeout(marker);
  } catch (e) {
    try {
      // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
      return cachedClearTimeout.call(null, marker);
    } catch (e) {
      // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
      // Some versions of I.E. have different rules for clearTimeout vs setTimeout
      return cachedClearTimeout.call(this, marker);
    }
  }
}

var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
  if (!draining || !currentQueue) {
    return;
  }

  draining = false;

  if (currentQueue.length) {
    queue = currentQueue.concat(queue);
  } else {
    queueIndex = -1;
  }

  if (queue.length) {
    drainQueue();
  }
}

function drainQueue() {
  if (draining) {
    return;
  }

  var timeout = runTimeout(cleanUpNextTick);
  draining = true;
  var len = queue.length;

  while (len) {
    currentQueue = queue;
    queue = [];

    while (++queueIndex < len) {
      if (currentQueue) {
        currentQueue[queueIndex].run();
      }
    }

    queueIndex = -1;
    len = queue.length;
  }

  currentQueue = null;
  draining = false;
  runClearTimeout(timeout);
}

process.nextTick = function (fun) {
  var args = new Array(arguments.length - 1);

  if (arguments.length > 1) {
    for (var i = 1; i < arguments.length; i++) {
      args[i - 1] = arguments[i];
    }
  }

  queue.push(new Item(fun, args));

  if (queue.length === 1 && !draining) {
    runTimeout(drainQueue);
  }
}; // v8 likes predictible objects


function Item(fun, array) {
  this.fun = fun;
  this.array = array;
}

Item.prototype.run = function () {
  this.fun.apply(null, this.array);
};

process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues

process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;
process.prependListener = noop;
process.prependOnceListener = noop;

process.listeners = function (name) {
  return [];
};

process.binding = function (name) {
  throw new Error('process.binding is not supported');
};

process.cwd = function () {
  return '/';
};

process.chdir = function (dir) {
  throw new Error('process.chdir is not supported');
};

process.umask = function () {
  return 0;
};

/***/ }),

/***/ "./node_modules/webpack/buildin/global.js":
/*!***********************************!*\
  !*** (webpack)/buildin/global.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

var g; // This works in non-strict mode

g = function () {
  return this;
}();

try {
  // This works if eval is allowed (see CSP)
  g = g || new Function("return this")();
} catch (e) {
  // This works if the window reference is available
  if (typeof window === "object") g = window;
} // g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}


module.exports = g;

/***/ }),

/***/ "./node_modules/whatwg-fetch/fetch.js":
/*!********************************************!*\
  !*** ./node_modules/whatwg-fetch/fetch.js ***!
  \********************************************/
/*! exports provided: Headers, Request, Response, DOMException, fetch */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Headers", function() { return Headers; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Request", function() { return Request; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Response", function() { return Response; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DOMException", function() { return DOMException; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetch", function() { return fetch; });
var support = {
  searchParams: 'URLSearchParams' in self,
  iterable: 'Symbol' in self && 'iterator' in Symbol,
  blob: 'FileReader' in self && 'Blob' in self && function () {
    try {
      new Blob();
      return true;
    } catch (e) {
      return false;
    }
  }(),
  formData: 'FormData' in self,
  arrayBuffer: 'ArrayBuffer' in self
};

function isDataView(obj) {
  return obj && DataView.prototype.isPrototypeOf(obj);
}

if (support.arrayBuffer) {
  var viewClasses = ['[object Int8Array]', '[object Uint8Array]', '[object Uint8ClampedArray]', '[object Int16Array]', '[object Uint16Array]', '[object Int32Array]', '[object Uint32Array]', '[object Float32Array]', '[object Float64Array]'];

  var isArrayBufferView = ArrayBuffer.isView || function (obj) {
    return obj && viewClasses.indexOf(Object.prototype.toString.call(obj)) > -1;
  };
}

function normalizeName(name) {
  if (typeof name !== 'string') {
    name = String(name);
  }

  if (/[^a-z0-9\-#$%&'*+.^_`|~]/i.test(name)) {
    throw new TypeError('Invalid character in header field name');
  }

  return name.toLowerCase();
}

function normalizeValue(value) {
  if (typeof value !== 'string') {
    value = String(value);
  }

  return value;
} // Build a destructive iterator for the value list


function iteratorFor(items) {
  var iterator = {
    next: function () {
      var value = items.shift();
      return {
        done: value === undefined,
        value: value
      };
    }
  };

  if (support.iterable) {
    iterator[Symbol.iterator] = function () {
      return iterator;
    };
  }

  return iterator;
}

function Headers(headers) {
  this.map = {};

  if (headers instanceof Headers) {
    headers.forEach(function (value, name) {
      this.append(name, value);
    }, this);
  } else if (Array.isArray(headers)) {
    headers.forEach(function (header) {
      this.append(header[0], header[1]);
    }, this);
  } else if (headers) {
    Object.getOwnPropertyNames(headers).forEach(function (name) {
      this.append(name, headers[name]);
    }, this);
  }
}

Headers.prototype.append = function (name, value) {
  name = normalizeName(name);
  value = normalizeValue(value);
  var oldValue = this.map[name];
  this.map[name] = oldValue ? oldValue + ', ' + value : value;
};

Headers.prototype['delete'] = function (name) {
  delete this.map[normalizeName(name)];
};

Headers.prototype.get = function (name) {
  name = normalizeName(name);
  return this.has(name) ? this.map[name] : null;
};

Headers.prototype.has = function (name) {
  return this.map.hasOwnProperty(normalizeName(name));
};

Headers.prototype.set = function (name, value) {
  this.map[normalizeName(name)] = normalizeValue(value);
};

Headers.prototype.forEach = function (callback, thisArg) {
  for (var name in this.map) {
    if (this.map.hasOwnProperty(name)) {
      callback.call(thisArg, this.map[name], name, this);
    }
  }
};

Headers.prototype.keys = function () {
  var items = [];
  this.forEach(function (value, name) {
    items.push(name);
  });
  return iteratorFor(items);
};

Headers.prototype.values = function () {
  var items = [];
  this.forEach(function (value) {
    items.push(value);
  });
  return iteratorFor(items);
};

Headers.prototype.entries = function () {
  var items = [];
  this.forEach(function (value, name) {
    items.push([name, value]);
  });
  return iteratorFor(items);
};

if (support.iterable) {
  Headers.prototype[Symbol.iterator] = Headers.prototype.entries;
}

function consumed(body) {
  if (body.bodyUsed) {
    return Promise.reject(new TypeError('Already read'));
  }

  body.bodyUsed = true;
}

function fileReaderReady(reader) {
  return new Promise(function (resolve, reject) {
    reader.onload = function () {
      resolve(reader.result);
    };

    reader.onerror = function () {
      reject(reader.error);
    };
  });
}

function readBlobAsArrayBuffer(blob) {
  var reader = new FileReader();
  var promise = fileReaderReady(reader);
  reader.readAsArrayBuffer(blob);
  return promise;
}

function readBlobAsText(blob) {
  var reader = new FileReader();
  var promise = fileReaderReady(reader);
  reader.readAsText(blob);
  return promise;
}

function readArrayBufferAsText(buf) {
  var view = new Uint8Array(buf);
  var chars = new Array(view.length);

  for (var i = 0; i < view.length; i++) {
    chars[i] = String.fromCharCode(view[i]);
  }

  return chars.join('');
}

function bufferClone(buf) {
  if (buf.slice) {
    return buf.slice(0);
  } else {
    var view = new Uint8Array(buf.byteLength);
    view.set(new Uint8Array(buf));
    return view.buffer;
  }
}

function Body() {
  this.bodyUsed = false;

  this._initBody = function (body) {
    this._bodyInit = body;

    if (!body) {
      this._bodyText = '';
    } else if (typeof body === 'string') {
      this._bodyText = body;
    } else if (support.blob && Blob.prototype.isPrototypeOf(body)) {
      this._bodyBlob = body;
    } else if (support.formData && FormData.prototype.isPrototypeOf(body)) {
      this._bodyFormData = body;
    } else if (support.searchParams && URLSearchParams.prototype.isPrototypeOf(body)) {
      this._bodyText = body.toString();
    } else if (support.arrayBuffer && support.blob && isDataView(body)) {
      this._bodyArrayBuffer = bufferClone(body.buffer); // IE 10-11 can't handle a DataView body.

      this._bodyInit = new Blob([this._bodyArrayBuffer]);
    } else if (support.arrayBuffer && (ArrayBuffer.prototype.isPrototypeOf(body) || isArrayBufferView(body))) {
      this._bodyArrayBuffer = bufferClone(body);
    } else {
      this._bodyText = body = Object.prototype.toString.call(body);
    }

    if (!this.headers.get('content-type')) {
      if (typeof body === 'string') {
        this.headers.set('content-type', 'text/plain;charset=UTF-8');
      } else if (this._bodyBlob && this._bodyBlob.type) {
        this.headers.set('content-type', this._bodyBlob.type);
      } else if (support.searchParams && URLSearchParams.prototype.isPrototypeOf(body)) {
        this.headers.set('content-type', 'application/x-www-form-urlencoded;charset=UTF-8');
      }
    }
  };

  if (support.blob) {
    this.blob = function () {
      var rejected = consumed(this);

      if (rejected) {
        return rejected;
      }

      if (this._bodyBlob) {
        return Promise.resolve(this._bodyBlob);
      } else if (this._bodyArrayBuffer) {
        return Promise.resolve(new Blob([this._bodyArrayBuffer]));
      } else if (this._bodyFormData) {
        throw new Error('could not read FormData body as blob');
      } else {
        return Promise.resolve(new Blob([this._bodyText]));
      }
    };

    this.arrayBuffer = function () {
      if (this._bodyArrayBuffer) {
        return consumed(this) || Promise.resolve(this._bodyArrayBuffer);
      } else {
        return this.blob().then(readBlobAsArrayBuffer);
      }
    };
  }

  this.text = function () {
    var rejected = consumed(this);

    if (rejected) {
      return rejected;
    }

    if (this._bodyBlob) {
      return readBlobAsText(this._bodyBlob);
    } else if (this._bodyArrayBuffer) {
      return Promise.resolve(readArrayBufferAsText(this._bodyArrayBuffer));
    } else if (this._bodyFormData) {
      throw new Error('could not read FormData body as text');
    } else {
      return Promise.resolve(this._bodyText);
    }
  };

  if (support.formData) {
    this.formData = function () {
      return this.text().then(decode);
    };
  }

  this.json = function () {
    return this.text().then(JSON.parse);
  };

  return this;
} // HTTP methods whose capitalization should be normalized


var methods = ['DELETE', 'GET', 'HEAD', 'OPTIONS', 'POST', 'PUT'];

function normalizeMethod(method) {
  var upcased = method.toUpperCase();
  return methods.indexOf(upcased) > -1 ? upcased : method;
}

function Request(input, options) {
  options = options || {};
  var body = options.body;

  if (input instanceof Request) {
    if (input.bodyUsed) {
      throw new TypeError('Already read');
    }

    this.url = input.url;
    this.credentials = input.credentials;

    if (!options.headers) {
      this.headers = new Headers(input.headers);
    }

    this.method = input.method;
    this.mode = input.mode;
    this.signal = input.signal;

    if (!body && input._bodyInit != null) {
      body = input._bodyInit;
      input.bodyUsed = true;
    }
  } else {
    this.url = String(input);
  }

  this.credentials = options.credentials || this.credentials || 'same-origin';

  if (options.headers || !this.headers) {
    this.headers = new Headers(options.headers);
  }

  this.method = normalizeMethod(options.method || this.method || 'GET');
  this.mode = options.mode || this.mode || null;
  this.signal = options.signal || this.signal;
  this.referrer = null;

  if ((this.method === 'GET' || this.method === 'HEAD') && body) {
    throw new TypeError('Body not allowed for GET or HEAD requests');
  }

  this._initBody(body);
}

Request.prototype.clone = function () {
  return new Request(this, {
    body: this._bodyInit
  });
};

function decode(body) {
  var form = new FormData();
  body.trim().split('&').forEach(function (bytes) {
    if (bytes) {
      var split = bytes.split('=');
      var name = split.shift().replace(/\+/g, ' ');
      var value = split.join('=').replace(/\+/g, ' ');
      form.append(decodeURIComponent(name), decodeURIComponent(value));
    }
  });
  return form;
}

function parseHeaders(rawHeaders) {
  var headers = new Headers(); // Replace instances of \r\n and \n followed by at least one space or horizontal tab with a space
  // https://tools.ietf.org/html/rfc7230#section-3.2

  var preProcessedHeaders = rawHeaders.replace(/\r?\n[\t ]+/g, ' ');
  preProcessedHeaders.split(/\r?\n/).forEach(function (line) {
    var parts = line.split(':');
    var key = parts.shift().trim();

    if (key) {
      var value = parts.join(':').trim();
      headers.append(key, value);
    }
  });
  return headers;
}

Body.call(Request.prototype);
function Response(bodyInit, options) {
  if (!options) {
    options = {};
  }

  this.type = 'default';
  this.status = options.status === undefined ? 200 : options.status;
  this.ok = this.status >= 200 && this.status < 300;
  this.statusText = 'statusText' in options ? options.statusText : 'OK';
  this.headers = new Headers(options.headers);
  this.url = options.url || '';

  this._initBody(bodyInit);
}
Body.call(Response.prototype);

Response.prototype.clone = function () {
  return new Response(this._bodyInit, {
    status: this.status,
    statusText: this.statusText,
    headers: new Headers(this.headers),
    url: this.url
  });
};

Response.error = function () {
  var response = new Response(null, {
    status: 0,
    statusText: ''
  });
  response.type = 'error';
  return response;
};

var redirectStatuses = [301, 302, 303, 307, 308];

Response.redirect = function (url, status) {
  if (redirectStatuses.indexOf(status) === -1) {
    throw new RangeError('Invalid status code');
  }

  return new Response(null, {
    status: status,
    headers: {
      location: url
    }
  });
};

var DOMException = self.DOMException;

try {
  new DOMException();
} catch (err) {
  DOMException = function (message, name) {
    this.message = message;
    this.name = name;
    var error = Error(message);
    this.stack = error.stack;
  };

  DOMException.prototype = Object.create(Error.prototype);
  DOMException.prototype.constructor = DOMException;
}

function fetch(input, init) {
  return new Promise(function (resolve, reject) {
    var request = new Request(input, init);

    if (request.signal && request.signal.aborted) {
      return reject(new DOMException('Aborted', 'AbortError'));
    }

    var xhr = new XMLHttpRequest();

    function abortXhr() {
      xhr.abort();
    }

    xhr.onload = function () {
      var options = {
        status: xhr.status,
        statusText: xhr.statusText,
        headers: parseHeaders(xhr.getAllResponseHeaders() || '')
      };
      options.url = 'responseURL' in xhr ? xhr.responseURL : options.headers.get('X-Request-URL');
      var body = 'response' in xhr ? xhr.response : xhr.responseText;
      resolve(new Response(body, options));
    };

    xhr.onerror = function () {
      reject(new TypeError('Network request failed'));
    };

    xhr.ontimeout = function () {
      reject(new TypeError('Network request failed'));
    };

    xhr.onabort = function () {
      reject(new DOMException('Aborted', 'AbortError'));
    };

    xhr.open(request.method, request.url, true);

    if (request.credentials === 'include') {
      xhr.withCredentials = true;
    } else if (request.credentials === 'omit') {
      xhr.withCredentials = false;
    }

    if ('responseType' in xhr && support.blob) {
      xhr.responseType = 'blob';
    }

    request.headers.forEach(function (value, name) {
      xhr.setRequestHeader(name, value);
    });

    if (request.signal) {
      request.signal.addEventListener('abort', abortXhr);

      xhr.onreadystatechange = function () {
        // DONE (success or failure)
        if (xhr.readyState === 4) {
          request.signal.removeEventListener('abort', abortXhr);
        }
      };
    }

    xhr.send(typeof request._bodyInit === 'undefined' ? null : request._bodyInit);
  });
}
fetch.polyfill = true;

if (!self.fetch) {
  self.fetch = fetch;
  self.Headers = Headers;
  self.Request = Request;
  self.Response = Response;
}

/***/ }),

/***/ "./src/public/js/core/data-manager.js":
/*!********************************************!*\
  !*** ./src/public/js/core/data-manager.js ***!
  \********************************************/
/*! exports provided: DataManager */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataManager", function() { return DataManager; });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var DataManager =
/*#__PURE__*/
function () {
  function DataManager() {
    _classCallCheck(this, DataManager);
  }

  _createClass(DataManager, null, [{
    key: "storeBanks",
    value: function storeBanks(collection) {
      localStorage.setItem("swyp-businesses", JSON.stringify(collection));
    }
  }, {
    key: "getBanks",
    value: function getBanks() {
      return JSON.parse(localStorage.getItem("swyp-businesses"));
    }
  }, {
    key: "findBank",
    value: function findBank(slug) {
      var businesses = JSON.parse(localStorage.getItem("swyp-businesses"));
      return businesses.find(function (biz) {
        return biz.slug === slug;
      });
    }
  }, {
    key: "storeForms",
    value: function storeForms(collection) {
      localStorage.setItem("swyp-forms", JSON.stringify(collection));
    }
  }, {
    key: "getForms",
    value: function getForms() {
      return JSON.parse(localStorage.getItem("swyp-forms"));
    }
  }, {
    key: "findForm",
    value: function findForm(slug) {
      try {
        var forms = JSON.parse(localStorage.getItem("swyp-forms"));
        return forms.find(function (form) {
          return form.slug === slug;
        });
      } catch (error) {
        return null;
      }
    }
  }]);

  return DataManager;
}();

/***/ }),

/***/ "./src/public/js/core/http.js":
/*!************************************!*\
  !*** ./src/public/js/core/http.js ***!
  \************************************/
/*! exports provided: Http */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Http", function() { return Http; });
/* harmony import */ var whatwg_fetch__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! whatwg-fetch */ "./node_modules/whatwg-fetch/fetch.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);


var baseUrl = "https://business-backend-service.herokuapp.com/api/v1/"; // const baseUrl = "http://localhost:4000/api/v1/";

var Http = {
  get: function get(url) {
    return fetch("".concat(baseUrl).concat(url)).then(function (response) {
      return processResponse(response);
    });
  },
  post: function post(url, data) {
    return fetch("".concat(baseUrl).concat(url), {
      method: "POST",
      headers: {
        "Content-type": "application/json"
      },
      body: JSON.stringify(data)
    }).then(function (response) {
      return processResponse(response);
    });
  },
  put: function put(url, data) {
    return fetch("".concat(baseUrl).concat(url), {
      method: "PUT",
      headers: {
        "Content-type": "application/json"
      },
      body: JSON.stringify(data)
    }).then(function (response) {
      return processResponse(response);
    });
  },
  // upLoadForm(url, formData) {
  //   return fetch(`${baseUrl}${url}`, {
  //     method: "POST",
  //     body: formData
  //   }).then(response => processResponse(response));
  // }
  upLoadForm: function upLoadForm(url, formData, id, type) {
    var pb = "<div class=\"pb-cont\">\n                <span class=\"placeholder\">Uploading...</span>\n                <div class=\"pb-inside\"></div>\n              </div>";

    if (document.querySelector("div.uploadstatus[data-q-id=\"".concat(id, "\"]")) !== null) {
      var uploadIndicator = document.querySelector("div.uploadstatus[data-q-id=\"".concat(id, "\"]")); // var t = document.createElement('span');
      // t.innerHTML = eve.loaded;

      uploadIndicator.innerHTML = pb;
    }

    return axios__WEBPACK_IMPORTED_MODULE_1___default()({
      url: "".concat(baseUrl).concat(url),
      method: "POST",
      data: formData,
      onUploadProgress: function onUploadProgress(eve) {
        console.log("logging progress event", eve);
        var total = eve.total;
        var loaded = eve.loaded;
        var fraction = eve.loaded / eve.total;
        var perc = Math.floor(fraction * 100) + "%";

        if (type == "videos") {
          var target = ".uploadButton[data-qType=video] .cam-progress"; // if (loaded >= total) {
          //   document.querySelector("video#live").style.display = "none";
          //   document.querySelector("#record").style.display = "none";
          //   var mediaPermissionBtn = document.querySelector(
          //     ".askPermissionBtn"
          //   );
          //   document
          //     .querySelector(".answerWrapper")
          //     .appendChild(mediaPermissionBtn);
          // }
        } else {
          var target = ".uploadButton[data-qType=picture] .cam-progress";
        }

        var pb = document.querySelector(".pb-cont .pb-inside") || document.querySelector(target);

        if (loaded >= total) {
          pb.style.width = "100%";
        } else {
          pb.style.width = perc;
        }
      }
    }).then(function (response) {
      return processResponse2(response);
    });
  }
};

var processResponse = function processResponse(response) {
  if (response.ok) {
    return response.json();
  }

  var error = new Error("NetworkError");
  error.detials = response.json();
  throw error;
};

var processResponse2 = function processResponse2(response) {
  if (response.status == 200) {
    var ress = new Promise(function (res, rej) {
      res(response.data);
    });
    return ress;
  }

  var error = new Error("NetworkError");
  error.detials = response.statusText;
  throw error;
};

/***/ }),

/***/ "./src/public/js/core/index.js":
/*!*************************************!*\
  !*** ./src/public/js/core/index.js ***!
  \*************************************/
/*! exports provided: DataManager, Http */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _data_manager__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./data-manager */ "./src/public/js/core/data-manager.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "DataManager", function() { return _data_manager__WEBPACK_IMPORTED_MODULE_0__["DataManager"]; });

/* harmony import */ var _http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./http */ "./src/public/js/core/http.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Http", function() { return _http__WEBPACK_IMPORTED_MODULE_1__["Http"]; });




/***/ }),

/***/ "./src/public/js/partners/index.js":
/*!*****************************************!*\
  !*** ./src/public/js/partners/index.js ***!
  \*****************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _partners_controller__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./partners-controller */ "./src/public/js/partners/partners-controller.js");

new _partners_controller__WEBPACK_IMPORTED_MODULE_0__["default"](document.querySelector(".banks-section"));

/***/ }),

/***/ "./src/public/js/partners/partners-controller.js":
/*!*******************************************************!*\
  !*** ./src/public/js/partners/partners-controller.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return PartnersControllers; });
/* harmony import */ var _templates_banks_hbs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../templates/banks.hbs */ "./src/public/js/templates/banks.hbs");
/* harmony import */ var _templates_banks_hbs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_templates_banks_hbs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../core */ "./src/public/js/core/index.js");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../utils */ "./src/public/js/utils/index.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }





var PartnersControllers =
/*#__PURE__*/
function () {
  function PartnersControllers(container) {
    _classCallCheck(this, PartnersControllers);

    this._container = container;

    this._fetchAll();
  }

  _createClass(PartnersControllers, [{
    key: "_fetchAll",
    value: function _fetchAll() {
      var _this = this;

      var loading = document.querySelector("#loading-text");
      _core__WEBPACK_IMPORTED_MODULE_1__["Http"].get("businesses").then(function (banks) {
        if (!Array.isArray(banks)) throw new error("BadResponse");
        banks = banks.reverse();
        _core__WEBPACK_IMPORTED_MODULE_1__["DataManager"].storeBanks(banks);
        loading.classList.add("hide-elem");
        var banksHtml = _templates_banks_hbs__WEBPACK_IMPORTED_MODULE_0___default()({
          banks: banks
        });
        var nodes = Object(_utils__WEBPACK_IMPORTED_MODULE_2__["parseHtml"])(banksHtml);

        _this._container.insertBefore(nodes, _this._container.firstChild);
      })["catch"](function (err) {
        loading.classList.add("hide-elem");
        alert("Sorry we are experiencing issues fetching content from our servers");
      });
    }
  }]);

  return PartnersControllers;
}();



/***/ }),

/***/ "./src/public/js/templates/banks.hbs":
/*!*******************************************!*\
  !*** ./src/public/js/templates/banks.hbs ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Handlebars = __webpack_require__(/*! ../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var alias1=container.lambda, alias2=container.escapeExpression;

  return "  <div class=\"bank-wrapper col-md-2 u-margidn-bottom--small\">\r\n    <a href=\""
    + alias2(alias1((depth0 != null ? depth0.slug : depth0), depth0))
    + "\" style=\"text-decoration: none;\">\r\n      <div class=\"bank-box\" style=\"background: "
    + alias2(alias1((depth0 != null ? depth0.color : depth0), depth0))
    + ";\">\r\n        <div class=\"bank-box__header u-margin-bottom--thin\">\r\n          <h3 class=\"tertiary-heading\"></h3>\r\n        </div>\r\n        <div class=\"bank-box__logo\">\r\n          <!--<p>Yeh</p>-->\r\n          <img src=\""
    + alias2(alias1((depth0 != null ? depth0.logo : depth0), depth0))
    + "\" class=\"bank-box__img\" alt=\""
    + alias2(alias1((depth0 != null ? depth0.slug : depth0), depth0))
    + " logo\">\r\n        </div>\r\n        <div class=\"bank-box__footer  u-margin-bottom--thin\">\r\n          <!--<a href=\"////\" class=\"btn-text bank-box__link\">--->\r\n          <span class=\"footer-text\"> "
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + " Forms </span>\r\n        </div>\r\n      </div>\r\n    </a>\r\n  </div>\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.banks : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"useData":true});

/***/ }),

/***/ "./src/public/js/utils/countryStates.js":
/*!**********************************************!*\
  !*** ./src/public/js/utils/countryStates.js ***!
  \**********************************************/
/*! exports provided: countryStates */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "countryStates", function() { return countryStates; });
var countryStates = [{
  state: {
    name: "Abia State",
    id: 1,
    locals: [{
      name: "Aba South",
      id: 1
    }, {
      name: "Arochukwu",
      id: 2
    }, {
      name: "Bende",
      id: 3
    }, {
      name: "Ikwuano",
      id: 4
    }, {
      name: "Isiala Ngwa North",
      id: 5
    }, {
      name: "Isiala Ngwa South",
      id: 6
    }, {
      name: "Isuikwuato",
      id: 7
    }, {
      name: "Obi Ngwa",
      id: 8
    }, {
      name: "Ohafia",
      id: 9
    }, {
      name: "Osisioma",
      id: 10
    }, {
      name: "Ugwunagbo",
      id: 11
    }, {
      name: "Ukwa East",
      id: 12
    }, {
      name: "Ukwa West",
      id: 13
    }, {
      name: "Umuahia North",
      id: 14
    }, {
      name: "Umuahia South",
      id: 15
    }, {
      name: "Umu Nneochi",
      id: 16
    }]
  }
}, {
  state: {
    name: "Adamawa State",
    id: 2,
    locals: [{
      name: "Fufure",
      id: 1
    }, {
      name: "Ganye",
      id: 2
    }, {
      name: "Gayuk",
      id: 3
    }, {
      name: "Gombi",
      id: 4
    }, {
      name: "Grie",
      id: 5
    }, {
      name: "Hong",
      id: 6
    }, {
      name: "Jada",
      id: 7
    }, {
      name: "Lamurde",
      id: 8
    }, {
      name: "Madagali",
      id: 9
    }, {
      name: "Maiha",
      id: 10
    }, {
      name: "Mayo Belwa",
      id: 11
    }, {
      name: "Michika",
      id: 12
    }, {
      name: "Mubi North",
      id: 13
    }, {
      name: "Mubi South",
      id: 14
    }, {
      name: "Numan",
      id: 15
    }, {
      name: "Shelleng",
      id: 16
    }, {
      name: "Song",
      id: 17
    }, {
      name: "Toungo",
      id: 18
    }, {
      name: "Yola North",
      id: 19
    }, {
      name: "Yola South",
      id: 20
    }]
  }
}, {
  state: {
    name: "Akwa Ibom State",
    id: 3,
    locals: [{
      name: "Eastern Obolo",
      id: 1
    }, {
      name: "Eket",
      id: 2
    }, {
      name: "Esit Eket",
      id: 3
    }, {
      name: "Essien Udim",
      id: 4
    }, {
      name: "Etim Ekpo",
      id: 5
    }, {
      name: "Etinan",
      id: 6
    }, {
      name: "Ibeno",
      id: 7
    }, {
      name: "Ibesikpo Asutan",
      id: 8
    }, {
      name: "Ibiono-Ibom",
      id: 9
    }, {
      name: "Ika",
      id: 10
    }, {
      name: "Ikono",
      id: 11
    }, {
      name: "Ikot Abasi",
      id: 12
    }, {
      name: "Ikot Ekpene",
      id: 13
    }, {
      name: "Ini",
      id: 14
    }, {
      name: "Itu",
      id: 15
    }, {
      name: "Mbo",
      id: 16
    }, {
      name: "Mkpat-Enin",
      id: 17
    }, {
      name: "Nsit-Atai",
      id: 18
    }, {
      name: "Nsit-Ibom",
      id: 19
    }, {
      name: "Nsit-Ubium",
      id: 20
    }, {
      name: "Obot Akara",
      id: 21
    }, {
      name: "Okobo",
      id: 22
    }, {
      name: "Onna",
      id: 23
    }, {
      name: "Oron",
      id: 24
    }, {
      name: "Oruk Anam",
      id: 25
    }, {
      name: "Udung-Uko",
      id: 26
    }, {
      name: "Ukanafun",
      id: 27
    }, {
      name: "Uruan",
      id: 28
    }, {
      name: "Urue-Offong/Oruko",
      id: 29
    }, {
      name: "Uyo",
      id: 30
    }]
  }
}, {
  state: {
    name: "Anambra State",
    id: 4,
    locals: [{
      name: "Anambra East",
      id: 1
    }, {
      name: "Anambra West",
      id: 2
    }, {
      name: "Anaocha",
      id: 3
    }, {
      name: "Awka North",
      id: 4
    }, {
      name: "Awka South",
      id: 5
    }, {
      name: "Ayamelum",
      id: 6
    }, {
      name: "Dunukofia",
      id: 7
    }, {
      name: "Ekwusigo",
      id: 8
    }, {
      name: "Idemili North",
      id: 9
    }, {
      name: "Idemili South",
      id: 10
    }, {
      name: "Ihiala",
      id: 11
    }, {
      name: "Njikoka",
      id: 12
    }, {
      name: "Nnewi North",
      id: 13
    }, {
      name: "Nnewi South",
      id: 14
    }, {
      name: "Ogbaru",
      id: 15
    }, {
      name: "Onitsha North",
      id: 16
    }, {
      name: "Onitsha South",
      id: 17
    }, {
      name: "Orumba North",
      id: 18
    }, {
      name: "Orumba South",
      id: 19
    }, {
      name: "Oyi",
      id: 20
    }]
  }
}, {
  state: {
    name: "Bauchi State",
    id: 5,
    locals: [{
      name: "Bauchi",
      id: 1
    }, {
      name: "Bogoro",
      id: 2
    }, {
      name: "Damban",
      id: 3
    }, {
      name: "Darazo",
      id: 4
    }, {
      name: "Dass",
      id: 5
    }, {
      name: "Gamawa",
      id: 6
    }, {
      name: "Ganjuwa",
      id: 7
    }, {
      name: "Giade",
      id: 8
    }, {
      name: "Itas/Gadau",
      id: 9
    }, {
      name: "Jama'are",
      id: 10
    }, {
      name: "Katagum",
      id: 11
    }, {
      name: "Kirfi",
      id: 12
    }, {
      name: "Misau",
      id: 13
    }, {
      name: "Ningi",
      id: 14
    }, {
      name: "Shira",
      id: 15
    }, {
      name: "Tafawa Balewa",
      id: 16
    }, {
      name: "Toro",
      id: 17
    }, {
      name: "Warji",
      id: 18
    }, {
      name: "Zaki",
      id: 19
    }]
  }
}, {
  state: {
    name: "Bayelsa State",
    id: 6,
    locals: [{
      name: "Ekeremor",
      id: 1
    }, {
      name: "Kolokuma/Opokuma",
      id: 2
    }, {
      name: "Nembe",
      id: 3
    }, {
      name: "Ogbia",
      id: 4
    }, {
      name: "Sagbama",
      id: 5
    }, {
      name: "Southern Ijaw",
      id: 6
    }, {
      name: "Yenagoa",
      id: 7
    }]
  }
}, {
  state: {
    name: "Benue State",
    id: 7,
    locals: [{
      name: "Apa",
      id: 1
    }, {
      name: "Ado",
      id: 2
    }, {
      name: "Buruku",
      id: 3
    }, {
      name: "Gboko",
      id: 4
    }, {
      name: "Guma",
      id: 5
    }, {
      name: "Gwer East",
      id: 6
    }, {
      name: "Gwer West",
      id: 7
    }, {
      name: "Katsina-Ala",
      id: 8
    }, {
      name: "Konshisha",
      id: 9
    }, {
      name: "Kwande",
      id: 10
    }, {
      name: "Logo",
      id: 11
    }, {
      name: "Makurdi",
      id: 12
    }, {
      name: "Obi",
      id: 13
    }, {
      name: "Ogbadibo",
      id: 14
    }, {
      name: "Ohimini",
      id: 15
    }, {
      name: "Oju",
      id: 16
    }, {
      name: "Okpokwu",
      id: 17
    }, {
      name: "Oturkpo",
      id: 18
    }, {
      name: "Tarka",
      id: 19
    }, {
      name: "Ukum",
      id: 20
    }, {
      name: "Ushongo",
      id: 21
    }, {
      name: "Vandeikya",
      id: 22
    }]
  }
}, {
  state: {
    name: "Borno State",
    id: 8,
    locals: [{
      name: "Askira/Uba",
      id: 1
    }, {
      name: "Bama",
      id: 2
    }, {
      name: "Bayo",
      id: 3
    }, {
      name: "Biu",
      id: 4
    }, {
      name: "Chibok",
      id: 5
    }, {
      name: "Damboa",
      id: 6
    }, {
      name: "Dikwa",
      id: 7
    }, {
      name: "Gubio",
      id: 8
    }, {
      name: "Guzamala",
      id: 9
    }, {
      name: "Gwoza",
      id: 10
    }, {
      name: "Hawul",
      id: 11
    }, {
      name: "Jere",
      id: 12
    }, {
      name: "Kaga",
      id: 13
    }, {
      name: "Kala/Balge",
      id: 14
    }, {
      name: "Konduga",
      id: 15
    }, {
      name: "Kukawa",
      id: 16
    }, {
      name: "Kwaya Kusar",
      id: 17
    }, {
      name: "Mafa",
      id: 18
    }, {
      name: "Magumeri",
      id: 19
    }, {
      name: "Maiduguri",
      id: 20
    }, {
      name: "Marte",
      id: 21
    }, {
      name: "Mobbar",
      id: 22
    }, {
      name: "Monguno",
      id: 23
    }, {
      name: "Ngala",
      id: 24
    }, {
      name: "Nganzai",
      id: 25
    }, {
      name: "Shani",
      id: 26
    }]
  }
}, {
  state: {
    name: "Cross River State",
    id: 9,
    locals: [{
      name: "Akamkpa",
      id: 1
    }, {
      name: "Akpabuyo",
      id: 2
    }, {
      name: "Bakassi",
      id: 3
    }, {
      name: "Bekwarra",
      id: 4
    }, {
      name: "Biase",
      id: 5
    }, {
      name: "Boki",
      id: 6
    }, {
      name: "Calabar Municipal",
      id: 7
    }, {
      name: "Calabar South",
      id: 8
    }, {
      name: "Etung",
      id: 9
    }, {
      name: "Ikom",
      id: 10
    }, {
      name: "Obanliku",
      id: 11
    }, {
      name: "Obubra",
      id: 12
    }, {
      name: "Obudu",
      id: 13
    }, {
      name: "Odukpani",
      id: 14
    }, {
      name: "Ogoja",
      id: 15
    }, {
      name: "Yakuur",
      id: 16
    }, {
      name: "Yala",
      id: 17
    }]
  }
}, {
  state: {
    name: "Delta State",
    id: 10,
    locals: [{
      name: "Aniocha South",
      id: 1
    }, {
      name: "Bomadi",
      id: 2
    }, {
      name: "Burutu",
      id: 3
    }, {
      name: "Ethiope East",
      id: 4
    }, {
      name: "Ethiope West",
      id: 5
    }, {
      name: "Ika North East",
      id: 6
    }, {
      name: "Ika South",
      id: 7
    }, {
      name: "Isoko North",
      id: 8
    }, {
      name: "Isoko South",
      id: 9
    }, {
      name: "Ndokwa East",
      id: 10
    }, {
      name: "Ndokwa West",
      id: 11
    }, {
      name: "Okpe",
      id: 12
    }, {
      name: "Oshimili North",
      id: 13
    }, {
      name: "Oshimili South",
      id: 14
    }, {
      name: "Patani",
      id: 15
    }, {
      name: "Sapele",
      id: 16
    }, {
      name: "Udu",
      id: 17
    }, {
      name: "Ughelli North",
      id: 18
    }, {
      name: "Ughelli South",
      id: 19
    }, {
      name: "Ukwuani",
      id: 20
    }, {
      name: "Uvwie",
      id: 21
    }, {
      name: "Warri North",
      id: 22
    }, {
      name: "Warri South",
      id: 23
    }, {
      name: "Warri South West",
      id: 24
    }]
  }
}, {
  state: {
    name: "Ebonyi State",
    id: 11,
    locals: [{
      name: "Afikpo North",
      id: 1
    }, {
      name: "Afikpo South",
      id: 2
    }, {
      name: "Ebonyi",
      id: 3
    }, {
      name: "Ezza North",
      id: 4
    }, {
      name: "Ezza South",
      id: 5
    }, {
      name: "Ikwo",
      id: 6
    }, {
      name: "Ishielu",
      id: 7
    }, {
      name: "Ivo",
      id: 8
    }, {
      name: "Izzi",
      id: 9
    }, {
      name: "Ohaozara",
      id: 10
    }, {
      name: "Ohaukwu",
      id: 11
    }, {
      name: "Onicha",
      id: 12
    }]
  }
}, {
  state: {
    name: "Edo State",
    id: 12,
    locals: [{
      name: "Egor",
      id: 1
    }, {
      name: "Esan Central",
      id: 2
    }, {
      name: "Esan North-East",
      id: 3
    }, {
      name: "Esan South-East",
      id: 4
    }, {
      name: "Esan West",
      id: 5
    }, {
      name: "Etsako Central",
      id: 6
    }, {
      name: "Etsako East",
      id: 7
    }, {
      name: "Etsako West",
      id: 8
    }, {
      name: "Igueben",
      id: 9
    }, {
      name: "Ikpoba Okha",
      id: 10
    }, {
      name: "Orhionmwon",
      id: 11
    }, {
      name: "Oredo",
      id: 12
    }, {
      name: "Ovia North-East",
      id: 13
    }, {
      name: "Ovia South-West",
      id: 14
    }, {
      name: "Owan East",
      id: 15
    }, {
      name: "Owan West",
      id: 16
    }, {
      name: "Uhunmwonde",
      id: 17
    }]
  }
}, {
  state: {
    name: "Ekiti State",
    id: 13,
    locals: [{
      name: "Efon",
      id: 1
    }, {
      name: "Ekiti East",
      id: 2
    }, {
      name: "Ekiti South-West",
      id: 3
    }, {
      name: "Ekiti West",
      id: 4
    }, {
      name: "Emure",
      id: 5
    }, {
      name: "Gbonyin",
      id: 6
    }, {
      name: "Ido Osi",
      id: 7
    }, {
      name: "Ijero",
      id: 8
    }, {
      name: "Ikere",
      id: 9
    }, {
      name: "Ikole",
      id: 10
    }, {
      name: "Ilejemeje",
      id: 11
    }, {
      name: "Irepodun/Ifelodun",
      id: 12
    }, {
      name: "Ise/Orun",
      id: 13
    }, {
      name: "Moba",
      id: 14
    }, {
      name: "Oye",
      id: 15
    }]
  }
}, {
  state: {
    name: "Enugu State",
    id: 14,
    locals: [{
      name: "Awgu",
      id: 1
    }, {
      name: "Enugu East",
      id: 2
    }, {
      name: "Enugu North",
      id: 3
    }, {
      name: "Enugu South",
      id: 4
    }, {
      name: "Ezeagu",
      id: 5
    }, {
      name: "Igbo Etiti",
      id: 6
    }, {
      name: "Igbo Eze North",
      id: 7
    }, {
      name: "Igbo Eze South",
      id: 8
    }, {
      name: "Isi Uzo",
      id: 9
    }, {
      name: "Nkanu East",
      id: 10
    }, {
      name: "Nkanu West",
      id: 11
    }, {
      name: "Nsukka",
      id: 12
    }, {
      name: "Oji River",
      id: 13
    }, {
      name: "Udenu",
      id: 14
    }, {
      name: "Udi",
      id: 15
    }, {
      name: "Uzo Uwani",
      id: 16
    }]
  }
}, {
  state: {
    name: "FCT",
    id: 15,
    locals: [{
      name: "Bwari",
      id: 1
    }, {
      name: "Gwagwalada",
      id: 2
    }, {
      name: "Kuje",
      id: 3
    }, {
      name: "Kwali",
      id: 4
    }, {
      name: "Municipal Area Council",
      id: 5
    }]
  }
}, {
  state: {
    name: "Gombe State",
    id: 16,
    locals: [{
      name: "Balanga",
      id: 1
    }, {
      name: "Billiri",
      id: 2
    }, {
      name: "Dukku",
      id: 3
    }, {
      name: "Funakaye",
      id: 4
    }, {
      name: "Gombe",
      id: 5
    }, {
      name: "Kaltungo",
      id: 6
    }, {
      name: "Kwami",
      id: 7
    }, {
      name: "Nafada",
      id: 8
    }, {
      name: "Shongom",
      id: 9
    }, {
      name: "Yamaltu/Deba",
      id: 10
    }]
  }
}, {
  state: {
    name: "Imo State",
    id: 17,
    locals: [{
      name: "Ahiazu Mbaise",
      id: 1
    }, {
      name: "Ehime Mbano",
      id: 2
    }, {
      name: "Ezinihitte",
      id: 3
    }, {
      name: "Ideato North",
      id: 4
    }, {
      name: "Ideato South",
      id: 5
    }, {
      name: "Ihitte/Uboma",
      id: 6
    }, {
      name: "Ikeduru",
      id: 7
    }, {
      name: "Isiala Mbano",
      id: 8
    }, {
      name: "Isu",
      id: 9
    }, {
      name: "Mbaitoli",
      id: 10
    }, {
      name: "Ngor Okpala",
      id: 11
    }, {
      name: "Njaba",
      id: 12
    }, {
      name: "Nkwerre",
      id: 13
    }, {
      name: "Nwangele",
      id: 14
    }, {
      name: "Obowo",
      id: 15
    }, {
      name: "Oguta",
      id: 16
    }, {
      name: "Ohaji/Egbema",
      id: 17
    }, {
      name: "Okigwe",
      id: 18
    }, {
      name: "Orlu",
      id: 19
    }, {
      name: "Orsu",
      id: 20
    }, {
      name: "Oru East",
      id: 21
    }, {
      name: "Oru West",
      id: 22
    }, {
      name: "Owerri Municipal",
      id: 23
    }, {
      name: "Owerri North",
      id: 24
    }, {
      name: "Owerri West",
      id: 25
    }, {
      name: "Unuimo",
      id: 26
    }]
  }
}, {
  state: {
    name: "Jigawa State",
    id: 18,
    locals: [{
      name: "Babura",
      id: 1
    }, {
      name: "Biriniwa",
      id: 2
    }, {
      name: "Birnin Kudu",
      id: 3
    }, {
      name: "Buji",
      id: 4
    }, {
      name: "Dutse",
      id: 5
    }, {
      name: "Gagarawa",
      id: 6
    }, {
      name: "Garki",
      id: 7
    }, {
      name: "Gumel",
      id: 8
    }, {
      name: "Guri",
      id: 9
    }, {
      name: "Gwaram",
      id: 10
    }, {
      name: "Gwiwa",
      id: 11
    }, {
      name: "Hadejia",
      id: 12
    }, {
      name: "Jahun",
      id: 13
    }, {
      name: "Kafin Hausa",
      id: 14
    }, {
      name: "Kazaure",
      id: 15
    }, {
      name: "Kiri Kasama",
      id: 16
    }, {
      name: "Kiyawa",
      id: 17
    }, {
      name: "Kaugama",
      id: 18
    }, {
      name: "Maigatari",
      id: 19
    }, {
      name: "Malam Madori",
      id: 20
    }, {
      name: "Miga",
      id: 21
    }, {
      name: "Ringim",
      id: 22
    }, {
      name: "Roni",
      id: 23
    }, {
      name: "Sule Tankarkar",
      id: 24
    }, {
      name: "Taura",
      id: 25
    }, {
      name: "Yankwashi",
      id: 26
    }]
  }
}, {
  state: {
    name: "Kaduna State",
    id: 19,
    locals: [{
      name: "Chikun",
      id: 1
    }, {
      name: "Giwa",
      id: 2
    }, {
      name: "Igabi",
      id: 3
    }, {
      name: "Ikara",
      id: 4
    }, {
      name: "Jaba",
      id: 5
    }, {
      name: "Jema'a",
      id: 6
    }, {
      name: "Kachia",
      id: 7
    }, {
      name: "Kaduna North",
      id: 8
    }, {
      name: "Kaduna South",
      id: 9
    }, {
      name: "Kagarko",
      id: 10
    }, {
      name: "Kajuru",
      id: 11
    }, {
      name: "Kaura",
      id: 12
    }, {
      name: "Kauru",
      id: 13
    }, {
      name: "Kubau",
      id: 14
    }, {
      name: "Kudan",
      id: 15
    }, {
      name: "Lere",
      id: 16
    }, {
      name: "Makarfi",
      id: 17
    }, {
      name: "Sabon Gari",
      id: 18
    }, {
      name: "Sanga",
      id: 19
    }, {
      name: "Soba",
      id: 20
    }, {
      name: "Zangon Kataf",
      id: 21
    }, {
      name: "Zaria",
      id: 22
    }]
  }
}, {
  state: {
    name: "Kano State",
    id: 20,
    locals: [{
      name: "Albasu",
      id: 1
    }, {
      name: "Bagwai",
      id: 2
    }, {
      name: "Bebeji",
      id: 3
    }, {
      name: "Bichi",
      id: 4
    }, {
      name: "Bunkure",
      id: 5
    }, {
      name: "Dala",
      id: 6
    }, {
      name: "Dambatta",
      id: 7
    }, {
      name: "Dawakin Kudu",
      id: 8
    }, {
      name: "Dawakin Tofa",
      id: 9
    }, {
      name: "Doguwa",
      id: 10
    }, {
      name: "Fagge",
      id: 11
    }, {
      name: "Gabasawa",
      id: 12
    }, {
      name: "Garko",
      id: 13
    }, {
      name: "Garun Mallam",
      id: 14
    }, {
      name: "Gaya",
      id: 15
    }, {
      name: "Gezawa",
      id: 16
    }, {
      name: "Gwale",
      id: 17
    }, {
      name: "Gwarzo",
      id: 18
    }, {
      name: "Kabo",
      id: 19
    }, {
      name: "Kano Municipal",
      id: 20
    }, {
      name: "Karaye",
      id: 21
    }, {
      name: "Kibiya",
      id: 22
    }, {
      name: "Kiru",
      id: 23
    }, {
      name: "Kumbotso",
      id: 24
    }, {
      name: "Kunchi",
      id: 25
    }, {
      name: "Kura",
      id: 26
    }, {
      name: "Madobi",
      id: 27
    }, {
      name: "Makoda",
      id: 28
    }, {
      name: "Minjibir",
      id: 29
    }, {
      name: "Nasarawa",
      id: 30
    }, {
      name: "Rano",
      id: 31
    }, {
      name: "Rimin Gado",
      id: 32
    }, {
      name: "Rogo",
      id: 33
    }, {
      name: "Shanono",
      id: 34
    }, {
      name: "Sumaila",
      id: 35
    }, {
      name: "Takai",
      id: 36
    }, {
      name: "Tarauni",
      id: 37
    }, {
      name: "Tofa",
      id: 38
    }, {
      name: "Tsanyawa",
      id: 39
    }, {
      name: "Tudun Wada",
      id: 40
    }, {
      name: "Ungogo",
      id: 41
    }, {
      name: "Warawa",
      id: 42
    }, {
      name: "Wudil",
      id: 43
    }]
  }
}, {
  state: {
    name: "Katsina State",
    id: 21,
    locals: [{
      name: "Batagarawa",
      id: 1
    }, {
      name: "Batsari",
      id: 2
    }, {
      name: "Baure",
      id: 3
    }, {
      name: "Bindawa",
      id: 4
    }, {
      name: "Charanchi",
      id: 5
    }, {
      name: "Dandume",
      id: 6
    }, {
      name: "Danja",
      id: 7
    }, {
      name: "Dan Musa",
      id: 8
    }, {
      name: "Daura",
      id: 9
    }, {
      name: "Dutsi",
      id: 10
    }, {
      name: "Dutsin Ma",
      id: 11
    }, {
      name: "Faskari",
      id: 12
    }, {
      name: "Funtua",
      id: 13
    }, {
      name: "Ingawa",
      id: 14
    }, {
      name: "Jibia",
      id: 15
    }, {
      name: "Kafur",
      id: 16
    }, {
      name: "Kaita",
      id: 17
    }, {
      name: "Kankara",
      id: 18
    }, {
      name: "Kankia",
      id: 19
    }, {
      name: "Katsina",
      id: 20
    }, {
      name: "Kurfi",
      id: 21
    }, {
      name: "Kusada",
      id: 22
    }, {
      name: "Mai'Adua",
      id: 23
    }, {
      name: "Malumfashi",
      id: 24
    }, {
      name: "Mani",
      id: 25
    }, {
      name: "Mashi",
      id: 26
    }, {
      name: "Matazu",
      id: 27
    }, {
      name: "Musawa",
      id: 28
    }, {
      name: "Rimi",
      id: 29
    }, {
      name: "Sabuwa",
      id: 30
    }, {
      name: "Safana",
      id: 31
    }, {
      name: "Sandamu",
      id: 32
    }, {
      name: "Zango",
      id: 33
    }]
  }
}, {
  state: {
    name: "Kebbi State",
    id: 22,
    locals: [{
      name: "Arewa Dandi",
      id: 1
    }, {
      name: "Argungu",
      id: 2
    }, {
      name: "Augie",
      id: 3
    }, {
      name: "Bagudo",
      id: 4
    }, {
      name: "Birnin Kebbi",
      id: 5
    }, {
      name: "Bunza",
      id: 6
    }, {
      name: "Dandi",
      id: 7
    }, {
      name: "Fakai",
      id: 8
    }, {
      name: "Gwandu",
      id: 9
    }, {
      name: "Jega",
      id: 10
    }, {
      name: "Kalgo",
      id: 11
    }, {
      name: "Koko/Besse",
      id: 12
    }, {
      name: "Maiyama",
      id: 13
    }, {
      name: "Ngaski",
      id: 14
    }, {
      name: "Sakaba",
      id: 15
    }, {
      name: "Shanga",
      id: 16
    }, {
      name: "Suru",
      id: 17
    }, {
      name: "Wasagu/Danko",
      id: 18
    }, {
      name: "Yauri",
      id: 19
    }, {
      name: "Zuru",
      id: 20
    }]
  }
}, {
  state: {
    name: "Kogi State",
    id: 23,
    locals: [{
      name: "Ajaokuta",
      id: 1
    }, {
      name: "Ankpa",
      id: 2
    }, {
      name: "Bassa",
      id: 3
    }, {
      name: "Dekina",
      id: 4
    }, {
      name: "Ibaji",
      id: 5
    }, {
      name: "Idah",
      id: 6
    }, {
      name: "Igalamela Odolu",
      id: 7
    }, {
      name: "Ijumu",
      id: 8
    }, {
      name: "Kabba/Bunu",
      id: 9
    }, {
      name: "Kogi",
      id: 10
    }, {
      name: "Lokoja",
      id: 11
    }, {
      name: "Mopa Muro",
      id: 12
    }, {
      name: "Ofu",
      id: 13
    }, {
      name: "Ogori/Magongo",
      id: 14
    }, {
      name: "Okehi",
      id: 15
    }, {
      name: "Okene",
      id: 16
    }, {
      name: "Olamaboro",
      id: 17
    }, {
      name: "Omala",
      id: 18
    }, {
      name: "Yagba East",
      id: 19
    }, {
      name: "Yagba West",
      id: 20
    }]
  }
}, {
  state: {
    name: "Kwara State",
    id: 24,
    locals: [{
      name: "Baruten",
      id: 1
    }, {
      name: "Edu",
      id: 2
    }, {
      name: "Ekiti",
      id: 3
    }, {
      name: "Ifelodun",
      id: 4
    }, {
      name: "Ilorin East",
      id: 5
    }, {
      name: "Ilorin South",
      id: 6
    }, {
      name: "Ilorin West",
      id: 7
    }, {
      name: "Irepodun",
      id: 8
    }, {
      name: "Isin",
      id: 9
    }, {
      name: "Kaiama",
      id: 10
    }, {
      name: "Moro",
      id: 11
    }, {
      name: "Offa",
      id: 12
    }, {
      name: "Oke Ero",
      id: 13
    }, {
      name: "Oyun",
      id: 14
    }, {
      name: "Pategi",
      id: 15
    }]
  }
}, {
  state: {
    name: "Lagos State",
    id: 25,
    locals: [{
      name: "Ajeromi-Ifelodun",
      id: 1
    }, {
      name: "Alimosho",
      id: 2
    }, {
      name: "Amuwo-Odofin",
      id: 3
    }, {
      name: "Apapa",
      id: 4
    }, {
      name: "Badagry",
      id: 5
    }, {
      name: "Epe",
      id: 6
    }, {
      name: "Eti Osa",
      id: 7
    }, {
      name: "Ibeju-Lekki",
      id: 8
    }, {
      name: "Ifako-Ijaiye",
      id: 9
    }, {
      name: "Ikeja",
      id: 10
    }, {
      name: "Ikorodu",
      id: 11
    }, {
      name: "Kosofe",
      id: 12
    }, {
      name: "Lagos Island",
      id: 13
    }, {
      name: "Lagos Mainland",
      id: 14
    }, {
      name: "Mushin",
      id: 15
    }, {
      name: "Ojo",
      id: 16
    }, {
      name: "Oshodi-Isolo",
      id: 17
    }, {
      name: "Shomolu",
      id: 18
    }, {
      name: "Surulere",
      id: 19
    }]
  }
}, {
  state: {
    name: "Nasarawa State",
    id: 26,
    locals: [{
      name: "Awe",
      id: 1
    }, {
      name: "Doma",
      id: 2
    }, {
      name: "Karu",
      id: 3
    }, {
      name: "Keana",
      id: 4
    }, {
      name: "Keffi",
      id: 5
    }, {
      name: "Kokona",
      id: 6
    }, {
      name: "Lafia",
      id: 7
    }, {
      name: "Nasarawa",
      id: 8
    }, {
      name: "Nasarawa Egon",
      id: 9
    }, {
      name: "Obi",
      id: 10
    }, {
      name: "Toto",
      id: 11
    }, {
      name: "Wamba",
      id: 12
    }]
  }
}, {
  state: {
    name: "Niger State",
    id: 27,
    locals: [{
      name: "Agwara",
      id: 1
    }, {
      name: "Bida",
      id: 2
    }, {
      name: "Borgu",
      id: 3
    }, {
      name: "Bosso",
      id: 4
    }, {
      name: "Chanchaga",
      id: 5
    }, {
      name: "Edati",
      id: 6
    }, {
      name: "Gbako",
      id: 7
    }, {
      name: "Gurara",
      id: 8
    }, {
      name: "Katcha",
      id: 9
    }, {
      name: "Kontagora",
      id: 10
    }, {
      name: "Lapai",
      id: 11
    }, {
      name: "Lavun",
      id: 12
    }, {
      name: "Magama",
      id: 13
    }, {
      name: "Mariga",
      id: 14
    }, {
      name: "Mashegu",
      id: 15
    }, {
      name: "Mokwa",
      id: 16
    }, {
      name: "Moya",
      id: 17
    }, {
      name: "Paikoro",
      id: 18
    }, {
      name: "Rafi",
      id: 19
    }, {
      name: "Rijau",
      id: 20
    }, {
      name: "Shiroro",
      id: 21
    }, {
      name: "Suleja",
      id: 22
    }, {
      name: "Tafa",
      id: 23
    }, {
      name: "Wushishi",
      id: 24
    }]
  }
}, {
  state: {
    name: "Ogun State",
    id: 28,
    locals: [{
      name: "Abeokuta South",
      id: 1
    }, {
      name: "Ado-Odo/Ota",
      id: 2
    }, {
      name: "Egbado North",
      id: 3
    }, {
      name: "Egbado South",
      id: 4
    }, {
      name: "Ewekoro",
      id: 5
    }, {
      name: "Ifo",
      id: 6
    }, {
      name: "Ijebu East",
      id: 7
    }, {
      name: "Ijebu North",
      id: 8
    }, {
      name: "Ijebu North East",
      id: 9
    }, {
      name: "Ijebu Ode",
      id: 10
    }, {
      name: "Ikenne",
      id: 11
    }, {
      name: "Imeko Afon",
      id: 12
    }, {
      name: "Ipokia",
      id: 13
    }, {
      name: "Obafemi Owode",
      id: 14
    }, {
      name: "Odeda",
      id: 15
    }, {
      name: "Odogbolu",
      id: 16
    }, {
      name: "Ogun Waterside",
      id: 17
    }, {
      name: "Remo North",
      id: 18
    }, {
      name: "Shagamu",
      id: 19
    }]
  }
}, {
  state: {
    name: "Ondo State",
    id: 29,
    locals: [{
      name: "Akoko North-West",
      id: 1
    }, {
      name: "Akoko South-West",
      id: 2
    }, {
      name: "Akoko South-East",
      id: 3
    }, {
      name: "Akure North",
      id: 4
    }, {
      name: "Akure South",
      id: 5
    }, {
      name: "Ese Odo",
      id: 6
    }, {
      name: "Idanre",
      id: 7
    }, {
      name: "Ifedore",
      id: 8
    }, {
      name: "Ilaje",
      id: 9
    }, {
      name: "Ile Oluji/Okeigbo",
      id: 10
    }, {
      name: "Irele",
      id: 11
    }, {
      name: "Odigbo",
      id: 12
    }, {
      name: "Okitipupa",
      id: 13
    }, {
      name: "Ondo East",
      id: 14
    }, {
      name: "Ondo West",
      id: 15
    }, {
      name: "Ose",
      id: 16
    }, {
      name: "Owo",
      id: 17
    }]
  }
}, {
  state: {
    name: "Osun State",
    id: 30,
    locals: [{
      name: "Atakunmosa West",
      id: 1
    }, {
      name: "Aiyedaade",
      id: 2
    }, {
      name: "Aiyedire",
      id: 3
    }, {
      name: "Boluwaduro",
      id: 4
    }, {
      name: "Boripe",
      id: 5
    }, {
      name: "Ede North",
      id: 6
    }, {
      name: "Ede South",
      id: 7
    }, {
      name: "Ife Central",
      id: 8
    }, {
      name: "Ife East",
      id: 9
    }, {
      name: "Ife North",
      id: 10
    }, {
      name: "Ife South",
      id: 11
    }, {
      name: "Egbedore",
      id: 12
    }, {
      name: "Ejigbo",
      id: 13
    }, {
      name: "Ifedayo",
      id: 14
    }, {
      name: "Ifelodun",
      id: 15
    }, {
      name: "Ila",
      id: 16
    }, {
      name: "Ilesa East",
      id: 17
    }, {
      name: "Ilesa West",
      id: 18
    }, {
      name: "Irepodun",
      id: 19
    }, {
      name: "Irewole",
      id: 20
    }, {
      name: "Isokan",
      id: 21
    }, {
      name: "Iwo",
      id: 22
    }, {
      name: "Obokun",
      id: 23
    }, {
      name: "Odo Otin",
      id: 24
    }, {
      name: "Ola Oluwa",
      id: 25
    }, {
      name: "Olorunda",
      id: 26
    }, {
      name: "Oriade",
      id: 27
    }, {
      name: "Orolu",
      id: 28
    }, {
      name: "Osogbo",
      id: 29
    }]
  }
}, {
  state: {
    name: "Oyo State",
    id: 31,
    locals: [{
      name: "Akinyele",
      id: 1
    }, {
      name: "Atiba",
      id: 2
    }, {
      name: "Atisbo",
      id: 3
    }, {
      name: "Egbeda",
      id: 4
    }, {
      name: "Ibadan North",
      id: 5
    }, {
      name: "Ibadan North-East",
      id: 6
    }, {
      name: "Ibadan North-West",
      id: 7
    }, {
      name: "Ibadan South-East",
      id: 8
    }, {
      name: "Ibadan South-West",
      id: 9
    }, {
      name: "Ibarapa Central",
      id: 10
    }, {
      name: "Ibarapa East",
      id: 11
    }, {
      name: "Ibarapa North",
      id: 12
    }, {
      name: "Ido",
      id: 13
    }, {
      name: "Irepo",
      id: 14
    }, {
      name: "Iseyin",
      id: 15
    }, {
      name: "Itesiwaju",
      id: 16
    }, {
      name: "Iwajowa",
      id: 17
    }, {
      name: "Kajola",
      id: 18
    }, {
      name: "Lagelu",
      id: 19
    }, {
      name: "Ogbomosho North",
      id: 20
    }, {
      name: "Ogbomosho South",
      id: 21
    }, {
      name: "Ogo Oluwa",
      id: 22
    }, {
      name: "Olorunsogo",
      id: 23
    }, {
      name: "Oluyole",
      id: 24
    }, {
      name: "Ona Ara",
      id: 25
    }, {
      name: "Orelope",
      id: 26
    }, {
      name: "Ori Ire",
      id: 27
    }, {
      name: "Oyo",
      id: 28
    }, {
      name: "Oyo East",
      id: 29
    }, {
      name: "Saki East",
      id: 30
    }, {
      name: "Saki West",
      id: 31
    }, {
      name: "Surulere",
      id: 32
    }]
  }
}, {
  state: {
    name: "Plateau State",
    id: 32,
    locals: [{
      name: "Barkin Ladi",
      id: 1
    }, {
      name: "Bassa",
      id: 2
    }, {
      name: "Jos East",
      id: 3
    }, {
      name: "Jos North",
      id: 4
    }, {
      name: "Jos South",
      id: 5
    }, {
      name: "Kanam",
      id: 6
    }, {
      name: "Kanke",
      id: 7
    }, {
      name: "Langtang South",
      id: 8
    }, {
      name: "Langtang North",
      id: 9
    }, {
      name: "Mangu",
      id: 10
    }, {
      name: "Mikang",
      id: 11
    }, {
      name: "Pankshin",
      id: 12
    }, {
      name: "Qua'an Pan",
      id: 13
    }, {
      name: "Riyom",
      id: 14
    }, {
      name: "Shendam",
      id: 15
    }, {
      name: "Wase",
      id: 16
    }]
  }
}, {
  state: {
    name: "Rivers State",
    id: 33,
    locals: [{
      name: "Ahoada East",
      id: 1
    }, {
      name: "Ahoada West",
      id: 2
    }, {
      name: "Akuku-Toru",
      id: 3
    }, {
      name: "Andoni",
      id: 4
    }, {
      name: "Asari-Toru",
      id: 5
    }, {
      name: "Bonny",
      id: 6
    }, {
      name: "Degema",
      id: 7
    }, {
      name: "Eleme",
      id: 8
    }, {
      name: "Emuoha",
      id: 9
    }, {
      name: "Etche",
      id: 10
    }, {
      name: "Gokana",
      id: 11
    }, {
      name: "Ikwerre",
      id: 12
    }, {
      name: "Khana",
      id: 13
    }, {
      name: "Obio/Akpor",
      id: 14
    }, {
      name: "Ogba/Egbema/Ndoni",
      id: 15
    }, {
      name: "Ogu/Bolo",
      id: 16
    }, {
      name: "Okrika",
      id: 17
    }, {
      name: "Omuma",
      id: 18
    }, {
      name: "Opobo/Nkoro",
      id: 19
    }, {
      name: "Oyigbo",
      id: 20
    }, {
      name: "Port Harcourt",
      id: 21
    }, {
      name: "Tai",
      id: 22
    }]
  }
}, {
  state: {
    name: "Sokoto State",
    id: 34,
    locals: [{
      name: "Bodinga",
      id: 1
    }, {
      name: "Dange Shuni",
      id: 2
    }, {
      name: "Gada",
      id: 3
    }, {
      name: "Goronyo",
      id: 4
    }, {
      name: "Gudu",
      id: 5
    }, {
      name: "Gwadabawa",
      id: 6
    }, {
      name: "Illela",
      id: 7
    }, {
      name: "Isa",
      id: 8
    }, {
      name: "Kebbe",
      id: 9
    }, {
      name: "Kware",
      id: 10
    }, {
      name: "Rabah",
      id: 11
    }, {
      name: "Sabon Birni",
      id: 12
    }, {
      name: "Shagari",
      id: 13
    }, {
      name: "Silame",
      id: 14
    }, {
      name: "Sokoto North",
      id: 15
    }, {
      name: "Sokoto South",
      id: 16
    }, {
      name: "Tambuwal",
      id: 17
    }, {
      name: "Tangaza",
      id: 18
    }, {
      name: "Tureta",
      id: 19
    }, {
      name: "Wamako",
      id: 20
    }, {
      name: "Wurno",
      id: 21
    }, {
      name: "Yabo",
      id: 22
    }]
  }
}, {
  state: {
    name: "Taraba State",
    id: 35,
    locals: [{
      name: "Bali",
      id: 1
    }, {
      name: "Donga",
      id: 2
    }, {
      name: "Gashaka",
      id: 3
    }, {
      name: "Gassol",
      id: 4
    }, {
      name: "Ibi",
      id: 5
    }, {
      name: "Jalingo",
      id: 6
    }, {
      name: "Karim Lamido",
      id: 7
    }, {
      name: "Kumi",
      id: 8
    }, {
      name: "Lau",
      id: 9
    }, {
      name: "Sardauna",
      id: 10
    }, {
      name: "Takum",
      id: 11
    }, {
      name: "Ussa",
      id: 12
    }, {
      name: "Wukari",
      id: 13
    }, {
      name: "Yorro",
      id: 14
    }, {
      name: "Zing",
      id: 15
    }]
  }
}, {
  state: {
    name: "Yobe State",
    id: 36,
    locals: [{
      name: "Bursari",
      id: 1
    }, {
      name: "Damaturu",
      id: 2
    }, {
      name: "Fika",
      id: 3
    }, {
      name: "Fune",
      id: 4
    }, {
      name: "Geidam",
      id: 5
    }, {
      name: "Gujba",
      id: 6
    }, {
      name: "Gulani",
      id: 7
    }, {
      name: "Jakusko",
      id: 8
    }, {
      name: "Karasuwa",
      id: 9
    }, {
      name: "Machina",
      id: 10
    }, {
      name: "Nangere",
      id: 11
    }, {
      name: "Nguru",
      id: 12
    }, {
      name: "Potiskum",
      id: 13
    }, {
      name: "Tarmuwa",
      id: 14
    }, {
      name: "Yunusari",
      id: 15
    }, {
      name: "Yusufari",
      id: 16
    }]
  }
}, {
  state: {
    name: "Zamfara State",
    id: 37,
    locals: [{
      name: "Bakura",
      id: 1
    }, {
      name: "Birnin Magaji/Kiyaw",
      id: 2
    }, {
      name: "Bukkuyum",
      id: 3
    }, {
      name: "Bungudu",
      id: 4
    }, {
      name: "Gummi",
      id: 5
    }, {
      name: "Gusau",
      id: 6
    }, {
      name: "Kaura Namoda",
      id: 7
    }, {
      name: "Maradun",
      id: 8
    }, {
      name: "Maru",
      id: 9
    }, {
      name: "Shinkafi",
      id: 10
    }, {
      name: "Talata Mafara",
      id: 11
    }, {
      name: "Chafe",
      id: 12
    }, {
      name: "Zurmi",
      id: 13
    }]
  }
}];

/***/ }),

/***/ "./src/public/js/utils/helpers.js":
/*!****************************************!*\
  !*** ./src/public/js/utils/helpers.js ***!
  \****************************************/
/*! exports provided: capitalizeFirstLatter, chunkData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "capitalizeFirstLatter", function() { return capitalizeFirstLatter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "chunkData", function() { return chunkData; });
var capitalizeFirstLatter = function capitalizeFirstLatter(s) {
  return s[0].toUpperCase() + s.slice(1);
};
var chunkData = function chunkData(arr) {
  var size = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 3;
  var result = [];
  var currentChunk = [];
  arr.forEach(function (item) {
    if (currentChunk.length === size) {
      result.push(currentChunk);
      currentChunk = [];
    }

    currentChunk.push(item);
  });
  result.push(currentChunk);
  return result;
};

/***/ }),

/***/ "./src/public/js/utils/hexToHSL.js":
/*!*****************************************!*\
  !*** ./src/public/js/utils/hexToHSL.js ***!
  \*****************************************/
/*! exports provided: hexToHSL */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "hexToHSL", function() { return hexToHSL; });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var hexToHSL =
/*#__PURE__*/
function () {
  function hexToHSL(hex) {
    _classCallCheck(this, hexToHSL);

    this.hex = hex;
    this.H_Value = "";
    this.S_Value = "";
    this.L_Value = "";
    this.init();
  }

  _createClass(hexToHSL, [{
    key: "init",
    value: function init() {
      var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(this.hex);
      var r = parseInt(result[1], 16);
      var g = parseInt(result[2], 16);
      var b = parseInt(result[3], 16);
      r /= 255, g /= 255, b /= 255;
      var max = Math.max(r, g, b),
          min = Math.min(r, g, b);
      var h,
          s,
          l = (max + min) / 2;

      if (max == min) {
        h = s = 0; // achromatic
      } else {
        var d = max - min;
        s = l > 0.5 ? d / (2 - max - min) : d / (max + min);

        switch (max) {
          case r:
            h = (g - b) / d + (g < b ? 6 : 0);
            break;

          case g:
            h = (b - r) / d + 2;
            break;

          case b:
            h = (r - g) / d + 4;
            break;
        }

        h /= 6;
      }

      s = s * 100;
      s = Math.round(s);
      l = l * 100;
      l = Math.round(l);
      h = Math.round(360 * h);
      this.H_Value = h;
      this.S_Value = s;
      this.L_Value = l;
    }
  }, {
    key: "getColor",
    value: function getColor() {
      var s = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.S_Value;
      var l = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.L_Value;
      var h = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : this.H_Value;
      var colorInHSL = 'hsl(' + h + ', ' + s + '%, ' + l + '%)';
      return colorInHSL;
    }
  }, {
    key: "getHValue",
    value: function getHValue() {
      return this.H_Value;
    }
  }, {
    key: "getSValue",
    value: function getSValue() {
      return this.S_Value;
    }
  }, {
    key: "getLValue",
    value: function getLValue() {
      return this.L_Value;
    }
  }]);

  return hexToHSL;
}();

/***/ }),

/***/ "./src/public/js/utils/index.js":
/*!**************************************!*\
  !*** ./src/public/js/utils/index.js ***!
  \**************************************/
/*! exports provided: parseHtml, capitalizeFirstLatter, chunkData, countryStates, hexToHSL, themeMaker */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _parse_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./parse-html */ "./src/public/js/utils/parse-html.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "parseHtml", function() { return _parse_html__WEBPACK_IMPORTED_MODULE_0__["parseHtml"]; });

/* harmony import */ var _helpers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./helpers */ "./src/public/js/utils/helpers.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "capitalizeFirstLatter", function() { return _helpers__WEBPACK_IMPORTED_MODULE_1__["capitalizeFirstLatter"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "chunkData", function() { return _helpers__WEBPACK_IMPORTED_MODULE_1__["chunkData"]; });

/* harmony import */ var _countryStates__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./countryStates */ "./src/public/js/utils/countryStates.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "countryStates", function() { return _countryStates__WEBPACK_IMPORTED_MODULE_2__["countryStates"]; });

/* harmony import */ var _hexToHSL__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./hexToHSL */ "./src/public/js/utils/hexToHSL.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "hexToHSL", function() { return _hexToHSL__WEBPACK_IMPORTED_MODULE_3__["hexToHSL"]; });

/* harmony import */ var _themeMaker__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./themeMaker */ "./src/public/js/utils/themeMaker.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "themeMaker", function() { return _themeMaker__WEBPACK_IMPORTED_MODULE_4__["themeMaker"]; });







/***/ }),

/***/ "./src/public/js/utils/parse-html.js":
/*!*******************************************!*\
  !*** ./src/public/js/utils/parse-html.js ***!
  \*******************************************/
/*! exports provided: parseHtml */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "parseHtml", function() { return parseHtml; });
var contextRange = document.createRange();
contextRange.setStart(document.body, 0);
var parseHtml = function parseHtml(str) {
  return contextRange.createContextualFragment(str);
};

/***/ }),

/***/ "./src/public/js/utils/themeMaker.js":
/*!*******************************************!*\
  !*** ./src/public/js/utils/themeMaker.js ***!
  \*******************************************/
/*! exports provided: themeMaker */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "themeMaker", function() { return themeMaker; });
/* harmony import */ var _hexToHSL__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./hexToHSL */ "./src/public/js/utils/hexToHSL.js");

var themeMaker = function themeMaker(hexColor) {
  // let color =  new ToHSL("var(--color1)");
  var color = new _hexToHSL__WEBPACK_IMPORTED_MODULE_0__["hexToHSL"](hexColor);
  var color1 = color.getColor();
  document.documentElement.style.setProperty("--color1", color1);
  var color2_S = color.getSValue() - 60;
  var color2_L = color.getLValue();
  var color2 = color.getColor(color2_S, color2_L);
  document.documentElement.style.setProperty("--color2", color2);
  var color3_S = color.getSValue() - 5;
  var color3_L = color.getLValue() + 23;
  var color3 = color.getColor(color3_S, color3_L);
  document.documentElement.style.setProperty("--color3", color3);
  var color4_S = color.getSValue() - 14;
  var color4_L = color.getLValue() + 40;
  var color4 = color.getColor(color4_S, color4_L);
  document.documentElement.style.setProperty("--color4", color4);
};

/***/ })

/******/ });
//# sourceMappingURL=partners.js.map