"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_handlebars_1 = __importDefault(require("express-handlebars"));
const express_1 = __importDefault(require("express"));
const router_1 = __importDefault(require("./router"));
const path_1 = __importDefault(require("path"));
class App {
    constructor(port) {
        this.app = express_1.default();
        this.port = port;
        this.app.set("views", path_1.default.join(__dirname, "../views"));
        this.app.engine(".hbs", express_handlebars_1.default({
            extname: ".hbs",
            defaultLayout: "main"
        }));
        this.app.set("view engine", ".hbs");
        this.app.disable("x-powered-by").use(router_1.default);
    }
    start() {
        this.app.listen(this.port, () => {
            console.log(`[P ${process.pid}] Listening at port ${this.port}`);
        });
    }
}
exports.default = App;
//# sourceMappingURL=app.js.map