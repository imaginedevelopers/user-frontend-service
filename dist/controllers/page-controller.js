"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
exports.PageController = {
    get router() {
        const router = express_1.Router();
        router
            .get("/", this.index)
            .get("/faqs", this.faq)
            .get("/partners", this.partners)
            .get("/search", this.searchResult)
            .get("/thankyou", this.thankYou);
        return router;
    },
    index(_req, res) {
        res.render("home", { bodyClass: "home-page" });
    },
    faq(_req, res) {
        res.render("faq", { bodyClass: "faq-page" });
    },
    partners(_req, res) {
        res.render("partners", {
            scripts: '<script src="/js/partners.js"></script>',
            bodyClass: "partners-page"
        });
    },
    searchResult(_req, res) {
        res.render("search-result", {
            bankFooter: true,
            bodyClass: "search-result-page"
        });
    },
    thankYou(req, res) {
        const { bank } = req.query;
        res.render("thank-you", {
            bankFooter: true,
            bodyClass: "thank-you",
            scripts: '<script src="/js/thankyou.js"></script>',
            bank
        });
    }
};
//# sourceMappingURL=page-controller.js.map