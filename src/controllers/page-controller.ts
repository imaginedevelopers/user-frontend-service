import { Request, Response, Router } from "express";

export const PageController = {
  get router() {
    const router = Router();
    router
      .get("/", this.index)
      .get("/faqs", this.faq)
      .get("/partners", this.partners)
      .get("/search", this.searchResult)
      .get("/thankyou", this.thankYou);
    return router;
  },

  index(_req: Request, res: Response) {
    res.render("home", { bodyClass: "home-page" });
  },

  faq(_req: Request, res: Response) {
    res.render("faq", { bodyClass: "faq-page" });
  },

  partners(_req: Request, res: Response) {
    res.render("partners", {
      scripts: '<script src="/js/partners.js"></script>',
      bodyClass: "partners-page"
    });
  },

  searchResult(_req: Request, res: Response) {
    res.render("search-result", {
      bankFooter: true,
      bodyClass: "search-result-page"
    });
  },

  thankYou(req: Request, res: Response) {
    const { bank } = req.query;
    res.render("thank-you", {
      bankFooter: true,
      bodyClass: "thank-you",
      scripts: '<script src="/js/thankyou.js"></script>',
      bank
    });
  }
};
