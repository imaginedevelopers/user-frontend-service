import dotenv from "dotenv";
import App from "./app";

dotenv.config();

console.log(process.env.NODE_ENV);
const app = new App(process.env.PORT as string);
app.start();
