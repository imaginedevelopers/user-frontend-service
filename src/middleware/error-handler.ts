import { Response, Request, NextFunction } from "express";
import Status from "http-status";

export const errorHandler = (
  err: any,
  _req: Request,
  res: Response,
  _next: NextFunction
) => {
  if (err.message === "ValidationError") {
    return res.status(Status.BAD_REQUEST).json({
      type: "ValidationError",
      message: err.details
    });
  }
  console.log(err);
  return res.status(Status.INTERNAL_SERVER_ERROR).json({
    type: "InternalServerError",
    message: "The server failed to handle the request",
    details: err.message
  });
};
