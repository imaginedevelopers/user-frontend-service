import { constraints } from "./util";
import { EventHandler } from "../page-event-handlers";

export class PictureManager {
  constructor(componenntUI, id) {
    this.uploadBtn = document.querySelector(`.uploadButton[id="${id}"]`);
    this.retakeButton = document.querySelector(`.resetButton[id="${id}"]`);
    this.photoContainer = componenntUI.querySelector(".picture__output");
    this.toggleCameraButton = componenntUI.querySelector("button#pToggleCam");
    this.videoContainer = componenntUI.querySelector(".picture__feed");
    this.video = componenntUI.getElementById("livefeed");
    this.takePictureBtn = componenntUI.getElementById("capture");
    this.canvas = componenntUI.getElementById("canvas");
    this.photo = componenntUI.getElementById("photo");
    this.streaming = false;
    this.pictureTaken = false;
    this.width = 340;
    this.questionId = id;
    this.stream = null;
    this.height = 0;
    this.iniate(constraints);
  }

  iniate(constraints) {
    // this.retakeButton.addEventListener("click", this.startAgain);
    this.toggleCameraButton.addEventListener(
      "click",
      this.handleCameraToggling
    );
    const settings = { ...constraints };
    delete settings.audio;

    // register events
    this.takePictureBtn.addEventListener("click", this.takePicture, false);

    // request stream data from media devices on auser system
    navigator.mediaDevices
      .getUserMedia(settings)
      .then(stream => {
        this.video.srcObject = stream;
        this.stream = stream;
        this.video.play();
      })
      .catch(err => alert("Sorry we can't access your camera"));
    this.video.addEventListener("canplay", this.handleVideoCanPlay);
    // reset picture canvas
    this.clearPhoto();
  }

  /**
   * this function stopMediaTracks loops through each media track in the stream, and stop each of them.  Ezugudor addendum
   * @param { MediaStream } stream
   */
  stopMediaTracks(stream) {
    stream.getTracks().forEach(track => {
      track.stop();
    });
  }

  /**
   * function gets the facing mode of the camera - Ezugudor
   */
  getFacingMode = () => {
    return constraints.video.facingMode;
  };

  /**
   * function sets the facing mode of the camera - Ezugudor
   */
  setFacingMode = mode => {
    constraints.video.facingMode = mode;
  };

  /**
   * response to user's intent to change the prefered camera - Ezugudor
   */
  handleCameraToggling = () => {
    this.stopMediaTracks(this.stream);
    if (this.getFacingMode() === "user") {
      this.setFacingMode("environment");
    } else {
      this.setFacingMode("user");
    }
    this.iniate(constraints);
  };

  /**
   * configure window elements when video start receiving data from media devices
   */
  handleVideoCanPlay = ev => {
    this.takePictureBtn.style.opacity = 1;
    if (!this.streaming) {
      this.height =
        this.video.videoWidth /
        (this.video.videoWidth / this.video.videoHeight);
      this.tak;
      this.canvas.setAttribute("height", this.height);
      this.video.setAttribute("height", this.height);
      this.canvas.setAttribute("width", this.width);
      this.video.setAttribute("width", this.width);
      this.streaming = false;
    }
  };

  /**
   * Take a frame from the video and save as picture
   */
  takePicture = () => {
    const context = this.canvas.getContext("2d");
    if (this.width && this.height) {
      this.canvas.height = this.height;
      this.canvas.width = this.width;

      context.drawImage(this.video, 0, 0, this.width, this.height);
      const data = this.canvas.toDataURL("image/png");
      this.photo.setAttribute("src", data);
      this.pictureTaken = true;
      // this.photoContainer.classList.remove("des-content");
      // this.retakeButton.classList.remove("des-content");
      this.uploadBtn.classList.remove("hide-elem", "des-content");
      // this.videoContainer.classList.add("des-content");
    } else {
      this.clearPhoto();
    }
  };

  /**
   * Prepare the canvas for another picture
   */
  clearPhoto() {
    const context = this.canvas.getContext("2d");
    context.fillStyle = "#AAA";
    context.fillRect(0, 0, this.canvas.width, this.canvas.height);
    const data = this.canvas.toDataURL("image/png");
    this.photo.setAttribute("src", data);
  }

  startAgain = () => {
    this.clearPhoto();
    this.videoContainer.classList.remove("des-content");
    this.photoContainer.classList.add("des-content");
    this.retakeButton.classList.add("des-content");
    this.uploadBtn.classList.add("des-content");
  };

  /**
   * return asset that was recorded
   */
  getAsset() {
    if (this.pictureTaken) {
      const data = this.canvas.toDataURL("image/png");
      const blobBin = atob(data.split(",")[1]);
      const array = [];
      for (let i = 0; i < blobBin.length; i++) {
        array.push(blobBin.charCodeAt(i));
      }
      return new Blob([new Uint8Array(array)], { type: "image/png" });
    }
    return null;
  }
}
