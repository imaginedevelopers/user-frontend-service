import { constraints } from "./util";

export class VideoManager {
  constructor(componentUI, id) {
    this.uploadBtn = document.querySelector(`.uploadButton[id="${id}"]`);
    this.recordedVideo = componentUI.querySelector("video#playback");
    this.recordButton = componentUI.querySelector("button#record");
    this.toggleCameraButton = componentUI.querySelector("button#toggleCam");
    this.errorMsgElement = componentUI.querySelector("span#errorMsg");
    this.liveVideo = componentUI.querySelector("video#live");
    this.secondsSpan = componentUI.querySelector(".seconds");
    this.minutesSpan = componentUI.querySelector(".minutes");
    this.hoursSpan = componentUI.querySelector(".hours");
    this.mediaSource = new MediaSource();
    this.componentUI = componentUI;
    this.timeInterval = null;
    this.mediaRecorder = null;
    this.recordedBlob = null;
    this.sourceBuffer = null;
    this.stream = null;
    this.init(constraints);
  }

  /**
   * Ask permission to user media and set up camera
   * start streaming media data from users devices
   */
  init(constraints) {
    this.mediaSource.addEventListener("sourceopen", this.handleMediaSourceOpen);
    this.recordButton.addEventListener("click", this.handleRecording);
    this.toggleCameraButton.addEventListener(
      "click",
      this.handleCameraToggling
    );
    try {
      navigator.mediaDevices.getUserMedia(constraints).then(stream => {
        this.stream = stream;
        this.liveVideo.srcObject = stream;
        this.recordButton.disabled = false;
      });
    } catch (ex) {}
  }

  /**
   * this function stopMediaTracks loops through each media track in the stream, and stop each of them.  Ezugudor addendum
   * @param { MediaStream } stream
   */
  stopMediaTracks(stream) {
    stream.getTracks().forEach(track => {
      track.stop();
    });
  }

  /**
   * function gets the facing mode of the camera - Ezugudor
   */
  getFacingMode = () => {
    return constraints.video.facingMode;
  };

  /**
   * function sets the facing mode of the camera - Ezugudor
   */
  setFacingMode = mode => {
    constraints.video.facingMode = mode;
  };

  /**
   * response to user's intent to change the prefered camera - Ezugudor
   */
  handleCameraToggling = () => {
    this.stopMediaTracks(this.stream);
    if (this.getFacingMode() === "user") {
      this.setFacingMode("environment");
    } else {
      this.setFacingMode("user");
    }
    this.init(constraints);
  };

  /**
   * process media data from user devices
   * @param { MediaStream } stream
   */
  handleMediaSourceOpen = () => {
    this.sourceBuffer = this.mediaSource.addSourceBuffer(
      'video/webm; codecs="vp8"'
    );
  };

  /**
   * response to user's intent to record video
   */
  handleRecording = () => {
    const text = this.recordButton.textContent;
    if (text === "Start Recording") {
      this.startRecording();
    } else if (text === "Record Again") {
      this.recordAgain();
    } else {
      this.stopRecording();
    }
  };

  /**
   * play back recorded video
   */
  playRecordedVideo = () => {
    const buffer = new Blob(this.recordedBlob, { type: "video/webm" });
    this.recordedVideo.src = null;
    this.recordedVideo.srcObject = null;
    this.recordedVideo.src = window.URL.createObjectURL(buffer);
    this.recordedVideo.controls = true;
    this.recordedVideo.play();
  };

  /**
   * prepare environment to record another video
   */
  recordAgain() {
    this.recordedVideo.classList.add("des-content", "hide-elem");
    //below commented out so we dont get playback error in the recorded video element
    // this.recordedVideo.src = null;
    // this.recordedVideo.srcObject = null;
    this.liveVideo.classList.remove("des-content", "hide-elem");
    this.uploadBtn.classList.add("des-content", "hide-elem");

    this.secondsSpan.innerHTML = "00";
    this.minutesSpan.innerHTML = "0";
    this.hoursSpan.innerHTML = "0";

    this.startRecording();
  }

  /**
   * Save the data coming from users media devices camera/mic
   */
  startRecording() {
    this.recordedBlob = [];
    let options = { mimeType: "video/webm;codec=vp9" };
    if (!MediaRecorder.isTypeSupported(options.mimeType)) {
      options = { mimeType: "video/webm;codec=vp8" };
      if (!MediaRecorder.isTypeSupported(options.mimeType)) {
        options = { mimeType: "video/webm" };
      }
    }
    try {
      this.mediaRecorder = new MediaRecorder(this.stream, options);
      this.recordButton.textContent = "Stop Recording";
      this.mediaRecorder.ondataavailable = this.handleDataAvailable;
      this.mediaRecorder.start(10); // collect 10ms of data;
      this.startCountDown();
    } catch (ex) {}
  }

  /**
   * Save raw data from user's devices to memory
   */
  handleDataAvailable = event => {
    if (event.data && event.data.size > 0) {
      this.recordedBlob.push(event.data);
    }
  };

  /**
   * stop collecting media data from user's device
   */
  stopRecording() {
    this.mediaRecorder.stop();
    this.recordButton.textContent = "Record Again";
    this.recordedVideo.classList.remove("des-content", "hide-elem");
    this.uploadBtn.classList.remove("des-content", "hide-elem");
    // this.liveVideo.classList.add("des-content");

    this.playRecordedVideo();
    this.clearTimer();
  }

  /**
   * display elapsed time
   */
  startCountDown() {
    const startTime = new Date();
    this.timeInterval = setInterval(() => {
      const time = new Date() - startTime;
      const seconds = Math.floor((time / 1000) % 60);
      const munites = Math.floor((time / 1000 / 60) % 60);
      const hours = Math.floor((time / (1000 * 60 * 60)) % 24);
      const days = Math.floor(time / (1000 * 60 * 60 * 24));

      this.secondsSpan.innerHTML = ("0" + seconds).slice(-2);
      this.minutesSpan.innerHTML = munites;
      this.hoursSpan.innerHTML = hours;
    }, 1000);
  }

  clearTimer() {
    clearInterval(this.timeInterval);
  }

  /**
   * return asset that was recorded
   */
  getAsset() {
    if (this.recordedBlob.length > 0)
      return new Blob(this.recordedBlob, { type: "video/webm" });
    return null;
  }
}
