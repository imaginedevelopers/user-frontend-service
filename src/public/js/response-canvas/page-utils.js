import dropdownOptionTemplate from "../templates/response-canvas/dropdown-options.hbs";
import optionTemplate from "../templates/response-canvas/option.hbs";
import { countryStates } from "../utils/index";

/**
 * used to for building label for question with options
 */
const alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

export class PageUtils {
  /**
   * Extract out form element that need users interaction
   * @param {array} formInputs form elements
   */
  static getQuestions(formInputs) {
    return formInputs
      .filter(
        element => element.type !== "section" && element.type !== "introduction"
      )
      .map((question, index) => {
        question.qPosition = index + 1;
        return question;
      });
  }

  static getStates = () => {
    return countryStates.map(state => ({
      name: state.state.name,
      id: state.state.id
    }));
  };

  /**
   * gets the default state LGAs .
   * Note: Default state is assumed to be Abia State as
   * its the first element of the country object
   */
  static getDefaultStateLGA = () => {
    return countryStates[0].state.locals;
  };

  /**
   * gets the default state LGAs .
   * Note: Default state is assumed to be Abia State as
   * its the first element of the country object
   */
  static getTotalQuestions = (questions) => {
    let total = 0;
    questions.forEach(question => {
      if(question.type !== "statement"){
         if(PageUtils.compactTypes().includes(question.type)){
           total+= question.children.length;
         }else{ total++; }   
      }
    });
    return total;
  };
/**
 *  search down a given object tree (sub children) and return any object whose id @param questionId is supplied
 * @param questions The object to search through the children tree.
 * @param questionId supplied id of the object to look out for
 */
  static questionFinder = (questions, questionId)=>{
    return questions.map(elem=>{
      if(PageUtils.compactTypes().includes(elem.type)){
        return elem.children.find(cElem=>{
          if(cElem.id == questionId) return cElem;
        });
      }else{
        if(elem.id === questionId) return elem;
      }
    }).find(e=>e!==undefined);
   
  }

  /**
   *  Retrives the selected state LGA and additional areas in case
   *  of major cities like Lagos, PH, Enugu, Abuja , etc
   * @param stateID the selected state ID that the LGA/areas is to be retrived
   */
  static getSelectedStateLGA = stateID => {
    return countryStates.find(state => state.state.id == stateID).state.locals;
  };

  
  /**
   * check if a form has intro section
   * @param {*} formInputs form elements
   */
  static containsIntro(formInputs) {
    const intro = formInputs.find(el => el.type === "introduction");
    if (intro) return true;
    return false;
  }

  static compactTypes() {
    return ["address", "branch"];
  }

  /**
   * pick the ids of all form elements that needs user's interaction
   * @param {*} questions
   */
  static pickIds(questions) {
    // console.log(this.compactTypes)
    let allIDs = [];
    let name = questions => {
      questions.forEach(el => {
        if (this.compactTypes().includes(el.type)) {
          name(el.children);
        } else {
          allIDs.push(el.id);
        }
      });
    };
    name(questions);
    return allIDs;
  }

  /**
   * get the intro section of a form
   * @param {*} formInputs
   */
  static getIntroData(formInputs) {
    return formInputs.find(el => el.type === "introduction");
  }

  /**
   * extract the first section data to display on the page header
   */
  static getFirstSection(formQuestions) {
    return formQuestions
      .slice(0, 3)
      .find(question => question.type === "section");
  }

  /**
   * Extract the next section data to display on the page header
   *  @param {*} formInputs
   */
  static getNextSection(formQuestions, currentQuestionId) {
    const currentQuestionIndex = formQuestions.findIndex(
      question => question.id === currentQuestionId
    );
    const nextQuestion = formQuestions[currentQuestionIndex + 1];
    return (nextQuestion && nextQuestion.type) === "section"
      ? nextQuestion
      : null;
  }

  /**
   * return the parent node for document fragment acting as a question option
   * that recieved a click event
   * @param {DocumentNode} node
   */
  static getRootNode(node) {
    while (node.dataset.optionId === undefined && node.nodeName !== "SECTION") {
      node = node.parentElement;
    }
    return node;
  }

  /**
   * transform array of text to arry of option object
   * @param {array} array array with text to be transformed into option object
   * @param {string} filterText value by which array element would be filted
   */
  static buildOptionFromArray(array, filterText = null, controlType = null) {
    if (filterText) {
      array = array.filter(item => {
        if(controlType){
          return item.name.toLowerCase().indexOf(filterText.toLowerCase()) !== -1
        }else{
          return item.toLowerCase().indexOf(filterText.toLowerCase()) !== -1
        }
        
      });

    }
    return array.map((el, index) => {
      return { label: alphabet[index], text: el };
    });
  }

  /**
   * transform array of text to arry of option object
   * @param {array} array array with text to be transformed into option object
   * @param {string} filterText value by which array element would be filted
   */
  static buildStateOptionFromArray(array, filterText = null) {
    if (filterText) {
      array = array.filter(
        item =>
          item.stateName.toLowerCase().indexOf(filterText.toLowerCase()) !== -1
      );
    }
    return array.map((el, index) => {
      return { label: alphabet[index], text: el };
    });
  }

  /**
   * Build UI for each multi choice question's option
   * @param {array} options option object
   * @param {string} questionId id of question
   * @param {boolean} questionType type of question e.g dropdown/card etc.
   */
  static buildStateOptionTemplate(options = [], questionId, questionType) {
    // IF = no need for labels
    if (
      questionType === "dropdown" ||
      questionType === "branch" ||
      questionType === "state" ||
      questionType === "country" ||
      questionType === "lga"
    ) {
      return options
        .map(({ text }) => dropdownOptionTemplate({ text, id: questionId }))
        .join("");
    }
    return options
      .map(({ label, text }) => optionTemplate({ label, text, id: questionId }))
      .join("");
  }

  /**
   * Transform the data in a branch list to option objects
   * @param {Array} branches list of a bank branch
   * @param {string} filterText value by which array element would be filted
   */
  static buildOptionsFromBranches(branches, filterText = null) {
    branches = branches.map(branch => `${branch.name} | ${branch.address}`);
    if (filterText) {
      branches = branches.filter(
        branch => branch.toLowerCase().indexOf(filterText.toLowerCase()) !== -1
      );
    }
    return branches.map((text, index) => ({ label: alphabet[index], text }));
  }

  static stripStates(str) {
    return str == "FCT" ? str : str.substring(0, str.length - 5);
  }
  /**
   * Build UI for each multi choice question's option
   * @param {array} options option object
   * @param {string} questionId id of question
   * @param {boolean} questionType type of question e.g dropdown/card etc.
   */
  static buildOptionTemplate(
    options = [],
    questionId,
    questionType,
    dropdownType,
    parentID
  ) {
    // IF = no need for labels
    if (
      questionType === "dropdown" ||
      questionType === "branch" ||
      questionType === "country" ||
      questionType === "lga"
    ) {
      if (dropdownType == "state") {
        return options
          .map(({ text }) =>
            dropdownOptionTemplate({
              text: this.stripStates(text.name),
              resourceId: text.id,
              id: questionId,
              parent: parentID,
              dropdownType
            })
          )
          .join("");

      }else if (dropdownType == "lga") {
        return options
          .map(({ text }) =>
            dropdownOptionTemplate({
              text: text.name,
              resourceId: text.id,
              id: questionId,
              parent: parentID,
              dropdownType
            })
          )
          .join("");
      } else {
        return options
          .map(({ text }) =>
            dropdownOptionTemplate({ text, id: questionId, parent: parentID })
          )
          .join("");
      }
    }

    return options
      .map(({ label, text }) => optionTemplate({ label, text, id: questionId }))
      .join("");
  }

  /**
   * Return the next data to be be displayed on the page header
   */
  static getNextSectionData(formQuestions, currentQuestionId) {
    const currentQuestionIndex = formQuestions.findIndex(
      question => question.id === currentQuestionId
    );
    const nextQuestion = formQuestions[currentQuestionIndex + 1];
    return (nextQuestion && nextQuestion.type) === "section"
      ? nextQuestion
      : null;
  }

  /**
   * return the next question in the series
   * @param {array} formQuestions
   * @param {string} currentQuestionId
   */
  static getNextQuestion(formQuestions, currentQuestionId) {
    let nextQuestion = {};
      formQuestions.forEach((question, index) => {
        if (this.compactTypes().includes(question.type)) {
          question.children.forEach((cElem,cIndex)=>{
             if (cElem.id === currentQuestionId) {
               nextQuestion = question.children[cIndex + 1];
             }
          });
        } else {
          if (question.id === currentQuestionId) {
               nextQuestion = formQuestions[index + 1];
          }
        }
      });
   

    return nextQuestion;
  }

  /**
   * get the distance between the top of the canvas to an element
   * @param {DocumentNode} elem
   */
  static getDistanceToTop(elem) {
    let distance = 0;
    if (elem.offsetParent) {
      do {
        distance += elem.offsetTop;
        elem = elem.offsetParent;
      } while (elem);
    }
    return distance < 0 ? 0 : distance;
  }

  /**
   * get the branch a user want their answers sent to
   * @param {array} answers all the answers a user has given
   */
  static getBranchData(answers) {
    const branch = answers.find(answer => answer.questionType === "branch");
    return branch ? removeAddresspart(branch.answer) : "HQ";
  }

  /**
   * return the node of the current question a user is answering
   */
  static getCurrentQuestionUI() {
    return Array.from(document.querySelectorAll(".elementWrapper")).find(
      node => !node.classList.contains("dull")
    );
  }

  /**
   * since we are not collect users data this is our default form respondant value for now
   */
  static getDefaultRespondant() {
    return {
      id: "5b51e1706fa9c80029476871",
      firstname: "Ezugudor",
      lastname: "Okpara",
      phone: "08030659872"
    };
  }
}

const removeAddresspart = branchAnswer => {
  const index = branchAnswer.indexOf(" ");
  return branchAnswer.substring(0, index);
};
