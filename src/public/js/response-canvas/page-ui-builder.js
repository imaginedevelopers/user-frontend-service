import multiChoiceTemplate from "../templates/response-canvas/multi-choice-question.hbs";
import uploadIndicatorTemplate from "../templates/response-canvas/upload-indicator.hbs";
import askForDataPermission from "../templates/response-canvas/media-permission.hbs";
import dropdownTemplate from "../templates/response-canvas/dropdown-question.hbs";
import compactHeaderTemplate from "../templates/response-canvas/compact-question-header.hbs";
import simpleQTemplate from "../templates/response-canvas/simple-question.hbs";
import statementTemplate from "../templates/response-canvas/statement.hbs";
import longQTemplate from "../templates/response-canvas/long-question.hbs";
import pictureTemplate from "../templates/response-canvas/picture.hbs";
import { PageUtils } from "./page-utils";

/**
 * transform form data into interactive UI
 */
export class PageUIBuilder {
  /**
   * Transform data about a question to a UI component
   */
  static buildUIFor = (questionData, parentID = null) => {
    const { validationRules } = questionData;
    const option = {
      isRequired: confirmRequirement(validationRules),
      placeholder: generatePlaceholder(questionData),
      type: generateType(questionData),
      description: questionData.description,
      position: questionData.qPosition,
      children: questionData.children,
      question: questionData.name,
      uploadIndicator: "",
      id: questionData.id,
      optionsString: "",
      compact: false
    };
    let options = null;
    if (questionData.isCompact) {
      option.compact = true;
    }

    switch (questionData.type) {
      case "introduction":
        return null;

      case "section":
        return null;

      case "statement":
        return statementTemplate(option);

      case "picture":
        option.text = "Turn on Camera";
        return askForDataPermission(option);

      case "video":
        option.text = "Start Recording";
        return askForDataPermission(option);

      case "signature":
      case "passport":
        option.labelText = "Or Click to upload";
        option.uploadIndicator = uploadIndicatorTemplate(option);
        return pictureTemplate(option);
      case "dob":
      case "date":
        option.class = "date-element";
        return simpleQTemplate(option);

      case "multichoice":
        options = PageUtils.buildOptionFromArray(option.children);
        option.optionsString = PageUtils.buildOptionTemplate(
          options,
          option.id,
          option.type
        );
        return multiChoiceTemplate(option);

      case "dropdown":
        if (!questionData.isCompact) {
          options = PageUtils.buildOptionFromArray(option.children);
          option.optionsString = PageUtils.buildOptionTemplate(
            options,
            option.id,
            option.type
          );
          return dropdownTemplate(option);
        } else {
          option.controlType = questionData.controlType;
          option.parent = parentID;
          // check the type of dropdown, if state,branch,lga,branch name etc and auto fill as needed
          if (questionData.controlType == "state") {
            //auto generate all the states
            options = PageUtils.buildOptionFromArray(PageUtils.getStates());
            option.optionsString = PageUtils.buildOptionTemplate(
              options,
              option.id,
              option.type,
              questionData.controlType,
              parentID
            );
            return dropdownTemplate(option);
          } else if (questionData.controlType == "branch") {
            // auto generate all the branch in this business account
          } else if (questionData.controlType == "lga") {
            // this will vary depending on the selected state
            //auto generate all the states
            options = PageUtils.buildOptionFromArray(
              PageUtils.getDefaultStateLGA()
            );
            option.optionsString = PageUtils.buildOptionTemplate(
              options,
              option.id,
              option.type,
              questionData.controlType,
              parentID
            );
            return dropdownTemplate(option);
          }

          return dropdownTemplate(option);
        }

      case "address":
      case "branch":
      // let subQns = '<div class="compactContainer" style="background:blue">';

      //           subQns += questionData.children.map(child => {
      //             return this.buildUIFor(child);
      //           }).join("");

      //    subQns+= "</div>";

      // return compactHeaderTemplate(option) + subQns;

      case "creditcards":
        options = generateCardOptions();
        option.optionsString = PageUtils.buildOptionTemplate(
          options,
          option.id,
          option.type
        );
        return multiChoiceTemplate(option);

      case "yesorno":
        options = generateYesOrNoOptions();
        option.optionsString = PageUtils.buildOptionTemplate(
          options,
          option.id,
          option.type
        );
        return multiChoiceTemplate(option);

      case "gender":
        options = generateGenderOptions();
        option.optionsString = PageUtils.buildOptionTemplate(
          options,
          option.id,
          option.type
        );
        return multiChoiceTemplate(option);

      case "longtext":
        return longQTemplate(option);
      default:
        return simpleQTemplate(option);
    }
  };
}

/**
 * Get the right input type property
 * @param {string} el
 */
const generateType = el => {
  switch (el.type) {
    case "shorttext":
    case "firstname":
    case "lastname":
    case "address":
      return "text";

    case "date":
    case "dob":
      // return "date";
      return "text";
    case "tel":
      return "tel";
    case "email":
      return "email";
    case "mobile":
    case "tel":
    case "bvn":
      return "number";
    default:
      return el.type;
  }
};

/**
 * Return the right input placeholver property
 * @param {obje3ct} el
 */
const generatePlaceholder = el => {
  switch (el.type) {
    case "address":
      return "Like 16 Karimu Ikotun VI, Lagos";
    case "mobile":
      return "Like 08030659872";
    case "tel":
      return "Like 01729011";
    case "email":
      return "Like jendoe@cool.com";
    case "bvn":
      return "Like 22123803000";
    case "dob":
      return "DD/MM/YYYY e.g 09/12/1982";
    case "date":
      return "DD/MM/YYYY e.g 09/12/1982";
    case "dropdown":
      return "Start typing to filter options";
    default:
      return "Enter Your Answer Here";
  }
};

/**
 * return array of options to display for card questions
 */
const generateCardOptions = () => {
  return [
    { label: "A", text: "Master" },
    { label: "B", text: "Valve" },
    { label: "C", text: "Visa" }
  ];
};

/**
 * return array of options to display for yes/no questions
 */
const generateYesOrNoOptions = () => {
  return [{ label: "Y", text: "Yes" }, { label: "N", text: "No" }];
};

/**
 * return array of options to display for yes/no questions
 */
const generateGenderOptions = () => {
  return [{ label: "M", text: "Male" }, { label: "F", text: "Female" }];
};

/**
 * check if a question has required as part of its validation rules
 * @param {object} validationRules
 */
const confirmRequirement = validationRules => {
  if (!validationRules) return;
  const requiredRule = validationRules.find(rule => rule.name === "required");
  return requiredRule !== undefined;
};
