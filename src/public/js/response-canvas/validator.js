import {capitalizeFirstLatter as ucFirst} from "../utils"

/**
 * object to hold error messages for rules that each value
 * Beign validated fail
 */
const validationError = {
  hasError: false,
  messages: []
};

/**
 * question types that require digit values
 */
const digitTypes = ["tel", "number", "account", "bvn", "mobile"];

/**
 * class with interfaces to validate values users pass to response canvas
 * interactive elements
 */
export class Validator {
  /**
   * 
   * @param {object} rules // object containing containing rules that value entered must meet
   * @param {string} questionType string of charactered by user
   * @param {string} answer string of charactered entered by user
   */
  static validate(rules, questionType, answer) {
    // empty validation error to remove hold over values
    validationError.hasError = false;
    validationError.messages = [];
    rules.forEach(rule => {
      const methodName = `validate${ucFirst(rule.name)}Rule`;
      if(Validator.hasOwnProperty(methodName)) {
        Validator[methodName](rule, questionType, answer)
      }
    });
    Validator.validateType(questionType, answer);
    return validationError;
  }

  /**
   * validate that value meets the mininum value required for the value
   * @param {object} rule includes the amount of value that satisfies the minimum
   * characters for answer
   * @param {string} questionType string of charactered by user
   * @param {string} answer string of characters entered by user that needs to be validated
   */
  static validateMinRule(rule, questionType, answer) {
    if(answer.length < rule.value) {
      buildErrorMessage(questionType, rule);
    }
  }

  /**
   * validate that value entered by users meet the maximum rule required
   * @param {object} rule includes the amount of value that satisfies the maximum
   * characters for answer
   * @param {string} questionType string of charactered by user
   * @param {string} answer  string of characters entered by user that needs to be validated
   */
  static validateMaxRule(rule, questionType, answer) {
    if(answer.length > rule.value) {
      buildErrorMessage(questionType, rule);
    }
  }

  /**
   * validate that users answer required questions
   * @param {object} rule includes the amount of answer that satisfies the maximum requirement
   * @param {string} questionType string of charactered by user
   * @param {string} answer string of characters entered by user that needs to be validated
   */
  static validateRequiredRule(rule, questionType, answer) {
    if(!answer) {
      buildErrorMessage(questionType, rule);
    }
  }

  /**
   * validate that users answer is a valid email address
   * @param {object} rule includes the amount of answer that satisfies the maximum requirement
   * @param {string} questionType string of charactered by user
   * @param {string} answer string of characters entered by user that needs to be validated
   */
  static validateEmailRule(rule, questionType, answer) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(!re.test(String(answer).toLowerCase())) {
      buildErrorMessage(questionType, rule);
    }
  }

   /**
   * validate that users answer is of the right type
   * @param {string} questionType string of charactered by user
   * @param {string} anwer string of characters entered by user that needs to be validated
   */
  static validateType(questionType, anwer) {
    if(digitTypes.includes(questionType) && isNaN(anwer)){
      buildErrorMessage(questionType, {name: "need-digits"});
    }
  }
}

/**
 * Add suitable error message to be show users
 * @param {string} questionType the type of question user is answering
 * @param {object} rule // object containing containing rules that answer entered must meet
 */
const buildErrorMessage = (questionType, rule) => {
  let message = ""
  switch(rule.name) {
    case "need-digits":
      message = "You need to enter digits not letters";
      break;
    case "email":
      message = `You need to enter a valid email address`;
      break;
    case "min":
      message = `You need to enter at least ${rule.value} ${lettersOrDigits(questionType)}`;
      break;
    case "max":
      message = `You can't enter more than ${rule.value} ${lettersOrDigits(questionType)}`;
      break;
    case "required":
      message = "This question is required";
      break;
    default:
    message = "";
  }
  validationError.messages.push(message);
  validationError.hasError = true;
}

/**
 * figure out whether to include digits or letters to error message
 * @param {string} questionType the type of question user is answering
 */
const lettersOrDigits = questionType => {
  if(digitTypes.includes(questionType)) {
    return "digits";
  }
  return "letters";
}