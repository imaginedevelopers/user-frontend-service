import {Http, DataManager} from "../core";
import {parseHtml, themeMaker, chunkData} from "../utils";
import formTemplate from "../templates/forms.hbs";


export default class FormController {
  constructor(container) {
    this._container = container;
    this._headingElement = this._container.querySelector("#heading");
    this.logoMbi = document.querySelector("#banklogo-mobi");
    this.logoDes = document.querySelector("#banklogo-des");
    const data = this._headingElement.dataset;
    this.info = DataManager.findBank(data.slug);
    const headingText = `${this.info.name} Forms`
    // this._headingElement.insertBefore(parseHtml(headingText), this._headingElement.firstChild);
    this.logoMbi.src = this.info.logo;
    this.logoDes.src = this.info.logo;
    this._fetchBusinessForms(data.formtype,data.formtypeparent);
   // generate the business color hues and store for themeing
    themeMaker(this.info.color);
  }

  _fetchBusinessForms(formType,formTypeParent) {
    const url = `forms/businesses/${this.info.id}/${formTypeParent}/${formType}`;
    Http.get(url)
      .then(forms => {
        DataManager.storeForms(forms);
        const formBatches = chunkData(forms, 2);
        const formsString = formTemplate({
          formBatches,
          slug: this.info.slug
        });
        const formNodes = parseHtml(formsString);
        const formHouse = this._container.querySelector(".forms-section");
        formHouse.insertBefore(formNodes, formHouse.firstChild);
      })
      .catch(err => {
        alert(
          "Sorry we are experiencing issues fetching content from our servers"
        );
      });
  }
}
