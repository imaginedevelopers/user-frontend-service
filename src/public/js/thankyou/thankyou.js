import { DataManager } from "../core";

export default class ThankYou {
  constructor(pageView) {
    // this.pageView = pageView;
    const message = pageView.querySelector("#bankname");
    const { bankslug } = pageView.dataset;
    const localBankData = DataManager.findBank(bankslug);
    message.textContent = localBankData.name;
  }
}