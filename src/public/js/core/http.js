import "whatwg-fetch";
import axios from "axios";

const baseUrl = "https://business-backend-service.herokuapp.com/api/v1/";
// const baseUrl = "http://localhost:4000/api/v1/";

export const Http = {
  get(url) {
    return fetch(`${baseUrl}${url}`).then(response =>
      processResponse(response)
    );
  },

  post(url, data) {
    return fetch(`${baseUrl}${url}`, {
      method: "POST",
      headers: {
        "Content-type": "application/json"
      },
      body: JSON.stringify(data)
    }).then(response => processResponse(response));
  },

  put(url, data) {
    return fetch(`${baseUrl}${url}`, {
      method: "PUT",
      headers: {
        "Content-type": "application/json"
      },
      body: JSON.stringify(data)
    }).then(response => processResponse(response));
  },

  // upLoadForm(url, formData) {
  //   return fetch(`${baseUrl}${url}`, {
  //     method: "POST",
  //     body: formData
  //   }).then(response => processResponse(response));
  // }

  upLoadForm(url, formData, id, type) {
    var pb = `<div class="pb-cont">
                <span class="placeholder">Uploading...</span>
                <div class="pb-inside"></div>
              </div>`;
    if (
      document.querySelector(`div.uploadstatus[data-q-id="${id}"]`) !== null
    ) {
      const uploadIndicator = document.querySelector(
        `div.uploadstatus[data-q-id="${id}"]`
      );
      // var t = document.createElement('span');
      // t.innerHTML = eve.loaded;
      uploadIndicator.innerHTML = pb;
    }

    return axios({
      url: `${baseUrl}${url}`,
      method: "POST",
      data: formData,
      onUploadProgress: eve => {
        console.log("logging progress event", eve);
        let total = eve.total;
        let loaded = eve.loaded;
        let fraction = eve.loaded / eve.total;
        let perc = Math.floor(fraction * 100) + "%";
        if (type == "videos") {
          var target = ".uploadButton[data-qType=video] .cam-progress";
          // if (loaded >= total) {
          //   document.querySelector("video#live").style.display = "none";
          //   document.querySelector("#record").style.display = "none";
          //   var mediaPermissionBtn = document.querySelector(
          //     ".askPermissionBtn"
          //   );
          //   document
          //     .querySelector(".answerWrapper")
          //     .appendChild(mediaPermissionBtn);
          // }
        } else {
          var target = ".uploadButton[data-qType=picture] .cam-progress";
        }
        var pb =
          document.querySelector(".pb-cont .pb-inside") ||
          document.querySelector(target);
        if (loaded >= total) {
          pb.style.width = "100%";
        } else {
          pb.style.width = perc;
        }
      }
    }).then(response => processResponse2(response));
  }
};

const processResponse = response => {
  if (response.ok) {
    return response.json();
  }
  const error = new Error("NetworkError");
  error.detials = response.json();
  throw error;
};

const processResponse2 = response => {
  if (response.status == 200) {
    let ress = new Promise(function(res, rej) {
      res(response.data);
    });

    return ress;
  }
  const error = new Error("NetworkError");
  error.detials = response.statusText;
  throw error;
};
