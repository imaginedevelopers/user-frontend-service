import banksTemplate from "../templates/banks.hbs";
import { Http, DataManager } from "../core";
import { parseHtml } from "../utils";

export default class PartnersControllers {
  constructor(container) {
    this._container = container;
    this._fetchAll();
  }

  _fetchAll() {
    const loading = document.querySelector("#loading-text");
    Http.get("businesses")
      .then(banks => {
        if (!Array.isArray(banks)) throw new error("BadResponse");
        banks = banks.reverse();
        DataManager.storeBanks(banks);
        loading.classList.add("hide-elem");
        const banksHtml = banksTemplate({ banks: banks });
        const nodes = parseHtml(banksHtml);
        this._container.insertBefore(nodes, this._container.firstChild);
      })
      .catch(err => {
        loading.classList.add("hide-elem");
        alert(
          "Sorry we are experiencing issues fetching content from our servers"
        );
      });
  }
}
