var contextRange = document.createRange();
contextRange.setStart(document.body, 0);

export const parseHtml = str => {
  return contextRange.createContextualFragment(str);
};