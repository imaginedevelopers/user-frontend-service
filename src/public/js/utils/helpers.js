export const capitalizeFirstLatter = s => s[0].toUpperCase() + s.slice(1);

export const chunkData = (arr, size = 3) => {
  const result = [];
  let currentChunk = [];
  arr.forEach(item => {
    if(currentChunk.length === size) {
      result.push(currentChunk);
      currentChunk = [];
    }
    currentChunk.push(item);
  });
  result.push(currentChunk);
  return result;
}