import { PageController, PartnerController } from "./controllers";
import { errorHandler, validator } from "./middleware";
import express, { Router } from "express";
import compression from "compression";
import bodyParser from "body-parser";
import zlib from "zlib";
import path from "path";

const compressor = compression({
  flush: zlib.Z_PARTIAL_FLUSH
});

const staticOptions = {
  maxAge: 1
};

const AppRouter = Router();

AppRouter.use(bodyParser.json())
  .use(bodyParser.urlencoded({ extended: false }))
  .use(compressor)
  .use(validator)
  .use(express.static(path.join(__dirname, "public"), staticOptions))
  .use("/", PageController.router)
  .use("/", PartnerController.router)
  .use(errorHandler);

export default AppRouter;
