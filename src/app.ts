import exphbs from "express-handlebars";
import express from "express";
import router from "./router";
import path from "path";

export default class App {
  private app: express.Application;
  private port: string;

  constructor(port: string) {
    this.app = express();
    this.port = port;
    this.app.set("views", path.join(__dirname, "../views"));
    this.app.engine(
      ".hbs",
      exphbs({
        extname: ".hbs",
        defaultLayout: "main"
      })
    );
    this.app.set("view engine", ".hbs");
    this.app.disable("x-powered-by").use(router);
  }

  public start() {
    this.app.listen(this.port, () => {
      console.log(`[P ${process.pid}] Listening at port ${this.port}`);
    });
  }
}
